<?php

use \AcceptanceTester;

class ContactsCest
{
     public function _before(AcceptanceTester $I)
    {
    	$I->setCookie("codeception", TRUE);
    	$I->seeCookie("codeception");
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.spravce@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    public function _after(AcceptanceTester $I)
    {
        $I->dontSee("Nette Framework");
    }

    public function editContactDetails(AcceptanceTester $I) {
        $I->wantTo("EDIT CONTACT DETAILS AND CHECK IT IN ADMIN AND IN FRONTEND");
        $I->click("Kontakty");
        $I->see("Kontakty");
        $I->see("kontaktní informace");
        $I->click("Upravit kontakty");
        $I->see("Upravit kontakty");
        $I->fillField("client_title", "Název realitky");
        $I->fillField("client_ic", "12345678");
        $I->fillField("client_dic", "CZ12345678");
        $I->fillField("client_incorporation", "Zapsáno u soudu v Praze");
        $I->fillField("client_phone", "+420 123 456 789");
        $I->fillField("client_email", "email@strankyrealitky.cz");
        $I->fillField("client_address", "Náměstí repuliky 1, 277 11 Neratovice");
        $I->fillField("client_office_address", "Byškovická 2, 277 11 Neratovice");
        $I->click("Uložit změny");
        $I->see("Kontaktní informace byly uloženy");
        $I->see("Název realitky");
        $I->see("12345678");
        $I->see("CZ12345678");
        $I->see("Zapsáno u soudu v Praze");
        $I->see("+420 123 456 789");
        $I->see("email@strankyrealitky.cz");
        $I->see("Náměstí repuliky 1, 277 11 Neratovice");
        $I->see("Byškovická 2, 277 11 Neratovice");
        $I->click("Upravit kontakty");
        $I->seeInField("client_title", "Název realitky");
        $I->seeInField("client_ic", "12345678");
        $I->seeInField("client_dic", "CZ12345678");
        $I->seeInField("client_incorporation", "Zapsáno u soudu v Praze");
        $I->seeInField("client_phone", "+420 123 456 789");
        $I->seeInField("client_email", "email@strankyrealitky.cz");
        $I->seeInField("client_address", "Náměstí repuliky 1, 277 11 Neratovice");
        $I->seeInField("client_office_address", "Byškovická 2, 277 11 Neratovice");
        $I->amOnPage("/kontakty");
        $I->see("Kontakty");
        $I->see("Kde nás najdete");
        $I->see("Název realitky");
        $I->see("12345678");
        $I->see("CZ12345678");
        $I->see("Zapsáno u soudu v Praze");
        $I->see("+420 123 456 789");
        $I->see("email@strankyrealitky.cz");
        $I->see("Náměstí repuliky 1");
        $I->see("277 11 Neratovice");
        $I->see("Byškovická 2, 277 11 Neratovice");
    }
}