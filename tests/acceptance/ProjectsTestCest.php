<?php
use \AcceptanceTester;


class ProjectsCest
{
    public $project_id;

    public $article_id;

    public function _before(AcceptanceTester $I)
    {
        $I->setCookie("codeception", TRUE);
        $I->seeCookie("codeception");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    protected function loginAsSuperadmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "info@strankyrealitky.cz");
        $I->fillField("user_password", "cprstrankyrealitky");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsAdmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.spravce@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsBroker(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.makler@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    /**
    * @before loginAsAdmin
    */
    public function addProject(AcceptanceTester $I) {
        $I->wantTo("ADD AND EDIT PROJECT");
        $I->click("Projekty");
        $I->click("Přidat projekt");
        $this->project_id = $I->grabAttributeFrom("//input[@name='project_id']", "value");
        $I->fillField("project_title[cs]", "Testovací projekt");
        $I->fillField("project_description[cs]", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->fillField("project_address", "U Závor 1362, Neratovice");
        $I->fillField("project_established", "19.8.2015");
        $I->fillField("project_finished", "22.4.2017");
        $I->selectOption("project_status", "Realizuje se");

        $I->attachFile("//input[@name='images[files][]']", "img/avatar.png");
        $I->click("//button[@name='images[addImages]']");
        //$I->see("Přílohy byly přidány");
        $I->dontSee("Zatím nebyly přidány žádné fotografie");
        $I->see("Počet fotografií: <strong>1</strong>");

        $I->see("Žádní makléři");
        $I->selectOption("users[user_id]", "Tomáš Makléř");
        $I->click("users[add]");
        $I->see("Makléř byl přidán");
        $I->see("Tomáš Makléř");
        
        $I->selectOption("users[user_id]", "Martina Makléřka");
        $I->click("users[add]");
        $I->see("Makléř byl přidán");
        $I->see("Martina Makléřka");
        $I->see("Tomáš Makléř");

        $I->click("Uložit projekt");
        $I->see("Projekt byl uložen");
        $I->click("//a[contains(@href, '" . $this->project_id . "')][contains(@href, 'edit')]");
        $I->seeInField("project_title[cs]", "Testovací projekt");
        $I->seeInField("project_description[cs]", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->seeInField("project_address", "U Závor 1362, Neratovice");
        //$I->seeInField("project_established", "19.8.2015");
        //$I->seeInField("project_finished", "22.4.2017");
        $I->seeOptionIsSelected("project_status", "Realizuje se");

        $I->see("Počet fotografií: <strong>1</strong>");
        $I->see("Martina Makléřka");
        $I->see("Tomáš Makléř");

        $I->fillField("project_title[cs]", "Upravený testovací projekt");
        $I->fillField("project_description[cs]", "Upravený Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->fillField("project_address", "U Závor 1, Neratovice");
        $I->fillField("project_established", "19.8.2016");
        $I->fillField("project_finished", "22.4.2018");
        $I->selectOption("project_status", "V prodeji");

        $I->click("Uložit projekt");
        $I->see("Projekt byl uložen");

        $I->click("//a[contains(@href, '" . $this->project_id . "')][contains(@href, 'MoveUp')]");
        $I->see("Projekt byl v seznamu posunut nahoru");
        $I->click("//a[contains(@href, '" . $this->project_id . "')][contains(@href, 'MoveDown')]");
        $I->see("Projekt byl v seznamu posunut dolů");

        $I->click("//a[contains(@href, '" . $this->project_id . "')][contains(@href, 'articles')]");
        $I->see("Články");
        $I->click("Přidat článek");
        $this->article_id = $I->grabAttributeFrom("//input[@name='article_id']", "value");
        $I->fillField("article_title[cs]", "Testovací článek 1");
        $I->fillField("article_snippet[cs]", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer placerat mi vel massa hendrerit dictum. Donec id urna dolor. Pellentesque dignissim nibh sed felis fringilla, id pulvinar nisi vehicula. Donec tortor massa, vestibulum sed nisl sit amet, pharetra volutpat turpis. Mauris facilisis facilisis ante et finibus. Duis eu est vel nunc pellentesque tempus. Pellentesque posuere dui vel bibendum rutrum. Donec id mattis mi.");
        $I->fillField("article_content[cs]", "​Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer placerat mi vel massa hendrerit dictum. Donec id urna dolor. Pellentesque dignissim nibh sed felis fringilla, id pulvinar nisi vehicula. Donec tortor massa, vestibulum sed nisl sit amet, pharetra volutpat turpis. Mauris facilisis facilisis ante et finibus. Duis eu est vel nunc pellentesque tempus. Pellentesque posuere dui vel bibendum rutrum. Donec id mattis mi.Phasellus velit nisl, facilisis vitae lacus vel, commodo scelerisque augue. Integer bibendum vehicula lacus egestas finibus. Praesent tellus orci, lobortis in dictum a, varius et sem. Aliquam sit amet risus vitae neque accumsan pretium. In sit amet arcu ac sem malesuada condimentum ac ut risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus a orci in neque laoreet sagittis. Vestibulum id fermentum lacus, id sodales nibh. Mauris nec lectus eu tortor mollis tristique eu eu ante. Ut efficitur velit in justo scelerisque, eget efficitur dui sodales. Cras nisl eros, vehicula ac purus ac, luctus pretium tortor. Sed vel mi magna. Proin in pretium mauris. Sed ornare congue volutpat. Aenean eget egestas leo, vitae malesuada erat. Duis eget sapien eu eros blandit maximus. Morbi varius, risus eget vulputate feugiat, arcu lectus laoreet sem, a maximus massa nunc tincidunt mi. Mauris imperdiet erat ac massa vestibulum venenatis. Phasellus ac enim justo. Aliquam est mi, efficitur quis pulvinar et, malesuada quis velit. Curabitur sit amet dolor at quam maximus efficitur nec vitae justo. Vestibulum nibh risus, blandit sit amet aliquam feugiat, faucibus ut lorem. Nunc sollicitudin ornare sapien id scelerisque. Nam feugiat leo nec consectetur ultricies. Sed eu magna massa.");
        $I->fillField("article_published", "20.6.2015");

        $I->attachFile("//input[@name='images[files][]']", "img/avatar.png");
        $I->click("//button[@name='images[addImages]']");
        $I->dontSee("Zatím nebyly přidány žádné fotografie");
        $I->see("Počet fotografií: <strong>1</strong>");

        $I->click("//button[@name='save']");
        $I->amOnPage("projects");
        $I->see("Článek byl uložen");

        $I->click("//a[contains(@href, '" . $this->article_id . "')][contains(@href, 'edit')]");

        $I->seeInField("article_title[cs]", "Testovací článek 1");
        $I->seeInField("article_snippet[cs]", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer placerat mi vel massa hendrerit dictum. Donec id urna dolor. Pellentesque dignissim nibh sed felis fringilla, id pulvinar nisi vehicula. Donec tortor massa, vestibulum sed nisl sit amet, pharetra volutpat turpis. Mauris facilisis facilisis ante et finibus. Duis eu est vel nunc pellentesque tempus. Pellentesque posuere dui vel bibendum rutrum. Donec id mattis mi.");
        $I->seeInField("article_content[cs]", "​Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer placerat mi vel massa hendrerit dictum. Donec id urna dolor. Pellentesque dignissim nibh sed felis fringilla, id pulvinar nisi vehicula. Donec tortor massa, vestibulum sed nisl sit amet, pharetra volutpat turpis. Mauris facilisis facilisis ante et finibus. Duis eu est vel nunc pellentesque tempus. Pellentesque posuere dui vel bibendum rutrum. Donec id mattis mi.Phasellus velit nisl, facilisis vitae lacus vel, commodo scelerisque augue. Integer bibendum vehicula lacus egestas finibus. Praesent tellus orci, lobortis in dictum a, varius et sem. Aliquam sit amet risus vitae neque accumsan pretium. In sit amet arcu ac sem malesuada condimentum ac ut risus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus a orci in neque laoreet sagittis. Vestibulum id fermentum lacus, id sodales nibh. Mauris nec lectus eu tortor mollis tristique eu eu ante. Ut efficitur velit in justo scelerisque, eget efficitur dui sodales. Cras nisl eros, vehicula ac purus ac, luctus pretium tortor. Sed vel mi magna. Proin in pretium mauris. Sed ornare congue volutpat. Aenean eget egestas leo, vitae malesuada erat. Duis eget sapien eu eros blandit maximus. Morbi varius, risus eget vulputate feugiat, arcu lectus laoreet sem, a maximus massa nunc tincidunt mi. Mauris imperdiet erat ac massa vestibulum venenatis. Phasellus ac enim justo. Aliquam est mi, efficitur quis pulvinar et, malesuada quis velit. Curabitur sit amet dolor at quam maximus efficitur nec vitae justo. Vestibulum nibh risus, blandit sit amet aliquam feugiat, faucibus ut lorem. Nunc sollicitudin ornare sapien id scelerisque. Nam feugiat leo nec consectetur ultricies. Sed eu magna massa.");
        //$I->seeInField("article_published", "20.6.2015");
        $I->see("Počet fotografií: <strong>1</strong>");
        $I->click("Odebrat článek");
        $I->see("Článek byl odebrán");

        $I->see("Články");
        $I->click("Přidat článek");
        $this->article_id = $I->grabAttributeFrom("//input[@name='article_id']", "value");
        $I->click("Uložit článek");
        $I->see("Článek byl uložen");
        $I->click("//a[contains(@href, '" . $this->article_id . "')][contains(@href, 'remove')]");
        $I->see("Článek byl odebrán");

        $I->amOnPage("nase-projekty");
        $I->click("Upravený testovací projekt");
        $I->see("Upravený testovací projekt");
        $I->see("Upravený Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->see("U Závor 1, Neratovice");
        $I->see("Martina Makléřka");
        $I->see("Tomáš Makléř");


        $I->amOnPage("admin/projects/edit?project_id=" . $this->project_id);
        $I->click("Odebrat projekt");
        $I->see("Projekt byl odebrán");

        $I->click("Přidat projekt");
        $this->project_id = $I->grabAttributeFrom("//input[@name='project_id']", "value");
        $I->click("Uložit projekt");
        $I->see("Projekt byl uložen");
        $I->click("//a[contains(@href, '" . $this->projekt_id . "')][contains(@href, 'Remove')]");



    }
}