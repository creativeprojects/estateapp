<?php
use \AcceptanceTester;


class EstatesCest
{
    public $id;

    public function _before(AcceptanceTester $I)
    {
        $I->setCookie("codeception", TRUE);
        $I->seeCookie("codeception");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    protected function loginAsSuperadmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "info@strankyrealitky.cz");
        $I->fillField("user_password", "cprstrankyrealitky");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsAdmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.spravce@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsBroker(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.makler@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    /**
     * @before loginAsBroker
     */
    public function addEstate(AcceptanceTester $I) {
        $I->click("Zakázky");
        $I->click("Přidat nemovitost");
        $I->see("Upravit zakázku");
        $this->id = $I->grabAttributeFrom("//input[@name='estate_id']", "value");

        $I->wantTo("ZÁKLADNÍ INFORMACE");
        $I->selectOption("advert_function", "Pronájem");
        $I->selectOption("advert_type", "Domy");
        $I->selectOption("advert_subtype", "Vila");
        $I->fillField("estate_title[cs]", "Pronájem super vily.");
        $I->fillField("estate_description[cs]", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->checkOption("advert_onhomepage");

        $I->wantTo("CENA");
        $I->fillField("advert_price", "1000");
        $I->selectOption("advert_price_currency", "USD");
        $I->selectOption("advert_price_unit", "za měsíc");
        $I->checkOption("advert_price_negotiation");
        $I->checkOption("advert_price_hidden");
        $I->fillField("advert_price_text_note[cs]", "Toto je poznámka k ceně");
        $I->selectOption("advert_price_commission", "+ provize RK");
        $I->selectOption("advert_price_charge", "bez poplatků");
        $I->selectOption("advert_price_vat", "bez DPH");
        $I->selectOption("advert_price_legal_services", "bez právního servisu");
        $I->fillField("spor_percent", "20");

        $I->wantTo("LOKALITA");
        $I->fillField("locality_street", "U Závor");
        $I->fillField("locality_cp", "1362");
        $I->fillField("locality_co", "1");
        $I->fillField("locality_zip", "27711");
        $I->fillField("locality_city", "Neratovice");
        $I->fillField("locality_district", "Neratovice");
        $I->selectOption("locality_inaccuracy_level", "Místo adresy se zobrazuje pouze část města");
        $I->selectOption("object_kind", "Rohový");
        $I->selectOption("object_location", "Rušná část obce");

        $I->wantTo("ZAKÁZKA");
        $I->selectOption("advert_status", "Rezervováno (neaktivní)");
        $I->fillField("advert_code", "ECZ001");
        $I->selectOption("advert_lifetime", "30 dní");
        $I->selectOption("project_id", "Lucky Gardens");
        $I->selectOption("contract_type", "výhradní");
        $I->fillField("contract_sign_date", "1.1.2015");
        $I->fillField("contract_valid_date", "1.1.2015");
        $I->fillField("contract_exkl_valid_date", "1.1.2015");
        $I->fillField("locality_ce", "1");
        $I->fillField("locality_cj", "1");
        $I->fillField("internal_description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");

        $I->wantTo("TERMÍNY");
        $I->fillField("first_tour_date", "1.1.2015");
        $I->fillField("first_tour_date_to", "1.1.2015");
        $I->fillField("ready_date", "1.1.2015");
        $I->fillField("beginning_date", "1.1.2015");
        $I->fillField("finish_date", "1.1.2015");
        $I->fillField("sale_date", "1.1.2015");

        $I->wantTo("STAVBA");
        $I->selectOption("building_type", "Cihlová");
        $I->selectOption("building_condition", "Projekt");
        $I->selectOption("object_type", "Patrový");
        $I->selectOption("advert_room_count", "2 pokoje");
        $I->selectOption("furnished", "Ne");
        $I->selectOption("elevator", "Ne");
        $I->selectOption("easy_access", "Ne");
        $I->selectOption("energy_efficiency_rating", "A - Mimořádně úsporná");
        $I->fillField("energy_performance_summary", "100");

        $I->attachFile("//input[@name='energy_performance_attachment[files]']", "files/pdf.pdf");
        $I->click("//button[@name='energy_performance_attachment[add]']");
        $I->see("Přílohy byly přidány");

        $I->checkOption("advert_low_energy");
        $I->fillField("floors", "2");
        $I->fillField("underground_floors", "2");

        $I->wantTo("PLOCHA A ROZMĚRY");
        $I->fillField("estate_area", "1000");
        $I->fillField("usable_area", "500");
        $I->fillField("building_area", "300");
        $I->fillField("floor_area", "400");
        $I->fillField("garden_area", "2000");
        $I->fillField("offices_area", "100");
        $I->fillField("usable_area_ground", "200");
        $I->fillField("nolive_total_area", "50");
        $I->fillField("workshop_area", "60");
        $I->checkOption("//input[@name='balcony'][@type='checkbox']");
        $I->fillField("balcony_area", "30");
        $I->checkOption("//input[@name='loggia'][@type='checkbox']");
        $I->fillField("loggia_area", "30");
        $I->checkOption("//input[@name='terrace'][@type='checkbox']");
        $I->fillField("terrace_area", "30");
        $I->checkOption("//input[@name='cellar'][@type='checkbox']");
        $I->fillField("cellar_area", "30");
        $I->checkOption("//input[@name='basin'][@type='checkbox']");
        $I->fillField("basin_area", "30");
        $I->fillField("ceiling_height", "3,6");
        $I->checkOption("//input[@name='parking_lots'][@type='checkbox']");
        $I->fillField("parking", "2");
        $I->checkOption("//input[@name='garage'][@type='checkbox']");
        $I->fillField("garage_count", "2");

        $I->wantTo("VYBAVENÍ");
        $I->checkOption("//input[@name='water[]'][@value='1']");
        $I->checkOption("//input[@name='water[]'][@value='2']");
        $I->checkOption("//input[@name='electricity[]'][@value='1']");
        $I->checkOption("//input[@name='electricity[]'][@value='2']");
        $I->checkOption("//input[@name='electricity[]'][@value='4']");
        $I->checkOption("//input[@name='gas[]'][@value='1']");
        $I->checkOption("//input[@name='gas[]'][@value='2']");
        $I->checkOption("//input[@name='gully[]'][@value='1']");
        $I->checkOption("//input[@name='gully[]'][@value='2']");
        $I->checkOption("//input[@name='gully[]'][@value='3']");
        $I->checkOption("//input[@name='gully[]'][@value='4']");
        $I->checkOption("//input[@name='heating[]'][@value='1']");
        $I->checkOption("//input[@name='heating[]'][@value='2']");
        $I->checkOption("//input[@name='heating[]'][@value='3']");
        $I->checkOption("//input[@name='heating[]'][@value='4']");
        $I->checkOption("//input[@name='heating[]'][@value='5']");
        $I->checkOption("//input[@name='heating[]'][@value='6']");
        $I->checkOption("//input[@name='heating[]'][@value='7']");
        $I->checkOption("//input[@name='heating[]'][@value='8']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='1']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='2']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='3']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='4']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='5']");
        $I->checkOption("//input[@name='telecommunication[]'][@value='6']");
        $I->checkOption("//input[@name='road_type[]'][@value='1']");
        $I->checkOption("//input[@name='road_type[]'][@value='2']");
        $I->checkOption("//input[@name='road_type[]'][@value='3']");
        $I->checkOption("//input[@name='road_type[]'][@value='4']");
        $I->checkOption("//input[@name='transport[]'][@value='1']");
        $I->checkOption("//input[@name='transport[]'][@value='2']");
        $I->checkOption("//input[@name='transport[]'][@value='3']");
        $I->checkOption("//input[@name='transport[]'][@value='4']");
        $I->checkOption("//input[@name='transport[]'][@value='5']");

        //$I->wantTo("PŘÍPAD");
        //$I->fillField("reservation_name", "Pepa Novák");
        //$I->fillField("reservation_phone", "123 456 789");
        //$I->fillField("reservation_email", "pepanovak@strankyrealitky.cz");
        //$I->fillField("reservation_sign_date", "1.1.2015");
        //$I->fillField("reservation_valid_date", "1.1.2015");
        // TODO UPLOAD SOUBORU
        //$I->fillField("reservation_poznamka","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        //$I->fillField("buyer_name", "Jan Skočdopole");
        //$I->fillField("buyer_phone", "123 456 789");
        //$I->fillField("buyer_email", "janskoc@strankyrealitky.cz");
        //$I->fillField("buyer_poznamka","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        //$I->fillField("contract_sobks_sign_date", "1.1.2015");
        // TODO UPLOAD SOUBORU

        $I->click("Uložit změny");

        $I->see("Nemovitost byla uložena");
        
        ///////////////////////////////////////////////ú

        $I->see("Pronájem super vily.");
        $I->see("(V přípravě (neaktivní))");
        //$I->see("U závor 1362/1, 27711 Neratovice");
        $I->seeElement("//a[contains(@href, '" . $this->id . "')][contains(@href, 'onhomepage=0')]");
        $I->click("//a[contains(@href, '" . $this->id . "')][contains(@href, 'edit')]");

        $I->see("Upravit zakázku");
        $I->wantTo("ZÁKLADNÍ INFORMACE");
        $I->seeOptionIsSelected("advert_function", "Pronájem");
        $I->seeOptionIsSelected("advert_type", "Domy");
        $I->seeOptionIsSelected("advert_subtype", "Vila");
        $I->seeInField("estate_title[cs]", "Pronájem super vily.");
        $I->seeInField("estate_description[cs]","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->seeInField("advert_onhomepage", true);

        $I->wantTo("CENA");
        $I->seeInField("advert_price", "1000");
        $I->seeOptionIsSelected("advert_price_currency", "USD");
        $I->seeOptionIsSelected("advert_price_unit", "za měsíc");
        $I->seeInField("advert_price_negotiation", true);
        $I->seeInField("advert_price_hidden", true);
        $I->seeInField("advert_price_text_note[cs]", "Toto je poznámka k ceně");
        $I->seeOptionIsSelected("advert_price_commission", "+ provize RK");
        $I->seeOptionIsSelected("advert_price_charge", "bez poplatků");
        $I->seeOptionIsSelected("advert_price_vat", "bez DPH");
        $I->seeOptionIsSelected("advert_price_legal_services", "bez právního servisu");
        $I->seeInField("spor_percent", "20");

        $I->wantTo("Zakázka");
        $I->seeOptionIsSelected("advert_status", "Rezervováno (neaktivní)");
        $I->seeInField("advert_code", "ECZ001");
        $I->seeOptionIsSelected("advert_lifetime", "30 dní");
        $I->seeOptionIsSelected("project_id", "Lucky Gardens");
        $I->seeOptionIsSelected("contract_type", "výhradní");
        //$I->seeInField("contract_sign_date", "1.1.2015");
        //$I->seeInField("contract_valid_date", "1.1.2015");
        //$I->seeInField("contract_exkl_valid_date", "1.1.2015");
        $I->seeInField("locality_ce", "1");
        $I->seeInField("locality_cj", "1");
        $I->seeInField("internal_description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");

        $I->wantTo("Termíny");
        //$I->seeInField("first_tour_date", "1.1.2015");
        //$I->seeInField("first_tour_date_to", "1.1.2015");
        //$I->seeInField("ready_date", "1.1.2015");
        //$I->seeInField("beginning_date", "1.1.2015");
        //$I->seeInField("finish_date", "1.1.2015");
        //$I->seeInField("sale_date", "1.1.2015");

        $I->wantTo("Stavba");
        $I->seeOptionIsSelected("building_type", "Cihlová");
        $I->seeOptionIsSelected("building_condition", "Projekt");
        $I->seeOptionIsSelected("object_type", "Patrový");
        $I->seeOptionIsSelected("advert_room_count", "2 pokoje");
        $I->seeOptionIsSelected("furnished", "Ne");
        $I->seeOptionIsSelected("elevator", "Ne");
        $I->seeOptionIsSelected("easy_access", "Ne");
        $I->seeOptionIsSelected("energy_efficiency_rating", "A - Mimořádně úsporná");
        $I->seeInField("energy_performance_summary", "100");
        $I->seeInField("advert_low_energy", true);
        $I->seeInField("floors", "2");
        $I->seeInField("underground_floors", "2");

        $I->wantTo("PLOCHA A ROZMĚRY");
        $I->seeInField("estate_area", "1000");
        $I->seeInField("usable_area", "500");
        $I->seeInField("building_area", "300");
        $I->seeInField("floor_area", "400");
        $I->seeInField("garden_area", "2000");
        $I->seeInField("offices_area", "100");
        $I->seeInField("usable_area_ground", "200");
        $I->seeInField("nolive_total_area", "50");
        $I->seeInField("workshop_area", "60");

        $I->seeInField("//input[@name='balcony'][@type='checkbox']", true);
        $I->seeInField("balcony_area", "30");
        $I->seeInField("//input[@name='loggia'][@type='checkbox']", true);
        $I->seeInField("loggia_area", "30");
        $I->seeInField("//input[@name='terrace'][@type='checkbox']", true);
        $I->seeInField("terrace_area", "30");
        $I->seeInField("//input[@name='cellar'][@type='checkbox']", true);
        $I->seeInField("cellar_area", "30");
        $I->seeInField("//input[@name='basin'][@type='checkbox']", true);
        $I->seeInField("basin_area", "30");
        $I->seeInField("ceiling_height", "3.6");
        $I->seeInField("//input[@name='parking_lots'][@type='checkbox']", true);
        $I->fillField("parking", "2");
        $I->seeInField("//input[@name='garage'][@type='checkbox']", true);
        $I->fillField("garage_count", "2");

        $I->wantTo("VYBAVENÍ");
        $I->seeInField("//input[@name='water[]'][@value='1']", true );
        $I->seeInField("//input[@name='water[]'][@value='2']", true );
        $I->seeInField("//input[@name='electricity[]'][@value='1']", true );
        $I->seeInField("//input[@name='electricity[]'][@value='2']", true );
        $I->seeInField("//input[@name='electricity[]'][@value='4']", true );
        $I->seeInField("//input[@name='gas[]'][@value='1']", true );
        $I->seeInField("//input[@name='gas[]'][@value='2']", true );
        $I->seeInField("//input[@name='gully[]'][@value='1']", true );
        $I->seeInField("//input[@name='gully[]'][@value='2']", true );
        $I->seeInField("//input[@name='gully[]'][@value='3']", true );
        $I->seeInField("//input[@name='gully[]'][@value='4']", true );
        $I->seeInField("//input[@name='heating[]'][@value='1']", true );
        $I->seeInField("//input[@name='heating[]'][@value='2']", true );
        $I->seeInField("//input[@name='heating[]'][@value='3']", true );
        $I->seeInField("//input[@name='heating[]'][@value='4']", true );
        $I->seeInField("//input[@name='heating[]'][@value='5']", true );
        $I->seeInField("//input[@name='heating[]'][@value='6']", true );
        $I->seeInField("//input[@name='heating[]'][@value='7']", true );
        $I->seeInField("//input[@name='heating[]'][@value='8']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='1']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='2']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='3']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='4']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='5']", true );
        $I->seeInField("//input[@name='telecommunication[]'][@value='6']", true );
        $I->seeInField("//input[@name='road_type[]'][@value='1']", true );
        $I->seeInField("//input[@name='road_type[]'][@value='2']", true );
        $I->seeInField("//input[@name='road_type[]'][@value='3']", true );
        $I->seeInField("//input[@name='road_type[]'][@value='4']", true );
        $I->seeInField("//input[@name='transport[]'][@value='1']", true );
        $I->seeInField("//input[@name='transport[]'][@value='2']", true );
        $I->seeInField("//input[@name='transport[]'][@value='3']", true );
        $I->seeInField("//input[@name='transport[]'][@value='4']", true );
        $I->seeInField("//input[@name='transport[]'][@value='5']", true );

        //$I->wantTo("PŘÍPAD");
        //$I->seeInField("reservation_name", "Pepa Novák");
        //$I->seeInField("reservation_phone", "123 456 789");
        //$I->seeInField("reservation_email", "pepanovak@strankyrealitky.cz");
        //$I->seeInField("reservation_sign_date", "1.1.2015");
        //$I->seeInField("reservation_valid_date", "1.1.2015");
        //$I->seeInField("reservation_poznamka","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        //$I->seeInField("buyer_name", "Jan Skočdopole");
        //$I->seeInField("buyer_phone", "123 456 789");
        //$I->seeInField("buyer_email", "janskoc@strankyrealitky.cz");
        //$I->seeInField("buyer_poznamka","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        //$I->seeInField("contract_sobks_sign_date", "1.1.2015");

        $I->see("Upravit zakázku");

        $I->click("//a[contains(@data-confirm-title, 'Odebrat nemovitost')]");
        $I->see("Nemovitost byla odebrána");

        $I->click("Přidat nemovitost");
        $I->see("Upravit zakázku");
        $this->id = $I->grabAttributeFrom("//input[@name='estate_id']", "value");
        $I->click("//a[contains(@href, '" . $this->id . "')][contains(@href, 'Remove')]");
        $I->see("Nemovitost byla odebrána");

    }
}