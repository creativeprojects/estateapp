<?php
use \AcceptanceTester;


class AddressBookCest
{
     public function _before(AcceptanceTester $I)
    {
    	$I->setCookie("codeception", TRUE);
    	$I->seeCookie("codeception");
    	
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.makler@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    public function addContactProfile1(AcceptanceTester $I) {
        $I->wantTo("ADD NEW CONTACT AND DELETE IT IN DETAIL OF CONTACT");
        $I->click("Adresář");
        $I->click("Přidat kontakt");
        $I->fillField("contact_name", "Creative Projects s.r.o.");
        $I->fillField("contact_ic", "28948157");
        $I->fillField("contact_dic", "CZ28948157");
        $I->fillField("contact_phone", "123 456 789");
        $I->fillField("contact_email", "info@creativeprojects.cz");
        $I->fillField("contact_address_street", "Na Výsluní");
        $I->fillField("contact_address_no", "1468");
        $I->fillField("contact_address_postcode", "27711");
        $I->fillField("contact_address_city", "Neratovice");
        $id = $I->grabAttributeFrom("//input[@name='contact_id']", "value");
        $I->click("Uložit kontakt");
        $I->see("Kontakt byl uložen");
        $I->see("Creative Projects s.r.o.");
        //$I->see("Na Výsluní 1468");
        $I->see("123 456 789");
        $I->see("info@creativeprojects.cz");
        $I->click("//a[contains(@href, '" . $id . "')][contains(@href, 'edit')]");
        $I->see("Upravit kontakt");
        $I->see("Creative Projects s.r.o.");
        $I->see("28948157");
        $I->see("CZ28948157");
        $I->see("123 456 789");
       // $I->see("info@creativeprojects.cz");
        $I->see("Na Výsluní");
        $I->see("1468");
        $I->see("27711");
        $I->see("Neratovice");
        $I->click("Odebrat kontakt");
        $I->see("Kontakt byl odebrán");
    }

    public function addContactProfile2(AcceptanceTester $I) {
        $I->wantTo("ADD NEW CONTACT AND DELETE IT IN DETAIL OF CONTACT");
        $I->click("Adresář");
        $I->click("Přidat kontakt");
        $I->fillField("contact_name", "Creative Projects s.r.o.");
        $I->fillField("contact_ic", "28948157");
        $I->fillField("contact_dic", "CZ28948157");
        $I->fillField("contact_phone", "123 456 789");
        $I->fillField("contact_email", "info@creativeprojects.cz");
        $I->fillField("contact_address_street", "Na Výsluní");
        $I->fillField("contact_address_no", "1468");
        $I->fillField("contact_address_postcode", "27711");
        $I->fillField("contact_address_city", "Neratovice");
        $id = $I->grabAttributeFrom("//input[@name='contact_id']", "value");
        $I->click("Uložit kontakt");
        $I->see("Kontakt byl uložen");
        $I->see("Creative Projects s.r.o.");
        //$I->see("Na Výsluní 1468");
        $I->see("123 456 789");
        $I->see("info@creativeprojects.cz");
        $I->click("//a[contains(@href, '" . $id . "')][contains(@href, 'Remove')]");
    }
}