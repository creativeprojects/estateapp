<?php
use \AcceptanceTester;


class DemandsCest
{
     public function _before(AcceptanceTester $I)
    {
    	$I->setCookie("codeception", TRUE);
    	$I->seeCookie("codeception");
    	
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.makler@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    public function addDemand(AcceptanceTester $I) {
        $I->wantTo("ADD NEW DEMAND");
        $I->click("Poptávky");
        $I->click("Přidat poptávku");
        $I->fillField("demand_client_title", "Pepa Novák");
        $I->fillField("demand_client_phone", "123 456 789");
        $I->fillField("demand_client_email", "pepanovak@creativeprojects.cz");
        $I->selectOption("advert_function", "Pronájem");
        $I->selectOption("advert_type", "Domy");
        $I->checkOption("//input[@value='37']");
        $I->checkOption("//input[@value='39']");
        $I->fillField("demand_locality", "Neratovice");
        $I->fillField("demand_locality_radius", "5");
        $I->fillField("demand_area_from", "80");
        $I->fillField("demand_area_to", "150");
        $I->fillField("demand_price_from", "1000000");
        $I->fillField("demand_price_to", "2000000");
        $I->fillField("demand_description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->click("Uložit poptávku");
        $I->see("Poptávka byla uložena");

        $I->see("Pepa Novák");
        $I->see("123 456 789");
        $I->see("Pronájem");
        $I->see("Domy");
        $I->see("Rodinný, Vila");
        $I->see("Neratovice");
        $I->see("v okolí 5 km");

        $I->click("//a[contains(@href, 'edit')]");

        $I->seeInField("//input[@name='demand_client_title']", "Pepa Novák");
        $I->seeInField("//input[@name='demand_client_phone']", "123 456 789");
        $I->seeInField("//input[@name='demand_client_email']", "pepanovak@creativeprojects.cz");
        $I->seeOptionIsSelected("#frm-demandForm-advert_function", "Pronájem");
        $I->seeOptionIsSelected("#frm-demandForm-advert_type", "Domy");
        $I->seeInField("//input[@value='37']", true);
        $I->seeInField("//input[@value='39']", true);
        $I->seeInField("//input[@name='demand_locality']", "Neratovice");
        $I->seeInField("//input[@name='demand_locality_radius']", "5");
        $I->seeInField("//input[@name='demand_area_from']", "80");
        $I->seeInField("//input[@name='demand_area_to']", "150");
        $I->seeInField("//input[@name='demand_price_from']", "1000000");
        $I->seeInField("//input[@name='demand_price_to']", "2000000");
        $I->seeInField("//textarea[@name='demand_description']", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");

        $I->click("Odebrat poptávku");
        $I->see("Poptávka byla odebrána");

        $I->click("Přidat poptávku");
        $I->fillField("demand_client_title", "Pepa Novák");
        $I->fillField("demand_client_phone", "123 456 789");
        $I->fillField("demand_client_email", "pepanovak@creativeprojects.cz");
        $I->selectOption("advert_function", "Pronájem");
        $I->selectOption("advert_type", "Domy");
        $I->checkOption("//input[@value='37']");
        $I->checkOption("//input[@value='39']");
        $I->fillField("demand_locality", "Neratovice");
        $I->fillField("demand_locality_radius", "5");
        $I->fillField("demand_area_from", "80");
        $I->fillField("demand_area_to", "150");
        $I->fillField("demand_price_from", "1000000");
        $I->fillField("demand_price_to", "20000000");
        $I->fillField("demand_description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin erat ligula, vehicula sed varius at, euismod at felis. Ut elementum viverra tortor, sit amet aliquam lorem tincidunt a. Duis ac augue bibendum, interdum erat id, cursus mi. Cras eu nulla nec urna fringilla hendrerit ac vel mi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse potenti. Cras euismod sem luctus velit aliquet euismod.");
        $I->click("Uložit poptávku");
        $I->see("Poptávka byla uložena");

        $I->see("Pepa Novák");
        $I->see("123 456 789");
        $I->see("Pronájem");
        $I->see("Domy");
        $I->see("Rodinný, Vila");
        $I->see("Neratovice");
        $I->see("v okolí 5 km");

        $I->click("//a[contains(@href, 'Remove')]");
    }
}