<?php
use \AcceptanceTester;


class PagesCest
{
    public $id;

    public function _before(AcceptanceTester $I)
    {
    	$I->setCookie("codeception", TRUE);
    	$I->seeCookie("codeception");
    	
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.spravce@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    public function addPage(AcceptanceTester $I) {
        $I->wantTo("ADD NEW PAGE");
        $I->click("Stránky");
        $I->click("Přidat stránku");
        $I->fillField("page_title[cs]", "Testovací stránka");
        $I->fillField("blocks[main][block_html][cs]", "<h1>Testovací stránka</h1><p>testík</p>");
        $I->checkOption("page_published");
        $this->id = $I->grabAttributeFrom("//input[@name='page_id']", "value");
        $I->click("Uložit změny");
        $I->see("Stránka byla uložena");
        $I->see("Testovací stránka");
        $I->see("Publikovaná");
        $I->amOnPage("testovaci-stranka");
        $I->see("Testovací stránka");
        //$I->see("<p>testík</p>");
        $I->amOnPage("admin/pages");
        $I->click("//a[contains(@href, '" . $this->id . "')][contains(@href, 'page_published')]");
        $I->see("Stránka byla skryta");
        $I->see("Skrytá");
        // Skrývání stránek na frontendu.
        $I->click("//a[contains(@href, '" . $this->id . "')][contains(@href, 'edit')]");
        $I->seeInField("page_title[cs]", "Testovací stránka");
        $I->seeInField("blocks[main][block_html][cs]", "<h1>Testovací stránka</h1><p>testík</p>");
        $I->seeInField("page_published", false);
        $I->fillField("page_title[cs]", "Testovací stránka oprava");    
        $I->fillField("blocks[main][block_html][cs]", "<h1>Opravená testovací stránka</h1><p>opravený testík</p>");
        $I->click("Uložit změny");
        $I->see("Stránka byla uložena");
        $I->amOnPage("/testovaci-stranka");
        $I->canSeeInCurrentUrl("testovaci-stranka-oprava");
        $I->amOnPage("admin/pages");
        $I->click("//a[contains(@href, '" . $this->id . "')][contains(@href, 'Remove')]");
        $I->see("Stránka byla odebrána");
    }
}