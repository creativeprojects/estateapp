<?php
// Here you can initialize variables that will be available to your tests

date_default_timezone_set("Europe/Prague");

require 'MySQLDump.php';

$env = $_SERVER['argv'][array_search("--env",$_SERVER['argv']) + 1];

// local
if($env == "local"){
	$db_original = new mysqli("127.0.0.1", "root", "", "estateapp");
	$db_testing = new mysqli("127.0.0.1", "root", "", "estateapp_test");
}
// online
elseif($env == "online"){
	$db_original = new mysqli("81.0.214.194", "strankyrealitky", "cprstrankyrealitky", "strankyrealitky");
	$db_testing = new mysqli("81.0.214.194", "strankyrealitky", "cprstrankyrealitky", "strankyrealitkytest");
}
else{
	echo "Set --env to 'local' or 'online'";
	die;
}
define('FILENAME', date('Y-m-d') . '@'.date('H.i.s') . '.sql');
define('FILEPATH', './tests/acceptance/myBackups/' .FILENAME);

$dump = new MySQLDump($db_original);
$dump->save(FILEPATH);

mysqli_multi_query($db_testing, file_get_contents(FILEPATH)) or die ("$database 4" .mysqli_error($db_testing)); 

do {
    if($result = mysqli_store_result($db_testing)){
        mysqli_free_result($result);
    }
} while(mysqli_next_result($db_testing));

if(mysqli_error($db_testing)) {
    die(mysqli_error($db_testing));
}
