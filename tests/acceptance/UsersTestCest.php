<?php
use \AcceptanceTester;


class UsersCest
{
    public $id;

    public function _before(AcceptanceTester $I)
    {
    	$I->setCookie("codeception", TRUE);
    	$I->seeCookie("codeception");
    }

    public function _after(AcceptanceTester $I)
    {
      $I->dontSee("Nette Framework");
    }

    protected function loginAsSuperadmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "info@strankyrealitky.cz");
        $I->fillField("user_password", "cprstrankyrealitky");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsAdmin(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.spravce@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    protected function loginAsBroker(AcceptanceTester $I) {
        $I->amOnPage('/admin/');
        $I->see('Přihlašovací email');
        $I->fillField("user_email", "demo.makler@strankyrealitky.cz");
        $I->fillField("user_password", "demo");
        $I->click("Přihlásit se");
        $I->see("Byl jste přihlášen");
    }

    /**
     * @before loginAsAdmin
     */
    public function logoutAdmin(AcceptanceTester $I) {
        $I->wantTo('LOGIN AND LOGOUT - ADMIN MODULE');
        $I->click("Odhlásit se");
        $I->see("Byl jste odhlášen");
    }

    /**
     * @before loginAsAdmin
     */
    public function logoutFront(AcceptanceTester $I) {
        $I->wantTo('LOGIN AND LOGOUT - FRONT MODULE');
        $I->click("Přejít na web");
        $I->click("Odhlásit se");
        $I->see("Byl jste odhlášen");
    }

/**
     * @before loginAsAdmin
     */
    public function changePassword(AcceptanceTester $I) {
        $I->wantTo('CHANGE MY PASSWORD');
        $I->click("//a[@title='Zobrazit či upravit můj účet']");
        $I->see("Můj účet");
        $I->click("Změnit heslo");
        $I->see("Změnit heslo");
        $I->fillField("user_password", "demo");
        $I->fillFIeld("newPassword", "test");
        $I->fillField("verifyPassword", "test");
        $I->click("Uložit heslo");
        $I->see("Vaše heslo bylo uloženo");
        $I->click("//a[@title='Zobrazit či upravit můj účet']");
        $I->see("Můj účet");
        $I->click("Změnit heslo");
        $I->see("Změnit heslo");
        $I->fillField("user_password", "test");
        $I->fillFIeld("newPassword", "demo");
        $I->fillField("verifyPassword", "demo");
        $I->click("Uložit heslo");
        $I->see("Vaše heslo bylo uloženo");
    }

    /**
     * @before loginAsAdmin
     */
    public function editAccount(AcceptanceTester $I) {
        $I->wantTo('SEE AND EDIT MY PROFILE - admin');
        $I->click("//a[@title='Zobrazit či upravit můj účet']");
        $I->see("Můj účet");
        $I->click("//a[@title='Upravit můj účet']");
        $I->fillField("user_title", "Pepa Novák");
        $I->click("//button[@name='save']");
        $I->see("Změny ve Vašem uživatelském účtu byly uloženy");
        $I->see("Pepa Novák");
    }

    /**
     * @before loginAsBroker
     */
    public function editBrokerProfile(AcceptanceTester $I) {
        $I->wantTo('SEE AND EDIT BROKERS PROFILE');
        $I->click("//a[@title='Zobrazit či upravit můj účet']");
        $I->see("Můj účet");

        $I->click("//a[@title='Upravit můj účet']");
        $I->fillField("user_title", "Pepa Novák");
        $I->fillField("broker_phone", "987 654 321");        
        $I->click("//button[@name='save']");
        $I->see("Změny ve Vašem makléřském účtu byly uloženy");
        $I->see("Pepa Novák");

        $I->click("//a[@title='Upravit profil makléře']");
        $I->see("Upravit profil makléře");

        $I->click("//a[@title='Fotografie']");
        $I->see("Profilové fotografie");
        $I->attachFile("//input[@name='broker[files][]']", "img/avatar.png");

        $I->click("//button[@name='broker[addImages]']");
        $I->fillField("profile_title[cs]", "Pepa_Novák");
        $I->fillField("profile_position[cs]", "pozice_pozice");
        $I->fillField("profile_description[cs]", "predstaveni_maklere");
        $I->click("//button[@name='save']");
        $I->see("Profil makléře byl uložen");
        $I->click("Přejít na web");
        $I->click("Kontakty");
        $I->see("Naši makléři");
        $I->see("Pepa Novák");
        $I->click("Pepa Novák");
        $I->see("Pepa_Novák");
        $I->see("pozice_pozice");
        $I->see("predstaveni_maklere");
    }

    /**
     * @before loginAsSuperadmin
     */
    public function addAdmin(AcceptanceTester $I) {
        $I->wantTo("ADD NEW ADMIN");
        $I->click("Uživatelé");
        $I->click("Přidat správce");
        $I->fillField("user_title", "Nový správce");
        $I->fillField("user_email", "danielmarhan+test@gmail.com");
        $I->click("Vytvořit účet");
        $I->see("Uživatelský účet byl vytvořen. Přihlašovací údaje byly odeslány na email uživatele.");
        $I->click("//tr//*[contains(text(),'danielmarhan+test@gmail.com')]//..//..//a[contains(@href, 'edit')]");
        $I->see("Upravit účet");
        $I->seeInField("user_title", "Nový správce");
        $I->seeInField("user_email", "danielmarhan+test@gmail.com");
        $I->seeOptionIsSelected("role", "Správce");
        $I->fillField("user_title", "Nový správce2");
        $I->fillField("user_email", "danielmarhan+test2@gmail.com");
        //$I->selectOption("role", "Programátor");
        $I->click("Uložit změny");
        $I->see("Změny v uživatelském účtu byly uloženy");
        $I->see("danielmarhan+test2@gmail.com");
        $I->see("Nový správce2");
        $I->click("//tr//*[contains(text(),'danielmarhan+test2@gmail.com')]//..//..//a[contains(@href, 'edit')]");
        //$I->see("Upravit účet");
        $I->seeInField("user_title", "Nový správce2");
        $I->seeInField("user_email", "danielmarhan+test2@gmail.com");
        //$I->seeOptionIsSelected("role", "Programátor");
        $I->click("Odebrat účet");
        $I->see("Uživatelský účet byl odebrán");

        /////////////

        $I->click("Uživatelé");
        $I->click("Přidat správce");
        $I->fillField("user_title", "Nový správce");
        $I->fillField("user_email", "danielmarhan+test@gmail.com");
        $I->click("Vytvořit účet");
        $I->see("Uživatelský účet byl vytvořen. Přihlašovací údaje byly odeslány na email uživatele.");
        $I->click("//tr//*[contains(text(),'danielmarhan+test@gmail.com')]//..//..//a[contains(@href, 'Remove')]");
        $I->canSee("Uživatelský účet byl odebrán");
    }
}