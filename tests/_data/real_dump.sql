--
-- Database : estateapp
--
-- --------------------------------------------------
-- ---------------------------------------------------
SET AUTOCOMMIT = 0 ;
SET FOREIGN_KEY_CHECKS=0 ;
--
-- Tabel structure for table `addressbook_contacts`
--
DROP TABLE  IF EXISTS `addressbook_contacts`;
CREATE TABLE `addressbook_contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_ic` varchar(255) DEFAULT NULL,
  `contact_dic` varchar(255) DEFAULT NULL,
  `contact_address_street` varchar(255) DEFAULT NULL,
  `contact_address_no` varchar(255) DEFAULT NULL,
  `contact_address_city` varchar(255) DEFAULT NULL,
  `contact_address_postcode` varchar(255) DEFAULT NULL,
  `contact_address_country` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `addressbook_contacts`  VALUES ( "1","2","","","","","","","","","","","2015-03-30 16:14:14","19","","","","");


--
-- Tabel structure for table `articles`
--
DROP TABLE  IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `article_published` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `articles`  VALUES ( "1","projects","1","2","2015-03-30 00:00:00","","2015-03-30 16:15:46","19","2015-04-13 12:43:12","19","","");
INSERT INTO `articles`  VALUES ( "2","projects","2","2","2015-03-31 00:00:00","22","2015-03-30 18:55:05","20","2015-03-30 18:57:12","20","","");


--
-- Tabel structure for table `articles_cs`
--
DROP TABLE  IF EXISTS `articles_cs`;
CREATE TABLE `articles_cs` (
  `article_id` int(11) NOT NULL,
  `article_title` varchar(255) DEFAULT NULL,
  `article_snippet` text,
  `article_content` longtext,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `articles_cs`  VALUES ( "1","Nov� technologie v na�em projektu","","<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent quis  erat nisi. Curabitur eros nibh, placerat ac pulvinar in, rutrum iaculis  ante. Cras ut justo molestie, hendrerit enim sit amet, finibus mauris.  Sed ut eleifend purus, a ullamcorper tortor. Morbi sit amet sem nisl.  Proin fermentum mi quis nibh porta, id feugiat odio posuere. Nulla  facilisi. Maecenas ex magna, iaculis at sem et, luctus malesuada orci.  Morbi varius cursus nibh quis malesuada. Donec vel vehicula tortor. Nam  ut nulla ut ipsum sagittis dignissim. Vivamus augue augue, volutpat ut  vehicula vel, malesuada non mi. Phasellus fringilla, quam sed faucibus  interdum, ex tellus imperdiet ex, ullamcorper semper lectus arcu sit  amet lectus. Aliquam ullamcorper tortor in efficitur volutpat. Proin ut  orci nisi. Quisque congue, nibh vel pretium vulputate, mi nisi gravida  est, in elementum risus ex vel purus.</p><p> Duis tincidunt finibus lacus, id efficitur dolor aliquet hendrerit.  Nullam dictum, eros in dignissim auctor, augue felis dapibus velit, nec  vehicula augue mauris sed dolor. Fusce bibendum tellus vel fermentum  hendrerit. Vestibulum at vehicula lacus. Praesent ultrices lorem odio,  ut efficitur ligula pretium in. Interdum et malesuada fames ac ante  ipsum primis in faucibus. Pellentesque porta, nulla vitae aliquam  tempor, metus turpis mattis mi, facilisis vulputate eros ligula ut  massa. Nunc lobortis semper felis, ut posuere massa vestibulum ut.  Aenean quam turpis, commodo faucibus mattis et, posuere commodo urna.  Aenean suscipit tortor ipsum. Etiam dignissim erat non nulla vestibulum,  in finibus nibh elementum. Mauris ultrices sollicitudin elit at  convallis. Duis sollicitudin lobortis metus non elementum. Vestibulum  elementum justo vel leo tristique, nec pretium felis pellentesque.  Praesent in ullamcorper neque.</p>");
INSERT INTO `articles_cs`  VALUES ( "2","N�zk� energetick� n�ro?nost prok�z�na","","<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit  ultrices mollis. Curabitur et augue et erat commodo sodales. Quisque ac  volutpat est. Suspendisse potenti. Quisque cursus efficitur facilisis.  Vivamus ut congue eros. Curabitur egestas mi nec metus varius, porta  ullamcorper tortor tristique. Vivamus eget mi lacus. Sed in erat lectus.  Fusce malesuada eleifend metus non mattis. Etiam ac blandit dui. Nulla  facilisi.</p><p> Nunc iaculis, est sit amet sagittis bibendum, erat dui pellentesque  odio, id lobortis sem eros ac tellus. Quisque tristique feugiat justo ut  vestibulum. Donec consectetur nisl eu risus pretium congue. Nullam  sagittis orci ultricies finibus pharetra. Phasellus nec tellus  condimentum, auctor felis nec, pretium tellus. In malesuada mauris nibh,  id scelerisque tellus euismod ac. Sed mi sapien, semper eu enim sit  amet, hendrerit iaculis lectus. Maecenas condimentum hendrerit rhoncus.  Donec vitae viverra enim. Quisque et dictum nunc. Proin massa turpis,  volutpat eu posuere et, ultricies non sem. Fusce auctor a turpis quis  dictum. Sed tristique, metus ornare bibendum interdum, augue diam  gravida nisl, ut tempus augue tellus et quam. Etiam interdum ultrices  urna, eget accumsan quam posuere nec. Vivamus venenatis ex vitae lacus  maximus viverra. Etiam accumsan, sem vel condimentum scelerisque, nisi  leo vulputate ex, at venenatis nisl leo id leo.</p>");


--
-- Tabel structure for table `brokerprofiles`
--
DROP TABLE  IF EXISTS `brokerprofiles`;
CREATE TABLE `brokerprofiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `profile_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles`  VALUES ( "19","2","1","2015-04-13 11:46:05","19","2015-04-15 10:34:36","20","","");


--
-- Tabel structure for table `brokerprofiles_cs`
--
DROP TABLE  IF EXISTS `brokerprofiles_cs`;
CREATE TABLE `brokerprofiles_cs` (
  `user_id` int(11) NOT NULL,
  `profile_title` varchar(255) DEFAULT NULL,
  `profile_position` varchar(255) DEFAULT NULL,
  `profile_description` longtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles_cs`  VALUES ( "19","Demo Makl�?","Realitn� expert pro ?eskou Republiku","<p class=\"lead\">\n	S realitn� ?innost� jsem za?ala poprv� po �kole v jedn� v?t�� realitn� kancel�?i. Bylo to na za?�tku sam� �kolen� a pozitivn� lad?n�, jen�e bohu�el praxe byla trochu jinde. Za?al b�t velk� tlak sp�e na kvatitu, ne�li na kvalitu pr�ce a tak i p?esto, �e jsem byla velmi vd??n� za v�e co mne nau?ili a n?kter� postupy, kter� mi uk�zali, na�e cesty se roze�li.</p><p>\n	Nyn� pracuji syst�mem, kter� p?in�� m�m klient?m kvalitu a p?�m� jedn�n�, pom�h�m v�e ?e�it lidsk�m p?�stupem a nezat?�uji je v?cmi, kter� mohu ?e�it sama. V�dy je ov�em informuji a m� klienti p?esn? v?d� co zrovna d?l�m a mohou toho b�t i sou?�st�.</p>");


--
-- Tabel structure for table `brokerprofiles_testimonials`
--
DROP TABLE  IF EXISTS `brokerprofiles_testimonials`;
CREATE TABLE `brokerprofiles_testimonials` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `testimonial_author` varchar(255) DEFAULT NULL,
  `testimonial_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles_testimonials`  VALUES ( "1","19","2","Spokojen� klient, Neratovice","","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `brokerprofiles_testimonials`  VALUES ( "2","19","2","Jan Nov�k, M?ln�k","","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `brokerprofiles_testimonials`  VALUES ( "3","19","2","Jm�no P?�jmen�, Praha","","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");


--
-- Tabel structure for table `brokerprofiles_testimonials_cs`
--
DROP TABLE  IF EXISTS `brokerprofiles_testimonials_cs`;
CREATE TABLE `brokerprofiles_testimonials_cs` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_text` text,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles_testimonials_cs`  VALUES ( "1","<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. Curabitur rutrum venenatis sem, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. Nam porta pellentesque varius. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>");
INSERT INTO `brokerprofiles_testimonials_cs`  VALUES ( "2","<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. Curabitur rutrum venenatis sem, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. <em>Nam porta pellentesque varius</em>. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>");
INSERT INTO `brokerprofiles_testimonials_cs`  VALUES ( "3","<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. <strong>Curabitur rutrum venenatis sem</strong>, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. Nam porta pellentesque varius. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>");


--
-- Tabel structure for table `brokerprofiles_videos`
--
DROP TABLE  IF EXISTS `brokerprofiles_videos`;
CREATE TABLE `brokerprofiles_videos` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `video_address` varchar(255) DEFAULT NULL,
  `video_code` varchar(100) DEFAULT NULL,
  `video_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles_videos`  VALUES ( "1","19","2","Pod?brady","RIcKaKe5ves","","2015-04-13 12:37:32","19","2015-04-15 10:34:36","20","","");


--
-- Tabel structure for table `brokerprofiles_videos_cs`
--
DROP TABLE  IF EXISTS `brokerprofiles_videos_cs`;
CREATE TABLE `brokerprofiles_videos_cs` (
  `video_id` int(11) NOT NULL,
  `video_title` varchar(255) DEFAULT NULL,
  `video_description` text,
  PRIMARY KEY (`video_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `brokerprofiles_videos_cs`  VALUES ( "1","Prodej bytu","<p>Na�e videoprezentace bytu v Pod?bradech k prodeji</p>");


--
-- Tabel structure for table `clients`
--
DROP TABLE  IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_title` varchar(255) DEFAULT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `client_office_address` varchar(255) DEFAULT NULL,
  `client_office_latitude` double DEFAULT NULL,
  `client_office_longitude` double DEFAULT NULL,
  `client_ic` varchar(255) DEFAULT NULL,
  `client_dic` varchar(255) DEFAULT NULL,
  `client_incorporation` varchar(255) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `client_admin_properties` text,
  `client_subdomain` varchar(255) DEFAULT NULL,
  `client_domain` varchar(255) DEFAULT NULL,
  `client_expire` timestamp NULL DEFAULT NULL,
  `source_id` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `sreality_client_id` varchar(255) DEFAULT NULL,
  `sreality_password` varchar(255) DEFAULT NULL,
  `sreality_key` varchar(255) DEFAULT NULL,
  `idnes_ftp_user` varchar(255) DEFAULT NULL,
  `idnes_ftp_password` varchar(255) DEFAULT NULL,
  `idnes_import_login` varchar(255) DEFAULT NULL,
  `idnes_import_password` varchar(255) DEFAULT NULL,
  `client_dir` varchar(255) DEFAULT NULL,
  `export_settings` text,
  `realitymix_export_counter` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `clients`  VALUES ( "1","BELARTIS s.r.o.","Du�n� 112/16, Praha 1 - Star� M?sto, 110 00","Du�n� 112/16, Praha 1 - Star� M?sto, 110 00","50.0907497","14.4206115","2934299","CZ02934299","C 225585 veden� u M?stsk�ho soudu v Praze","+420 602 603 604","info@belartis.cz","{\"logoPath\":\"/theme/www-belartis-cz/img/belartis-logotype.png\", \"faviconPath\":\"/theme/www-belartis-cz/img/favicon.ico\"}","belartis","www.belartis.cz","","","2015-01-05 16:04:03","","2015-04-29 18:23:45","21","","","","","","external_export_test","XYC3p_71","28948157","93dF4EeF1F","theme/www-belartis-cz","{\"sreality\":true,\"sreality_client_id\":\"13001\",\"sreality_password\":\"db9349ade56a325bb948e89ddc3af0b0\",\"sreality_key\":\"bela-451-ad\",\"idnes\":true,\"idnes_ftp_user\":\"external_export_test\",\"idnes_ftp_password\":\"XYC3p_71\",\"idnes_import_login\":\"28948157\",\"idnes_import_password\":\"93dF4EeF1F\",\"realitymix\":false,\"realitymix_client_id\":\"\",\"realitymix_password\":\"\",\"realitymix_key\":\"\",\"realcity\":false,\"realcity_client_id\":\"\",\"realcity_password\":\"\",\"realcity_key\":\"\"}","0");
INSERT INTO `clients`  VALUES ( "2","Str�nkyRealitky.Cz","Na V�slun� 1468, 277 11 Neratovice","U Z�vor 1362, 277 11 Neratovice","50.2543394","14.5057423","28948157","CZ28948157","C 155109 veden� u M?stsk�ho soudu v Praze","+420 725 814 440","info@strankyrealitky.cz","{\"logoPath\":\"/theme/www-demo-cz/img/strankyrealitky-logotype.svg\", \"faviconPath\":\"/theme/www-demo-cz/img/favicon.ico\"}","demo","","","","2015-03-09 19:17:30","","2015-04-29 18:23:28","21","","","13490","3685227c64f66ea8888deca4984ed442","sreality-test","external_export_test","XYC3p_71","28948157","93dF4EeF1F","theme/www-demo-cz","{\"sreality\":true,\"sreality_client_id\":\"13490\",\"sreality_password\":\"3685227c64f66ea8888deca4984ed442\",\"sreality_key\":\"sreality-test\",\"idnes\":true,\"idnes_ftp_user\":\"external_export_test\",\"idnes_ftp_password\":\"XYC3p_71\",\"idnes_import_login\":\"28948157\",\"idnes_import_password\":\"93dF4EeF1F\",\"realitymix\":false,\"realitymix_client_id\":\"5\",\"realitymix_password\":\"5c2cc95a\",\"realitymix_key\":\"test_1_0\",\"realcity\":false,\"realcity_client_id\":\"140784\",\"realcity_password\":\"2028ceb06602c9525b0c5bb220d1fe6f\",\"realcity_key\":\"rcpro\"}","1");
INSERT INTO `clients`  VALUES ( "3","Elite Living - premium estates by Aktivreality","28.?�jna 919, 277 11 Neratovice\n277 11 Neratovice","28.?�jna 919, 277 11 Neratovice","","","70796068","CZ8308070144","","","info@eliteliving.cz","{\"logoPath\":\"/theme/www-eliteliving-cz/img/eliteliving-logotype.svg\", \"faviconPath\":\"/theme/www-eliteliving-cz/img/favicon.ico\"}","eliteliving","www.eliteliving.cz","","","2015-04-15 10:38:18","","2015-04-29 11:52:13","","","","","","","","","","","","","0");


--
-- Tabel structure for table `demands`
--
DROP TABLE  IF EXISTS `demands`;
CREATE TABLE `demands` (
  `demand_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `demand_client_title` varchar(100) NOT NULL,
  `demand_client_phone` varchar(20) NOT NULL,
  `demand_client_email` varchar(100) NOT NULL,
  `advert_type` tinyint(1) NOT NULL,
  `advert_function` tinyint(1) NOT NULL,
  `advert_subtype` text,
  `demand_locality` varchar(255) DEFAULT NULL,
  `demand_locality_radius` int(11) DEFAULT NULL,
  `demand_locality_latitude` double DEFAULT NULL,
  `demand_locality_longitude` double DEFAULT NULL,
  `demand_area_from` int(11) DEFAULT NULL,
  `demand_area_to` int(11) DEFAULT NULL,
  `demand_price_from` int(11) DEFAULT NULL,
  `demand_price_to` int(11) DEFAULT NULL,
  `demand_description` text,
  `demand_status` tinyint(4) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`demand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `demands`  VALUES ( "1","1","","Jan Sal�k","12345678","j.salak@seznam.cz","1","1","[2,3]","Neratovice","3","50.2592611","14.5176026","","","","100000","po�adavky","0","2015-01-07 15:35:09","1","2015-01-07 15:43:17","1","2015-01-07 15:43:17","1");
INSERT INTO `demands`  VALUES ( "2","1","2","Jan Sal�k","12345678","j.salak@seznam.cz","1","1","[2,3]","M?ln�k","3","50.3539017","14.4817813","50","","","1000000","po�adavky","1","2015-01-07 15:53:11","1","","","2015-01-21 17:35:32","9");
INSERT INTO `demands`  VALUES ( "3","1","","Jan Sal�k","123456789","j.salak@seznam.cz","1","1","[2,3]","Neratovice","2","50.2592611","14.5176026","50","","","2000000","jin�","2","2015-01-07 16:20:22","1","","","","");
INSERT INTO `demands`  VALUES ( "4","1","5","dfs","sfa","danielmarhan@gmail.com","1","1","","Neratovice","2","50.2592611","14.5176026","","","","","","1","2015-01-12 20:23:05","1","2015-01-19 17:04:20","1","2015-01-19 17:04:25","1");
INSERT INTO `demands`  VALUES ( "5","1","","Testova�c","dfjlsj","danielmarhan@gmail.com","1","1","","Neratovice","2","50.2592611","14.5176026","","","","","","0","2015-01-20 15:54:23","","","","2015-01-20 16:02:02","1");
INSERT INTO `demands`  VALUES ( "6","1","","Dafd","dafds","danielmarhan@gmail.com","1","1","","Neratovice","2","50.2592611","14.5176026","","","","","","0","2015-01-20 15:58:55","1","","","2015-01-20 16:01:59","1");
INSERT INTO `demands`  VALUES ( "7","1","","DSFDS","dafds","danielmarhan@gmail.com","1","1","","Neratovice","2","50.2592611","14.5176026","","","","","","0","2015-01-20 15:59:57","1","","","2015-01-20 16:01:55","1");
INSERT INTO `demands`  VALUES ( "8","1","","test","1","danielmarhan@gmail.com","1","1","","Neratovice","2","50.2592611","14.5176026","","","","","","0","2015-01-20 16:07:03","1","","","2015-01-20 16:12:16","1");
INSERT INTO `demands`  VALUES ( "9","1","","dfs","fsadd","danielmarhan@gmail.com","1","1","","Neratovice","1","50.2592611","14.5176026","","","","","","0","2015-01-20 16:11:42","1","","","2015-01-20 16:12:20","1");


--
-- Tabel structure for table `emails`
--
DROP TABLE  IF EXISTS `emails`;
CREATE TABLE `emails` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email_from_name` varchar(255) DEFAULT NULL,
  `email_from_email` varchar(255) DEFAULT NULL,
  `email_to` text,
  `email_cc` text,
  `email_bcc` text,
  `email_subject` varchar(255) DEFAULT NULL,
  `email_message` longtext,
  `email_attachments` text,
  `email_sent` timestamp NULL DEFAULT NULL,
  `email_priority` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

INSERT INTO `emails`  VALUES ( "1","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>info@creativeprojects.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>yvsnod</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-01-19 19:24:22","0","2015-01-19 19:23:47","1","2015-01-19 19:24:22","1","","");
INSERT INTO `emails`  VALUES ( "2","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>info@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>ul9u9v</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-01-19 21:14:20","0","2015-01-19 19:27:31","1","2015-01-19 21:14:20","","","");
INSERT INTO `emails`  VALUES ( "4","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"helena@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>helena@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>w3ttb1</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-01-19 21:14:20","0","2015-01-19 20:08:55","8","2015-01-19 21:14:20","","","");
INSERT INTO `emails`  VALUES ( "5","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>lukas@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>ycxizu</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-01-19 23:13:43","0","2015-01-19 21:51:25","9","2015-01-19 23:13:43","9","","");
INSERT INTO `emails`  VALUES ( "6","11","stwe","helenpisova@gmail.com","[{\"email\":\"helena@belartis.cz\",\"name\":\"Helena P\\u00ed\\u0161ov\\u00e1\"}]","","","Dotaz na nemovitost ev. ?. 5","<h1>Dotaz k nemovitosti ?. 5</h1>\n<p>\n	we?tw?\n</p>","","2015-01-21 12:05:57","0","2015-01-21 11:57:08","11","2015-01-21 12:05:57","","","");
INSERT INTO `emails`  VALUES ( "7","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=Qresbq7XDbXRESZeEjnNCRnsjtOk3w\">http://belartis.strankyrealitky.cz/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=Qresbq7XDbXRESZeEjnNCRnsjtOk3w</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-01-23 20:10:55","0","2015-01-23 20:07:16","","2015-01-23 20:10:55","","","");
INSERT INTO `emails`  VALUES ( "8","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=YJkocx43eoDz7VBTaeVl9aQkHAMxxk\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=YJkocx43eoDz7VBTaeVl9aQkHAMxxk</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-01-23 20:57:55","0","2015-01-23 20:27:29","8","2015-01-23 20:57:55","","","");
INSERT INTO `emails`  VALUES ( "9","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"david@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>david@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>mmu6wu</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-01-28 17:29:56","0","2015-01-28 17:29:02","9","2015-01-28 17:29:56","","","");
INSERT INTO `emails`  VALUES ( "10","","hokus pokus","lukas.matocha@gmail.com","[{\"email\":\"josef@belartis.cz\",\"name\":\"Ing. Josef \\u017d\\u00e1ra\"}]","","","Dotaz na nemovitost ev. ?. 2","<h1>Dotaz k nemovitosti ?. 2</h1>\n<p>\n	rad bych se zeptal k nemovitosti kotoucov. kam ted dotaz dojde?\n</p>","","2015-01-28 20:13:55","0","2015-01-28 20:13:02","","2015-01-28 20:13:55","","","");
INSERT INTO `emails`  VALUES ( "11","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Ing. Luk� Matocha</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=35dhkSgI1izET3Rfa6c7XEB5QohQ5c\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=35dhkSgI1izET3Rfa6c7XEB5QohQ5c</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-01-29 18:24:59","0","2015-01-29 18:24:20","","2015-01-29 18:24:59","","","");
INSERT INTO `emails`  VALUES ( "12","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"ivo@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>ivo@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>ydqmsw</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-02-09 11:05:05","0","2015-02-04 16:14:12","9","2015-02-09 11:05:05","","","");
INSERT INTO `emails`  VALUES ( "13","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"ivana@belartis.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://belartis.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://belartis.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>ivana@belartis.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>6d4ti9</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-02-13 10:17:10","0","2015-02-13 10:17:06","9","2015-02-13 10:17:10","","","");
INSERT INTO `emails`  VALUES ( "14","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>BELARTIS s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=2JlWjdYBn9yqKqRvhWDVtmGLJNcHgd\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=2JlWjdYBn9yqKqRvhWDVtmGLJNcHgd</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-17 17:13:00","0","2015-02-17 17:12:41","","2015-02-17 17:13:00","","","");
INSERT INTO `emails`  VALUES ( "15","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>BELARTIS s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=6al6XpzpQa369Uikx5FRJJQO5TeDQb\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=6al6XpzpQa369Uikx5FRJJQO5TeDQb</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-17 20:55:00","0","2015-02-17 20:54:13","","2015-02-17 20:55:00","","","");
INSERT INTO `emails`  VALUES ( "16","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"helena@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Ing. Helena P�ov�</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=helena@belartis.cz&amp;user_recovery_code=E9tOuT74uRMSYmRhzTt7FZgaY0t2Mi\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=helena@belartis.cz&amp;user_recovery_code=E9tOuT74uRMSYmRhzTt7FZgaY0t2Mi</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-20 09:57:00","0","2015-02-20 09:56:41","","2015-02-20 09:57:00","","","");
INSERT INTO `emails`  VALUES ( "17","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://belartis.strankyrealitky.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Ing. Luk� Matocha</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=F5eWf0tMGL9ct9GTxIPMwWklWbhonj\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=F5eWf0tMGL9ct9GTxIPMwWklWbhonj</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-22 10:33:00","0","2015-02-22 10:32:39","","2015-02-22 10:33:00","","","");
INSERT INTO `emails`  VALUES ( "18","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://www.belartis.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Ing. Luk� Matocha</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=SepMnTyzMJmNDa1KY2m5m8KqxpyNOT\">http://www.belartis.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=SepMnTyzMJmNDa1KY2m5m8KqxpyNOT</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-22 10:36:00","0","2015-02-22 10:35:27","","2015-02-22 10:36:00","","","");
INSERT INTO `emails`  VALUES ( "19","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://www.belartis.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=aoWqOUDTSvGtBYl5ZFVMmsg8hyFsWX\">http://www.belartis.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=aoWqOUDTSvGtBYl5ZFVMmsg8hyFsWX</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-22 14:45:00","","2015-02-22 14:44:36","","2015-02-22 14:45:00","","","");
INSERT INTO `emails`  VALUES ( "20","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://www.belartis.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/user/reset-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=CtnOW74MjRD8l515bMJmrPxiYv7wq0\">http://www.belartis.cz/admin/user/reset-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=CtnOW74MjRD8l515bMJmrPxiYv7wq0</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-22 14:50:01","","2015-02-22 14:49:43","","2015-02-22 14:50:01","","","");
INSERT INTO `emails`  VALUES ( "21","","BELARTIS s.r.o.","info@belartis.cz","[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]","","","��dost o reset hesla na http://www.belartis.cz","\n\n<h1>\n  Zapom?li jste heslo? \n  <small>Dostali jsme ��dost o zm?nu hesla k Va�emu �?tu</small>\n</h1>\n\n<p>\n    N?kdo, pravd?podobn? Vy za��dal(a) o zm?nu hesla k �?tu <strong>Ing. Luk� Matocha</strong>. Pokud jste to o reset hesla ne��dal(a) tento email ignorujte. V opa?n�m p?�pad? si m?�ete heslo zm?nit na n�e uveden� adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro zm?nu Va�eho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/user/reset-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=bX3bXDgGk3JtkwUvbekJHKm5ds1kP2\">http://www.belartis.cz/admin/user/reset-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=bX3bXDgGk3JtkwUvbekJHKm5ds1kP2</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na v��e uveden�m odkazu naleznete formul�? pro zad�n� nov�ho hesla k Va�emu �?tu.\n</p>\n","","2015-02-22 21:55:00","","2015-02-22 21:54:48","9","2015-02-22 21:55:00","","","");
INSERT INTO `emails`  VALUES ( "22","","zorro mstitel","helenpisova@gmail.com","[{\"email\":\"lukas@belartis.cz\",\"name\":\"Ing. Luk\\u00e1\\u0161 Matocha\"}]","","","Dotaz na nemovitost ev. ?. 8","<h1>Dotaz k nemovitosti ?. 8</h1>\n<p>\n	grrrr\n</p>","","2015-02-23 10:09:00","","2015-02-23 10:08:26","","2015-02-23 10:09:00","","","");
INSERT INTO `emails`  VALUES ( "23","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"demo.makler@strankyrealitky.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>demo.makler@strankyrealitky.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>2g2hnr</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-10 15:06:00","","2015-03-10 15:05:59","8","2015-03-10 15:06:00","","","");
INSERT INTO `emails`  VALUES ( "24","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"demo.spravce@strankyrealitky.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>demo.spravce@strankyrealitky.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>u1h36z</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-10 15:16:00","","2015-03-10 15:15:38","8","2015-03-10 15:16:00","","","");
INSERT INTO `emails`  VALUES ( "25","","Jan Sal�k","j.salak@seznam.cz","","","","Dotaz z webov�ch str�nek","test","","2015-03-13 17:54:00","","2015-03-13 17:53:34","1","2015-03-13 17:54:00","","","");
INSERT INTO `emails`  VALUES ( "26","","Jan Sal�k","j.salak@seznam.cz","","","","Dotaz z webov�ch str�nek","test","","2015-03-13 17:55:00","","2015-03-13 17:54:28","1","2015-03-13 17:55:00","","","");
INSERT INTO `emails`  VALUES ( "27","","Jan Sal�k","j.salak@seznam.cz","","","","Dotaz z webov�ch str�nek","test","","2015-03-13 18:42:00","","2015-03-13 18:41:43","1","2015-03-13 18:42:00","","","");
INSERT INTO `emails`  VALUES ( "28","","Jan Sal�k","j.salak@seznam.cz","","","","Dotaz z webov�ch str�nek","test","","2015-03-13 18:46:01","","2015-03-13 18:45:24","1","2015-03-13 18:46:01","","","");
INSERT INTO `emails`  VALUES ( "29","","Jan Sal�k","j.salak@seznam.cz","","","","Dotaz z webov�ch str�nek","test","","2015-03-13 18:47:00","","2015-03-13 18:46:55","1","2015-03-13 18:47:00","","","");
INSERT INTO `emails`  VALUES ( "36","","daniel","danielmarhan@gmail.com","","","","Dotaz z webov�ch str�nek","test","","2015-03-14 18:07:00","","2015-03-14 18:06:20","","2015-03-14 18:07:00","","","");
INSERT INTO `emails`  VALUES ( "37","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"info@strankyrealitky.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://demo.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://demo.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>info@strankyrealitky.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>oscbiu</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 15:37:01","","2015-03-30 15:22:38","8","2015-03-30 15:37:01","","","");
INSERT INTO `emails`  VALUES ( "38","","Demo Makl�?","demo.makler@strankyrealitky.cz","[{\"email\":\"sallam@centrum.cz\",\"name\":\"\"}]","","","Karta nemovitosti Prodej bytu 4+1, Neratovice","<p>Dobr� den</p><p>\n	V p?�loze V�m zas�l�me kartu nemovitosti <strong>Prodej bytu 4+1, Neratovice</strong>.</p><p>\n	S pozdravem</p><p>fuck</p>","[\"\\/theme\\/www-demo-cz\\/files\\/estates_pdf\\/prodej-bytu-4-1-neratovice.pdf\"]","2015-03-30 16:11:01","","2015-03-30 16:10:09","19","2015-03-30 16:11:01","","","");
INSERT INTO `emails`  VALUES ( "39","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"sallam@centrum.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://demo.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://demo.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>sallam@centrum.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>g4leim</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:16:01","","2015-03-30 18:15:57","20","2015-03-30 18:16:01","","","");
INSERT INTO `emails`  VALUES ( "40","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"danielmarhan@gmail.com\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>danielmarhan@gmail.com</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>qrb2e3</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:21:00","","2015-03-30 18:20:14","1","2015-03-30 18:21:00","","","");
INSERT INTO `emails`  VALUES ( "41","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"danielmarhan+test@gmail.com\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>danielmarhan+test@gmail.com</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>hyvc3t</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:21:00","","2015-03-30 18:20:40","1","2015-03-30 18:21:00","","","");
INSERT INTO `emails`  VALUES ( "42","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"danielmarhan+test2@gmail.com\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>danielmarhan+test2@gmail.com</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>0mqwdm</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:22:00","","2015-03-30 18:21:46","1","2015-03-30 18:22:00","","","");
INSERT INTO `emails`  VALUES ( "43","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"danielmarhan+test3@gmail.com\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://localhost","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://localhost. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"P?ihl�en�\">http://localhost/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>danielmarhan+test3@gmail.com</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>w9w76l</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:27:01","","2015-03-30 18:26:18","1","2015-03-30 18:27:01","","","");
INSERT INTO `emails`  VALUES ( "44","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"tomas.husa@terragroup.cz\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://demo.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://demo.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>tomas.husa@terragroup.cz</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>qq7nv9</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:47:07","","2015-03-30 18:47:01","20","2015-03-30 18:47:07","","","");
INSERT INTO `emails`  VALUES ( "45","","Str�nkyRealitky.Cz","info@strankyrealitky.cz","[{\"email\":\"manager@soulcox.com\",\"name\":\"\"}]","","","Vytvo?en u�ivatelsk� �?et na http://demo.strankyrealitky.cz","<h1>\n	Vytvo?en u�ivatelsk� �?et\n</h1>\n<p>\n	Byl V�m vytvo?en u�ivatelsk� �?et na realitn�ch str�nk�ch http://demo.strankyrealitky.cz. P?ihl�sit se m?�ete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"P?ihl�en�\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	P?ihla�ovac� email: <strong>manager@soulcox.com</strong><br>\n	Do?asn� p?ihla�ovac� heslo: <strong>dn1d7z</strong><br>\n	<em>Do?asn� heslo doporu?ujeme ihned po p?ihl�en� zm?nit</em>\n</p>","","2015-03-30 18:51:00","","2015-03-30 18:50:12","20","2015-03-30 18:51:00","","","");
INSERT INTO `emails`  VALUES ( "46","","tom� husa","sallam@centrum.cz","","","","Dotaz z webov�ch str�nek","qw�peijfeorihjgoidfwjg�psj g jfdpj g�pwedj�gfpjsdfmg","","2015-04-07 15:47:01","","2015-04-07 15:46:56","","2015-04-07 15:47:01","","","");
INSERT INTO `emails`  VALUES ( "47","","fdgfhgfhf","sallam@centrum.cz","","","","Dotaz z webov�ch str�nek","ohfphsdfjglsg","","2015-04-07 15:48:01","","2015-04-07 15:47:27","","2015-04-07 15:48:01","","","");


--
-- Tabel structure for table `estates`
--
DROP TABLE  IF EXISTS `estates`;
CREATE TABLE `estates` (
  `estate_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `advert_function` tinyint(4) DEFAULT NULL,
  `advert_lifetime` tinyint(4) DEFAULT NULL,
  `advert_price` double DEFAULT NULL,
  `advert_price_original` double DEFAULT NULL,
  `advert_price_currency` tinyint(4) DEFAULT NULL,
  `advert_price_unit` tinyint(4) DEFAULT NULL,
  `advert_type` tinyint(4) DEFAULT NULL,
  `locality_city` varchar(255) DEFAULT NULL,
  `locality_zip` varchar(255) DEFAULT NULL,
  `locality_inaccuracy_level` int(11) DEFAULT NULL,
  `advert_id` int(11) DEFAULT NULL,
  `advert_rkid` varchar(255) DEFAULT NULL,
  `advert_room_count` tinyint(4) DEFAULT NULL,
  `advert_subtype` tinyint(4) DEFAULT NULL,
  `balcony` tinyint(4) DEFAULT NULL,
  `basin` tinyint(4) DEFAULT NULL,
  `building_condition` tinyint(4) DEFAULT NULL,
  `building_type` tinyint(4) DEFAULT NULL,
  `cellar` tinyint(4) DEFAULT NULL,
  `estate_area` int(11) DEFAULT NULL,
  `floor_number` int(11) DEFAULT NULL,
  `garage` tinyint(4) DEFAULT NULL,
  `locality_latitude` double DEFAULT NULL,
  `locality_longitude` double DEFAULT NULL,
  `locality_ruian` int(11) DEFAULT NULL,
  `locality_ruian_level` int(11) DEFAULT NULL,
  `locality_uir` int(11) DEFAULT NULL,
  `locality_uir_level` int(11) DEFAULT NULL,
  `loggia` tinyint(4) DEFAULT NULL,
  `object_type` tinyint(4) DEFAULT NULL,
  `ownership` tinyint(4) DEFAULT NULL,
  `parking_lots` tinyint(4) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_rkid` varchar(255) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `seller_rkid` varchar(255) DEFAULT NULL,
  `terrace` tinyint(4) DEFAULT NULL,
  `usable_area` int(11) DEFAULT NULL,
  `acceptance_year` int(11) DEFAULT NULL,
  `advert_code` varchar(255) DEFAULT NULL,
  `advert_low_energy` tinyint(4) DEFAULT NULL,
  `advert_price_charge` tinyint(4) DEFAULT NULL,
  `advert_price_commission` tinyint(4) DEFAULT NULL,
  `advert_price_legal_services` tinyint(4) DEFAULT NULL,
  `advert_price_negotiation` tinyint(4) DEFAULT NULL,
  `advert_price_vat` tinyint(4) DEFAULT NULL,
  `annuity` int(11) DEFAULT NULL,
  `auction_advertisement_pdf` text,
  `auction_date` datetime DEFAULT NULL,
  `auction_date_tour` datetime DEFAULT NULL,
  `auction_date_tour2` datetime DEFAULT NULL,
  `auction_kind` tinyint(4) DEFAULT NULL,
  `auction_place` varchar(255) DEFAULT NULL,
  `auction_review_pdf` text,
  `balcony_area` int(11) DEFAULT NULL,
  `basin_area` int(11) DEFAULT NULL,
  `beginning_date` date DEFAULT NULL,
  `building_area` int(11) DEFAULT NULL,
  `ceiling_height` double DEFAULT NULL,
  `cellar_area` int(11) DEFAULT NULL,
  `cost_of_living` varchar(255) DEFAULT NULL,
  `easy_access` tinyint(4) DEFAULT NULL,
  `electricity` text,
  `elevator` tinyint(4) DEFAULT NULL,
  `energy_efficiency_rating` tinyint(4) DEFAULT NULL,
  `energy_performance_attachment` text,
  `energy_performance_certificate` tinyint(4) DEFAULT NULL,
  `energy_performance_summary` double DEFAULT NULL,
  `extra_info` tinyint(4) DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `first_tour_date` datetime DEFAULT NULL,
  `first_tour_date_to` datetime DEFAULT NULL,
  `flat_class` tinyint(4) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `floors` int(11) DEFAULT NULL,
  `furnished` tinyint(4) DEFAULT NULL,
  `garage_count` int(11) DEFAULT NULL,
  `garden_area` int(11) DEFAULT NULL,
  `garret` tinyint(4) DEFAULT NULL,
  `gas` text,
  `gully` text,
  `heating` text,
  `locality_citypart` varchar(255) DEFAULT NULL,
  `locality_co` varchar(255) DEFAULT NULL,
  `locality_cp` varchar(255) DEFAULT NULL,
  `locality_street` varchar(255) DEFAULT NULL,
  `loggia_area` int(11) DEFAULT NULL,
  `mortgage` tinyint(4) DEFAULT NULL,
  `mortgage_percent` double DEFAULT NULL,
  `nolive_total_area` int(11) DEFAULT NULL,
  `object_age` int(11) DEFAULT NULL,
  `object_kind` tinyint(4) DEFAULT NULL,
  `object_location` tinyint(4) DEFAULT NULL,
  `offices_area` int(11) DEFAULT NULL,
  `parking` int(11) DEFAULT NULL,
  `personal` tinyint(4) DEFAULT NULL,
  `price_auction_principal` double DEFAULT NULL,
  `price_expert_report` double DEFAULT NULL,
  `price_minimum_bid` double DEFAULT NULL,
  `production_area` int(11) DEFAULT NULL,
  `protection` tinyint(4) DEFAULT NULL,
  `ready_date` date DEFAULT NULL,
  `reconstruction_year` int(11) DEFAULT NULL,
  `road_type` text,
  `sale_date` date DEFAULT NULL,
  `shop_area` int(11) DEFAULT NULL,
  `spor_percent` double DEFAULT NULL,
  `steps` varchar(255) DEFAULT NULL,
  `store_area` int(11) DEFAULT NULL,
  `surroundings_type` tinyint(4) DEFAULT NULL,
  `telecommunication` text,
  `terrace_area` int(11) DEFAULT NULL,
  `transport` text,
  `underground_floors` int(11) DEFAULT NULL,
  `usable_area_ground` int(11) DEFAULT NULL,
  `user_status` tinyint(4) DEFAULT NULL,
  `water` text,
  `workshop_area` int(11) DEFAULT NULL,
  `youtube_video` varchar(255) DEFAULT NULL,
  `advert_status` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `advert_price_hidden` tinyint(1) NOT NULL,
  `locality_district` varchar(255) DEFAULT NULL,
  `id_kraj` int(11) DEFAULT NULL,
  `id_okres` int(11) DEFAULT NULL,
  `id_obec` int(11) DEFAULT NULL,
  `kraj` varchar(255) DEFAULT NULL,
  `okres` varchar(255) DEFAULT NULL,
  `advert_onhomepage` tinyint(1) NOT NULL,
  `estate_pdf_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `estates`  VALUES ( "1","1","11","","1","","2500000","19000","1","1","2","Kotou?ov","25163","1","","","1","37","","","1","1","","","3","1","42.8412344","73.9872382","","","","","","1","1","","","","","","","80","","56","","1","1","1","","1","3333125","1419937407.pdf","","","","1","","","","","","","","","","1","[2]","1","1","","1","","1","","","","1","","","1","1","","","[2]","[3]","[8]","","","1","Kotoucov","","1","","","","4","1","","","1","","","","","1","","","[3]","","","10555","","","1","[1,2,3,4,5]","","","","","","[2]","","mr2_wjcyK7s","1","2014-12-30 12:00:53","0","2015-01-20 09:30:41","9","2015-01-20 09:30:48","12","0","","","","","","","0","");
INSERT INTO `estates`  VALUES ( "2","1","11","","2","","19000","","1","2","1","Praha","140000","","","","1","6","","","1","1","","","","","50.0444343","14.4354763","","","","","","1","1","","","","","","","80","","","","2","1","1","","1","","","","","","1","","","","","","","","","","1","","1","1","","1","","","","","","1","","","1","","","","","","","","","123","Jeremenkova","","","","","","1","1","","","1","","","","","1","","","","","","","","","1","","","","","","","","","","1","2015-01-11 20:30:01","0","2015-01-11 22:04:40","1","2015-01-19 22:06:50","9","0","","","","","","","0","");
INSERT INTO `estates`  VALUES ( "3","1","15","","2","1","21000","19000","1","1","1","Praha 4","14700","1","","","1","6","","","1","2","1","","2","","50.0466835","14.4160549","","","","","1","1","1","1","","","","","","80","","1","","2","1","1","","1","","","","","","1","","","","","","","","","","2","[2]","1","","","1","","","","","","","","","1","","","","[2]","[1]","[7]","","7","920","","","","","","","1","1","","","1","","","","","1","","","[3]","","","","","","1","[1,2,4,5]","","[1,2,3,4,5]","","","","[2]","","","1","2015-01-20 10:25:51","11","2015-04-09 09:27:14","9","","","0","Podol�","19","19","554782","Hlavn� m?sto Praha","Hlavn� m?sto Praha","0","");
INSERT INTO `estates`  VALUES ( "4","1","15","","1","1","1690000","1790000","1","1","2","Bohdane?","28522","1","","","4","37","","1","1","7","1","2205","","","49.7702564","15.231228","","","","","","1","1","","","","","","","100","","2","","1","2","1","","1","","","","","","1","","","","","","","","","","1","[2,4]","2","3","","1","","","","","","1","","","1","","","","","[2]","[5]","","","1","Kotou?ov","","","","","","4","2","","","1","","","","","1","","","[3]","","","","","","1","[1,2,3]","","[1,2,3,5]","","","","[1]","","","1","2015-01-20 11:36:35","11","2015-04-03 12:10:03","9","","","0","","27","3205","533980","St?edo?esk� kraj","Kutn� Hora","1","");
INSERT INTO `estates`  VALUES ( "5","1","15","","1","1","2300000","1900000","1","1","2","�n?�ov","330 38","1","","","5","37","","","1","2","1","3004","","1","49.9185347942","13.1118383831","","","","","","2","1","","","","","","","250","","3","","1","2","1","","1","","","","","","1","","","","","","","","","","2","[2,4]","2","5","","1","","","","","","1","","","1","","","","","[3]","[5]","","","3","Vojt?��n","","1","","","","4","2","","","1","","","","","1","","","","","","","","","1","[1,2]","","[3,5]","","","","[1]","","","1","2015-01-20 20:53:21","11","2015-04-03 12:11:46","9","","","0","","43","3407","559563","Plze?sk� kraj","Plze?-sever","1","");
INSERT INTO `estates`  VALUES ( "6","1","15","","1","1","1200000","","1","1","2","Pit�n","68771","1","","","3","37","","","1","7","","16595","","","49.0367214","17.8559227","","","","","","1","1","1","","","","","","95","","4","","1","2","1","","1","","","","","","1","","","","","","","","","","1","","2","3","","1","","","","","","1","","","1","","","","","","","","","125","Pit�n","","1","","","","4","1","","","1","","","","","1","","","","","","","","","1","","","","","","","","","","1","2015-01-20 21:04:32","11","2015-03-22 10:29:01","9","","","0","","141","3711","592498","Zl�nsk� kraj","Uhersk� Hradi�t?","0","");
INSERT INTO `estates`  VALUES ( "7","1","15","","1","1","12000000","1","1","1","1","Praha","16200","1","","","1","16","","","1","2","","","4","","50.0949115","14.3727936","","","","","","1","1","1","","","","","","120","","5","","1","2","1","","1","","","","","","1","","","","","","","","","","1","[2,4]","1","1","","1","","","1970-01-01","","","1","","","1","","850","","[2]","[1]","[1]","","","77","Na O?echovce","","1","","","","1","1","","","1","","","","","1","","","[3]","","","","","","1","[1,2,3,4,5]","","[1,2,3,4,5]","","","","[2]","","","1","2015-01-21 10:27:56","11","2015-04-03 12:13:18","9","","","0","","19","19","554782","Hlavn� m?sto Praha","Hlavn� m?sto Praha","0","");
INSERT INTO `estates`  VALUES ( "8","1","15","","1","1","1500000","","1","1","2","Kn?�most","29402","1","","","4","33","","","1","1","","386","","1","50.4917852","15.0741517","","","","","","2","1","","","","","","1","86","","6","","1","2","1","","1","","","","","","1","","","","","","46","","","","2","[2,4]","2","","","1","","","","","","1","86","","1","","340","","","[3]","[2]","","","82","Drhleny","","","","","","4","4","","","1","","","","","1","","","[4]","","","","","","1","","","[3,5]","","","","[1]","","","2","2015-01-21 17:09:57","11","2015-04-09 07:49:40","9","","","0","","27","3207","536041","St?edo?esk� kraj","Mlad� Boleslav","0","");
INSERT INTO `estates`  VALUES ( "9","1","15","","1","1","120000","","1","1","3","Nov� Ves","27752","1","","","1","23","","","1","1","","1231","","","50.364284","13.6688222","","","","","","1","1","","","","","","","","","7","","2","2","2","","2","","","","","","1","","","","","","","","","","1","","1","1","","1","","","","","","1","","","1","","","","","","","","","7","Nov� Ves","","","","","","1","1","","","1","","","","","1","","","","","","","","","1","","","","","","","","","","1","2015-01-22 21:57:23","11","2015-03-22 19:43:12","9","","","0","","27","3206","535117","St?edo?esk� kraj","M?ln�k","0","");
INSERT INTO `estates`  VALUES ( "10","1","11","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-02-20 09:57:57","11","2015-02-21 09:10:12","","2015-02-21 09:10:12","9","0","","","","","","","0","");
INSERT INTO `estates`  VALUES ( "11","1","11","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-02-21 08:30:18","11","2015-02-21 09:10:19","","2015-02-21 09:10:19","9","0","","","","","","","0","");
INSERT INTO `estates`  VALUES ( "12","1","15","","1","1","8500000","6500000","1","1","2","Skorkov","25901","1","","","5","39","","","6","1","","1133","","1","50.2086100745","14.745390255","","","","","","2","1","","","","","","1","151","","8","1","1","2","1","","1","","","","","","1","","","","","","","","","","1","[2,4]","2","","","1","","","","","","","","2","1","","","","[1]","[1]","[6,8]","","","84","","","1","","","","4","4","","","1","","","","","","","","[3]","","","","","","1","[1,2,3,4,5]","","[1,2,3,4,5]","","","","[1]","","","1","2015-02-22 21:57:59","15","2015-03-22 10:29:41","9","","","0","Otradovice","27","3201","530905","St?edo?esk� kraj","Bene�ov","0","");
INSERT INTO `estates`  VALUES ( "13","2","19","","1","1","890000","","1","1","1","Neratovice","27711","1","","","1","9","","","2","5","1","","2","1","50.2555993896","14.5129071698","","","","","1","1","1","1","1","","","","","75","","PB001","","1","1","1","","1","","","","","","1","","","","","","","3","2","","1","[2]","1","","","1","","","","","","","","4","1","1","","1","[2]","[1]","[4]","","","918","28. ?�jna","10","","","","","1","1","","2","1","","","","","","","","[3]","","","","","","1","[2,4]","","[1,3,4,5]","","","","[2]","","ef05D0RnOlI","1","2015-03-10 15:30:10","19","2015-04-24 15:20:29","21","","","0","","27","3206","535087","St?edo?esk� kraj","M?ln�k","0","/theme/www-demo-cz/files/estates_pdf/prodej-bytu-4-1-neratovice.pdf");
INSERT INTO `estates`  VALUES ( "14","2","19","","1","1","3800000","","1","1","2","Neratovice","27711","1","","","5","37","","1","6","2","","1200","","1","50.2590012","14.5063665","","","","","","2","1","","2","","","","1","150","","PD001","1","1","1","1","1","1","","","","","","1","","","","12","","75","","","","2","[2]","2","1","","1","","","","","","","70","1","1","2","1000","","[2]","[1]","[2]","Neratovice","","1294","Poln�","","1","","","","4","2","","","1","","","","","","","","[2]","","","","","","1","[2,3,4]","30","","1","","","[2]","","","1","2015-03-10 16:19:42","19","2015-04-29 17:23:48","21","","","0","","27","3206","535087","St?edo?esk� kraj","M?ln�k","1","");
INSERT INTO `estates`  VALUES ( "15","2","19","","2","1","19000","","1","2","4","Neratovice","27711","1","","","1","25","","","1","2","","2000","1","","50.2543394","14.5057423","","","","","","1","1","1","2","","","","","100","","NK001","","1","1","1","","1","","","","","","1","","","","","","","","","","1","[2,4]","2","5","","1","","","","","","","","1","1","","","","","[1,3]","[3]","","","1362","U Z�vor","","","","","","4","2","100","20","1","","","","","","","","[3]","","","","","","1","[2]","","","","","","[2]","","","1","2015-03-10 16:30:42","19","2015-03-30 20:56:25","19","","","0","","27","3206","535087","St?edo?esk� kraj","M?ln�k","1","");
INSERT INTO `estates`  VALUES ( "16","2","19","","1","1","123500000","","1","1","3","Neratovice","27711","3","","","1","19","","","1","1","","2000000","","","50.2592611","14.5176026","","","","","","1","1","","1","","","","","","","PP001","","1","1","1","","1","","","","","","1","","","","","","","","","","1","","1","","","1","","","","","","","","","1","","","","","","","","","1468","","","","","","","1","7","","","1","","","","","","","","","","","","","","6","","","","","","","","","","1","2015-03-10 16:35:50","19","2015-04-28 15:59:07","21","","","0","","27","3206","535087","St?edo?esk� kraj","M?ln�k","1","s");
INSERT INTO `estates`  VALUES ( "17","2","19","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-04-08 16:44:03","19","2015-04-08 16:45:27","","2015-04-08 16:45:27","20","0","","","","","","","0","");
INSERT INTO `estates`  VALUES ( "18","2","19","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","0","2015-04-13 15:11:26","19","","","","","0","","","","","","","0","");


--
-- Tabel structure for table `estates_cs`
--
DROP TABLE  IF EXISTS `estates_cs`;
CREATE TABLE `estates_cs` (
  `estate_id` int(11) NOT NULL,
  `estate_title` varchar(255) DEFAULT NULL,
  `estate_description` text,
  `advert_price_text_note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `estates_cs`  VALUES ( "1","Byt, 1+kk, osobn� vlastnictv�","Exkluzivn? nab�z�me k prodeji rodinn� d?m v malebn� vesni?ce Kotou?ov, okres Kutn� Hora. D?m je po kompletn� rekonstrukci, chyb� pouze fas�da. Skl�d� se z obytn� ?�sti (cca 100 m2) a z hospod�?sk� ?�sti � chl�vy (cca 100 m2). Obytnou plochu lze zv?t�it dobudov�n�m p?dn� vestavby (projekt je k dispozici). Obytn� ?�st je postavena z cihel, hospod�?sk� ?�st je postavena ze sm�en�ho zdiva z cihel a kamene. Na cel� budov? je nov� st?echa z roku 2007. V obytn� ?�sti jsou nov� �paletov� okna, nov� �st?edn� topen�, kotel se z�sobn�kem na uhl� (o?ech 2), nov� el. bojler, kter� je sou?asn? napojen na oh?ev z kotle. Obytnou ?�st tvo?� veranda, p?eds�?, kuchy? a ob�vac� pokoj, 2 lo�nice, koupelna a toaleta zvl�?. Odpady jsou svedeny do vlastn� ?OV. Pitn� voda je ?erp�na z vlastn� studny.\nNa pozemku se nach�z� velk� stodola o v�m??e cca 200 m2 se sklepem cca 40 m2. K objektu n�le�� pozemek o velikosti 2205 m2. K relaxaci slou�� baz�n a p?edzahr�dka s dom�c� ud�rnou a p?�jemn�m posezen�m.","Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `estates_cs`  VALUES ( "2","","Kr�sn� byt k pron�jmu 3+kk o velikosti 80 m2 s lod�i�, kter� pro�el n�kladnou rekonstrukc� se nach�z� ve druh�m pat?e cihlov� budovy v ulici Jeremenkova, v reziden?n� ?tvrti Podol�. Byt je velice �tuln� a luxusn? vybaven�. Kuchy?sk� linka se v�emi spot?ebi?i. V okol� naleznete ve�kerou ob?anskou vybavenost, lokalita pln� zelen?, park? a m�st k relaxaci. Bezprobl�mov� dopravn� dostupnost do centra m?sta, zast�vka tramvaje minutu ch?ze. V bl�zkosti bytu je mnoho sportovn�ho vy�it� � golfov� h?i�t?, �lut� l�zn?, cyklostezka apod. M?s�?n� n�jem ?in� 19tis. Poplatky za slu�by ?in� 500,- K?/osobu m?s�?n?, elekt?ina a plyn se p?ev�d� na n�jemce. Preferujeme dlouhodob� pron�jem.","Poplatky za slu�by ?in� 500,- K?/osobu m?s�?n?");
INSERT INTO `estates_cs`  VALUES ( "3","Pron�jem bytu 3+kk, Praha","Kr�sn� byt k pron�jmu 3+kk o velikosti 80 m2 s lod�i�, kter� pro�el n�kladnou rekonstrukc� se nach�z� ve druh�m pat?e cihlov� budovy v ulici Jeremenkova, v reziden?n� ?tvrti Podol�. Byt je velice �tuln� a luxusn? vybaven�. Kuchy?sk� linka se v�emi spot?ebi?i. V okol� naleznete ve�kerou ob?anskou vybavenost, lokalita pln� zelen?, park? a m�st k relaxaci. Bezprobl�mov� dopravn� dostupnost do centra m?sta, zast�vka tramvaje minutu ch?ze. V bl�zkosti bytu je mnoho sportovn�ho vy�it� � golfov� h?i�t?, �lut� l�zn?, cyklostezka apod. M?s�?n� n�jem ?in� 21tis. Poplatky za slu�by ?in� 500,- K?/osobu m?s�?n?, elekt?ina a plyn se p?ev�d� na n�jemce. Preferujeme dlouhodob� pron�jem.\nV p?�pad? jak�chkoliv dotaz? m? nev�hejte kontaktovat.\nWe speak english.\n\n","");
INSERT INTO `estates_cs`  VALUES ( "4","Rodinn� d?m, Kotou?ov","Exkluzivn? nab�z�me k prodeji rodinn� d?m v malebn� vesni?ce Kotou?ov, okres Kutn� Hora. D?m je po kompletn� rekonstrukci, chyb� pouze fas�da. Skl�d� se z obytn� ?�sti (cca 100 m2) a z hospod�?sk� ?�sti � chl�vy (cca 100 m2). Obytnou plochu lze zv?t�it dobudov�n�m p?dn� vestavby (projekt je k dispozici). Obytn� ?�st je postavena z cihel, hospod�?sk� ?�st je postavena ze sm�en�ho zdiva z cihel a kamene. Na cel� budov? je nov� st?echa z roku 2007. V obytn� ?�sti jsou nov� �paletov� okna, nov� �st?edn� topen�, kotel se z�sobn�kem na uhl� (o?ech 2), nov� el. bojler, kter� je sou?asn? napojen na oh?ev z kotle. Obytnou ?�st tvo?� veranda, p?eds�?, kuchy? a ob�vac� pokoj, 2 lo�nice, koupelna a toaleta zvl�?. Odpady jsou svedeny do vlastn� ?OV. Pitn� voda je ?erp�na z vlastn� studny.\nNa pozemku se nach�z� velk� stodola o v�m??e cca 200 m2 se sklepem cca 40 m2. K objektu n�le�� pozemek o velikosti 2205 m2. K relaxaci slou�� baz�n a p?edzahr�dka s dom�c� ud�rnou a p?�jemn�m posezen�m.\nV p?�pad? z�jmu m? nev�hejte kontaktovat, prohl�dka je mo�n� kdykoliv.","");
INSERT INTO `estates_cs`  VALUES ( "5","Rodinn� d?m, Vojt?��n","Exkluzivn? nab�z�me k prodeji prostorn� patrov� z?�sti podsklepen� rodinn� d?m se zastav?nou plochou 250m2 nach�zej�c� se na velmi kr�sn�m, klidn�m m�st? v bezprost?edn� bl�zkosti lesa v obci Vojt?��n (Plze?-sever). D?m je vystav?n na vlastn�m rozlehl�m pozemku 3004m2 (z?�sti slou�� jako v�b?h pro kon?, druh� ?�st zahrady k relaxaci se vzrostl�mi ovocn�mi a okrasn�mi stromy) domluven odkup p?�padn? pron�jem dal��ch 1000m2. Sou?�st� objektu jsou hospod�?sk� budovy. Stavba je napojena na elektrickou energii, kvalitn� voda z vlastn� studny. Moment�ln? prob�h� kompletn� n�kladn� rekonstrukce, voda, kanalizace, elektro, nov� d?ev?n� eurookna, nov� d?ev?n� dve?e, rozvody �st?edn�ho topen� - nov� kotel Viadrus na tuh� paliva s akumula?n� n�dobou 1000l, krbov� kamna, nov� fas�da - kontaktn� zateplovac� syst�m, koupelna s rohovou vanou 150x150, vybaven� Hansgrohe, Villeroy & Boch, v p?�zem� 4 m�stnosti, v 1.pat?e p?�prava pro v�stavbu 4 m�stnost�, prostorn� p?da. Internet 4G. Okoln� z�stavbu tvo?� rodinn� domy a chalupy se skv?l�mi sousedy, kte?� si vz�jemn? pom�haj� - v dne�n� dob? vz�cn�. Vojt?��n je v ojedin?lou o�zou klidu a pohody. Ve�ker� ob?ansk� vybavenost je v dosahu, obec �n?�ov 7km, Plze? 25km. Jedn� se skute?n? o kr�sn� m�sto s malebnou p?�rodou, vhodn� k rekreaci, houba?en�, cyklistice, turistice. Mo�no financovat hypot�kou. \nV p?�pad? jak�chkoliv dotaz? m? nev�hejte kontaktovat.","");
INSERT INTO `estates_cs`  VALUES ( "6","Prodej rodinn�ho domu Pit�n","Exkluzivn? nab�z�me k prodeji p?�zemn� rodinn� d?m 3+1 s p?�slu�enstv�m, navazuj�c� k?lnou, stodolou a p?dou. Sou?�st� domu je vstupn� hala, komora, kuchy?, dva pokoje, koupelna a samostatn� toaleta. K domu p?�mo p?�slu�� n�dvo?� a ovocn� zahrada.\nD?m je napojen na ve?ejn� vodovod, kanalizaci, elekt?inu a plyn.\nCelkov� u�itn� plocha domu je 95 m2. Z n�dvo?�  119 m2 je p?�stupn� k?lna a stodola s p?dou. Na n�dvo?� navazuje zahrada s ovocn�mi stromy o v�m??e 591 m2. K domu d�le n�le�� pozemky (pole a lesy) o celkov� v�m??e 16 595 m2. \nD?m je um�st?n v klidn� ?�sti obce s ve�kerou ob?anskou vybavenost� � mate?sk� �kola, z�kladn� �kola, knihovna, h?i�t?, tenisov� kurty, koupali�t?, obchody, restaurace.\nD?m je ihned k dispozici, mo�nost prohl�dky.\n\n","");
INSERT INTO `estates_cs`  VALUES ( "7","Prodej mezonetov�ho bytu Praha 6","Exkluzivn? nab�z�me k prodeji p?dn� mezonetov� byt o celkov� plo�e 120m�, na lukrativn�m m�st? v Praze 6 na O?echovce. Designov� mezonetov� apartm�n situovan� v 3. a 4.NP, kter� vznikne v p?dn� vestavb? kompletn? zrekonstruovan�ho prvorepublikov�ho domu.\n\nDispozice prvn� �rovn? nab�z� otev?en� a vzdu�n� koncept ob�vac�ho pokoje s kuchyn�, prostorem pro j�deln� st?l a schodi�t?m vedouc�m na galerii, centr�ln� halu, koupelnu a samostatnou toaletu. Druh� �rove? je pak tvo?ena lo�nic� a hlavn� koupelnou s vanou a toaletou.\nVysok� standard bytu odpov�d� sou?asn�m p?edstav�m o modern�m, nad?asov�m bydlen�, kter� vkusn? kombinuje p?vodn� architektonick� prvky a pln? uspokoj� i n�ro?n?j�� klienty. Vybaven� zahrnuje d?ev?n� palubkov� podlahy, ateli�rov�, st?e�n� okna, koupelnov� za?izovac� p?edm?ty Hansgrohe, Villeroy & Boch, klimatizaci, podlahov� vyt�p?n� v koupeln�ch, intercom. Pl�novan� dokon?en� 2015/2016.\n\nD?m stoj� v klidn�, slep� ulici, s mo�nost� u��v�n� vlastn� zahrady. V okol� se nach�z� mno�stv� zelen?, kav�rny, restaurace, �koly, �st?edn� vojensk� nemocnice. Parkovat lze bezprobl�mu p?ed domem.","");
INSERT INTO `estates_cs`  VALUES ( "8","Chata v ?esk�m r�ji","Exkluzivn? nab�z�me k prodeji kr�snou chatu ve vyhled�van� rekrea?n� oblasti Drhleny v ?esk�m r�ji. Dvoupatrov� zd?n� chata s prostornou terasou na jih, gar�� a zahradou v chatov� lokalit? na okraji lesa. Dispozice: ob�vac� pokoj se vstupem na terasu, kuchy?, 2x lo�nice, koupelna s vanou, WC. Prodej v?etn? ve�ker�ho vybaven�. Koup�n�, rybolov, cyklostezky, turistika.","");
INSERT INTO `estates_cs`  VALUES ( "9","Zahrada 1231m2","Exkluzivn? nab�z�me prodeji pozemek o celkov� plo�e 1231m2 veden� v katastru nemovitost� jako zahrada. ","");
INSERT INTO `estates_cs`  VALUES ( "10","","","");
INSERT INTO `estates_cs`  VALUES ( "11","","","");
INSERT INTO `estates_cs`  VALUES ( "12","Vila, Otradovice","Exkluzivn? nab�z�me k prodeji samostatn? stoj�c� n�zkoenergetickou vilu 6+1, v nadstandardn�m proveden�, zastav?n� plocha 151m2, pozemek 1133m2, gar�, v ��dan� lokalit?, 15minut severn? od Prahy. V p?�zem� hala s vestav?n�m n�bytkem, prostorn� ob�v�k spojen� s j�delnou, velkorys� kuchy? na m�ru, koupelna se sprchov�m koutem a toaletou, pracovna, technick� m�stnost � tepeln� ?erpadlo vzduch/voda, elektrokotel. V prvn�m pat?e lo�nice s �atnou, koupelnou se sprchov�m koutem, toaletou a vstupem na velkou ji�n� terasu, dva d?tsk� pokoje, koupelna s vanou a toaletou.\nD?m je postaven v nov� z�stavb? na rovinat�m pozemku v klidn� ?�sti obce na okraji lesa. Ide�ln� bydlen� v p?�rod? se skv?lou dostupnost� do centra Prahy. V m�st? ve�ker� ob?ansk� vybavenost. Zajist�me pro V�s v�hodn� financov�n�. Voln� ihned. \n","");
INSERT INTO `estates_cs`  VALUES ( "13","Prodej bytu 4+1, Neratovice","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.\n\nDEMO - Neexistuj�c� uk�zkov� nemovitost","");
INSERT INTO `estates_cs`  VALUES ( "14","Prodej rodinn�ho domu 7+2, Neratovice","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.","");
INSERT INTO `estates_cs`  VALUES ( "15","Kancel�?sk� prostory k pron�jmu","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.","cena zahrnuje i p?ipojen� k internetu");
INSERT INTO `estates_cs`  VALUES ( "16","Prodej �dol� 2 mil. m2","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.","");
INSERT INTO `estates_cs`  VALUES ( "17","","","");
INSERT INTO `estates_cs`  VALUES ( "18","","","");


--
-- Tabel structure for table `estates_en`
--
DROP TABLE  IF EXISTS `estates_en`;
CREATE TABLE `estates_en` (
  `estate_id` int(11) NOT NULL,
  `estate_title` varchar(255) DEFAULT NULL,
  `estate_description` text,
  `advert_price_text_note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `estates_en`  VALUES ( "1","dsf","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque molestie vel metus ac venenatis. Fusce finibus in velit et luctus. Suspendisse ac urna nulla. Vestibulum justo dui, fringilla in mauris ut, consectetur cursus neque. Suspendisse interdum justo arcu, rhoncus pellentesque mauris molestie in. Nulla neque velit, posuere at sem eu, ullamcorper faucibus nunc. Maecenas nec nisl eu odio convallis tincidunt et in purus.\n\nDonec sed metus sollicitudin, imperdiet tellus ac, elementum arcu. Integer a pretium metus, id finibus lacus. Etiam tellus leo, sodales et erat et, pharetra interdum quam. Etiam sed lacinia lacus. Quisque odio nisl, aliquet nec dui ut, ornare interdum ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et imperdiet lorem. Cras tincidunt posuere urna. Sed vitae purus libero. Sed at ex eu dolor porttitor eleifend. Etiam eget tortor id augue rutrum venenatis. Maecenas neque diam, efficitur eget accumsan nec, fringilla at tortor. Integer ut posuere arcu, non dignissim augue. Donec a quam efficitur, venenatis ipsum eget, consectetur tellus.","Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
INSERT INTO `estates_en`  VALUES ( "2","","","");


--
-- Tabel structure for table `export`
--
DROP TABLE  IF EXISTS `export`;
CREATE TABLE `export` (
  `estate_id` int(11) NOT NULL AUTO_INCREMENT,
  `images` timestamp NULL DEFAULT NULL,
  `images_error` varchar(255) DEFAULT NULL,
  `sreality` timestamp NULL DEFAULT NULL,
  `sreality_error` varchar(255) DEFAULT NULL,
  `idnes` timestamp NULL DEFAULT NULL,
  `idnes_error` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO `export`  VALUES ( "6","2015-03-22 10:30:03","","2015-03-22 10:30:08","","","","2015-02-14 10:52:41","1","2015-03-22 10:30:08","9","","");
INSERT INTO `export`  VALUES ( "7","2015-04-03 12:14:03","","2015-04-03 12:14:03","","2015-04-03 12:14:03","","2015-02-14 11:41:52","1","2015-04-03 12:14:03","9","","");
INSERT INTO `export`  VALUES ( "8","2015-04-09 07:50:02","","2015-04-09 07:50:05","","","","2015-02-14 11:41:53","1","2015-04-09 07:50:05","9","","");
INSERT INTO `export`  VALUES ( "9","2015-03-22 19:44:01","","2015-03-22 19:44:01","","2015-03-22 19:44:01","","2015-02-14 11:41:55","1","2015-03-22 19:44:01","9","","");
INSERT INTO `export`  VALUES ( "14","2015-03-30 19:02:00","","","","","","2015-03-10 16:28:17","19","2015-03-30 19:02:00","19","","");
INSERT INTO `export`  VALUES ( "4","2015-04-03 12:11:05","","2015-04-03 12:11:12","","","","2015-02-14 11:21:54","1","2015-04-03 12:11:12","9","","");
INSERT INTO `export`  VALUES ( "5","2015-04-03 12:12:03","","2015-04-03 12:12:08","","","","2015-02-14 11:41:28","1","2015-04-03 12:12:08","9","","");
INSERT INTO `export`  VALUES ( "15","2015-03-30 20:57:00","","","","","","2015-03-10 16:35:09","19","2015-03-30 20:57:00","19","","");
INSERT INTO `export`  VALUES ( "16","2015-04-13 12:42:01","","","","","","2015-03-10 16:54:57","19","2015-04-13 12:42:01","19","","");
INSERT INTO `export`  VALUES ( "3","2015-04-09 09:28:03","","2015-04-09 09:28:03","","2015-04-09 09:28:04","","2015-02-14 11:41:24","1","2015-04-09 09:28:04","9","","");
INSERT INTO `export`  VALUES ( "12","2015-03-22 10:32:04","","2015-03-22 10:32:04","","2015-03-22 10:32:05","","2015-02-22 22:02:57","15","2015-03-22 10:32:05","9","","");
INSERT INTO `export`  VALUES ( "13","","","","","","","2015-03-10 15:33:19","19","2015-04-16 12:24:42","21","","");


--
-- Tabel structure for table `export_brokers`
--
DROP TABLE  IF EXISTS `export_brokers`;
CREATE TABLE `export_brokers` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `export_timestamp` timestamp NULL DEFAULT NULL,
  `last_export_timestamp` timestamp NULL DEFAULT NULL,
  `message` text,
  `url` varchar(255) DEFAULT NULL,
  `export` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`export_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `export_brokers`  VALUES ( "2","19","1","2015-04-28 10:56:11","2015-04-28 10:56:11","200 - OK","","1","2015-04-21 12:21:41","1","2015-04-28 10:56:11","21","","");
INSERT INTO `export_brokers`  VALUES ( "4","19","3","","","","","1","2015-04-24 16:16:19","21","2015-04-28 10:55:39","21","","");
INSERT INTO `export_brokers`  VALUES ( "5","19","4","","2015-04-24 16:19:26","200 - OK","","1","2015-04-24 16:16:19","21","2015-04-28 10:55:39","21","","");


--
-- Tabel structure for table `export_portals`
--
DROP TABLE  IF EXISTS `export_portals`;
CREATE TABLE `export_portals` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `estate_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `export_timestamp` timestamp NULL DEFAULT NULL,
  `last_export_timestamp` timestamp NULL DEFAULT NULL,
  `message` text,
  `export` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`export_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO `export_portals`  VALUES ( "16","13","1","2015-03-14 16:42:20","2015-04-27 11:23:27","200 - OK","1","2015-04-16 12:55:53","21","2015-04-28 16:42:23","21","","","http://www.sreality.cz/detail/prodej/byt/4+1/neratovice-neratovice-28--rijna/4294705244");
INSERT INTO `export_portals`  VALUES ( "17","13","2","","2015-04-23 16:55:08","OK Nemovitost byla �sp?�n? aktualizov�na (id= 65504) / ?as zpracov�n� 0.207s POZOR! Detekovali jsme reimport a inzer�t vr�tili na p?vodn� m�sto ve v�pisu. Chcete-li se dostat na p?edn� pozice, pou�ijte HOPOV�N�. POZOR! N�sleduj�c� atributy vykazuj� nesrovnalosti oproti po�adavk?m: ( [makler_mobil] => �daj neodpov�d� tel. ?�slu mobiln�ho oper�tora ) ","1","2015-04-16 12:55:53","21","2015-04-24 15:20:30","21","","","http://rk.mobil.cz/detail/prodej/byt/4+1/neratovice-28-rijna/65504");
INSERT INTO `export_portals`  VALUES ( "18","14","1","","2015-04-28 17:34:23","200 - OK","0","2015-04-16 15:58:04","21","2015-04-29 17:23:48","21","","","http://www.sreality.cz/detail/prodej/dum/rodinny/neratovice-neratovice-polni/1845231708");
INSERT INTO `export_portals`  VALUES ( "19","14","2","","2015-04-23 16:55:15","OK Nemovitost byla �sp?�n? aktualizov�na (id= 65505) / ?as zpracov�n� 0.621s POZOR! N�sleduj�c� atributy vykazuj� nesrovnalosti oproti po�adavk?m: ( [makler_mobil] => �daj neodpov�d� tel. ?�slu mobiln�ho oper�tora ) ","0","2015-04-16 15:58:04","21","2015-04-29 17:23:49","21","","","http://rk.mobil.cz/detail/prodej/dum/samostatny/neratovice-polni/65505");
INSERT INTO `export_portals`  VALUES ( "22","13","4","2015-03-22 17:36:46","","","0","2015-04-24 15:20:12","21","2015-04-28 17:36:48","21","","","");
INSERT INTO `export_portals`  VALUES ( "23","14","4","","2015-04-28 17:41:41","200 - OK","0","2015-04-24 15:20:43","21","2015-04-29 17:23:49","21","","","http://www.realcity.cz/nemovitost/prodej-domu-70-m-neratovice-polni/3040152");
INSERT INTO `export_portals`  VALUES ( "24","16","1","","","","0","2015-04-28 14:51:19","21","2015-04-28 15:59:07","21","","","");
INSERT INTO `export_portals`  VALUES ( "25","16","2","","","","0","2015-04-28 14:51:19","21","2015-04-28 15:59:07","21","","","");
INSERT INTO `export_portals`  VALUES ( "26","16","3","2015-04-28 16:00:44","2015-04-28 16:00:44","200 - OK","0","2015-04-28 14:51:19","21","2015-04-28 16:00:44","21","","","");
INSERT INTO `export_portals`  VALUES ( "20","13","3","2015-03-14 10:23:52","","452 - Povinn� polo�ka building_area chyb� a nebo je pr�zdn�.","1","2015-04-21 17:05:26","1","2015-04-29 10:23:54","21","","","");
INSERT INTO `export_portals`  VALUES ( "21","14","3","","2015-04-29 12:01:03","200 - OK","0","2015-04-22 15:55:43","1","2015-04-29 17:23:49","21","","","nourl");
INSERT INTO `export_portals`  VALUES ( "27","16","4","","","","0","2015-04-28 14:51:19","21","2015-04-28 15:59:07","21","","","");


--
-- Tabel structure for table `files`
--
DROP TABLE  IF EXISTS `files`;
CREATE TABLE `files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `file_src` varchar(255) NOT NULL,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `file_rank` int(11) DEFAULT NULL,
  `file_main` tinyint(1) NOT NULL,
  `file_rel` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=utf8;

INSERT INTO `files`  VALUES ( "1","1","theme/www-belartis-cz/files/estates/1420992875.jpg","estates","1","2","1","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "2","1","theme/www-belartis-cz/files/estates/1420997523.JPG","estates","1","1","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "3","1","theme/www-belartis-cz/files/estates/1420997523_1.JPG","estates","1","2","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "4","1","theme/www-belartis-cz/files/estates/1420997523_2.JPG","estates","1","3","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "5","1","theme/www-belartis-cz/files/estates/1420997523_3.JPG","estates","1","4","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "6","1","theme/www-belartis-cz/files/estates/1420997523_4.JPG","estates","1","5","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "7","1","theme/www-belartis-cz/files/estates/1420997523_5.JPG","estates","1","6","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "8","1","theme/www-belartis-cz/files/estates/1420997523_6.JPG","estates","1","7","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "9","1","theme/www-belartis-cz/files/estates/1421000392.JPG","estates","1","8","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "10","1","theme/www-belartis-cz/files/estates/1421000392_1.JPG","estates","1","9","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "11","1","theme/www-belartis-cz/files/estates/1421000392_2.JPG","estates","1","10","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "12","1","theme/www-belartis-cz/files/estates/1421000392_3.JPG","estates","1","11","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "13","1","theme/www-belartis-cz/files/estates/1421000392_4.JPG","estates","1","12","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "14","1","theme/www-belartis-cz/files/estates/1421000392_5.JPG","estates","1","13","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "15","1","theme/www-belartis-cz/files/estates/1421000392_6.JPG","estates","1","14","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "16","1","theme/www-belartis-cz/files/estates/1421000392_7.JPG","estates","1","15","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "17","1","theme/www-belartis-cz/files/estates/1421000392_8.JPG","estates","1","16","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "18","1","theme/www-belartis-cz/files/estates/1421000392_9.JPG","estates","1","17","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "19","1","theme/www-belartis-cz/files/estates/1421000392_10.JPG","estates","1","18","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "20","1","theme/www-belartis-cz/files/estates/1421000392_11.JPG","estates","1","19","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "21","1","theme/www-belartis-cz/files/estates/1421000392_12.JPG","estates","1","20","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "22","1","theme/www-belartis-cz/files/estates/1421000392_13.JPG","estates","1","21","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "23","1","theme/www-belartis-cz/files/estates/1421000392_14.JPG","estates","1","22","0","images","2015-01-20 09:30:48","0","2015-02-12 12:29:57","9","2015-01-20 09:30:48","12");
INSERT INTO `files`  VALUES ( "24","1","theme/www-belartis-cz/files/estates/1421004931.JPG","estates","2","2","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "25","1","theme/www-belartis-cz/files/estates/1421004931_1.JPG","estates","2","3","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "26","1","theme/www-belartis-cz/files/estates/1421004931_2.jpg","estates","2","4","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "27","1","theme/www-belartis-cz/files/estates/1421004931_3.JPG","estates","2","5","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "28","1","theme/www-belartis-cz/files/estates/1421004931_4.JPG","estates","2","6","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "29","1","theme/www-belartis-cz/files/estates/1421004931_5.JPG","estates","2","7","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "30","1","theme/www-belartis-cz/files/estates/1421004931_6.jpg","estates","2","8","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "31","1","theme/www-belartis-cz/files/estates/1421004931_7.JPG","estates","2","9","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "32","1","theme/www-belartis-cz/files/estates/1421004931_8.JPG","estates","2","10","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "33","1","theme/www-belartis-cz/files/estates/1421004931_9.JPG","estates","2","11","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "34","1","theme/www-belartis-cz/files/estates/1421004931_10.JPG","estates","2","12","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "35","1","theme/www-belartis-cz/files/estates/1421004946.JPG","estates","2","13","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "36","1","theme/www-belartis-cz/files/estates/1421004946_1.JPG","estates","2","14","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "37","1","theme/www-belartis-cz/files/estates/1421004946_2.JPG","estates","2","15","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "38","1","theme/www-belartis-cz/files/estates/1421004946_3.JPG","estates","2","16","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "39","1","theme/www-belartis-cz/files/estates/1421004946_4.JPG","estates","2","17","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "40","1","theme/www-belartis-cz/files/estates/1421004946_5.JPG","estates","2","18","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "41","1","theme/www-belartis-cz/files/estates/1421004946_6.JPG","estates","2","19","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "42","1","theme/www-belartis-cz/files/estates/1421004946_7.JPG","estates","2","20","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "43","1","theme/www-belartis-cz/files/estates/1421004946_8.JPG","estates","2","21","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "44","1","theme/www-belartis-cz/files/estates/1421004946_9.JPG","estates","2","22","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "45","1","theme/www-belartis-cz/files/estates/1421004946_10.JPG","estates","2","23","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "46","1","theme/www-belartis-cz/files/estates/1421004946_11.JPG","estates","2","24","0","images","2015-01-19 22:06:50","0","2015-02-12 12:29:57","","2015-01-19 22:06:50","9");
INSERT INTO `files`  VALUES ( "47","1","theme/www-belartis-cz/files/estates/private/1421075060.png","users","2","0","0","profile_picture","2015-01-12 16:04:20","0","2015-02-12 12:29:57","","","");
INSERT INTO `files`  VALUES ( "48","1","theme/www-belartis-cz/files/brokers/1423087796.jpg","users_broker","11","1","0","portrait","2015-02-04 23:10:12","8","2015-02-20 15:17:12","1","","");
INSERT INTO `files`  VALUES ( "49","0","","","","","0","","2015-01-20 10:26:05","11","","","","");
INSERT INTO `files`  VALUES ( "50","0","theme/www-belartis-cz/files/estates/1421778599.JPG","estates","3","1","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "51","0","theme/www-belartis-cz/files/estates/1421778629.JPG","estates","3","2","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "52","0","theme/www-belartis-cz/files/estates/1421778643.JPG","estates","3","3","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "53","0","theme/www-belartis-cz/files/estates/1421778676.JPG","estates","3","4","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "54","0","theme/www-belartis-cz/files/estates/1421778704.JPG","estates","3","5","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "55","0","theme/www-belartis-cz/files/estates/1421778733.JPG","estates","3","6","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "56","0","theme/www-belartis-cz/files/estates/1421778763.JPG","estates","3","7","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "57","0","theme/www-belartis-cz/files/estates/1421778801.JPG","estates","3","8","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "58","0","theme/www-belartis-cz/files/estates/1421778823.JPG","estates","3","9","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","2015-04-09 09:27:14","9");
INSERT INTO `files`  VALUES ( "59","0","theme/www-belartis-cz/files/estates/1421783001.JPG","estates","4","1","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "60","0","theme/www-belartis-cz/files/estates/1421783016.JPG","estates","4","2","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "61","0","theme/www-belartis-cz/files/estates/1421783049.JPG","estates","4","3","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "62","0","theme/www-belartis-cz/files/estates/1421783099.JPG","estates","4","4","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "63","0","theme/www-belartis-cz/files/estates/1421783124.JPG","estates","4","5","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "64","0","theme/www-belartis-cz/files/estates/1421783142.JPG","estates","4","6","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "65","0","theme/www-belartis-cz/files/estates/1421783175.JPG","estates","4","7","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "66","0","theme/www-belartis-cz/files/estates/1421783196.JPG","estates","4","8","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "67","0","theme/www-belartis-cz/files/estates/1421783216.JPG","estates","4","9","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "68","0","theme/www-belartis-cz/files/estates/1421783235.JPG","estates","4","10","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "69","0","theme/www-belartis-cz/files/estates/1421783251.JPG","estates","4","11","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "70","0","theme/www-belartis-cz/files/estates/1421783267.JPG","estates","4","12","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "71","0","theme/www-belartis-cz/files/estates/1421783286.JPG","estates","4","13","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "72","0","theme/www-belartis-cz/files/estates/1421783302.JPG","estates","4","14","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","2015-04-03 12:10:03","9");
INSERT INTO `files`  VALUES ( "73","0","theme/www-belartis-cz/files/estates/1421783817.jpg","estates","5","1","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "74","0","theme/www-belartis-cz/files/estates/1421783848.JPG","estates","5","2","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "75","0","theme/www-belartis-cz/files/estates/1421783874.JPG","estates","5","3","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "76","0","theme/www-belartis-cz/files/estates/1421783901.JPG","estates","5","4","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "77","0","theme/www-belartis-cz/files/estates/1421783923.JPG","estates","5","5","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "78","0","theme/www-belartis-cz/files/estates/1421783959.JPG","estates","5","6","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "79","0","theme/www-belartis-cz/files/estates/1421783995.JPG","estates","5","7","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "80","0","theme/www-belartis-cz/files/estates/1421784043.JPG","estates","5","8","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "81","0","theme/www-belartis-cz/files/estates/1421784075.JPG","estates","5","9","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "82","0","theme/www-belartis-cz/files/estates/1421784108.JPG","estates","5","10","0","images","2015-01-28 12:58:26","11","2015-04-03 12:11:46","9","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "83","0","theme/www-belartis-cz/files/estates/1421831236.png","estates","6","1","0","images","2015-01-25 18:32:20","8","2015-03-22 10:29:01","","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "84","0","theme/www-belartis-cz/files/estates/1421833906.JPG","estates","7","1","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "85","0","theme/www-belartis-cz/files/estates/1421834290.jpg","estates","7","2","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "86","0","theme/www-belartis-cz/files/estates/1421834886.jpg","estates","7","3","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "87","0","theme/www-belartis-cz/files/estates/1421834942.jpg","estates","7","4","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "88","0","theme/www-belartis-cz/files/estates/1421838200.jpg","estates","7","5","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "89","0","theme/www-belartis-cz/files/estates/1421838229.jpg","estates","7","6","0","images","2015-01-25 18:44:17","11","2015-04-03 12:13:18","11","2015-04-03 12:13:18","9");
INSERT INTO `files`  VALUES ( "90","0","theme/www-belartis-cz/files/estates/1421838373.jpg","estates","6","1","0","images","2015-01-25 18:32:20","11","2015-03-22 10:29:01","","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "91","0","theme/www-belartis-cz/files/estates/1421838513.jpg","estates","6","1","0","images","2015-01-25 18:32:20","11","2015-03-22 10:29:01","12","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "92","0","theme/www-belartis-cz/files/estates/1421855124.jpg","estates","6","1","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "93","0","theme/www-belartis-cz/files/estates/1421855185.jpg","estates","6","2","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "94","0","theme/www-belartis-cz/files/estates/1421855202.jpg","estates","6","3","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "95","0","theme/www-belartis-cz/files/estates/1421855232.jpg","estates","6","4","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "96","0","theme/www-belartis-cz/files/estates/1421855247.jpg","estates","6","5","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "97","0","theme/www-belartis-cz/files/estates/1421855266.jpg","estates","6","6","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "98","0","theme/www-belartis-cz/files/estates/1421855300.jpg","estates","6","7","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "99","0","theme/www-belartis-cz/files/estates/1421855370.jpg","estates","6","8","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "100","0","theme/www-belartis-cz/files/estates/1421855318.jpg","estates","6","9","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "101","0","theme/www-belartis-cz/files/estates/1421855332.jpg","estates","6","10","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "102","0","theme/www-belartis-cz/files/estates/1421855391.jpg","estates","6","11","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","2015-03-22 10:29:01","9");
INSERT INTO `files`  VALUES ( "103","0","theme/www-belartis-cz/files/estates/1421858664.jpg","estates","8","1","0","images","2015-01-25 18:45:39","11","2015-04-09 07:49:40","9","2015-04-09 07:49:40","9");
INSERT INTO `files`  VALUES ( "104","0","theme/www-belartis-cz/files/estates/1421859179.JPG","estates","8","2","0","images","2015-01-25 18:45:39","11","2015-04-09 07:49:40","9","2015-04-09 07:49:40","9");
INSERT INTO `files`  VALUES ( "105","0","","","13","1","0","","2015-01-22 14:29:08","9","","","2015-01-22 14:29:08","9");
INSERT INTO `files`  VALUES ( "106","0","","","14","1","0","","2015-01-22 14:28:47","9","","","","");
INSERT INTO `files`  VALUES ( "107","0","","","13","1","0","","2015-01-22 14:29:08","9","","","","");
INSERT INTO `files`  VALUES ( "108","0","theme/www-belartis-cz/files/estates/1421939382.JPG","estates","5","11","1","images","2015-01-28 12:58:26","12","2015-04-03 12:11:46","","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "109","0","theme/www-belartis-cz/files/estates/1421960623.JPG","estates","9","1","0","images","2015-01-25 19:08:56","11","2015-03-22 19:43:12","9","2015-03-22 19:43:12","9");
INSERT INTO `files`  VALUES ( "110","0","theme/www-belartis-cz/files/estates/1421960652.JPG","estates","9","2","0","images","2015-01-25 19:08:56","11","2015-03-22 19:43:12","9","2015-03-22 19:43:12","9");
INSERT INTO `files`  VALUES ( "111","0","theme/www-belartis-cz/files/estates/1421960672.JPG","estates","9","1","0","images","2015-01-25 19:08:56","11","2015-03-22 19:43:12","9","2015-03-22 19:43:12","9");
INSERT INTO `files`  VALUES ( "112","0","theme/www-belartis-cz/files/estates/1421960698.JPG","estates","9","2","0","images","2015-01-25 19:08:56","11","2015-03-22 19:43:12","9","2015-03-22 19:43:12","9");
INSERT INTO `files`  VALUES ( "113","0","theme/www-belartis-cz/files/estates/1421960717.jpg","estates","9","3","0","images","2015-01-25 19:08:56","11","2015-03-22 19:43:12","9","2015-03-22 19:43:12","9");
INSERT INTO `files`  VALUES ( "114","1","theme/www-belartis-cz/files/brokers/1422994292.png","users_broker","13","1","0","portrait","2015-02-03 21:11:35","8","2015-02-20 15:17:09","1","","");
INSERT INTO `files`  VALUES ( "115","0","theme/www-belartis-cz/files/estates/1422204253.JPG","estates","3","1","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "116","0","theme/www-belartis-cz/files/estates/1422204398.JPG","estates","3","2","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "117","0","theme/www-belartis-cz/files/estates/1422204412.JPG","estates","3","3","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "118","0","theme/www-belartis-cz/files/estates/1422204421.JPG","estates","3","4","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "119","0","theme/www-belartis-cz/files/estates/1422204440.JPG","estates","3","5","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "120","0","theme/www-belartis-cz/files/estates/1422204447.JPG","estates","3","6","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "121","0","theme/www-belartis-cz/files/estates/1422204457.JPG","estates","3","7","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "122","0","theme/www-belartis-cz/files/estates/1422204490.JPG","estates","3","8","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "123","0","theme/www-belartis-cz/files/estates/1422204516.JPG","estates","3","9","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "124","0","theme/www-belartis-cz/files/estates/1422204534.JPG","estates","3","10","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "125","0","theme/www-belartis-cz/files/estates/1422204550.JPG","estates","3","11","0","images","2015-02-04 23:11:06","9","2015-04-09 09:27:14","9","","");
INSERT INTO `files`  VALUES ( "126","0","theme/www-belartis-cz/files/estates/1422205797.JPG","estates","4","1","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "127","0","theme/www-belartis-cz/files/estates/1422205797_1.JPG","estates","4","2","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "128","0","theme/www-belartis-cz/files/estates/1422205797_2.JPG","estates","4","3","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "129","0","theme/www-belartis-cz/files/estates/1422205797_3.JPG","estates","4","4","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "130","0","theme/www-belartis-cz/files/estates/1422205797_4.JPG","estates","4","5","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "131","0","theme/www-belartis-cz/files/estates/1422205797_5.JPG","estates","4","6","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "132","0","theme/www-belartis-cz/files/estates/1422205797_6.JPG","estates","4","7","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "133","0","theme/www-belartis-cz/files/estates/1422205797_7.JPG","estates","4","8","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "134","0","theme/www-belartis-cz/files/estates/1422205797_8.JPG","estates","4","9","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "135","0","theme/www-belartis-cz/files/estates/1422205797_9.JPG","estates","4","10","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "136","0","theme/www-belartis-cz/files/estates/1422205797_10.JPG","estates","4","11","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "137","0","theme/www-belartis-cz/files/estates/1422205797_11.JPG","estates","4","12","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "138","0","theme/www-belartis-cz/files/estates/1422205797_12.JPG","estates","4","13","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "139","0","theme/www-belartis-cz/files/estates/1422205797_13.JPG","estates","4","14","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "140","0","theme/www-belartis-cz/files/estates/1422205797_14.JPG","estates","4","15","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "141","0","theme/www-belartis-cz/files/estates/1422205797_15.JPG","estates","4","16","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "142","0","theme/www-belartis-cz/files/estates/1422205797_16.JPG","estates","4","17","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "143","0","theme/www-belartis-cz/files/estates/1422205797_17.JPG","estates","4","18","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "144","0","theme/www-belartis-cz/files/estates/1422205797_18.JPG","estates","4","19","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "145","0","theme/www-belartis-cz/files/estates/1422205797_19.JPG","estates","4","20","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "146","0","theme/www-belartis-cz/files/estates/1422205930.JPG","estates","4","21","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "147","0","theme/www-belartis-cz/files/estates/1422205930_1.JPG","estates","4","22","0","images","2015-01-28 12:58:49","9","2015-04-03 12:10:03","9","","");
INSERT INTO `files`  VALUES ( "148","0","theme/www-belartis-cz/files/estates/1422206056.JPG","estates","5","1","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "149","0","theme/www-belartis-cz/files/estates/1422206056_1.JPG","estates","5","15","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "150","0","theme/www-belartis-cz/files/estates/1422206056_2.JPG","estates","5","16","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "151","0","theme/www-belartis-cz/files/estates/1422206056_3.JPG","estates","5","3","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "152","0","theme/www-belartis-cz/files/estates/1422206056_4.JPG","estates","5","5","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "153","0","theme/www-belartis-cz/files/estates/1422206056_5.JPG","estates","5","6","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "154","0","theme/www-belartis-cz/files/estates/1422206056_6.JPG","estates","5","4","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "155","0","theme/www-belartis-cz/files/estates/1422206056_7.JPG","estates","5","5","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "156","0","theme/www-belartis-cz/files/estates/1422206056.jpg","estates","5","1","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","1","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "157","0","theme/www-belartis-cz/files/estates/1422206056_8.JPG","estates","5","2","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "158","0","theme/www-belartis-cz/files/estates/1422206056_9.JPG","estates","5","11","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","","2015-04-03 12:11:46","9");
INSERT INTO `files`  VALUES ( "159","0","theme/www-belartis-cz/files/estates/1422206928_3.JPG","estates","5","6","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "160","0","theme/www-belartis-cz/files/estates/1422206257.JPG","estates","5","7","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "161","0","theme/www-belartis-cz/files/estates/1422206257_4.JPG","estates","5","8","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "162","0","theme/www-belartis-cz/files/estates/1422206257_7.JPG","estates","5","9","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "163","0","theme/www-belartis-cz/files/estates/1422206257_8.JPG","estates","5","10","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "164","0","theme/www-belartis-cz/files/estates/1422206257_10.JPG","estates","5","11","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "165","0","theme/www-belartis-cz/files/estates/1422206257_11.JPG","estates","5","12","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "166","0","theme/www-belartis-cz/files/estates/1422206928.JPG","estates","5","13","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "167","0","theme/www-belartis-cz/files/estates/1422206928_1.JPG","estates","5","14","0","images","2015-01-28 12:58:26","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "168","0","theme/www-belartis-cz/files/estates/1422207053.jpg","estates","6","1","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "169","0","theme/www-belartis-cz/files/estates/1422207053_1.jpg","estates","6","2","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "170","0","theme/www-belartis-cz/files/estates/1422207053_2.jpg","estates","6","3","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "171","0","theme/www-belartis-cz/files/estates/1422207053_3.jpg","estates","6","4","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "172","0","theme/www-belartis-cz/files/estates/1422207053_4.jpg","estates","6","5","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "173","0","theme/www-belartis-cz/files/estates/1422207053_5.jpg","estates","6","6","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "174","0","theme/www-belartis-cz/files/estates/1422207053_6.jpg","estates","6","7","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "175","0","theme/www-belartis-cz/files/estates/1422207053_7.jpg","estates","6","8","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "176","0","theme/www-belartis-cz/files/estates/1422207053_8.jpg","estates","6","9","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "177","0","theme/www-belartis-cz/files/estates/1422207053_9.jpg","estates","6","10","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "178","0","theme/www-belartis-cz/files/estates/1422207053_10.jpg","estates","6","11","0","images","2015-01-25 18:32:20","9","2015-03-22 10:29:01","9","","");
INSERT INTO `files`  VALUES ( "179","0","theme/www-belartis-cz/files/estates/1422207844.JPG","estates","7","1","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "180","0","theme/www-belartis-cz/files/estates/1422207431.jpg","estates","7","2","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "181","0","theme/www-belartis-cz/files/estates/1422207431_1.jpg","estates","7","3","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "182","0","theme/www-belartis-cz/files/estates/1422207431_2.jpg","estates","7","4","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "183","0","theme/www-belartis-cz/files/estates/1422207431_3.jpg","estates","7","5","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "184","0","theme/www-belartis-cz/files/estates/1422207431_4.jpg","estates","7","6","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "185","0","theme/www-belartis-cz/files/estates/1422207431_5.jpg","estates","7","7","0","images","2015-01-25 18:44:17","9","2015-04-03 12:13:18","9","","");
INSERT INTO `files`  VALUES ( "187","0","theme/www-belartis-cz/files/estates/1422207928.JPG","estates","8","1","0","images","2015-01-25 18:45:39","9","2015-04-09 07:49:40","9","","");
INSERT INTO `files`  VALUES ( "188","0","theme/www-belartis-cz/files/estates/1422209187.JPG","estates","9","1","0","images","2015-01-25 19:08:56","9","2015-03-22 19:43:12","9","","");
INSERT INTO `files`  VALUES ( "189","0","theme/www-belartis-cz/files/estates/1422209234.JPG","estates","9","2","0","images","2015-01-25 19:08:56","9","2015-03-22 19:43:12","9","","");
INSERT INTO `files`  VALUES ( "190","0","theme/www-belartis-cz/files/estates/1422209256.JPG","estates","9","3","0","images","2015-01-25 19:08:56","9","2015-03-22 19:43:12","9","","");
INSERT INTO `files`  VALUES ( "191","0","theme/www-belartis-cz/files/estates/1422209281.JPG","estates","9","4","0","images","2015-01-25 19:08:56","9","2015-03-22 19:43:12","9","","");
INSERT INTO `files`  VALUES ( "192","1","theme/www-belartis-cz/files/brokers/1428130689.png","users_broker","15","1","0","portrait","2015-02-03 21:12:01","9","2015-04-04 08:58:13","9","","");
INSERT INTO `files`  VALUES ( "193","1","theme/www-belartis-cz/files/brokers/1422994303.png","users_broker","14","1","0","portrait","2015-02-03 21:11:46","9","2015-02-20 15:17:06","1","","");
INSERT INTO `files`  VALUES ( "194","0","","","16","1","0","","2015-01-28 17:29:02","9","2015-02-20 15:16:55","","2015-02-20 15:16:55","1");
INSERT INTO `files`  VALUES ( "195","0","","","17","1","0","","2015-02-04 16:14:12","9","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "196","0","","","17","1","0","","2015-02-10 16:44:36","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "197","0","","","17","1","0","","2015-02-10 18:12:17","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "198","0","theme/www-belartis-cz/files/estates/1423818570.jpg","estates","5","1","0","images","2015-02-13 10:09:55","9","2015-04-03 12:11:46","9","","");
INSERT INTO `files`  VALUES ( "199","0","","","18","1","0","","2015-02-13 10:17:06","9","","","","");
INSERT INTO `files`  VALUES ( "200","0","","","17","1","0","","2015-02-14 10:30:17","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "201","0","","","16","1","0","","2015-02-14 10:30:23","1","2015-02-20 15:16:55","","2015-02-20 15:16:55","1");
INSERT INTO `files`  VALUES ( "202","0","","","17","1","0","","2015-02-14 10:31:08","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "203","0","","","17","1","0","","2015-02-14 10:31:54","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "204","0","","","16","1","0","","2015-02-20 11:17:27","1","2015-02-20 15:16:55","","2015-02-20 15:16:55","1");
INSERT INTO `files`  VALUES ( "205","0","","","17","1","0","","2015-02-20 11:17:32","1","2015-02-20 15:16:51","","2015-02-20 15:16:51","1");
INSERT INTO `files`  VALUES ( "206","0","","","17","1","0","","2015-02-20 15:16:51","1","","","","");
INSERT INTO `files`  VALUES ( "207","0","","","16","1","0","","2015-02-20 15:16:55","1","","","","");
INSERT INTO `files`  VALUES ( "208","0","theme/www-belartis-cz/files/estates/1424683981.jpg","estates","12","2","0","images","2015-02-23 10:35:17","8","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "209","0","theme/www-belartis-cz/files/estates/1424689052.jpg","estates","12","1","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "210","0","theme/www-belartis-cz/files/estates/1424689052_1.jpg","estates","12","3","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "211","0","theme/www-belartis-cz/files/estates/1424689052_2.jpg","estates","12","4","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "212","0","theme/www-belartis-cz/files/estates/1424689053.jpg","estates","12","5","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "213","0","theme/www-belartis-cz/files/estates/1424689053_1.jpg","estates","12","6","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "214","0","theme/www-belartis-cz/files/estates/1424689053_2.jpg","estates","12","7","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "215","0","theme/www-belartis-cz/files/estates/1424689053_3.jpg","estates","12","8","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "216","0","theme/www-belartis-cz/files/estates/1424689053_4.jpg","estates","12","9","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "217","0","theme/www-belartis-cz/files/estates/1424689053_5.jpg","estates","12","10","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "218","0","theme/www-belartis-cz/files/estates/1424689053_6.jpg","estates","12","11","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "219","0","theme/www-belartis-cz/files/estates/1424689053_7.jpg","estates","12","12","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "220","0","theme/www-belartis-cz/files/estates/1424689053_8.jpg","estates","12","13","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "221","0","theme/www-belartis-cz/files/estates/1424689053_9.jpg","estates","12","14","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "222","0","theme/www-belartis-cz/files/estates/1424689053_10.jpg","estates","12","15","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "223","0","theme/www-belartis-cz/files/estates/1424689053_11.jpg","estates","12","16","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "224","0","theme/www-belartis-cz/files/estates/1424689053_12.jpg","estates","12","17","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "225","0","theme/www-belartis-cz/files/estates/1424689053_13.jpg","estates","12","18","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","","2015-03-22 10:29:41","9");
INSERT INTO `files`  VALUES ( "226","0","theme/www-belartis-cz/files/estates/1424689053_14.jpg","estates","12","18","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "227","0","theme/www-belartis-cz/files/estates/1424689053_15.jpg","estates","12","20","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","","2015-03-22 10:29:41","9");
INSERT INTO `files`  VALUES ( "228","0","theme/www-belartis-cz/files/estates/1424689053_16.jpg","estates","12","19","0","images","2015-02-23 12:03:21","15","2015-03-22 10:29:41","9","","");
INSERT INTO `files`  VALUES ( "229","2","theme/www-demo-cz/files/brokers/1425996352.jpg","users_broker","19","1","0","portrait","2015-03-10 15:05:59","8","2015-04-28 10:55:39","21","","");
INSERT INTO `files`  VALUES ( "230","0","theme/www-demo-cz/files/estates/1425998426.jpg","estates","13","1","0","images","2015-03-10 15:41:12","19","2015-04-22 15:55:11","","2015-04-22 15:55:11","1");
INSERT INTO `files`  VALUES ( "231","2","theme/www-demo-cz/files/estates/1425999564.jpg","estates","13","1","0","images","2015-03-10 15:59:32","19","2015-04-22 16:22:51","1","2015-04-22 16:22:51","1");
INSERT INTO `files`  VALUES ( "232","2","theme/www-demo-cz/files/estates/1426000934.jpg","estates","14","1","0","images","2015-03-10 16:28:17","19","2015-04-29 17:23:49","21","","");
INSERT INTO `files`  VALUES ( "233","0","theme/www-demo-cz/files/estates/1426001544.jpg","estates","15","1","0","images","2015-03-10 16:35:09","19","2015-03-30 20:56:25","19","","");
INSERT INTO `files`  VALUES ( "234","2","theme/www-demo-cz/files/estates/1426002868.jpg","estates","16","1","0","images","2015-03-10 16:54:57","19","2015-04-28 15:59:07","21","","");
INSERT INTO `files`  VALUES ( "235","0","theme/www-demo-cz/files/projects/1427723078.jpg","projects","1","1","0","images","2015-03-30 15:47:06","19","2015-04-13 12:41:34","19","","");
INSERT INTO `files`  VALUES ( "236","0","theme/www-demo-cz/files/articles/1427725049.jpg","articles","1","1","0","images","2015-03-30 16:17:36","19","2015-04-13 12:43:12","19","","");
INSERT INTO `files`  VALUES ( "237","0","theme/www-demo-cz/files/projects/1427730630.jpg","projects","2","1","0","images","2015-03-30 17:50:51","19","2015-04-13 12:42:10","19","","");
INSERT INTO `files`  VALUES ( "238","0","","","22","1","0","","2015-03-30 18:15:57","20","","","","");
INSERT INTO `files`  VALUES ( "239","0","","","23","1","0","","2015-03-30 18:20:15","1","","","","");
INSERT INTO `files`  VALUES ( "240","0","","","24","1","0","","2015-03-30 18:20:40","1","","","","");
INSERT INTO `files`  VALUES ( "241","0","","","25","1","0","","2015-03-30 18:21:46","1","","","","");
INSERT INTO `files`  VALUES ( "242","0","","","26","1","0","","2015-03-30 18:26:18","1","","","","");
INSERT INTO `files`  VALUES ( "243","2","theme/www-demo-cz/files/brokers/1427733975.jpg","users_broker","22","1","0","portrait","2015-03-30 18:37:12","20","2015-03-30 18:48:17","20","","");
INSERT INTO `files`  VALUES ( "244","2","theme/www-demo-cz/files/brokers/1427734012.jpg","users_broker","27","1","0","portrait","2015-03-30 18:47:01","20","2015-03-30 18:48:32","20","","");
INSERT INTO `files`  VALUES ( "245","2","theme/www-demo-cz/files/brokers/1427734193.jpg","users_broker","28","1","0","portrait","2015-03-30 18:50:12","20","","","","");
INSERT INTO `files`  VALUES ( "246","0","theme/www-demo-cz/files/projects/1427734380.JPG","projects","3","1","0","images","2015-03-30 18:53:28","20","2015-03-30 20:37:32","19","","");
INSERT INTO `files`  VALUES ( "247","0","theme/www-demo-cz/files/articles/1427734627.jpg","articles","2","1","0","images","2015-03-30 18:57:12","20","","","","");
INSERT INTO `files`  VALUES ( "248","0","theme/www-belartis-cz/files/estates/1428057424.JPG","estates","8","2","0","images","2015-04-03 12:43:59","9","2015-04-09 07:49:40","9","","");
INSERT INTO `files`  VALUES ( "249","0","theme/www-belartis-cz/files/estates/1428057560.JPG","estates","8","3","0","images","2015-04-03 12:43:59","9","2015-04-09 07:49:40","9","","");
INSERT INTO `files`  VALUES ( "250","0","theme/www-belartis-cz/files/estates/1428057638.JPG","estates","8","4","0","images","2015-04-03 12:43:59","9","2015-04-09 07:49:40","9","","");
INSERT INTO `files`  VALUES ( "251","0","","","","","0","","2015-04-13 12:09:28","19","","","","");
INSERT INTO `files`  VALUES ( "252","0","","","","","0","","2015-04-13 12:24:26","19","","","","");
INSERT INTO `files`  VALUES ( "253","0","","","","","0","","2015-04-13 12:26:37","19","","","","");
INSERT INTO `files`  VALUES ( "254","0","","","","","0","","2015-04-13 12:27:51","19","","","","");
INSERT INTO `files`  VALUES ( "255","0","","","","","0","","2015-04-13 12:29:10","19","","","","");
INSERT INTO `files`  VALUES ( "256","2","","","16","","0","","2015-04-13 12:41:52","19","","","","");
INSERT INTO `files`  VALUES ( "257","2","","","13","","0","","2015-04-13 13:17:40","20","","","","");
INSERT INTO `files`  VALUES ( "258","2","theme/www-demo-cz/files/broker-profiles/1429024637.jpg","brokerprofiles","19","1","1","broker","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "259","2","theme/www-demo-cz/files/broker-profiles/1429024637_1.jpg","brokerprofiles","19","2","0","broker","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "260","2","theme/www-demo-cz/files/broker-profiles/1429024637_2.jpg","brokerprofiles","19","3","0","broker","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "261","2","theme/www-demo-cz/files/broker-profiles/1429024637_3.jpg","brokerprofiles","19","4","0","broker","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "262","2","theme/www-demo-cz/files/broker-profiles/1429024277.jpg","brokerprofiles","19","1","0","estates","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "263","2","theme/www-demo-cz/files/broker-profiles/1429024277_1.jpg","brokerprofiles","19","2","0","estates","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "264","2","theme/www-demo-cz/files/broker-profiles/1429024277_2.jpg","brokerprofiles","19","3","0","estates","2015-04-14 17:18:56","20","2015-04-15 10:34:36","20","","");
INSERT INTO `files`  VALUES ( "265","2","","","13","","0","","2015-04-16 12:24:42","21","","","","");
INSERT INTO `files`  VALUES ( "266","2","","","13","","0","","2015-04-16 12:53:47","21","","","","");
INSERT INTO `files`  VALUES ( "267","2","","","13","","0","","2015-04-16 12:55:54","21","","","","");
INSERT INTO `files`  VALUES ( "268","2","","","13","","0","","2015-04-16 15:55:37","21","","","","");
INSERT INTO `files`  VALUES ( "269","2","","","14","","0","","2015-04-16 15:56:21","21","","","","");
INSERT INTO `files`  VALUES ( "270","2","","","13","","0","","2015-04-16 15:56:54","21","","","","");
INSERT INTO `files`  VALUES ( "271","2","","","14","","0","","2015-04-16 15:58:04","21","","","","");
INSERT INTO `files`  VALUES ( "272","2","","","13","","0","","2015-04-16 17:46:16","21","","","","");
INSERT INTO `files`  VALUES ( "273","2","","","14","","0","","2015-04-16 17:46:28","21","","","","");
INSERT INTO `files`  VALUES ( "274","2","","","13","","0","","2015-04-17 11:32:46","21","","","","");
INSERT INTO `files`  VALUES ( "275","2","","","13","","0","","2015-04-17 11:35:18","21","","","","");
INSERT INTO `files`  VALUES ( "276","2","","","13","","0","","2015-04-17 11:35:44","21","","","","");
INSERT INTO `files`  VALUES ( "277","2","","","13","","0","","2015-04-17 12:54:37","21","","","","");
INSERT INTO `files`  VALUES ( "278","2","","","14","","0","","2015-04-20 11:17:05","1","","","","");
INSERT INTO `files`  VALUES ( "279","2","","","13","","0","","2015-04-20 11:17:33","1","","","","");
INSERT INTO `files`  VALUES ( "280","2","","","13","","0","","2015-04-20 11:52:34","1","","","","");
INSERT INTO `files`  VALUES ( "281","2","","","13","","0","","2015-04-20 12:07:27","1","","","","");
INSERT INTO `files`  VALUES ( "282","2","","","14","","0","","2015-04-20 12:07:36","1","","","","");
INSERT INTO `files`  VALUES ( "283","2","","","13","","0","","2015-04-20 12:21:20","1","","","","");
INSERT INTO `files`  VALUES ( "284","2","","","14","","0","","2015-04-20 12:23:10","1","","","","");
INSERT INTO `files`  VALUES ( "285","2","","","13","","0","","2015-04-20 12:23:46","1","","","","");
INSERT INTO `files`  VALUES ( "286","2","","","13","","0","","2015-04-20 12:24:29","1","","","","");
INSERT INTO `files`  VALUES ( "287","2","","","13","","0","","2015-04-20 12:26:10","1","","","","");
INSERT INTO `files`  VALUES ( "288","2","","","14","","0","","2015-04-20 12:26:15","1","","","","");
INSERT INTO `files`  VALUES ( "289","2","","","14","","0","","2015-04-20 12:26:36","1","","","","");
INSERT INTO `files`  VALUES ( "290","2","","","14","","0","","2015-04-20 12:27:34","1","","","","");
INSERT INTO `files`  VALUES ( "291","2","","","14","","0","","2015-04-20 12:27:48","1","","","","");
INSERT INTO `files`  VALUES ( "292","2","","","13","","0","","2015-04-20 12:28:15","1","","","","");
INSERT INTO `files`  VALUES ( "293","2","","","13","","0","","2015-04-20 12:28:49","1","","","","");
INSERT INTO `files`  VALUES ( "294","2","","","13","","0","","2015-04-20 12:29:19","1","","","","");
INSERT INTO `files`  VALUES ( "295","2","","","13","","0","","2015-04-20 12:29:34","1","","","","");
INSERT INTO `files`  VALUES ( "296","2","","","13","","0","","2015-04-20 12:30:07","1","","","","");
INSERT INTO `files`  VALUES ( "297","2","","","13","","0","","2015-04-20 12:30:25","1","","","","");
INSERT INTO `files`  VALUES ( "298","2","","","13","","0","","2015-04-20 12:30:50","1","","","","");
INSERT INTO `files`  VALUES ( "299","2","","","13","","0","","2015-04-20 12:31:06","1","","","","");
INSERT INTO `files`  VALUES ( "300","2","","","13","","0","","2015-04-20 12:54:28","1","","","","");
INSERT INTO `files`  VALUES ( "301","2","","","13","","0","","2015-04-20 15:30:43","1","","","","");
INSERT INTO `files`  VALUES ( "302","2","","","13","","0","","2015-04-20 15:32:46","1","","","","");
INSERT INTO `files`  VALUES ( "303","2","","","13","","0","","2015-04-20 15:33:23","1","","","","");
INSERT INTO `files`  VALUES ( "304","2","","","13","","0","","2015-04-20 15:34:02","1","","","","");
INSERT INTO `files`  VALUES ( "305","2","","","13","","0","","2015-04-20 15:34:20","1","","","","");
INSERT INTO `files`  VALUES ( "306","2","","","13","","0","","2015-04-21 10:42:52","1","","","","");
INSERT INTO `files`  VALUES ( "307","2","","","13","","0","","2015-04-21 17:05:26","1","","","","");
INSERT INTO `files`  VALUES ( "308","2","","","13","","0","","2015-04-21 17:27:04","1","","","","");
INSERT INTO `files`  VALUES ( "309","2","","","13","","0","","2015-04-22 15:55:11","1","","","","");
INSERT INTO `files`  VALUES ( "310","2","","","14","","0","","2015-04-22 15:55:43","1","","","","");
INSERT INTO `files`  VALUES ( "311","2","","","14","","0","","2015-04-22 15:57:17","1","","","","");
INSERT INTO `files`  VALUES ( "312","2","","","14","","0","","2015-04-23 16:28:12","21","","","","");
INSERT INTO `files`  VALUES ( "313","0","","","","","0","","2015-04-23 16:34:31","21","","","","");
INSERT INTO `files`  VALUES ( "314","0","","","","","0","","2015-04-23 16:34:51","21","","","","");
INSERT INTO `files`  VALUES ( "315","0","","","","","0","","2015-04-23 16:35:15","21","","","","");
INSERT INTO `files`  VALUES ( "316","2","","","14","","0","","2015-04-23 16:35:37","21","","","","");
INSERT INTO `files`  VALUES ( "317","0","","","","","0","","2015-04-23 16:36:43","21","","","","");
INSERT INTO `files`  VALUES ( "318","0","","","","","0","","2015-04-23 16:37:30","21","","","","");
INSERT INTO `files`  VALUES ( "319","0","","","","","0","","2015-04-23 16:38:01","21","","","","");
INSERT INTO `files`  VALUES ( "320","0","","","","","0","","2015-04-23 16:39:36","21","","","","");
INSERT INTO `files`  VALUES ( "321","0","","","","","0","","2015-04-23 16:40:25","21","","","","");
INSERT INTO `files`  VALUES ( "322","0","","","","","0","","2015-04-23 16:44:38","21","","","","");
INSERT INTO `files`  VALUES ( "323","0","","","","","0","","2015-04-23 16:45:17","21","","","","");
INSERT INTO `files`  VALUES ( "324","0","","","","","0","","2015-04-23 16:48:27","21","","","","");
INSERT INTO `files`  VALUES ( "325","0","","","","","0","","2015-04-23 16:48:43","21","","","","");
INSERT INTO `files`  VALUES ( "326","0","","","","","0","","2015-04-23 16:49:19","21","","","","");
INSERT INTO `files`  VALUES ( "327","0","","","","","0","","2015-04-23 16:49:53","21","","","","");
INSERT INTO `files`  VALUES ( "328","0","","","","","0","","2015-04-23 16:50:27","21","","","","");
INSERT INTO `files`  VALUES ( "329","0","","","","","0","","2015-04-23 16:50:47","21","","","","");
INSERT INTO `files`  VALUES ( "330","0","","","","","0","","2015-04-23 16:52:04","21","","","","");
INSERT INTO `files`  VALUES ( "331","0","","","","","0","","2015-04-23 16:52:28","21","","","","");
INSERT INTO `files`  VALUES ( "332","0","","","","","0","","2015-04-23 16:54:46","21","","","","");
INSERT INTO `files`  VALUES ( "333","2","","estates","14","","0","","2015-04-23 16:55:01","21","","","","");
INSERT INTO `files`  VALUES ( "334","0","","","","","0","","2015-04-23 16:55:56","21","","","","");
INSERT INTO `files`  VALUES ( "335","2","","estates","14","","0","","2015-04-23 16:57:31","21","","","","");
INSERT INTO `files`  VALUES ( "336","2","","estates","14","","0","","2015-04-23 17:16:46","21","","","","");
INSERT INTO `files`  VALUES ( "337","0","","","","","0","","2015-04-24 15:13:50","21","","","","");
INSERT INTO `files`  VALUES ( "338","0","","","","","0","","2015-04-24 15:20:12","21","","","","");
INSERT INTO `files`  VALUES ( "339","0","","","","","0","","2015-04-24 15:20:30","21","","","","");
INSERT INTO `files`  VALUES ( "340","2","","estates","14","","0","","2015-04-24 15:20:43","21","","","","");
INSERT INTO `files`  VALUES ( "341","2","","estates","14","","0","","2015-04-27 11:51:29","21","","","","");
INSERT INTO `files`  VALUES ( "342","2","","estates","14","","0","","2015-04-27 11:55:07","21","","","","");
INSERT INTO `files`  VALUES ( "343","2","","estates","14","","0","","2015-04-27 12:30:45","21","","","","");
INSERT INTO `files`  VALUES ( "344","2","","estates","16","","0","","2015-04-28 14:51:19","21","","","","");
INSERT INTO `files`  VALUES ( "345","2","","estates","16","","0","","2015-04-28 15:17:53","21","","","","");
INSERT INTO `files`  VALUES ( "346","2","","estates","16","","0","","2015-04-28 15:59:07","21","","","","");
INSERT INTO `files`  VALUES ( "347","2","","estates","14","","0","","2015-04-28 16:11:21","21","","","","");
INSERT INTO `files`  VALUES ( "348","2","theme/www-demo-cz/files/estates/1430230305.jpg","estates","14","2","0","images","2015-04-28 16:12:42","21","2015-04-29 17:23:49","","2015-04-29 17:23:49","21");
INSERT INTO `files`  VALUES ( "349","2","","estates","14","","0","","2015-04-28 16:12:42","21","","","","");
INSERT INTO `files`  VALUES ( "350","2","","estates","14","","0","","2015-04-28 17:27:18","21","","","","");
INSERT INTO `files`  VALUES ( "351","2","theme/www-demo-cz/files/estates/1430235040.jpg","estates","14","2","0","images","2015-04-28 17:30:58","21","2015-04-29 17:23:49","","2015-04-29 17:23:49","21");
INSERT INTO `files`  VALUES ( "352","2","","estates","14","","0","","2015-04-28 17:30:59","21","","","","");
INSERT INTO `files`  VALUES ( "353","2","","estates","14","","0","","2015-04-28 17:34:11","21","","","","");
INSERT INTO `files`  VALUES ( "354","2","theme/www-demo-cz/files/estates/1430235632.jpg","estates","14","2","0","images","2015-04-28 17:40:47","21","2015-04-29 17:23:49","","2015-04-29 17:23:49","21");
INSERT INTO `files`  VALUES ( "355","2","","estates","14","","0","","2015-04-28 17:40:48","21","","","","");
INSERT INTO `files`  VALUES ( "356","2","","estates","14","","0","","2015-04-28 17:41:26","21","","","","");
INSERT INTO `files`  VALUES ( "357","2","","estates","14","","0","","2015-04-29 17:23:49","21","","","","");


--
-- Tabel structure for table `files_cs`
--
DROP TABLE  IF EXISTS `files_cs`;
CREATE TABLE `files_cs` (
  `file_id` int(11) NOT NULL,
  `file_alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `files_cs`  VALUES ( "1","?esk� popisek");
INSERT INTO `files_cs`  VALUES ( "2","Prvn� obr�zek");
INSERT INTO `files_cs`  VALUES ( "3","");
INSERT INTO `files_cs`  VALUES ( "4","");
INSERT INTO `files_cs`  VALUES ( "5","");
INSERT INTO `files_cs`  VALUES ( "6","");
INSERT INTO `files_cs`  VALUES ( "7","");
INSERT INTO `files_cs`  VALUES ( "8","");
INSERT INTO `files_cs`  VALUES ( "9","");
INSERT INTO `files_cs`  VALUES ( "10","");
INSERT INTO `files_cs`  VALUES ( "11","");
INSERT INTO `files_cs`  VALUES ( "12","");
INSERT INTO `files_cs`  VALUES ( "13","");
INSERT INTO `files_cs`  VALUES ( "14","");
INSERT INTO `files_cs`  VALUES ( "15","");
INSERT INTO `files_cs`  VALUES ( "16","");
INSERT INTO `files_cs`  VALUES ( "17","");
INSERT INTO `files_cs`  VALUES ( "18","");
INSERT INTO `files_cs`  VALUES ( "19","");
INSERT INTO `files_cs`  VALUES ( "20","");
INSERT INTO `files_cs`  VALUES ( "21","");
INSERT INTO `files_cs`  VALUES ( "22","");
INSERT INTO `files_cs`  VALUES ( "23","");
INSERT INTO `files_cs`  VALUES ( "24","");
INSERT INTO `files_cs`  VALUES ( "25","");
INSERT INTO `files_cs`  VALUES ( "26","");
INSERT INTO `files_cs`  VALUES ( "27","");
INSERT INTO `files_cs`  VALUES ( "28","");
INSERT INTO `files_cs`  VALUES ( "29","");
INSERT INTO `files_cs`  VALUES ( "30","");
INSERT INTO `files_cs`  VALUES ( "31","");
INSERT INTO `files_cs`  VALUES ( "32","");
INSERT INTO `files_cs`  VALUES ( "33","");
INSERT INTO `files_cs`  VALUES ( "34","");
INSERT INTO `files_cs`  VALUES ( "35","");
INSERT INTO `files_cs`  VALUES ( "36","");
INSERT INTO `files_cs`  VALUES ( "37","");
INSERT INTO `files_cs`  VALUES ( "38","");
INSERT INTO `files_cs`  VALUES ( "39","");
INSERT INTO `files_cs`  VALUES ( "40","");
INSERT INTO `files_cs`  VALUES ( "41","");
INSERT INTO `files_cs`  VALUES ( "42","");
INSERT INTO `files_cs`  VALUES ( "43","");
INSERT INTO `files_cs`  VALUES ( "44","");
INSERT INTO `files_cs`  VALUES ( "45","");
INSERT INTO `files_cs`  VALUES ( "46","");
INSERT INTO `files_cs`  VALUES ( "48","");
INSERT INTO `files_cs`  VALUES ( "49","");
INSERT INTO `files_cs`  VALUES ( "50","");
INSERT INTO `files_cs`  VALUES ( "51","");
INSERT INTO `files_cs`  VALUES ( "52","");
INSERT INTO `files_cs`  VALUES ( "53","");
INSERT INTO `files_cs`  VALUES ( "54","");
INSERT INTO `files_cs`  VALUES ( "55","");
INSERT INTO `files_cs`  VALUES ( "56","");
INSERT INTO `files_cs`  VALUES ( "57","");
INSERT INTO `files_cs`  VALUES ( "58","");
INSERT INTO `files_cs`  VALUES ( "59","");
INSERT INTO `files_cs`  VALUES ( "60","");
INSERT INTO `files_cs`  VALUES ( "61","");
INSERT INTO `files_cs`  VALUES ( "62","");
INSERT INTO `files_cs`  VALUES ( "63","");
INSERT INTO `files_cs`  VALUES ( "64","");
INSERT INTO `files_cs`  VALUES ( "65","");
INSERT INTO `files_cs`  VALUES ( "66","");
INSERT INTO `files_cs`  VALUES ( "67","");
INSERT INTO `files_cs`  VALUES ( "68","");
INSERT INTO `files_cs`  VALUES ( "69","");
INSERT INTO `files_cs`  VALUES ( "70","");
INSERT INTO `files_cs`  VALUES ( "71","");
INSERT INTO `files_cs`  VALUES ( "72","");
INSERT INTO `files_cs`  VALUES ( "73","");
INSERT INTO `files_cs`  VALUES ( "74","");
INSERT INTO `files_cs`  VALUES ( "75","");
INSERT INTO `files_cs`  VALUES ( "76","");
INSERT INTO `files_cs`  VALUES ( "77","");
INSERT INTO `files_cs`  VALUES ( "78","");
INSERT INTO `files_cs`  VALUES ( "79","");
INSERT INTO `files_cs`  VALUES ( "80","");
INSERT INTO `files_cs`  VALUES ( "81","");
INSERT INTO `files_cs`  VALUES ( "82","");
INSERT INTO `files_cs`  VALUES ( "83","");
INSERT INTO `files_cs`  VALUES ( "84","");
INSERT INTO `files_cs`  VALUES ( "85","");
INSERT INTO `files_cs`  VALUES ( "86","");
INSERT INTO `files_cs`  VALUES ( "87","");
INSERT INTO `files_cs`  VALUES ( "88","");
INSERT INTO `files_cs`  VALUES ( "89","");
INSERT INTO `files_cs`  VALUES ( "90","");
INSERT INTO `files_cs`  VALUES ( "91","");
INSERT INTO `files_cs`  VALUES ( "92","");
INSERT INTO `files_cs`  VALUES ( "93","");
INSERT INTO `files_cs`  VALUES ( "94","");
INSERT INTO `files_cs`  VALUES ( "95","");
INSERT INTO `files_cs`  VALUES ( "96","");
INSERT INTO `files_cs`  VALUES ( "97","");
INSERT INTO `files_cs`  VALUES ( "98","");
INSERT INTO `files_cs`  VALUES ( "99","");
INSERT INTO `files_cs`  VALUES ( "100","");
INSERT INTO `files_cs`  VALUES ( "101","");
INSERT INTO `files_cs`  VALUES ( "102","");
INSERT INTO `files_cs`  VALUES ( "103","");
INSERT INTO `files_cs`  VALUES ( "104","");
INSERT INTO `files_cs`  VALUES ( "105","");
INSERT INTO `files_cs`  VALUES ( "106","");
INSERT INTO `files_cs`  VALUES ( "107","");
INSERT INTO `files_cs`  VALUES ( "108","");
INSERT INTO `files_cs`  VALUES ( "109","");
INSERT INTO `files_cs`  VALUES ( "110","");
INSERT INTO `files_cs`  VALUES ( "111","");
INSERT INTO `files_cs`  VALUES ( "112","");
INSERT INTO `files_cs`  VALUES ( "113","");
INSERT INTO `files_cs`  VALUES ( "114","");
INSERT INTO `files_cs`  VALUES ( "115","");
INSERT INTO `files_cs`  VALUES ( "116","");
INSERT INTO `files_cs`  VALUES ( "117","");
INSERT INTO `files_cs`  VALUES ( "118","");
INSERT INTO `files_cs`  VALUES ( "119","");
INSERT INTO `files_cs`  VALUES ( "120","");
INSERT INTO `files_cs`  VALUES ( "121","");
INSERT INTO `files_cs`  VALUES ( "122","");
INSERT INTO `files_cs`  VALUES ( "123","");
INSERT INTO `files_cs`  VALUES ( "124","");
INSERT INTO `files_cs`  VALUES ( "125","");
INSERT INTO `files_cs`  VALUES ( "126","");
INSERT INTO `files_cs`  VALUES ( "127","");
INSERT INTO `files_cs`  VALUES ( "128","");
INSERT INTO `files_cs`  VALUES ( "129","");
INSERT INTO `files_cs`  VALUES ( "130","");
INSERT INTO `files_cs`  VALUES ( "131","");
INSERT INTO `files_cs`  VALUES ( "132","");
INSERT INTO `files_cs`  VALUES ( "133","");
INSERT INTO `files_cs`  VALUES ( "134","");
INSERT INTO `files_cs`  VALUES ( "135","");
INSERT INTO `files_cs`  VALUES ( "136","");
INSERT INTO `files_cs`  VALUES ( "137","");
INSERT INTO `files_cs`  VALUES ( "138","");
INSERT INTO `files_cs`  VALUES ( "139","");
INSERT INTO `files_cs`  VALUES ( "140","");
INSERT INTO `files_cs`  VALUES ( "141","");
INSERT INTO `files_cs`  VALUES ( "142","");
INSERT INTO `files_cs`  VALUES ( "143","");
INSERT INTO `files_cs`  VALUES ( "144","");
INSERT INTO `files_cs`  VALUES ( "145","");
INSERT INTO `files_cs`  VALUES ( "146","");
INSERT INTO `files_cs`  VALUES ( "147","");
INSERT INTO `files_cs`  VALUES ( "148","");
INSERT INTO `files_cs`  VALUES ( "149","");
INSERT INTO `files_cs`  VALUES ( "150","");
INSERT INTO `files_cs`  VALUES ( "151","");
INSERT INTO `files_cs`  VALUES ( "152","");
INSERT INTO `files_cs`  VALUES ( "153","");
INSERT INTO `files_cs`  VALUES ( "154","");
INSERT INTO `files_cs`  VALUES ( "155","");
INSERT INTO `files_cs`  VALUES ( "156","");
INSERT INTO `files_cs`  VALUES ( "157","");
INSERT INTO `files_cs`  VALUES ( "158","");
INSERT INTO `files_cs`  VALUES ( "159","");
INSERT INTO `files_cs`  VALUES ( "160","");
INSERT INTO `files_cs`  VALUES ( "161","");
INSERT INTO `files_cs`  VALUES ( "162","");
INSERT INTO `files_cs`  VALUES ( "163","");
INSERT INTO `files_cs`  VALUES ( "164","");
INSERT INTO `files_cs`  VALUES ( "165","");
INSERT INTO `files_cs`  VALUES ( "166","");
INSERT INTO `files_cs`  VALUES ( "167","");
INSERT INTO `files_cs`  VALUES ( "168","");
INSERT INTO `files_cs`  VALUES ( "169","");
INSERT INTO `files_cs`  VALUES ( "170","");
INSERT INTO `files_cs`  VALUES ( "171","");
INSERT INTO `files_cs`  VALUES ( "172","");
INSERT INTO `files_cs`  VALUES ( "173","");
INSERT INTO `files_cs`  VALUES ( "174","");
INSERT INTO `files_cs`  VALUES ( "175","");
INSERT INTO `files_cs`  VALUES ( "176","");
INSERT INTO `files_cs`  VALUES ( "177","");
INSERT INTO `files_cs`  VALUES ( "178","");
INSERT INTO `files_cs`  VALUES ( "179","");
INSERT INTO `files_cs`  VALUES ( "180","");
INSERT INTO `files_cs`  VALUES ( "181","");
INSERT INTO `files_cs`  VALUES ( "182","");
INSERT INTO `files_cs`  VALUES ( "183","");
INSERT INTO `files_cs`  VALUES ( "184","");
INSERT INTO `files_cs`  VALUES ( "185","");
INSERT INTO `files_cs`  VALUES ( "187","");
INSERT INTO `files_cs`  VALUES ( "188","");
INSERT INTO `files_cs`  VALUES ( "189","");
INSERT INTO `files_cs`  VALUES ( "190","");
INSERT INTO `files_cs`  VALUES ( "191","");
INSERT INTO `files_cs`  VALUES ( "192","");
INSERT INTO `files_cs`  VALUES ( "193","");
INSERT INTO `files_cs`  VALUES ( "194","");
INSERT INTO `files_cs`  VALUES ( "195","");
INSERT INTO `files_cs`  VALUES ( "196","");
INSERT INTO `files_cs`  VALUES ( "197","");
INSERT INTO `files_cs`  VALUES ( "198","");
INSERT INTO `files_cs`  VALUES ( "199","");
INSERT INTO `files_cs`  VALUES ( "200","");
INSERT INTO `files_cs`  VALUES ( "201","");
INSERT INTO `files_cs`  VALUES ( "202","");
INSERT INTO `files_cs`  VALUES ( "203","");
INSERT INTO `files_cs`  VALUES ( "204","");
INSERT INTO `files_cs`  VALUES ( "205","");
INSERT INTO `files_cs`  VALUES ( "206","");
INSERT INTO `files_cs`  VALUES ( "207","");
INSERT INTO `files_cs`  VALUES ( "208","");
INSERT INTO `files_cs`  VALUES ( "209","");
INSERT INTO `files_cs`  VALUES ( "210","");
INSERT INTO `files_cs`  VALUES ( "211","");
INSERT INTO `files_cs`  VALUES ( "212","");
INSERT INTO `files_cs`  VALUES ( "213","");
INSERT INTO `files_cs`  VALUES ( "214","");
INSERT INTO `files_cs`  VALUES ( "215","");
INSERT INTO `files_cs`  VALUES ( "216","");
INSERT INTO `files_cs`  VALUES ( "217","");
INSERT INTO `files_cs`  VALUES ( "218","");
INSERT INTO `files_cs`  VALUES ( "219","");
INSERT INTO `files_cs`  VALUES ( "220","");
INSERT INTO `files_cs`  VALUES ( "221","");
INSERT INTO `files_cs`  VALUES ( "222","");
INSERT INTO `files_cs`  VALUES ( "223","");
INSERT INTO `files_cs`  VALUES ( "224","");
INSERT INTO `files_cs`  VALUES ( "225","");
INSERT INTO `files_cs`  VALUES ( "226","");
INSERT INTO `files_cs`  VALUES ( "227","");
INSERT INTO `files_cs`  VALUES ( "228","");
INSERT INTO `files_cs`  VALUES ( "229","");
INSERT INTO `files_cs`  VALUES ( "230","");
INSERT INTO `files_cs`  VALUES ( "231","");
INSERT INTO `files_cs`  VALUES ( "232","");
INSERT INTO `files_cs`  VALUES ( "233","");
INSERT INTO `files_cs`  VALUES ( "234","");
INSERT INTO `files_cs`  VALUES ( "235","Ps� bouda");
INSERT INTO `files_cs`  VALUES ( "236","");
INSERT INTO `files_cs`  VALUES ( "237","");
INSERT INTO `files_cs`  VALUES ( "238","");
INSERT INTO `files_cs`  VALUES ( "239","");
INSERT INTO `files_cs`  VALUES ( "240","");
INSERT INTO `files_cs`  VALUES ( "241","");
INSERT INTO `files_cs`  VALUES ( "242","");
INSERT INTO `files_cs`  VALUES ( "243","");
INSERT INTO `files_cs`  VALUES ( "244","");
INSERT INTO `files_cs`  VALUES ( "245","");
INSERT INTO `files_cs`  VALUES ( "246","");
INSERT INTO `files_cs`  VALUES ( "247","");
INSERT INTO `files_cs`  VALUES ( "248","");
INSERT INTO `files_cs`  VALUES ( "249","");
INSERT INTO `files_cs`  VALUES ( "250","");
INSERT INTO `files_cs`  VALUES ( "251","");
INSERT INTO `files_cs`  VALUES ( "252","");
INSERT INTO `files_cs`  VALUES ( "253","");
INSERT INTO `files_cs`  VALUES ( "254","");
INSERT INTO `files_cs`  VALUES ( "255","");
INSERT INTO `files_cs`  VALUES ( "256","");
INSERT INTO `files_cs`  VALUES ( "257","");
INSERT INTO `files_cs`  VALUES ( "258","");
INSERT INTO `files_cs`  VALUES ( "259","");
INSERT INTO `files_cs`  VALUES ( "260","");
INSERT INTO `files_cs`  VALUES ( "261","");
INSERT INTO `files_cs`  VALUES ( "262","");
INSERT INTO `files_cs`  VALUES ( "263","");
INSERT INTO `files_cs`  VALUES ( "264","");
INSERT INTO `files_cs`  VALUES ( "265","");
INSERT INTO `files_cs`  VALUES ( "266","");
INSERT INTO `files_cs`  VALUES ( "267","");
INSERT INTO `files_cs`  VALUES ( "268","");
INSERT INTO `files_cs`  VALUES ( "269","");
INSERT INTO `files_cs`  VALUES ( "270","");
INSERT INTO `files_cs`  VALUES ( "271","");
INSERT INTO `files_cs`  VALUES ( "272","");
INSERT INTO `files_cs`  VALUES ( "273","");
INSERT INTO `files_cs`  VALUES ( "274","");
INSERT INTO `files_cs`  VALUES ( "275","");
INSERT INTO `files_cs`  VALUES ( "276","");
INSERT INTO `files_cs`  VALUES ( "277","");
INSERT INTO `files_cs`  VALUES ( "278","");
INSERT INTO `files_cs`  VALUES ( "279","");
INSERT INTO `files_cs`  VALUES ( "280","");
INSERT INTO `files_cs`  VALUES ( "281","");
INSERT INTO `files_cs`  VALUES ( "282","");
INSERT INTO `files_cs`  VALUES ( "283","");
INSERT INTO `files_cs`  VALUES ( "284","");
INSERT INTO `files_cs`  VALUES ( "285","");
INSERT INTO `files_cs`  VALUES ( "286","");
INSERT INTO `files_cs`  VALUES ( "287","");
INSERT INTO `files_cs`  VALUES ( "288","");
INSERT INTO `files_cs`  VALUES ( "289","");
INSERT INTO `files_cs`  VALUES ( "290","");
INSERT INTO `files_cs`  VALUES ( "291","");
INSERT INTO `files_cs`  VALUES ( "292","");
INSERT INTO `files_cs`  VALUES ( "293","");
INSERT INTO `files_cs`  VALUES ( "294","");
INSERT INTO `files_cs`  VALUES ( "295","");
INSERT INTO `files_cs`  VALUES ( "296","");
INSERT INTO `files_cs`  VALUES ( "297","");
INSERT INTO `files_cs`  VALUES ( "298","");
INSERT INTO `files_cs`  VALUES ( "299","");
INSERT INTO `files_cs`  VALUES ( "300","");
INSERT INTO `files_cs`  VALUES ( "301","");
INSERT INTO `files_cs`  VALUES ( "302","");
INSERT INTO `files_cs`  VALUES ( "303","");
INSERT INTO `files_cs`  VALUES ( "304","");
INSERT INTO `files_cs`  VALUES ( "305","");
INSERT INTO `files_cs`  VALUES ( "306","");
INSERT INTO `files_cs`  VALUES ( "307","");
INSERT INTO `files_cs`  VALUES ( "308","");
INSERT INTO `files_cs`  VALUES ( "309","");
INSERT INTO `files_cs`  VALUES ( "310","");
INSERT INTO `files_cs`  VALUES ( "311","");
INSERT INTO `files_cs`  VALUES ( "312","");
INSERT INTO `files_cs`  VALUES ( "313","");
INSERT INTO `files_cs`  VALUES ( "314","");
INSERT INTO `files_cs`  VALUES ( "315","");
INSERT INTO `files_cs`  VALUES ( "316","");
INSERT INTO `files_cs`  VALUES ( "317","");
INSERT INTO `files_cs`  VALUES ( "318","");
INSERT INTO `files_cs`  VALUES ( "319","");
INSERT INTO `files_cs`  VALUES ( "320","");
INSERT INTO `files_cs`  VALUES ( "321","");
INSERT INTO `files_cs`  VALUES ( "322","");
INSERT INTO `files_cs`  VALUES ( "323","");
INSERT INTO `files_cs`  VALUES ( "324","");
INSERT INTO `files_cs`  VALUES ( "325","");
INSERT INTO `files_cs`  VALUES ( "326","");
INSERT INTO `files_cs`  VALUES ( "327","");
INSERT INTO `files_cs`  VALUES ( "328","");
INSERT INTO `files_cs`  VALUES ( "329","");
INSERT INTO `files_cs`  VALUES ( "330","");
INSERT INTO `files_cs`  VALUES ( "331","");
INSERT INTO `files_cs`  VALUES ( "332","");
INSERT INTO `files_cs`  VALUES ( "333","");
INSERT INTO `files_cs`  VALUES ( "334","");
INSERT INTO `files_cs`  VALUES ( "335","");
INSERT INTO `files_cs`  VALUES ( "336","");
INSERT INTO `files_cs`  VALUES ( "337","");
INSERT INTO `files_cs`  VALUES ( "338","");
INSERT INTO `files_cs`  VALUES ( "339","");
INSERT INTO `files_cs`  VALUES ( "340","");
INSERT INTO `files_cs`  VALUES ( "341","");
INSERT INTO `files_cs`  VALUES ( "342","");
INSERT INTO `files_cs`  VALUES ( "343","");
INSERT INTO `files_cs`  VALUES ( "344","");
INSERT INTO `files_cs`  VALUES ( "345","");
INSERT INTO `files_cs`  VALUES ( "346","");
INSERT INTO `files_cs`  VALUES ( "347","");
INSERT INTO `files_cs`  VALUES ( "348","");
INSERT INTO `files_cs`  VALUES ( "349","");
INSERT INTO `files_cs`  VALUES ( "350","");
INSERT INTO `files_cs`  VALUES ( "351","");
INSERT INTO `files_cs`  VALUES ( "352","");
INSERT INTO `files_cs`  VALUES ( "353","");
INSERT INTO `files_cs`  VALUES ( "354","");
INSERT INTO `files_cs`  VALUES ( "355","");
INSERT INTO `files_cs`  VALUES ( "356","");
INSERT INTO `files_cs`  VALUES ( "357","");


--
-- Tabel structure for table `files_en`
--
DROP TABLE  IF EXISTS `files_en`;
CREATE TABLE `files_en` (
  `file_id` int(11) NOT NULL,
  `file_alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  CONSTRAINT `files_en_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `files` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `files_en`  VALUES ( "1","Anglicky");
INSERT INTO `files_en`  VALUES ( "2","");
INSERT INTO `files_en`  VALUES ( "3","");
INSERT INTO `files_en`  VALUES ( "4","");
INSERT INTO `files_en`  VALUES ( "5","");
INSERT INTO `files_en`  VALUES ( "6","");
INSERT INTO `files_en`  VALUES ( "7","");
INSERT INTO `files_en`  VALUES ( "8","");
INSERT INTO `files_en`  VALUES ( "9","");
INSERT INTO `files_en`  VALUES ( "10","");
INSERT INTO `files_en`  VALUES ( "11","");
INSERT INTO `files_en`  VALUES ( "12","");
INSERT INTO `files_en`  VALUES ( "13","");
INSERT INTO `files_en`  VALUES ( "14","");
INSERT INTO `files_en`  VALUES ( "15","");
INSERT INTO `files_en`  VALUES ( "16","");
INSERT INTO `files_en`  VALUES ( "17","");
INSERT INTO `files_en`  VALUES ( "18","");
INSERT INTO `files_en`  VALUES ( "19","");
INSERT INTO `files_en`  VALUES ( "20","");
INSERT INTO `files_en`  VALUES ( "21","");
INSERT INTO `files_en`  VALUES ( "22","");
INSERT INTO `files_en`  VALUES ( "23","");
INSERT INTO `files_en`  VALUES ( "24","");
INSERT INTO `files_en`  VALUES ( "25","");
INSERT INTO `files_en`  VALUES ( "26","");
INSERT INTO `files_en`  VALUES ( "27","");
INSERT INTO `files_en`  VALUES ( "28","");
INSERT INTO `files_en`  VALUES ( "29","");
INSERT INTO `files_en`  VALUES ( "30","");
INSERT INTO `files_en`  VALUES ( "31","");
INSERT INTO `files_en`  VALUES ( "32","");
INSERT INTO `files_en`  VALUES ( "33","");
INSERT INTO `files_en`  VALUES ( "34","");
INSERT INTO `files_en`  VALUES ( "35","");
INSERT INTO `files_en`  VALUES ( "36","");
INSERT INTO `files_en`  VALUES ( "37","");
INSERT INTO `files_en`  VALUES ( "38","");
INSERT INTO `files_en`  VALUES ( "39","");
INSERT INTO `files_en`  VALUES ( "40","");
INSERT INTO `files_en`  VALUES ( "41","");
INSERT INTO `files_en`  VALUES ( "42","");
INSERT INTO `files_en`  VALUES ( "43","");
INSERT INTO `files_en`  VALUES ( "44","");
INSERT INTO `files_en`  VALUES ( "45","");
INSERT INTO `files_en`  VALUES ( "46","");


--
-- Tabel structure for table `menu_items`
--
DROP TABLE  IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `menu` varchar(255) DEFAULT NULL,
  `item_rank` int(11) DEFAULT NULL,
  `slug_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

INSERT INTO `menu_items`  VALUES ( "1","1","main","1","2","2015-02-06 10:36:02","","","","","");
INSERT INTO `menu_items`  VALUES ( "2","1","main","2","1","2015-02-06 10:36:02","","","","","");
INSERT INTO `menu_items`  VALUES ( "3","1","main","3","3","2015-02-06 10:36:02","","","","","");
INSERT INTO `menu_items`  VALUES ( "4","1","main","7","4","2015-02-06 10:36:02","","2015-04-07 15:54:06","8","","");
INSERT INTO `menu_items`  VALUES ( "5","1","main","","5","2015-02-06 10:36:02","","2015-02-09 12:18:19","","2014-12-01 17:02:28","");
INSERT INTO `menu_items`  VALUES ( "6","1","main","8","6","2015-02-06 10:36:02","","2015-04-07 15:53:51","8","","");
INSERT INTO `menu_items`  VALUES ( "7","1","main","4","7","2015-02-06 10:36:02","","2015-04-07 15:54:14","","2015-04-07 15:54:14","8");
INSERT INTO `menu_items`  VALUES ( "8","2","header","1","91","2015-03-10 11:09:07","8","","","","");
INSERT INTO `menu_items`  VALUES ( "9","2","header","2","1","2015-03-10 11:09:14","8","","","","");
INSERT INTO `menu_items`  VALUES ( "10","2","header","3","92","2015-03-10 11:10:16","8","","","","");
INSERT INTO `menu_items`  VALUES ( "11","2","header","4","5","2015-03-10 11:10:29","8","","","","");
INSERT INTO `menu_items`  VALUES ( "12","2","header","6","6","2015-03-10 11:10:37","8","2015-03-30 15:24:49","8","","");
INSERT INTO `menu_items`  VALUES ( "13","2","header","5","97","2015-03-30 15:24:46","8","2015-03-30 15:24:49","8","","");
INSERT INTO `menu_items`  VALUES ( "14","1","","1","84","2015-04-07 15:52:57","8","","","","");
INSERT INTO `menu_items`  VALUES ( "15","1","main","6","85","2015-04-07 15:53:14","8","2015-04-07 15:54:10","8","","");
INSERT INTO `menu_items`  VALUES ( "16","1","main","5","84","2015-04-07 15:53:22","8","2015-04-07 15:54:10","8","","");


--
-- Tabel structure for table `notifications`
--
DROP TABLE  IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_key` varchar(255) DEFAULT NULL,
  `notification_table_name` varchar(255) DEFAULT NULL,
  `notification_table_key` int(11) DEFAULT NULL,
  `notification_deadline` timestamp NULL DEFAULT NULL,
  `notification_done` timestamp NULL DEFAULT NULL,
  `notification_url` varchar(255) DEFAULT NULL,
  `notification_content` text,
  `user_id` int(11) DEFAULT NULL,
  `role` text,
  `client_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `notifications`  VALUES ( "4","newDemand","demands","8","2015-01-21 16:07:03","2015-01-20 16:07:18","belartis.strankyrealitky.cz/demands/edit?demand_id=8","Byla vytvo?ena nov� popt�vka na nemovitost v obci Neratovice","","admin","1","2015-01-20 16:07:03","1","2015-01-20 16:07:18","","","");
INSERT INTO `notifications`  VALUES ( "5","newDemand","demands","9","2015-01-21 16:11:42","2015-01-20 16:12:00","http://belartis.strankyrealitky.cz/admin/demands/edit?demand_id=9","Byla vytvo?ena nov� popt�vka na nemovitost v obci Neratovice","","admin","1","2015-01-20 16:11:42","1","2015-01-20 16:12:00","","","");
INSERT INTO `notifications`  VALUES ( "6","changePassword","","","","","http://belartis.strankyrealitky.cz/admin/users/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","16","","1","2015-01-28 17:29:02","9","","","","");
INSERT INTO `notifications`  VALUES ( "7","changePassword","","","","","http://belartis.strankyrealitky.cz/admin/users/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","17","","1","2015-02-04 16:14:12","9","","","","");
INSERT INTO `notifications`  VALUES ( "8","changePassword","","","","","http://belartis.strankyrealitky.cz/admin/users/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","18","","1","2015-02-13 10:17:06","9","","","","");
INSERT INTO `notifications`  VALUES ( "9","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","19","","2","2015-03-10 15:05:59","8","","","","");
INSERT INTO `notifications`  VALUES ( "10","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","20","","2","2015-03-10 15:15:38","8","","","","");
INSERT INTO `notifications`  VALUES ( "11","changePassword","","","","2015-03-30 15:36:34","http://demo.strankyrealitky.cz/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","21","","2","2015-03-30 15:22:38","8","2015-03-30 15:36:34","21","","");
INSERT INTO `notifications`  VALUES ( "12","changePassword","","","","","http://demo.strankyrealitky.cz/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","22","","2","2015-03-30 18:15:57","20","","","","");
INSERT INTO `notifications`  VALUES ( "13","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","23","","2","2015-03-30 18:20:15","1","","","","");
INSERT INTO `notifications`  VALUES ( "14","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","24","","2","2015-03-30 18:20:40","1","","","","");
INSERT INTO `notifications`  VALUES ( "15","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","25","","2","2015-03-30 18:21:46","1","","","","");
INSERT INTO `notifications`  VALUES ( "16","changePassword","","","","","http://localhost/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","26","","2","2015-03-30 18:26:18","1","","","","");
INSERT INTO `notifications`  VALUES ( "17","changePassword","","","","","http://demo.strankyrealitky.cz/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","27","","2","2015-03-30 18:47:01","20","","","","");
INSERT INTO `notifications`  VALUES ( "18","changePassword","","","","","http://demo.strankyrealitky.cz/admin/user/change-password/","Zm??te si va�e heslo pro p?�stup do administrace.","28","","2","2015-03-30 18:50:12","20","","","","");


--
-- Tabel structure for table `pages`
--
DROP TABLE  IF EXISTS `pages`;
CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `page_published` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `pages`  VALUES ( "1","1","1","2015-01-09 10:13:03","1","2015-04-01 11:09:05","9","","");
INSERT INTO `pages`  VALUES ( "2","1","1","2015-01-09 12:01:05","1","2015-04-07 17:33:09","8","","");
INSERT INTO `pages`  VALUES ( "3","1","1","2015-01-09 12:05:30","1","2015-01-20 16:28:00","8","2015-01-20 16:31:35","8");
INSERT INTO `pages`  VALUES ( "4","1","1","2015-01-09 12:06:19","1","2015-01-20 16:29:52","8","2015-01-20 16:31:40","8");
INSERT INTO `pages`  VALUES ( "5","1","1","2015-01-09 12:07:13","1","2015-01-20 16:30:28","8","2015-01-20 16:31:44","8");
INSERT INTO `pages`  VALUES ( "6","1","1","2015-01-09 12:07:59","1","2015-04-08 22:17:15","8","","");
INSERT INTO `pages`  VALUES ( "7","1","0","2015-01-09 16:14:37","1","2015-04-06 11:04:51","8","","");
INSERT INTO `pages`  VALUES ( "8","1","1","2015-01-19 17:01:51","1","2015-01-19 17:02:05","1","2015-01-19 17:03:12","1");
INSERT INTO `pages`  VALUES ( "9","1","1","2015-01-20 10:47:14","9","2015-01-23 17:42:58","8","","");
INSERT INTO `pages`  VALUES ( "10","1","1","2015-01-20 16:22:12","8","2015-01-23 17:43:34","8","","");
INSERT INTO `pages`  VALUES ( "11","1","1","2015-01-20 16:22:43","8","2015-01-23 17:45:05","8","","");
INSERT INTO `pages`  VALUES ( "12","1","1","2015-01-20 16:23:54","8","2015-01-20 16:24:18","8","2015-01-20 16:36:06","8");
INSERT INTO `pages`  VALUES ( "13","1","1","2015-01-20 16:25:11","8","2015-01-23 17:48:46","8","","");
INSERT INTO `pages`  VALUES ( "14","1","1","2015-01-20 16:25:46","8","2015-04-10 08:06:12","9","","");
INSERT INTO `pages`  VALUES ( "15","1","1","2015-01-20 16:26:12","8","2015-04-10 08:05:03","9","","");
INSERT INTO `pages`  VALUES ( "16","1","1","2015-02-02 10:05:26","9","2015-04-06 20:12:30","9","","");
INSERT INTO `pages`  VALUES ( "17","2","1","2015-03-10 11:00:03","8","2015-03-13 16:30:35","1","","");
INSERT INTO `pages`  VALUES ( "18","2","1","2015-03-10 11:09:27","8","2015-03-13 16:34:52","1","","");


--
-- Tabel structure for table `pages_blocks`
--
DROP TABLE  IF EXISTS `pages_blocks`;
CREATE TABLE `pages_blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `block_key` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

INSERT INTO `pages_blocks`  VALUES ( "1","1","main","2015-01-09 10:13:03","1","2015-04-01 11:09:05","9","","");
INSERT INTO `pages_blocks`  VALUES ( "2","2","main","2015-01-09 12:01:05","1","2015-04-07 17:33:09","8","","");
INSERT INTO `pages_blocks`  VALUES ( "3","3","main","2015-01-09 12:05:30","1","2015-01-20 16:31:35","8","2015-01-20 16:31:35","8");
INSERT INTO `pages_blocks`  VALUES ( "4","4","main","2015-01-09 12:06:19","1","2015-01-20 16:31:40","8","2015-01-20 16:31:40","8");
INSERT INTO `pages_blocks`  VALUES ( "5","5","main","2015-01-09 12:07:13","1","2015-01-20 16:31:44","8","2015-01-20 16:31:44","8");
INSERT INTO `pages_blocks`  VALUES ( "6","6","main","2015-01-09 12:07:59","1","2015-04-08 22:17:15","8","","");
INSERT INTO `pages_blocks`  VALUES ( "7","7","main","2015-01-09 16:14:37","1","2015-02-02 10:25:11","9","","");
INSERT INTO `pages_blocks`  VALUES ( "8","8","main","2015-01-19 17:01:51","1","2015-01-19 17:03:12","1","2015-01-19 17:03:12","1");
INSERT INTO `pages_blocks`  VALUES ( "9","9","main","2015-01-20 10:47:14","9","2015-01-23 17:42:58","8","","");
INSERT INTO `pages_blocks`  VALUES ( "10","10","main","2015-01-20 16:22:12","8","2015-01-23 17:43:34","8","","");
INSERT INTO `pages_blocks`  VALUES ( "11","11","main","2015-01-20 16:22:43","8","2015-01-23 17:45:05","8","","");
INSERT INTO `pages_blocks`  VALUES ( "12","12","main","2015-01-20 16:23:55","8","2015-01-20 16:36:06","8","2015-01-20 16:36:06","8");
INSERT INTO `pages_blocks`  VALUES ( "13","13","main","2015-01-20 16:25:11","8","2015-01-23 17:48:46","8","","");
INSERT INTO `pages_blocks`  VALUES ( "14","14","main","2015-01-20 16:25:46","8","2015-04-10 08:06:12","9","","");
INSERT INTO `pages_blocks`  VALUES ( "15","15","main","2015-01-20 16:26:12","8","2015-04-10 08:05:03","9","","");
INSERT INTO `pages_blocks`  VALUES ( "16","16","main","2015-02-02 10:05:26","9","2015-04-06 20:12:30","9","","");
INSERT INTO `pages_blocks`  VALUES ( "17","17","main","2015-03-10 11:00:04","8","2015-03-13 16:30:35","1","","");
INSERT INTO `pages_blocks`  VALUES ( "18","18","main","2015-03-10 11:09:27","8","2015-03-13 16:34:53","1","","");


--
-- Tabel structure for table `pages_blocks_cs`
--
DROP TABLE  IF EXISTS `pages_blocks_cs`;
CREATE TABLE `pages_blocks_cs` (
  `block_id` int(11) NOT NULL,
  `block_html` longtext,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages_blocks_cs`  VALUES ( "1","<h1>O n�s</h1><p class=\"lead\">\n	Reality n�s bav�, proto jsme se rozhodli nab�dnout V�m na�i pomoc a dlouholet� zku�enosti na realitn�m trhu p?i prodeji ?i pron�jmu Va�� nemovitosti.</p><hr><p>\n	Jsme si v?domi, �e ot�zka bydlen� je v �ivot? ka�d�ho ?lov?ka jednou z priorit. V�me, �e ?as je to nejdra���, co m�me, proto jsme p?ipraveni v?novat se V�m se 100% nasazen�m, 24hodin, 7dn� v t�dnu.</p><p>\n	Garantujeme V�m rychl�, bezpe?n� a v neposledn� ?ad? tak� p?�jemn� pr?b?h cel�ho obchodu.</p><p>\n	Zajist�me pro V�s prodej ?i pron�jem va�eho domu, bytu, chaty, chalupy, pozemku za nejlep�� mo�nou cenu, v co nejkrat��m ?ase.</p>\n<div class=\"row\">\n	<div class=\"col-sm-6\">\n		<h2>Etick� kodex<br>\n		<small>makl�?e BELARTIS</small></h2>\n		<ul>\n			<li>Makl�? v�dy vykon�v� svoji ?innost profesion�ln?, ?estn?, v souladu s dobr�mi  mravy, obchodn� etikou a platn�mi pr�vn�mi normami ?esk� republiky.</li>\n			<li>Makl�? h�j� z�jmy a pr�va sv�ho klienta dle sv�ho nejlep��ho v?dom� a sv?dom�.</li>\n			<li>Makl�? v�dy jedn� se sv�m klientem korektn?, srozumiteln?, dodr�uje domluven� dohody a je povinen dost�t v�em sv�m z�vazk?m.</li>\n			<li>Makl�? jedn� pouze v rozsahu, kter� mu byl klientem ur?en, nikdy nep?ekra?uje sv� kompetence.</li>\n			<li>Makl�? zachov�v� ml?enlivost v??i t?et�m osob�m o obchodn�ch p?�padech, a to i po jejich ukon?en�.</li>\n			<li>Makl�? si je v?dom sv� odpov?dnosti v procesu obchodov�n� s nemovitostmi, a proto se neust�le zdokonaluje ve v�ech oblastech realitn�ho trhu.</li>\n			<li>Makl�? je vizitkou firmy, buduje dobr� jm�no a zvy�uje presti� realitn� kancel�?e BELARTIS.</li>\n		</ul>\n	</div>	<div class=\"col-sm-6\">\n		<h2>Centr�la spole?nosti<br>\n		<small>Du�n� 112/16, Praha 1 - Star� M?sto, 110 00</small></h2>\n		<div class=\"row\">\n			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/dum.jpg\" title=\"Zv?t�it obr�zek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/dum.jpg\" alt=\"BELARTIS - Reality n�s bav�\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar.jpg\" title=\"Zv?t�it obr�zek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar.jpg\" alt=\"BELARTIS - Reality n�s bav�\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar1.jpg\" title=\"Zv?t�it obr�zek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar1.jpg\" alt=\"BELARTIS - Reality n�s bav�\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar2.jpg\" title=\"Zv?t�it obr�zek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar2.jpg\" alt=\"BELARTIS - Reality n�s bav�\">\n				</a>\n			</div></div></div></div><p class=\"text-center lead\">\n	<br>\n	<a n:href=\"contacts:default\" title=\"Zobrazit podrobn� kontaktn� informace\"><i class=\"fa fa-chevron-right\"></i> Kontaktn� informace</a></p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "2","<h1>Na�e slu�by</h1>\n<div class=\"row services\">\n	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 9\" title=\"V�ce informac� o prodeji nemovitost�\">\n		<i class=\"fa fa-home\"></i><strong>Prodej nemovitost�</strong><small>na n�s se m?�ete spolehnout</small></a></h2>\n		<p>\n			Proces prodeje nemovitosti se skl�d� z cel� ?ady d�l?�ch �kon? a slu�eb. Provedeme V�s cel�m procesem prodeje ?i pron�jmu nemovitosti tak, aby v�e prob?hlo k Va�� pln� spokojenosti.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 10\" title=\"V�ce informac� o pron�jmu nemovitost�\">\n		<i class=\"fa fa-building\"></i><strong>Pron�jem nemovitost�</strong><small>v�e k Va�� spokojenosti</small></a></h2>\n		<p>\n			Pokud pro svoji nemovitost sh�n�te n�jemce, r�di V�m s cel�m procesem pom?�eme.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2><a href=\"http://belartis.strankyrealitky.cz/nase-sluzby/rekonstrukce-nemovitosti\">\n		<i class=\"fa fa-arrow-circle-up\"></i>\n		<strong>Rekonstrukce nemovitost�</strong>&nbsp;</a><a href=\"Pages:default 10\"><small>s n�mi to zvl�dnete</small></a></h2>\n		<p>\n			<a href=\"Pages:default 11\"></a>\n			Zab�v�me se kompletn�mi rekonstrukcemi byt?, rodinn�ch a bytov�ch&nbsp;dom? a projektovou ?innost�.&nbsp;\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 11\" title=\"V�ce informac� o spr�v? nemovitost�\">\n		<i class=\"fa fa-gear\"></i><strong>Spr�va nemovitost�</strong><small>jako by byla na�e vlastn�</small></a></h2>\n		<p>\n			Vlastn�te nemovitost, ale nem�te ?as ?i chu? starat se o jej� spr�vu? Sv??te svoji nemovitost do na�ich rukou, postar�me se o ni jako by byla na�e vlastn�.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 14\" title=\"V�ce informac� o finan?n�ch slu�b�ch\">\n		<i class=\"fa fa-money\"></i><strong>Finan?n� slu�by</strong><small>vy?�d�me&nbsp;hypote?n� �v?r</small></a></h2>\n		<p>\n			Pro v?t�inu lid� je n�kup nemovitosti jedn�m z nejv?t��ch a nejdra���ch rozhodnut� v �ivot?. Uva�ujete-li o koupi nemovitosti a chyb� V�m pot?ebn� finan?n� ?�stka, m?�ete situaci ?e�it pomoc� hypote?n�ho �v?ru nebo �v?ru ze stavebn�ho spo?en�.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 15\" title=\"V�ce informac� o pr�vn�ch slu�b�ch\">\n		<i class=\"fa fa-files-o\"></i><strong>Pr�vn� slu�by</strong><small>pro V� klid a bezpe?�</small></a></h2>\n		<p>\n			Realitn� kancel�? BELARTIS&nbsp;spolupracuje s renomovanou advok�tn� kancel�?�. Advok�tn� kancel�? se zab�v� v�emi �kony spojen�mi s p?evodem a n�jmem nemovitost� od vypracov�n� smlouvy o smlouv? budouc� a� po n�vrh na vklad do katastru nemovitost�.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 13\" title=\"V�ce informac� o slu�b? Homestaging\">\n		<i class=\"fa fa-camera\"></i><strong>Homestaging</strong><small>zv���me �anci na prodej</small></a></h2>\n		<p>\n			Aby byl celkov� dojem z prezentace Va�� nemovitosti co nejlep��, je nutn� V� d?m ?i byt p?ed samotn�m focen�m upravit. Pom?�eme V�m s �klidem a vhodn�m rozm�st?n�m n�bytku tak, aby co nejv�ce vynikl potenci�l prostoru.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<i class=\"fa fa-eye\"></i><strong>Ocen?n� nemovitost�</strong><small>zajist�me tr�n� odhad</small></h2>\n		<p>\n			 Na�e realitn� kancel�? pro v�s zajist� tr�n� odhad Va�� nemovitosti i odhad soudn�m znalcem pro pot?eby finan?n�ho �?adu.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<i class=\"fa fa-thumbs-o-up\"></i><strong>Ostatn� slu�by</strong><small>pro V�s cokoliv</small></h2>\n		<p>\n			Ve spolupr�ci s odborn�ky z dan�ch oblast�, poskytujeme i dal�� slu�by, mezi kter� pat?� nap?�klad architektonick� n�vrhy, projek?n� pr�ce, geodetick� pr�ce ?i&nbsp;energetick� audity.\n		</p>\n	</div></div>");
INSERT INTO `pages_blocks_cs`  VALUES ( "3","<h1>Prodej nemovitost�</h1>\n<p>\n	Proces prodeje nemovitosti se skl�d� z cel� ?ady d�l?�ch �kon? a slu�eb. Provedeme V�s cel�m procesem prodeje ?i pron�jmu nemovitosti tak, aby v�e prob?hlo k Va�� pln� spokojenosti. Mezi na�e z�kladn� slu�by pat?� zejm�na:\n</p>\n<ul class=\"lead\">\n	<li>Prohl�dka Va�� nemovitosti</li>\n	<li>Doporu?en� prodejn� ceny a strategie prodeje</li>\n	<li>Porad�me V�m co zlep�it aby se Va�e nemovitost co nejrychleji a co nejv�hodn?ji prodala</li>\n	<li>Po?�d�me profesion�ln� fotografie </li>\n	<li>Oslov�me klienty z vlastn� rozs�hl� datab�ze</li>\n	<li>Zajist�me kompletn� prezentaci a rozs�hlou inzerci Va�� nemovitosti</li>\n	<li>Ve�kerou komunikaci a prohl�dky se z�jemci ?e��me za V�s</li>\n	<li>Postar�me se o ve�ker� pr�vn� servis</li>\n	<li>P?ed�me Va�i nemovitost nov�mu majiteli</li>\n</ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "4","<h1>Pron�jem nemovitost�</h1>\n<p>\n	Pokud pro svoji nemovitost sh�n�te n�jemce, r�di V�m s cel�m procesem pom?�eme. Mezi na�e z�kladn� slu�by p?i pron�jmu nemovitost� pat?� zejm�na:\n</p>\n<ul class=\"lead\">\n	<li>Prohl�dka Va�� nemovitosti</li>\n	<li>Doporu?en� odpov�daj�c�ho n�jemn�ho pro Va�i nemovitost</li>\n	<li>Porad�me co zlep�it aby se Va�e nemovitost co nejrychleji a co nejv�hodn?ji pronajala</li>\n	<li>Po?�d�me profesion�ln� fotografie Va�� nemovitosti</li>\n	<li>Oslov�me klienty hledaj�c� pron�jem z vlastn� rozs�hl� datab�ze</li>\n	<li>Zajist�me kompletn� prezentaci a rozs�hlou inzerci Va�� nemovitosti</li>\n	<li>Ve�kerou komunikaci a prohl�dky se z�jemci ?e��me za V�s</li>\n	<li>Porad�me s v�b?rem vhodn�ho n�jemn�ka pro Va�i nemovitost</li>\n	<li>P?ed�me Va�i nemovitost nov�mu n�jemn�kovi</li>\n	<li>Zajist�me ve�ker� pr�vn� servis</li>\n	<li>Postar�me se o v�echny z�le�itosti a komunikaci s n�jemn�kem po celou dobu trv�n� n�jemn�ho vztahu</li>\n</ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "5","<h1>Spr�va nemovitost�</h1>\n<p>\n	Vlastn�te nemovitost, ale nem�te ?as ?i chu? starat se o jej� spr�vu? Sv??te svoji nemovitost do na�ich rukou, postar�me se o ni jako by byla na�e vlastn�. Mezi na�e z�kladn� slu�by p?i spr�v? nemovitost� pat?� zejm�na:\n</p>\n<ul class=\"lead\">\n	<li>P?evzet� objektu do spr�vy</li>\n	<li>Zaji�t?n� smluvn�ch dod�vek m�di�</li>\n	<li>�klid nemovitosti</li>\n	<li>B?�nou preventivn� �dr�bu nemovitosti s d?razem na p?edch�zen� vzniku z�vad</li>\n	<li>Drobn� ?emesln� pr�ce</li>\n	<li>Havarijn� slu�by</li>\n</ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "6","<h1>Kari�ra</h1><p class=\"lead\">\n	Bav� V�s reality stejn? jako n�s? R�di V�s p?iv�t�me v na�em t�mu!</p><hr>\n<div class=\"row\">\n	<div class=\"col-sm-6\">\n		<h2>Co nab�z�me?</h2>\n		<ul>\n			<li>Unik�tn� provizn� syst�m � 80% z provize pro V�s</li>\n			<li>Z�zem� modern� kancel�?e v centru Prahy</li>\n			<li>Firemn� v?z za bezkonkuren?n� cenu</li>\n			<li>Mobiln� telefon s neomezen�m tarifem</li>\n			<li>Mo�nost pr�ce z domova </li>\n			<li>Za�kolen� pro nov� makl�?e zdarma</li>\n			<li>Pr?b?�n� obchodn� a odborn� vzd?l�v�n� v oboru</li>\n			<li>Kari�rn� r?st s mo�nost� vybudov�n� vlastn�ho t�mu makl�??</li>\n			<li>Finan?n� nez�vislost</li>\n		</ul>\n	</div>	<div class=\"col-sm-6\">\n		<h2>Co po�adujeme?</h2>\n		<ul>\n			<li>Minim�ln? st?edo�kolsk� vzd?l�n� s maturitou</li>\n			<li>Vysok� pracovn� nasazen� a ?asovou flexibilitu</li>\n			<li>Komunika?n� a vyjedn�vac� dovednosti</li>\n			<li>Seri�zn� vzhled a vystupov�n�</li>\n			<li>Zodpov?dnost, spolehlivost</li>\n			<li>Znalost pr�ce na PC</li>\n			<li>?idi?sk� pr?kaz skupiny B</li>\n		</ul>\n	</div></div><p class=\"text-center\">\n	<br>\nPokud V�s na�e nab�dka zaujala, nev�hejte n�s kontaktovat:</p><p class=\"text-center lead\">\nna email:&nbsp;\n	<a href=\"mailto:mailto:info@belartis.cz\">info@belartis.cz</a> \n	nebo volejte na tel. ?�slo \n	<strong>+420 602 603 604</strong></p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "7","<h1>Realizace</h1><p class=\"text-center\">\n	<img style=\"width: 30%\" src=\"{$instancePath}/img/belartis-logotype.png\" alt=\"BELARTIS - Reality n�s bav�\"></p><p class=\"lead text-center\">\n	P?ipravujeme</p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "8","<p>dsfslkdjdflkds</p><p>sdfsdf</p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "9","<h1>Prodej nemovitost�</h1><p class=\"lead\">\n	Proces prodeje nemovitosti se skl�d� z cel� ?ady d�l?�ch �kon? a slu�eb.</p><hr><p>\nProvedeme V�s cel�m procesem prodeje ?i pron�jmu nemovitosti tak, aby v�e prob?hlo k Va�� pln� spokojenosti. Mezi na�e z�kladn� slu�by pat?� zejm�na:</p><ul>\n	\n<li>Prohl�dka Va�� nemovitosti</li>	\n<li>Doporu?en� prodejn� ceny a strategie prodeje</li>	\n<li>Porad�me V�m co zlep�it aby se Va�e nemovitost co nejrychleji a co nejv�hodn?ji prodala</li>	\n<li>Po?�d�me profesion�ln� fotografie </li>	\n<li>Oslov�me klienty z vlastn� rozs�hl� datab�ze</li>	\n<li>Zajist�me kompletn� prezentaci a rozs�hlou inzerci Va�� nemovitosti</li>	\n<li>Ve�kerou komunikaci a prohl�dky se z�jemci ?e��me za V�s</li>	\n<li>Postar�me se o ve�ker� pr�vn� servis</li>	\n<li>P?ed�me Va�i nemovitost nov�mu majiteli</li></ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "10","<h1>Pron�jem nemovitost�</h1><p class=\"lead\">\nPokud pro svoji nemovitost sh�n�te n�jemce, r�di V�m s cel�m procesem pom?�eme.</p><hr><p>Mezi na�e z�kladn� slu�by p?i pron�jmu nemovitost� pat?� zejm�na:</p><ul>\n	\n<li>Prohl�dka Va�� nemovitosti</li>	\n<li>Doporu?en� odpov�daj�c�ho n�jemn�ho pro Va�i nemovitost</li>	\n<li>Porad�me co zlep�it aby se Va�e nemovitost co nejrychleji a co nejv�hodn?ji pronajala</li>	\n<li>Po?�d�me profesion�ln� fotografie Va�� nemovitosti</li>	\n<li>Oslov�me klienty hledaj�c� pron�jem z vlastn� rozs�hl� datab�ze</li>	\n<li>Zajist�me kompletn� prezentaci a rozs�hlou inzerci Va�� nemovitosti</li>	\n<li>Ve�kerou komunikaci a prohl�dky se z�jemci ?e��me za V�s</li>	\n<li>Porad�me s v�b?rem vhodn�ho n�jemn�ka pro Va�i nemovitost</li>	\n<li>P?ed�me Va�i nemovitost nov�mu n�jemn�kovi</li>	\n<li>Zajist�me ve�ker� pr�vn� servis</li>	\n<li>Postar�me se o v�echny z�le�itosti a komunikaci s n�jemn�kem po celou dobu trv�n� n�jemn�ho vztahu</li></ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "11","<h1>Spr�va nemovitost�</h1>\n<p class=\"lead\">\n	Vlastn�te nemovitost, ale nem�te ?as ?i chu? starat se o jej� spr�vu?&nbsp;\n</p>\n<hr>\n<p>\n	Sv??te svoji nemovitost do na�ich rukou, postar�me se o ni jako by byla na�e vlastn�. Mezi na�e z�kladn� slu�by p?i spr�v? nemovitost� pat?� zejm�na:\n</p>\n<ul>\n	<li>P?evzet� objektu do spr�vy</li>\n	<li>Zaji�t?n� smluvn�ch dod�vek m�di�</li>\n	<li>�klid nemovitosti</li>\n	<li>B?�nou preventivn� �dr�bu nemovitosti s d?razem na p?edch�zen� vzniku z�vad</li>\n	<li>Drobn� ?emesln� pr�ce</li>\n	<li>Havarijn� slu�by</li>\n</ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "12","<h1>Ocen?n� nemovitost�</h1>\n<p>\n	Pot?ebujete zajistit ocen?n� Va�� nemovitosti? Na�e realitn� kancel�? pro v�s zajist� :\n</p>\n<ul class=\"lead\">\n	<li>Tr�n� odhad Va�� nemovitosti</li>\n	<li>Odhad soudn�m znalcem pro pot?eby finan?n�ho �?adu</li>\n</ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "13","<h1>Homestaging</h1><p class=\"lead\">\n	Aby byl celkov� dojem z prezentace Va�� nemovitosti co nejlep��, je nutn� V� d?m ?i byt p?ed samotn�m focen�m upravit.&nbsp;</p><hr>\n<p>\n	Pom?�eme V�m s �klidem a vhodn�m rozm�st?n�m n�bytku tak, aby co nejv�ce vynikl potenci�l prostoru. Byt ?i d?m slad�me v l�biv� celek, kter� bude p?sobit vzdu�n?, modern? a p?ita�liv?.&nbsp;</p><p>\n	V�znamn? t�m nemovitost zatraktivn�me v o?�ch potenci�ln�ch z�jemc? a zv���me tak �anci na rychl� a v�hodn� prodej.</p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "14","<h1>Finan?n� slu�by</h1><p class=\"lead\">\n	Pro v?t�inu lid� je n�kup nemovitosti jedn�m z nejv?t��ch a nejdra���ch rozhodnut� v �ivot?.</p><hr>\n<p>\n	Uva�ujete-li o koupi nemovitosti a chyb� V�m pot?ebn� finan?n� ?�stka, m?�ete situaci ?e�it pomoc� hypote?n�ho �v?ru nebo �v?ru ze stavebn�ho spo?en�. Na�e realitn� kancel�? V�m pom?�e s vy?�zen�m hypote?n�ho �v?ru nebo �v?ru ze stavebn�ho spo?en� u v�ech bankovn�ch �stav? a stavebn�ch spo?itelen p?sob�c�ch na ?esk�m trhu za mimo?�dn? v�hodn�ch podm�nek.</p><h2>BELARTIS hypot�ka � exkluzivn? pro V�s</h2><ul>\n	<li>sjednejte si s n�mi sch?zku na kter� se dozv�te v�e pot?ebn� pro sjedn�n� nejv�hodn?j�� hypot�ky</li>	<li>spolupracujeme se v�emi bankovn�mi �stavy poskytuj�c�mi hypote?n� �v?ry v ?R � m�te tak mo�nost relevantn�ho srovn�n� nab�zen�ch podm�nek a slu�eb z kter�ch si snadno vyberete</li>	<li>na�i hypote?n� specialist� pro V�s v�e za?�d� a z�rove? se o V�s staraj� po celou dobu ?erp�n� �v?ru.</li></ul><p>\n	Pro v�ce informac� jsme V�m k dispozici na tel. ?�sle +420&nbsp;602 603 604 nebo na e-mailu <a href=\"mailto:info@belartis.cz\">info@belartis.cz</a></p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "15","<h1>Pr�vn� slu�by</h1><p class=\"lead\">\n	Realitn� kancel�? BELARTIS dlouhodob? spolupracuje s renomovanou advok�tn� kancel�?�.&nbsp;</p><hr><ul>\n	\n<li>Advok�tn� kancel�? se zab�v� v�emi �kony spojen�mi s p?evodem a n�jmem nemovitost� od vypracov�n� smlouvy o smlouv? budouc� a� po n�vrh na vklad do katastru nemovitost�.</li>	\n<li>Krom? ve�ker�ch smluv zaji�?uje advok�tn� kancel�? pro na�e klienty i advok�tn� �schovu kupn� ceny.</li></ul><p>\n	Pro na�e klienty zpracov�v� ve�kerou smluvn� dokumentaci a realizuje advok�tn� �schovu kupn� ceny JUDr. Jakub Z�myslick�, advok�t se specializac� na pr�vo nemovitost� a stavebnictv�.</p><h2>JUDr. Jakub Z�myslick�</h2><ul><li>Univerzita Karlova, Pr�vnick� fakulta, Praha (2010), titul JUDr.</li><li>Univerzita Karlova, Pr�vnick� fakulta, Praha (2009), titul Mgr.</li><li>National and Kapodistrian University of Athens, At�ny (2008)</li><li>advok�t ev.?. ?AK 14916</li></ul><p>\n	Zam??en�: ob?ansk� pr�vo, bytov� pr�vo (vlastnictv�, n�jem, SVJ), obchodn� pr�vo, stavebn� pr�vo</p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "16","<h1>Rekonstrukce nemovitost�</h1><h3>Kompletn� slu�by p?i rekonstrukci Va�� nemovitosti</h3><hr><p>Rekonstrukce byt?, rodinn�ch a bytov�ch dom?, projektov� ?innost</p><ul><li>Zab�v�me se kompletn�mi rekonstrukcemi byt?, rodinn�ch a bytov�ch dom? a projektovou ?innost�. Byt ?i d?m kter� projde rekonstrukc� se stane hodnotn?j��m, luxusn?j��m a z�sk� v?t�� mno�stv� potenci�ln�ch z�jemc? o koupi ?i pron�jem. Pokud pot?ebujete jen drobn� opravy, �pravy ?i kompletn� p?estavbu na kterou sami nesta?�te, nev�hejte se na n�s obr�tit.&nbsp;</li><li>Zdarma vypracujeme n�vrh mo�n�ho ?e�en� a podrobnou cenovou kalkulaci.&nbsp;</li><li>Samoz?ejmost� jsou kompletn? zhotoven� projekty, revizn� zpr�vy a protokoly, kter� souvisej� s prov�d?nou rekonstrukc� a kter� jsou nezbytn� k n�sledn� kolaudaci.</li></ul>");
INSERT INTO `pages_blocks_cs`  VALUES ( "17","<h1>O n�s</h1><p class=\"lead\">\n	</p><p>Na StrankyRealitky.Cz se m?�ete obr�tit ve v�em, co bude pot?eba pro Va�e podnik�n� vytvo?it. Je dobr� um?t spr�vn? napsat str�nku O n�s, tak aby zaujala, dob?e se ?etla a p?inesla informace, kter� chcete klient?m o sob? sd?lit. Lid� maj� r�di p?�b?hy, m�lo jich zaj�maj� such� ?�sla, nebo Va�e minul� �sp?chy, je proto d?le�it� um?t sd?lit co m?�ete ud?lat pr�v? pro n?. Co m?�ete d?lat pro klienty v budoucnu, ?�m jim pomoci a jak k nim budete p?istupovat.</p><hr>\n<p>My, t�m StrankyRealitky.Cz, jsme se dali na cestu tohoto projektu, proto�e vn�m�me, �e v na�� republice je po?�d m�lo opravdu kvalitn�ch slu�eb a spousta z t?ch, kte?� je zde provozuj�, tak ?asto jedou na setrva?n�k a nijak se nevyv�j�. Kdy� komunikujeme p?i obchodn�ch jedn�n�ch, d?l�me n?jak� projekt, nebo jen n?co nakupujeme na internetu, chceme to ud?lat jednodu�e, kvalitn? a je�t? m�t radost, �e to bylo p?�jemn�.</p><p>To sam� jsme p?enesli do tohoto syst�mu, chceme, aby z�kazn�ci na�ich klient? m?li radost, �e nav�t�vili pr�v? jejich str�nky, �e se jim dob?e orientovalo a b?hem chvilky na�li, co pot?ebovali. Stejn? tak je d?le�it�, aby na�i klienti mohli velmi snadno ovl�dat sv?j syst�m a na p�r kliknut� dok�zali nastavit, vlo�it a pou��vat v�echno, co je pro n? v dan� moment d?le�it�, proto�e kde jsou lid� spokojeni, tam se i vrac�.</p>");
INSERT INTO `pages_blocks_cs`  VALUES ( "18","<h1>Na�e slu�by</h1><p class=\"lead\">\n	</p><p>StrankyRealitky.Cz poskytuj� komplexn� servis za?�naj�c�m i zaveden�m realitn�m kancel�?�m, modern�m samostatn�m makl�??m a posunuj� jejich mo�nosti na prodej nemovitosti na novou �rove?.</p><hr>\n<p>Chcete b�t vid?t? Chcete, aby se o V�s mluvilo? K tomu je pot?eba b�t jin�, ide�ln? lep�� ne� konkurence. My v�m zajist�me, �e grafika str�nek bude jen a ?ist? Va�e vizitka, kde lid� budou r�di hledat sv� nov� vysn?n� bydlen�, proto�e bude p?ehledn� a jednoduch�. Uk�eme V�m jak ud?lat prezentace nemovitost�, na kter� lid� nezapomenou a je�t? si je mezi sebou budou sd�let na soci�ln�ch s�t�ch. Otev?ete se nov�m mo�nostem a p?eneste svoje podnik�n� na novou �rove?.</p><h2>Co od n�s z�sk�te</h2>\n	<div class=\"row services\">\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-eye\"></i>		\n<h3>\n					Modern� Corporate Identity\n				</h3>\n				<p>\n					Zpracujeme logo a cel� vizu�ln� styl Va�� realitn� kancel�?e. Vizitky, ti�t?n� propaga?n� materi�ly, velkoplo�nou reklamu, jednotn� vizu�ln� styl Va�ich strategi�, statistik, smluv a v�ech ostatn�ch dokument?.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-paint-brush\"></i>\n									\n<h3>\n					Modern� design na m�ru\n				</h3>\n				<p>\n					N�mi vytvo?en� str�nky pro realitn� kancel�?e jsou modern�, p?ehledn� a vzbuzuj� d?v?ryhodnost co� jsou kl�?ov� aspekty pro �sp?�n� dokon?en� obchodu. \n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-mobile\"></i>			\n<h3>\n					Spr�vu i na mobilu\n				</h3>\n				<p>\n					Na�e str�nky, intranet i realitn� software m?�ete pohodln? spravovat i na mobiln�ch za?�zen�ch.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-signal\"></i>\n<h3>\n					\n					Exporty\n				</h3>\n				<p>\n					Automaticky exportujeme zvolen� inzer�ty i makl�?e na nejv?t�� ?esk� realitn� port�ly.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-users\"></i>\n							\n<h3>\n					Makl�?sk� �?ty a profily\n				</h3>\n				<p>\n					Spr�va makl�??, v?etn? fotografi�, p?ehled? i profiov�ch str�nek makl�?? V�m pom?�e dostat Va�i realitn� kancel�? na p?edn� m�sta ve vyhled�va?�ch.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-comment\"></i>\n									\n<h3>\n					Popt�vky a jejich p�rov�n�\n				</h3>\n				<p>\n					Evidence popt�vek Va�ich makl�?u i sb?r popt�vek od n�v�t?vn�k? Va�ich str�nek V�m poskytne p?ehled a zajist�, �e o ��dn� potenci�ln� obchod nep?ijdete.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-camera\"></i><h3>\n					Profesion�ln� fotografie a homestagging\n				</h3>\n				<p>\n					Zajist�me profesion�ln� p?�pravu, nasv�cen� a nafocen� Va�ich zak�zek a poskytneme tam Va�im klient?m profesion�ln� prezentaci nemovitost�.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-video-camera\"></i><h3>\n					Profesion�ln� videoprezentace\n				</h3>\n				<p>\n					P?iprav�me, nato?�me, sest?�h�me profesion�ln� videoprezentace nemovitost� v?etn? �vodn� zn?lky Va�� realitn� kancel�?e a dynamick� vizitky makl�?e v z�v?ru videa a n�sledn? publikujeme na sv?tov�ch videoport�lech a tak� p?iprav�me komprese prezentac� pro ?esk� realitn� port�ly. \n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-cubes\"></i><h3>\n					Developersk� projekty\n				</h3>\n				<p>\n					Nab�z�me tak� komponentu pro spr�vu Va�ich developersk�ch projekt?. P?�mo na str�nk�ch Va�� realitn� kancel�?e tak m?�ete prezentovat V�mi prod�van� developersk� projekty v?etn? aktualizac� stav?. Pro jednotliv� projekty tak� vytv�?�me vlastn� webov� prezentace, v?etn? v�ech n�le�itost�, jako jsou vizu�ln� identity projekt?, cen�ky, dispozice, 3D vizualizace, videoprezentace apod.\n				</p>\n			</div>\n</div>");


--
-- Tabel structure for table `pages_blocks_en`
--
DROP TABLE  IF EXISTS `pages_blocks_en`;
CREATE TABLE `pages_blocks_en` (
  `block_id` int(11) NOT NULL,
  `block_html` longtext,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages_blocks_en`  VALUES ( "1","","2015-01-09 10:13:03","1","2015-01-09 11:49:49","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "2","","2015-01-09 12:01:05","1","2015-01-09 12:29:04","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "3","","2015-01-09 12:05:30","1","2015-01-09 12:06:14","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "4","","2015-01-09 12:06:19","1","2015-01-09 12:07:10","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "5","","2015-01-09 12:07:13","1","2015-01-09 12:07:54","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "6","","2015-01-09 12:07:59","1","2015-01-09 12:44:15","1","","");
INSERT INTO `pages_blocks_en`  VALUES ( "7","","2015-01-09 16:14:37","1","2015-01-09 16:14:57","1","","");


--
-- Tabel structure for table `pages_cs`
--
DROP TABLE  IF EXISTS `pages_cs`;
CREATE TABLE `pages_cs` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_slug` varchar(255) DEFAULT NULL,
  `page_html` longtext,
  `page_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages_cs`  VALUES ( "1","O n�s","o-nas","","");
INSERT INTO `pages_cs`  VALUES ( "2","Na�e slu�by","nase-sluzby","","");
INSERT INTO `pages_cs`  VALUES ( "3","Prodej nemovitost�","prodej-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "4","Pron�jem nemovitost�","pronajem-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "5","Spr�va nemovitost�","sprava-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "6","Kari�ra","kariera","","");
INSERT INTO `pages_cs`  VALUES ( "7","Realizace","realizace","","");
INSERT INTO `pages_cs`  VALUES ( "8","Testovac� str�nky","testovaci-stranky","","");
INSERT INTO `pages_cs`  VALUES ( "9","Prodej nemovitost�","prodej-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "10","Pron�jem nemovitost�","pronajem-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "11","Spr�va nemovitost�","sprava-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "12","Ocen?n� nemovitost�","oceneni-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "13","Homestaging","homestaging","","");
INSERT INTO `pages_cs`  VALUES ( "14","Finan?n� slu�by","financni-sluzby","","");
INSERT INTO `pages_cs`  VALUES ( "15","Pr�vn� slu�by","pravni-sluzby","","");
INSERT INTO `pages_cs`  VALUES ( "16","Rekonstrukce nemovitost�","rekonstrukce-nemovitosti","","");
INSERT INTO `pages_cs`  VALUES ( "17","O n�s","","","");
INSERT INTO `pages_cs`  VALUES ( "18","Na�e slu�by","","","");


--
-- Tabel structure for table `pages_en`
--
DROP TABLE  IF EXISTS `pages_en`;
CREATE TABLE `pages_en` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_slag` varchar(255) DEFAULT NULL,
  `page_html` longtext,
  `page_template` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages_en`  VALUES ( "1","About us","about-us","","","2015-01-09 10:13:03","1","2015-01-09 11:49:49","1","","");
INSERT INTO `pages_en`  VALUES ( "2","Our services","our-services","","","2015-01-09 12:01:05","1","2015-01-09 12:29:04","1","","");
INSERT INTO `pages_en`  VALUES ( "3","","","","","2015-01-09 12:05:30","1","2015-01-09 12:06:14","1","","");
INSERT INTO `pages_en`  VALUES ( "4","","","","","2015-01-09 12:06:19","1","2015-01-09 12:07:10","1","","");
INSERT INTO `pages_en`  VALUES ( "5","","","","","2015-01-09 12:07:13","1","2015-01-09 12:07:54","1","","");
INSERT INTO `pages_en`  VALUES ( "6","","","","","2015-01-09 12:07:59","1","2015-01-09 12:44:15","1","","");
INSERT INTO `pages_en`  VALUES ( "7","","","","","2015-01-09 16:14:37","1","2015-01-09 16:14:57","1","","");


--
-- Tabel structure for table `pages_structure`
--
DROP TABLE  IF EXISTS `pages_structure`;
CREATE TABLE `pages_structure` (
  `structure_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_structure_id` int(11) DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `structure_rank` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`structure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Tabel structure for table `portals`
--
DROP TABLE  IF EXISTS `portals`;
CREATE TABLE `portals` (
  `portal_id` int(11) NOT NULL AUTO_INCREMENT,
  `portal_title` varchar(255) DEFAULT NULL,
  `portal_classname` varchar(255) DEFAULT NULL,
  `portal_url` varchar(255) DEFAULT NULL,
  `portal_active` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `export_broker` tinyint(4) DEFAULT NULL,
  `portal_settings` text,
  `portal_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`portal_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `portals`  VALUES ( "1","Sreality","srealityExport","www.sreality.cz","1","2015-04-15 18:22:28","","2015-04-23 11:59:12","","","","1","[\"sreality_client_id\",\"sreality_password\",\"sreality_key\"]","sreality");
INSERT INTO `portals`  VALUES ( "2","Reality Idnes","idnesExport","reality.idnes.cz","1","2015-04-15 18:23:14","","2015-04-23 11:59:16","","","","0","[\"idnes_ftp_user\",\"idnes_ftp_password\",\"idnes_import_login\",\"idnes_import_password\"]","idnes");
INSERT INTO `portals`  VALUES ( "3","Reality MIX","realityMixExport","www.realitymix.cz","1","2015-04-21 16:59:36","","2015-04-23 17:12:48","","","","1","[\"realitymix_client_id\",\"realitymix_password\",\"realitymix_key\"]","realitymix");
INSERT INTO `portals`  VALUES ( "4","Real City","realCityExport","www.realcity.cz","1","2015-04-24 15:16:23","","2015-04-24 15:19:01","","","","1","[\"realcity_client_id\",\"realcity_password\",\"realcity_key\"]","realcity");


--
-- Tabel structure for table `projects`
--
DROP TABLE  IF EXISTS `projects`;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `project_address` varchar(255) DEFAULT NULL,
  `project_latitude` double DEFAULT NULL,
  `project_longitude` double DEFAULT NULL,
  `project_established` date DEFAULT NULL,
  `project_finished` date DEFAULT NULL,
  `project_status` varchar(255) NOT NULL DEFAULT 'new',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `project_rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `projects`  VALUES ( "1","2","U z�vor 1367, Neratovice","50.2544773","14.5043104","2015-03-05","2015-05-08","progress","2015-03-30 15:41:53","19","2015-04-13 12:41:34","19","","","1");
INSERT INTO `projects`  VALUES ( "2","2","","49.8174366199","15.474335291","2015-03-07","2015-06-19","prepare","2015-03-30 17:31:26","19","2015-04-13 12:42:10","19","","","2");
INSERT INTO `projects`  VALUES ( "3","2","Houston","29.7604267","95.3698028","2015-03-19","2015-06-11","sale","2015-03-30 17:50:59","19","2015-03-30 20:37:32","19","","","");
INSERT INTO `projects`  VALUES ( "4","2","","","","","","prepare","2015-04-13 15:08:54","20","2015-04-14 16:38:26","","2015-04-14 16:38:26","8","3");


--
-- Tabel structure for table `projects_brokers`
--
DROP TABLE  IF EXISTS `projects_brokers`;
CREATE TABLE `projects_brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `projects_brokers`  VALUES ( "1","1","19","2015-03-30 15:47:06","19","2015-04-13 12:41:34","19","","");
INSERT INTO `projects_brokers`  VALUES ( "2","1","22","2015-03-30 18:47:44","20","2015-04-13 12:41:34","19","","");
INSERT INTO `projects_brokers`  VALUES ( "3","3","28","2015-03-30 18:53:28","20","2015-03-30 20:37:32","19","","");
INSERT INTO `projects_brokers`  VALUES ( "4","3","27","2015-03-30 18:53:28","20","2015-03-30 20:37:32","19","","");


--
-- Tabel structure for table `projects_cs`
--
DROP TABLE  IF EXISTS `projects_cs`;
CREATE TABLE `projects_cs` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_description` longtext,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `projects_cs`  VALUES ( "1","Lucky Gardens","<p> Curabitur tempor feugiat nisi, at finibus sapien. Curabitur ultrices  imperdiet mauris ut accumsan. Sed euismod, dolor ac eleifend dictum, leo  dui hendrerit dui, id consectetur orci quam in orci. Etiam lobortis  augue a venenatis suscipit. Proin placerat ullamcorper porta. Quisque  rutrum id nunc at egestas. Fusce erat risus, commodo vel mauris id,  pellentesque congue erat. Nunc in hendrerit orci, eget vestibulum elit.  Mauris maximus nunc ac augue fermentum, eu dapibus elit ornare. Duis  viverra bibendum auctor. Etiam in euismod magna, accumsan placerat nisl.  Quisque lacinia rhoncus justo, eget lobortis nulla. Phasellus vel  feugiat nulla.</p><p> Nullam aliquam ipsum est. Praesent ac lobortis ipsum. Etiam bibendum,  augue vitae sagittis cursus, felis neque mattis urna, sed aliquet ipsum  justo non massa. Fusce faucibus nisi justo, vitae facilisis metus  vestibulum vitae. Integer laoreet metus semper tempus ultrices. Nam in  enim vitae nibh efficitur faucibus vitae eu metus. Duis maximus porta  est eu elementum. Suspendisse pretium sollicitudin lacus, at accumsan  est fermentum sit amet. Vestibulum consequat at ligula non suscipit.  Vestibulum et eleifend enim, interdum sodales felis. Ut nisi ligula,  accumsan vitae finibus quis, luctus ut eros. Morbi venenatis, turpis sed  ultrices cursus, erat ipsum pharetra risus, dapibus vestibulum arcu  lacus ac felis. Nulla ac eros quis lacus vestibulum varius. Praesent  eget nunc dolor. Pellentesque id mi dolor.</p><p> Donec viverra dui nisl, eget faucibus libero vestibulum at. Suspendisse  vehicula augue mauris, vitae ultrices quam fringilla ut. Pellentesque  mauris orci, tristique a libero in, tempus ullamcorper risus. Nulla  facilisi. Mauris eget fringilla lectus. Proin semper arcu nunc, quis  maximus tortor congue id. Nulla volutpat augue orci, ac vulputate ligula  accumsan et. Ut sit amet porttitor nisl, eu maximus erat. Suspendisse  sit amet urna a purus eleifend congue.</p>");
INSERT INTO `projects_cs`  VALUES ( "2","Luxury town","<p> Suspendisse nec dignissim mauris. Curabitur eget ante ut enim rhoncus  sodales. Pellentesque viverra lorem urna, eget lacinia eros lobortis  non. Donec commodo ultricies aliquam. Aliquam sed accumsan nisl, in  ultricies erat. Morbi ut tortor ac nibh feugiat condimentum sed non leo.  Nam a risus justo. Proin id scelerisque dolor. Proin id fermentum elit.  Etiam at arcu sit amet quam volutpat gravida sed et ex. Phasellus  tristique eget magna ac venenatis. In hac habitasse platea dictumst.  Proin fermentum arcu nibh, sed blandit turpis tempus non. Sed sed  consectetur velit. Pellentesque feugiat sapien sed mauris ultrices, non  dignissim nisi faucibus. Nulla varius ligula nisl, eu blandit quam  congue in.</p><p> Mauris sed est dignissim, venenatis libero lacinia, placerat turpis.  Vivamus suscipit, nisl eget cursus aliquam, ex odio fermentum metus, sit  amet semper felis tellus et enim. Pellentesque habitant morbi tristique  senectus et netus et malesuada fames ac turpis egestas. Nunc congue dui  nisi, ut luctus nibh scelerisque a. Aliquam lobortis gravida  consectetur. Maecenas sit amet enim ornare, volutpat risus in, rhoncus  tellus. Vestibulum viverra convallis augue, ut lobortis risus fermentum  eu. Proin blandit, metus non tempor consequat, odio risus imperdiet  massa, in tincidunt nisl nisi ac nibh. Donec quis malesuada erat.  Aliquam sed velit ut odio rutrum aliquam. Donec et auctor nulla, sit  amet efficitur ex. Sed ut mi ac turpis aliquet faucibus ut id ante.</p>");
INSERT INTO `projects_cs`  VALUES ( "3","Billy Hills","<p>Tento projekt p?edstavuje mo�nost pohodln�ho bydlen� v n�dhern� p?�rodn� scen�rii vrchoviny Billy Hills</p>");
INSERT INTO `projects_cs`  VALUES ( "4","Nov� projekt","");


--
-- Tabel structure for table `roles`
--
DROP TABLE  IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role` varchar(30) NOT NULL,
  `parent_role` varchar(30) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles`  VALUES ( "admin","superAdmin","2014-11-04 10:47:45","0","","","","");
INSERT INTO `roles`  VALUES ( "broker","admin","2014-11-04 10:47:45","0","","","","");
INSERT INTO `roles`  VALUES ( "guest","broker","2014-11-04 10:47:45","0","","","","");
INSERT INTO `roles`  VALUES ( "superAdmin","","2014-11-04 10:47:45","0","","","","");


--
-- Tabel structure for table `roles_cs`
--
DROP TABLE  IF EXISTS `roles_cs`;
CREATE TABLE `roles_cs` (
  `role` varchar(30) NOT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles_cs`  VALUES ( "admin","Spr�vce");
INSERT INTO `roles_cs`  VALUES ( "broker","Makl�?");
INSERT INTO `roles_cs`  VALUES ( "guest","Host");
INSERT INTO `roles_cs`  VALUES ( "superAdmin","Program�tor");


--
-- Tabel structure for table `roles_en`
--
DROP TABLE  IF EXISTS `roles_en`;
CREATE TABLE `roles_en` (
  `role` varchar(30) NOT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles_en`  VALUES ( "Admin","Administrator","2014-11-04 10:48:22","0","","","","");
INSERT INTO `roles_en`  VALUES ( "broker","Broker","2014-11-04 10:48:22","0","","","","");
INSERT INTO `roles_en`  VALUES ( "guest","Guest","2014-11-04 10:48:22","0","","","","");
INSERT INTO `roles_en`  VALUES ( "superAdmin","Programer","2014-11-04 10:48:22","0","","","","");


--
-- Tabel structure for table `slugs`
--
DROP TABLE  IF EXISTS `slugs`;
CREATE TABLE `slugs` (
  `slug_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `slug_action` varchar(255) DEFAULT NULL,
  `slug_parameters` text,
  `slug_available` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `slug_meta_author` varchar(255) DEFAULT NULL,
  `slug_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

INSERT INTO `slugs`  VALUES ( "1","","","Estates:default","","1","2015-01-22 16:21:51","","2015-05-11 15:10:39","","","","","","ccd52a8e8a4b59587a5e7fd2ed6bf9d2");
INSERT INTO `slugs`  VALUES ( "2","pages","1","Pages:default","{\"page_id\":1}","1","2015-01-22 16:21:51","","2015-05-11 15:10:39","","","","1","","0c044734422a02875322048c03bcf4aa");
INSERT INTO `slugs`  VALUES ( "3","pages","2","Pages:default","{\"page_id\":2}","1","2015-01-22 16:21:51","","2015-05-11 15:10:39","","","","1","","577860d0d46917d106d86c4e1f2b52d1");
INSERT INTO `slugs`  VALUES ( "4","pages","6","Pages:default","{\"page_id\":6}","1","2015-01-22 16:21:51","","2015-05-11 15:10:39","","","","1","","7f0b7965df41281d6ea67fc517145591");
INSERT INTO `slugs`  VALUES ( "5","","","Demands:default","","1","2015-01-22 16:21:51","","2015-05-11 15:10:40","","","","","","03ec03d59d0e9594cd6a92b08ce59cd4");
INSERT INTO `slugs`  VALUES ( "6","","","Contacts:default","","1","2015-01-22 16:21:51","","2015-05-11 15:10:40","","","","","","03dfc94c5e6a759dc7091f4b07ecb57b");
INSERT INTO `slugs`  VALUES ( "7","pages","7","Pages:default","{\"page_id\":7}","1","2015-01-22 16:21:51","","2015-05-11 15:10:40","","","","1","","049aad9800ae4a7a30eca00aeca061cd");
INSERT INTO `slugs`  VALUES ( "8","pages","5","Pages:default","{\"page_id\":5}","1","2015-01-22 16:21:51","","2015-05-11 15:10:40","","","","","","5b31a32ebdf7040a9e580a6e3d25f488");
INSERT INTO `slugs`  VALUES ( "9","pages","6","Pages:default","{\"page_id\":6}","1","2015-01-22 16:21:51","","2015-05-11 15:10:40","","","","","","7f0b7965df41281d6ea67fc517145591");
INSERT INTO `slugs`  VALUES ( "10","pages","7","Pages:default","{\"page_id\":7}","1","2015-01-22 16:21:51","","2015-05-11 15:10:41","","","","","","049aad9800ae4a7a30eca00aeca061cd");
INSERT INTO `slugs`  VALUES ( "11","","","Estates:default","{\"advert_type\":1}","1","2015-01-22 16:21:51","","2015-05-11 15:10:41","","","","","","437e45f4f9e7d39feec7e46f5c0c8d4b");
INSERT INTO `slugs`  VALUES ( "12","","","Estates:default","{\"advert_type\":1,\"advert_function\":1}","1","2015-01-22 16:21:51","","2015-05-11 15:10:41","","","","","","151a78e61108af19ac603edec51bb277");
INSERT INTO `slugs`  VALUES ( "13","","","Estates:default","{\"advert_type\":1,\"advert_function\":2}","1","2015-01-22 16:21:51","","2015-05-11 15:10:41","","","","","","defee534ae2c00473088d97bc2c11ac1");
INSERT INTO `slugs`  VALUES ( "14","","","Estates:default","{\"advert_type\":1,\"advert_function\":3}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","610b68d857d15337fd1e3572b3cf6a27");
INSERT INTO `slugs`  VALUES ( "15","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":2}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","a106ac7c27b7c9f1ee2ac359c49d6356");
INSERT INTO `slugs`  VALUES ( "16","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":3}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","9a0b278324be95e404c302275c11bab6");
INSERT INTO `slugs`  VALUES ( "17","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":4}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","faac0f1879beb07bb36416cc93306998");
INSERT INTO `slugs`  VALUES ( "18","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":5}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","34b00aa8a48c496ecaced728863b0ead");
INSERT INTO `slugs`  VALUES ( "19","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":6}","1","2015-01-22 16:21:51","","2015-05-11 15:10:42","","","","","","f9e2b28637a78a46f11a8dcf2b8b94e6");
INSERT INTO `slugs`  VALUES ( "20","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":7}","1","2015-01-22 16:21:51","","2015-05-11 15:10:43","","","","","","afc1da9dac9876ad3d1d31fc69ad9d13");
INSERT INTO `slugs`  VALUES ( "21","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":8}","1","2015-01-22 16:21:51","","2015-05-11 15:10:43","","","","","","82f8a5805934484e9079c1a9b869f78b");
INSERT INTO `slugs`  VALUES ( "22","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":9}","1","2015-01-22 16:21:52","","2015-05-11 15:10:43","","","","","","395d0315302bc2bec0eb1777bf7ca58c");
INSERT INTO `slugs`  VALUES ( "23","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":10}","1","2015-01-22 16:21:52","","2015-05-11 15:10:43","","","","","","7c576cb7f42c76aebe0c0a77cb8e437d");
INSERT INTO `slugs`  VALUES ( "24","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":11}","1","2015-01-22 16:21:52","","2015-05-11 15:10:43","","","","","","a0267b27cd5e78b92b5f2dacc73c8671");
INSERT INTO `slugs`  VALUES ( "25","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":12}","1","2015-01-22 16:21:52","","2015-05-11 15:10:43","","","","","","cd2a699e554fb6dbd471138df12d1826");
INSERT INTO `slugs`  VALUES ( "26","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":16}","1","2015-01-22 16:21:52","","2015-05-11 15:10:43","","","","","","7ce95a337d8521d04c9283209a61a437");
INSERT INTO `slugs`  VALUES ( "27","","","Estates:default","{\"advert_type\":1,\"advert_subtype\":47}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","f6dc9f410c454f7ffc4e1d040118c25d");
INSERT INTO `slugs`  VALUES ( "28","","","Estates:default","{\"advert_type\":2}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","dd4833f1034b69e842c0b5cbb2bad00a");
INSERT INTO `slugs`  VALUES ( "29","","","Estates:default","{\"advert_type\":2,\"advert_function\":1}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","3946e8ba518323c88a17d4a0c15ed36a");
INSERT INTO `slugs`  VALUES ( "30","","","Estates:default","{\"advert_type\":2,\"advert_function\":2}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","ae5061ab13fe544b606bbf043a791389");
INSERT INTO `slugs`  VALUES ( "31","","","Estates:default","{\"advert_type\":2,\"advert_function\":3}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","227d351711a70c07cc69982b30dd1d95");
INSERT INTO `slugs`  VALUES ( "32","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":33}","1","2015-01-22 16:21:52","","2015-05-11 15:10:44","","","","","","2e10884bb835b777b1725b6bd0c8ddd2");
INSERT INTO `slugs`  VALUES ( "33","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":35}","1","2015-01-22 16:21:52","","2015-05-11 15:10:45","","","","","","79d26526e29c2d0e1b8892439e58642b");
INSERT INTO `slugs`  VALUES ( "34","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":37}","1","2015-01-22 16:21:52","","2015-05-11 15:10:45","","","","","","755e2feaac81a0b5cbf5703b527024e2");
INSERT INTO `slugs`  VALUES ( "35","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":39}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","88ca386edfde538a0cb1af8b9603ae3a");
INSERT INTO `slugs`  VALUES ( "36","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":40}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","1cbb0920f2dbb2d0f2035505a849e5ce");
INSERT INTO `slugs`  VALUES ( "37","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":43}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","aa6382ad22b0afd6955127ef4ea2ff07");
INSERT INTO `slugs`  VALUES ( "38","","","Estates:default","{\"advert_type\":2,\"advert_subtype\":44}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","689e685c6a2622debd3254dd1ea1a6a7");
INSERT INTO `slugs`  VALUES ( "39","","","Estates:default","{\"advert_type\":3}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","26a07bcbfd9c424f70d1952263ce55f4");
INSERT INTO `slugs`  VALUES ( "40","","","Estates:default","{\"advert_type\":3,\"advert_function\":1}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","bb79e5048b7992200299f122dba26203");
INSERT INTO `slugs`  VALUES ( "41","","","Estates:default","{\"advert_type\":3,\"advert_function\":2}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","733828fc6feed1bd710de98828059dd6");
INSERT INTO `slugs`  VALUES ( "42","","","Estates:default","{\"advert_type\":3,\"advert_function\":3}","1","2015-01-22 16:21:52","","2015-05-11 15:10:46","","","","","","b5c49a4558c41c921b0934126a68bbae");
INSERT INTO `slugs`  VALUES ( "43","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":18}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","0126cce4c52815f3b1a20d5646161bed");
INSERT INTO `slugs`  VALUES ( "44","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":19}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","fbe32f9287649afde3c364e04dbe731b");
INSERT INTO `slugs`  VALUES ( "45","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":20}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","7ff861f45167980cca011235a3ea6a03");
INSERT INTO `slugs`  VALUES ( "46","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":21}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","5c96f149db736d9247f0e8f97feb6222");
INSERT INTO `slugs`  VALUES ( "47","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":22}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","2b001aec70670705d5cb7a45d718594e");
INSERT INTO `slugs`  VALUES ( "48","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":23}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","cd048b3a3a46eaf221c3ad93a7f61472");
INSERT INTO `slugs`  VALUES ( "49","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":24}","1","2015-01-22 16:21:52","","2015-05-11 15:10:47","","","","","","5dadcc716de666b968c75779c08b577a");
INSERT INTO `slugs`  VALUES ( "50","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":46}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","b747eed2471b75a1a2504773b70cb590");
INSERT INTO `slugs`  VALUES ( "51","","","Estates:default","{\"advert_type\":3,\"advert_subtype\":48}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","a5a1556cb43950c5b638c81a3bdd1326");
INSERT INTO `slugs`  VALUES ( "52","","","Estates:default","{\"advert_type\":4}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","ff60651dd8aac1c1255e37159cc18e64");
INSERT INTO `slugs`  VALUES ( "53","","","Estates:default","{\"advert_type\":4,\"advert_function\":1}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","9287ccc04442ca9fddd19cece6a0b383");
INSERT INTO `slugs`  VALUES ( "54","","","Estates:default","{\"advert_type\":4,\"advert_function\":2}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","568080e9e462260712fcc5b8140b9fa7");
INSERT INTO `slugs`  VALUES ( "55","","","Estates:default","{\"advert_type\":4,\"advert_function\":3}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","f3276ca414a684bb126938cb0fe2ab1d");
INSERT INTO `slugs`  VALUES ( "56","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":25}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","beca7fd9034cef5efca2b4549a661a61");
INSERT INTO `slugs`  VALUES ( "57","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":26}","1","2015-01-22 16:21:52","","2015-05-11 15:10:48","","","","","","42c10e5481b6cb90a4841c67ff44f2be");
INSERT INTO `slugs`  VALUES ( "58","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":27}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","bcc24dfe5f10550655b46a20d89b99f5");
INSERT INTO `slugs`  VALUES ( "59","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":28}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","e9896eeb6431e8426b3bb2dc9aa15b42");
INSERT INTO `slugs`  VALUES ( "60","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":29}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","ac51dfda7a782ac4c1337e5c478fd0c1");
INSERT INTO `slugs`  VALUES ( "61","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":30}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","6e8ee87d32ca0cebd736f998a9d2ccab");
INSERT INTO `slugs`  VALUES ( "62","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":31}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","aeaaf503f3ce709ed99e8959b7e2126b");
INSERT INTO `slugs`  VALUES ( "63","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":32}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","cd0eb2967ae30ac7fbd16c1c218585fc");
INSERT INTO `slugs`  VALUES ( "64","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":38}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","4a09f21d68fe20d1a03e860f38f348b1");
INSERT INTO `slugs`  VALUES ( "65","","","Estates:default","{\"advert_type\":4,\"advert_subtype\":49}","1","2015-01-22 16:21:52","","2015-05-11 15:10:49","","","","","","b17dd652eb29b64750d3ae81ce2ab50c");
INSERT INTO `slugs`  VALUES ( "66","","","Estates:default","{\"advert_type\":5}","1","2015-01-22 16:21:52","","2015-05-11 15:10:50","","","","","","ea9b336b44f6b8083f0b1927324f312e");
INSERT INTO `slugs`  VALUES ( "67","","","Estates:default","{\"advert_type\":5,\"advert_function\":1}","1","2015-01-22 16:21:52","","2015-05-11 15:10:50","","","","","","a84bd15af34f5d9157794bb99fd6c91a");
INSERT INTO `slugs`  VALUES ( "68","","","Estates:default","{\"advert_type\":5,\"advert_function\":2}","1","2015-01-22 16:21:52","","2015-05-11 15:10:50","","","","","","240b0abfb5935d5d0ae74d4f130c137d");
INSERT INTO `slugs`  VALUES ( "69","","","Estates:default","{\"advert_type\":5,\"advert_function\":3}","1","2015-01-22 16:21:52","","2015-05-11 15:10:50","","","","","","15885ddde94f4e2924b37c23f31b571a");
INSERT INTO `slugs`  VALUES ( "70","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":34}","1","2015-01-22 16:21:53","","2015-05-11 15:10:50","","","","","","39a83d9dd22d85ab16ef5dcf6356e793");
INSERT INTO `slugs`  VALUES ( "71","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":36}","1","2015-01-22 16:21:53","","2015-05-11 15:10:50","","","","","","1578aa65e16ddbb0b251413bc7d4a82f");
INSERT INTO `slugs`  VALUES ( "72","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":50}","1","2015-01-22 16:21:53","","2015-05-11 15:10:50","","","","","","cccfa1b8aef1fe0fb3cfeaa51e6ca66d");
INSERT INTO `slugs`  VALUES ( "73","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":51}","1","2015-01-22 16:21:53","","2015-05-11 15:10:51","","","","","","da95df29ce03d4eefad2444b4f704388");
INSERT INTO `slugs`  VALUES ( "74","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":52}","1","2015-01-22 16:21:53","","2015-05-11 15:10:51","","","","","","0fcc041e2173c3a520bfb5f799b51f43");
INSERT INTO `slugs`  VALUES ( "75","","","Estates:default","{\"advert_type\":5,\"advert_subtype\":53}","1","2015-01-22 16:21:53","","2015-05-11 15:10:51","","","","","","b738081ef0b6bfdb9119a322be4bc1d5");
INSERT INTO `slugs`  VALUES ( "76","estates","3","Estates:detail","{\"estate_id\":3}","0","2015-01-22 21:07:38","9","2015-05-11 15:10:51","","","","1","","69c7952bf4a9564361ef029a6deb51e5");
INSERT INTO `slugs`  VALUES ( "77","estates","5","Estates:detail","{\"estate_id\":5}","0","2015-01-22 21:18:17","9","2015-05-11 15:10:51","","","","1","","3c93fe5d591778dfc0ac504de5361a71");
INSERT INTO `slugs`  VALUES ( "78","estates","9","Estates:detail","{\"estate_id\":9}","0","2015-01-22 22:03:14","11","2015-05-11 15:10:51","","","","1","","2c5877300d4407c0af2a8a264a7f13e4");
INSERT INTO `slugs`  VALUES ( "79","estates","6","Estates:detail","{\"estate_id\":6}","0","2015-01-23 13:29:04","9","2015-05-11 15:10:51","","","","1","","f32e60ef0b84f16a556c755f043e84aa");
INSERT INTO `slugs`  VALUES ( "80","pages","9","Pages:default","{\"page_id\":9}","1","2015-01-23 15:47:25","8","2015-05-11 15:10:52","","","","1","","e8a2ac597aae4437cb00e3228085dc20");
INSERT INTO `slugs`  VALUES ( "81","pages","10","Pages:default","{\"page_id\":10}","1","2015-01-23 15:49:30","8","2015-05-11 15:10:52","","","","1","","448bf998ed9686a17992f7ba020add38");
INSERT INTO `slugs`  VALUES ( "82","pages","11","Pages:default","{\"page_id\":11}","1","2015-01-23 17:45:05","8","2015-05-11 15:10:52","","","","1","","311a4c1e5441ebf4b45da2854c84f739");
INSERT INTO `slugs`  VALUES ( "83","pages","13","Pages:default","{\"page_id\":13}","1","2015-01-23 17:48:23","8","2015-05-11 15:10:52","","","","1","","d27b838daa16bbe42a8907caa3f596a8");
INSERT INTO `slugs`  VALUES ( "84","pages","14","Pages:default","{\"page_id\":14}","1","2015-01-23 17:49:54","8","2015-05-11 15:10:52","","","","1","","e10d0656ad3da71c90826bd3ce692bc8");
INSERT INTO `slugs`  VALUES ( "85","pages","15","Pages:default","{\"page_id\":15}","1","2015-01-23 17:50:37","8","2015-05-11 15:10:52","","","","1","","aa27208578aa12860c3efaead7a61871");
INSERT INTO `slugs`  VALUES ( "86","estates","8","Estates:detail","{\"estate_id\":8}","0","2015-01-23 18:25:35","9","2015-05-11 15:10:52","","","","1","","04c7a90403c3b133ab0b7088cb1cf35d");
INSERT INTO `slugs`  VALUES ( "87","estates","4","Estates:detail","{\"estate_id\":4}","0","2015-01-23 18:46:10","8","2015-05-11 15:10:52","","","","1","","95901dd4034ac4be8ae1f74b63c091cf");
INSERT INTO `slugs`  VALUES ( "88","estates","7","Estates:detail","{\"estate_id\":7}","0","2015-01-25 18:44:17","9","2015-05-11 15:10:53","","","","1","","7658160dfa20194836527f4bd9dffad4");
INSERT INTO `slugs`  VALUES ( "89","pages","16","Pages:default","{\"page_id\":16}","1","2015-02-02 10:05:26","9","2015-05-11 15:10:53","","","","1","","1cc809e56de09e967af5ee09109c2679");
INSERT INTO `slugs`  VALUES ( "90","estates","12","Estates:detail","{\"estate_id\":12}","0","2015-02-22 22:02:57","15","2015-05-11 15:10:53","","","","1","","bf9e0717a8018571d2a94bfc59293a50");
INSERT INTO `slugs`  VALUES ( "91","pages","17","Pages:default","{\"page_id\":17}","1","2015-03-10 11:00:04","8","2015-05-11 15:10:53","","","","2","","3f3ee05ab11ebe6768ccc9b443a10112");
INSERT INTO `slugs`  VALUES ( "92","pages","18","Pages:default","{\"page_id\":18}","1","2015-03-10 11:09:28","8","2015-05-11 15:10:53","","","","2","","c02fa361665584506c5175677176b6a3");
INSERT INTO `slugs`  VALUES ( "93","estates","13","Estates:detail","{\"estate_id\":13}","0","2015-03-10 15:33:19","19","2015-04-22 16:22:51","1","2015-04-22 16:22:51","1","2","","");
INSERT INTO `slugs`  VALUES ( "94","estates","14","Estates:detail","{\"estate_id\":14}","0","2015-03-10 16:28:17","19","2015-05-11 15:10:58","","","","2","","9693d45b4df66c488caf4e8dcd07c80f");
INSERT INTO `slugs`  VALUES ( "95","estates","15","Estates:detail","{\"estate_id\":15}","0","2015-03-10 16:35:09","19","2015-05-11 15:10:58","","","","2","","6714750f8ecbd7bd7960078716e500ac");
INSERT INTO `slugs`  VALUES ( "96","estates","16","Estates:detail","{\"estate_id\":16}","0","2015-03-10 16:54:57","19","2015-05-11 15:10:59","","","","2","","ea8c7cfbeca22a8a8eb253ef046a1463");
INSERT INTO `slugs`  VALUES ( "97","projects","","Projects:default","","1","2015-03-30 15:23:41","","2015-05-11 15:10:54","","","","","","e739ed60dc7c92e164c19f7cd4c56d79");
INSERT INTO `slugs`  VALUES ( "98","projects","1","Projects:detail","{\"project_id\":1}","0","2015-03-30 15:41:53","19","2015-05-11 15:10:54","","","","2","","9d4859c0da6098c1567c571079429d8c");
INSERT INTO `slugs`  VALUES ( "99","articles","1","Articles:detail","{\"article_id\":1}","0","2015-03-30 16:15:46","19","2015-05-11 15:10:54","","","","2","","1e4ac85e10917bf47d252c5437146fcf");
INSERT INTO `slugs`  VALUES ( "100","projects","2","Projects:detail","{\"project_id\":2}","0","2015-03-30 17:31:26","19","2015-05-11 15:10:54","","","","2","","a2bae6b65a9325ccf46239c29596b1df");
INSERT INTO `slugs`  VALUES ( "101","projects","3","Projects:detail","{\"project_id\":3}","0","2015-03-30 17:50:59","19","2015-05-11 15:10:54","","","","2","","0c24150d1e443587b383c4a93de73a4a");
INSERT INTO `slugs`  VALUES ( "102","articles","2","Articles:detail","{\"article_id\":2}","0","2015-03-30 18:55:05","20","2015-05-11 15:10:54","","","","2","","c485a273eb78f783dc10168fbefe31b4");
INSERT INTO `slugs`  VALUES ( "103","brokerprofiles","19","BrokerProfiles:detail","{\"user_id\":19}","0","2015-04-13 11:46:05","19","2015-05-11 15:10:54","","","","2","","ecdd858f5ab45798b0981473cba69773");
INSERT INTO `slugs`  VALUES ( "104","projects","4","Projects:detail","{\"project_id\":4}","0","2015-04-13 15:08:54","20","2015-05-11 15:10:54","","","","2","","edddc3b7b0a5e4aa0e853f5f2681838f");


--
-- Tabel structure for table `slugs_cs`
--
DROP TABLE  IF EXISTS `slugs_cs`;
CREATE TABLE `slugs_cs` (
  `slug_id` int(11) NOT NULL,
  `slug_url` varchar(255) DEFAULT NULL,
  `slug_label` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `slug_meta_keywords` varchar(255) DEFAULT NULL,
  `slug_meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`),
  CONSTRAINT `slugs_cs_ibfk_1` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`slug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `slugs_cs`  VALUES ( "1","nemovitosti","Nemovitosti","Na�e nemovitosti","","");
INSERT INTO `slugs_cs`  VALUES ( "2","o-nas","O n�s","","","");
INSERT INTO `slugs_cs`  VALUES ( "3","nase-sluzby","Na�e slu�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "4","kariera","Kari�ra","","","");
INSERT INTO `slugs_cs`  VALUES ( "5","poptavame","Popt�v�me","Popt�v�te nemovitost? Obra?te se na n�s","","");
INSERT INTO `slugs_cs`  VALUES ( "6","kontakty","Kontakty","Kontaktn� informace spole?nosti BELARTIS","","");
INSERT INTO `slugs_cs`  VALUES ( "7","realizace","Realizace","","","");
INSERT INTO `slugs_cs`  VALUES ( "8","","","","","");
INSERT INTO `slugs_cs`  VALUES ( "9","","","","","");
INSERT INTO `slugs_cs`  VALUES ( "10","","","","","");
INSERT INTO `slugs_cs`  VALUES ( "11","byty","Byty","","","");
INSERT INTO `slugs_cs`  VALUES ( "12","byty/prodej","Byty, Prodej","","","");
INSERT INTO `slugs_cs`  VALUES ( "13","byty/pronajem","Byty, Pron�jem","","","");
INSERT INTO `slugs_cs`  VALUES ( "14","byty/drazby","Byty, Dra�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "15","byty/1-kk","Byty, 1+kk","","","");
INSERT INTO `slugs_cs`  VALUES ( "16","byty/1-1","Byty, 1+1","","","");
INSERT INTO `slugs_cs`  VALUES ( "17","byty/2-kk","Byty, 2+kk","","","");
INSERT INTO `slugs_cs`  VALUES ( "18","byty/2-1","Byty, 2+1","","","");
INSERT INTO `slugs_cs`  VALUES ( "19","byty/3-kk","Byty, 3+kk","","","");
INSERT INTO `slugs_cs`  VALUES ( "20","byty/3-1","Byty, 3+1","","","");
INSERT INTO `slugs_cs`  VALUES ( "21","byty/4-kk","Byty, 4+kk","","","");
INSERT INTO `slugs_cs`  VALUES ( "22","byty/4-1","Byty, 4+1","","","");
INSERT INTO `slugs_cs`  VALUES ( "23","byty/5-kk","Byty, 5+kk","","","");
INSERT INTO `slugs_cs`  VALUES ( "24","byty/5-1","Byty, 5+1","","","");
INSERT INTO `slugs_cs`  VALUES ( "25","byty/6-a-vice","Byty, 6 a v�ce","","","");
INSERT INTO `slugs_cs`  VALUES ( "26","byty/atypicky","Byty, Atypick�","","","");
INSERT INTO `slugs_cs`  VALUES ( "27","byty/pokoj","Byty, Pokoj","","","");
INSERT INTO `slugs_cs`  VALUES ( "28","domy","Domy","","","");
INSERT INTO `slugs_cs`  VALUES ( "29","domy/prodej","Domy, Prodej","","","");
INSERT INTO `slugs_cs`  VALUES ( "30","domy/pronajem","Domy, Pron�jem","","","");
INSERT INTO `slugs_cs`  VALUES ( "31","domy/drazby","Domy, Dra�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "32","domy/chata","Domy, Chata","","","");
INSERT INTO `slugs_cs`  VALUES ( "33","domy/pamatka/jine","Domy, Pam�tka/jin�","","","");
INSERT INTO `slugs_cs`  VALUES ( "34","domy/rodinny","Domy, Rodinn�","","","");
INSERT INTO `slugs_cs`  VALUES ( "35","domy/vila","Domy, Vila","","","");
INSERT INTO `slugs_cs`  VALUES ( "36","domy/na-klic","Domy, Na kl�?","","","");
INSERT INTO `slugs_cs`  VALUES ( "37","domy/chalupa","Domy, Chalupa","","","");
INSERT INTO `slugs_cs`  VALUES ( "38","domy/zemedelska-usedlost","Domy, Zemed?lsk� usedlost","","","");
INSERT INTO `slugs_cs`  VALUES ( "39","pozemky","Pozemky","","","");
INSERT INTO `slugs_cs`  VALUES ( "40","pozemky/prodej","Pozemky, Prodej","","","");
INSERT INTO `slugs_cs`  VALUES ( "41","pozemky/pronajem","Pozemky, Pron�jem","","","");
INSERT INTO `slugs_cs`  VALUES ( "42","pozemky/drazby","Pozemky, Dra�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "43","pozemky/komercni","Pozemky, Komer?n�","","","");
INSERT INTO `slugs_cs`  VALUES ( "44","pozemky/bydleni","Pozemky, Bydlen�","","","");
INSERT INTO `slugs_cs`  VALUES ( "45","pozemky/pole","Pozemky, Pole","","","");
INSERT INTO `slugs_cs`  VALUES ( "46","pozemky/lesy","Pozemky, Lesy","","","");
INSERT INTO `slugs_cs`  VALUES ( "47","pozemky/louky","Pozemky, Louky","","","");
INSERT INTO `slugs_cs`  VALUES ( "48","pozemky/zahrady","Pozemky, Zahrady","","","");
INSERT INTO `slugs_cs`  VALUES ( "49","pozemky/ostatni","Pozemky, Ostatn�","","","");
INSERT INTO `slugs_cs`  VALUES ( "50","pozemky/rybniky","Pozemky, Rybn�ky","","","");
INSERT INTO `slugs_cs`  VALUES ( "51","pozemky/sady/vinice","Pozemky, Sady/vinice","","","");
INSERT INTO `slugs_cs`  VALUES ( "52","komercni","Komer?n�","","","");
INSERT INTO `slugs_cs`  VALUES ( "53","komercni/prodej","Komer?n�, Prodej","","","");
INSERT INTO `slugs_cs`  VALUES ( "54","komercni/pronajem","Komer?n�, Pron�jem","","","");
INSERT INTO `slugs_cs`  VALUES ( "55","komercni/drazby","Komer?n�, Dra�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "56","komercni/kancelare","Komer?n�, Kancel�?e","","","");
INSERT INTO `slugs_cs`  VALUES ( "57","komercni/sklady","Komer?n�, Sklady","","","");
INSERT INTO `slugs_cs`  VALUES ( "58","komercni/vyroba","Komer?n�, V�roba","","","");
INSERT INTO `slugs_cs`  VALUES ( "59","komercni/obchodni-prostory","Komer?n�, Obchodn� prostory","","","");
INSERT INTO `slugs_cs`  VALUES ( "60","komercni/ubytovani","Komer?n�, Ubytov�n�","","","");
INSERT INTO `slugs_cs`  VALUES ( "61","komercni/restaurace","Komer?n�, Restaurace","","","");
INSERT INTO `slugs_cs`  VALUES ( "62","komercni/zemedelsky","Komer?n�, Zemed?lsk�","","","");
INSERT INTO `slugs_cs`  VALUES ( "63","komercni/ostatni","Komer?n�, Ostatn�","","","");
INSERT INTO `slugs_cs`  VALUES ( "64","komercni/cinzovni-dum","Komer?n�, Cin�ovn� d?m","","","");
INSERT INTO `slugs_cs`  VALUES ( "65","komercni/virtualni-kancelar","Komer?n�, Virtu�ln� kancel�?","","","");
INSERT INTO `slugs_cs`  VALUES ( "66","ostatni","Ostatn�","","","");
INSERT INTO `slugs_cs`  VALUES ( "67","ostatni/prodej","Ostatn�, Prodej","","","");
INSERT INTO `slugs_cs`  VALUES ( "68","ostatni/pronajem","Ostatn�, Pron�jem","","","");
INSERT INTO `slugs_cs`  VALUES ( "69","ostatni/drazby","Ostatn�, Dra�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "70","ostatni/garaz","Ostatn�, Gar�","","","");
INSERT INTO `slugs_cs`  VALUES ( "71","ostatni/ostatni","Ostatn�, Ostatn�","","","");
INSERT INTO `slugs_cs`  VALUES ( "72","ostatni/vinny-sklep","Ostatn�, Vinn� sklep","","","");
INSERT INTO `slugs_cs`  VALUES ( "73","ostatni/pudni-prostor","Ostatn�, P?dn� prostor","","","");
INSERT INTO `slugs_cs`  VALUES ( "74","ostatni/garazove-stani","Ostatn�, Gar�ov� st�n�","","","");
INSERT INTO `slugs_cs`  VALUES ( "75","ostatni/mobilheim","Ostatn�, Mobilheim","","","");
INSERT INTO `slugs_cs`  VALUES ( "76","nemovitosti/pronajem/byty/pronajem-bytu-3-kk-praha","Pron�jem bytu 3+kk, Praha","","","");
INSERT INTO `slugs_cs`  VALUES ( "77","nemovitosti/prodej/domy/rodinny-dum-vojtesin","Rodinn� d?m, Vojt?��n","","","");
INSERT INTO `slugs_cs`  VALUES ( "78","nemovitosti/prodej/pozemky/zahrada-1231m2","Zahrada 1231m2","","","");
INSERT INTO `slugs_cs`  VALUES ( "79","nemovitosti/prodej/domy/prodej-rodinneho-domu-pitin","Prodej rodinn�ho domu Pit�n","","","");
INSERT INTO `slugs_cs`  VALUES ( "80","nase-sluzby/prodej-nemovitosti","Prodej nemovitost�","","","");
INSERT INTO `slugs_cs`  VALUES ( "81","nase-sluzby/pronajem-nemovitosti","Pron�jem nemovitost�","","","");
INSERT INTO `slugs_cs`  VALUES ( "82","nase-sluzby/sprava-nemovitosti","Spr�va nemovitost�","","","");
INSERT INTO `slugs_cs`  VALUES ( "83","nase-sluzby/homestaging","Homestaging","","","");
INSERT INTO `slugs_cs`  VALUES ( "84","nase-sluzby/financni-sluzby","Finan?n� slu�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "85","nase-sluzby/pravni-sluzby","Pr�vn� slu�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "86","nemovitosti/prodej/domy/chata-v-ceskem-raji","Chata v ?esk�m r�ji","","","");
INSERT INTO `slugs_cs`  VALUES ( "87","nemovitosti/prodej/domy/rodinny-dum-kotoucov","Rodinn� d?m, Kotou?ov","","","");
INSERT INTO `slugs_cs`  VALUES ( "88","nemovitosti/prodej/byty/prodej-mezonetoveho-bytu-praha-6","Prodej mezonetov�ho bytu Praha 6","","","");
INSERT INTO `slugs_cs`  VALUES ( "89","nase-sluzby/rekonstrukce-nemovitosti","Rekonstrukce nemovitost�","","","");
INSERT INTO `slugs_cs`  VALUES ( "90","nemovitosti/prodej/domy/vila-otradovice","Vila, Otradovice","","","");
INSERT INTO `slugs_cs`  VALUES ( "91","o-nas","O n�s","","","");
INSERT INTO `slugs_cs`  VALUES ( "92","nase-sluzby","Na�e slu�by","","","");
INSERT INTO `slugs_cs`  VALUES ( "93","nemovitosti/prodej/byty/prodej-bytu-4-1-neratovice","Prodej bytu 4+1, Neratovice","","","");
INSERT INTO `slugs_cs`  VALUES ( "94","nemovitosti/prodej/domy/prodej-rodinneho-domu-7-2-neratovice","Prodej rodinn�ho domu 7+2, Neratovice","","","");
INSERT INTO `slugs_cs`  VALUES ( "95","nemovitosti/pronajem/komercni/kancelarske-prostory-k-pronajmu","Kancel�?sk� prostory k pron�jmu","","","");
INSERT INTO `slugs_cs`  VALUES ( "96","nemovitosti/prodej/pozemky/prodej-udoli-2-mil-m2","Prodej �dol� 2 mil. m2","","","");
INSERT INTO `slugs_cs`  VALUES ( "97","nase-projekty","Na�e projekty","","","");
INSERT INTO `slugs_cs`  VALUES ( "98","projekty/lucky-gardens","Lucky Gardens","","","");
INSERT INTO `slugs_cs`  VALUES ( "99","clanky/nove-technologie-v-nasem-projektu","Nov� technologie v na�em projektu","","","");
INSERT INTO `slugs_cs`  VALUES ( "100","projekty/luxury-town","Luxury town","","","");
INSERT INTO `slugs_cs`  VALUES ( "101","projekty/billy-hills","Billy Hills","","","");
INSERT INTO `slugs_cs`  VALUES ( "102","clanky/nizka-energeticka-narocnost-prokazana","N�zk� energetick� n�ro?nost prok�z�na","","","");
INSERT INTO `slugs_cs`  VALUES ( "103","profil/demo-makler","Demo Makl�?","","","");
INSERT INTO `slugs_cs`  VALUES ( "104","projekty/novy-projekt","Nov� projekt","","","");


--
-- Tabel structure for table `slugs_en`
--
DROP TABLE  IF EXISTS `slugs_en`;
CREATE TABLE `slugs_en` (
  `slug_id` int(11) NOT NULL,
  `slug_url` varchar(255) DEFAULT NULL,
  `slug_label` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `slug_meta_keywords` varchar(255) DEFAULT NULL,
  `slug_meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `slugs_en`  VALUES ( "1","estates","Estates","Our Estates","","");
INSERT INTO `slugs_en`  VALUES ( "2","about-us","O n�s","O spole?nosti BELARTIS","","");
INSERT INTO `slugs_en`  VALUES ( "3","nase-sluzby","Na�e slu�by","Informace o na�ich slu�b�ch","","");
INSERT INTO `slugs_en`  VALUES ( "4","kariera","Kari�ra","Pracovn� p?�le�itosti","","");
INSERT INTO `slugs_en`  VALUES ( "5","poptavame","Popt�v�me","Popt�v�te nemovitost? Obra?te se na n�s","","");
INSERT INTO `slugs_en`  VALUES ( "6","kontakty","Kontakty","Kontaktn� informace spole?nosti BELARTIS","","");
INSERT INTO `slugs_en`  VALUES ( "7","reference","Reference","Reference a realizovan� projekty","","");
INSERT INTO `slugs_en`  VALUES ( "97","our-projects","Our Projects","","","");


--
-- Tabel structure for table `sources`
--
DROP TABLE  IF EXISTS `sources`;
CREATE TABLE `sources` (
  `source_id` int(11) NOT NULL AUTO_INCREMENT,
  `source_title` varchar(100) NOT NULL,
  `source_method` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Tabel structure for table `test`
--
DROP TABLE  IF EXISTS `test`;
CREATE TABLE `test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_bool` tinyint(1) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Tabel structure for table `test_cs`
--
DROP TABLE  IF EXISTS `test_cs`;
CREATE TABLE `test_cs` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_title_edit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



--
-- Tabel structure for table `users`
--
DROP TABLE  IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_title` varchar(255) DEFAULT NULL,
  `user_lastloged` timestamp NULL DEFAULT NULL,
  `user_lastloged_ip` varchar(255) DEFAULT NULL,
  `user_activation_code` varchar(255) DEFAULT NULL,
  `user_recovery_code` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `user_redirect_onlogin` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

INSERT INTO `users`  VALUES ( "1","1","admin","danielmarhan@gmail.com","$2y$10$8MC1ov9dIR.SlrJFDz.Mo.HHrbzx6tngKoGXQpS7ubp7kmiqXTAly","Daniel Marhan","2015-02-26 11:32:11","185.36.160.19","","BUsFWwHSTtktaTqPlOkoFQJyZ7SEUS","cs","","","","","2014-12-19 10:54:24","","2015-02-26 11:32:11","","","");
INSERT INTO `users`  VALUES ( "5","1","broker","pisova@belartis.cz","$2y$10$CJ/F4Vi5Qo6DfZJjjO6rqe.1RK0XdgK/PfJLKNUg0.iDnf4z88wna","Helena P�ov�","","","","","cs","","","","","2015-01-12 19:56:48","1","2015-01-19 20:11:49","8","2015-01-19 20:11:49","");
INSERT INTO `users`  VALUES ( "8","","superAdmin","info@creativeprojects.cz","$2y$10$cG/bxxQCInQaiofoCktXWOBJS5y0oEakoqiPI09ESeocTc/rQ/.bm","Creative Projects s.r.o.","2015-04-14 16:50:49","185.36.160.19","","","cs","","","","","2015-01-19 19:23:47","1","2015-04-14 16:50:49","","","");
INSERT INTO `users`  VALUES ( "9","1","admin","info@belartis.cz","$2y$10$eA4N45HlgC3tyIYoZQxkWOr4GV6XFaeiBd6Y1ZUS.eZvNtrcNUdNi","BELARTIS s.r.o.","2015-04-13 11:21:08","185.36.160.19","","","cs","","","","","2015-01-19 19:27:31","1","2015-04-13 11:21:08","","","");
INSERT INTO `users`  VALUES ( "11","1","broker","helena@belartis.cz","$2y$10$nXKiOc6ix809DT9qgq6FI.f8eHCzvz9GSodNRqVJACAw7mmJvx5U.","Ing. Helena P�ov�","2015-02-21 08:26:16","","","","cs","","","","","2015-01-19 20:08:55","8","2015-02-21 16:38:01","","2015-02-21 16:38:01","8");
INSERT INTO `users`  VALUES ( "13","1","broker","hana@belartis.cz","$2y$10$Ajamw0XBV/L8qHAbIVPmMecSpr/w/4eLrKYb0/s4tbP0AX5W/wpla","Ing. Hana Vejvodov�","","","","","cs","","","","","2015-01-21 16:33:59","12","2015-02-20 15:17:09","1","","");
INSERT INTO `users`  VALUES ( "14","1","broker","josef@belartis.cz","$2y$10$i68AwAToVm1Jab7tj4wVbOQYMjbdckw7nbY9GRVSAppYEpeT36r8y","Ing. Josef ��ra","","","","","cs","","","","","2015-01-22 08:49:09","9","2015-02-20 15:17:06","1","","");
INSERT INTO `users`  VALUES ( "15","1","broker","lukas@belartis.cz","$2y$10$JCPcwDIwdPN1nvPv2Z8NDOkEqXuKwqGW6iTuHnDDV5YKg4XRAR5VO","Ing. Luk� Matocha","2015-02-24 19:24:54","46.135.10.15","","","cs","","","","","2015-01-22 21:06:46","9","2015-04-04 08:58:13","9","","");
INSERT INTO `users`  VALUES ( "16","1","broker","david@belartis.cz","$2y$10$RgB4bR4VwHDcrPfsaTmxAOpLmLvjDpO8JMQsqZJOV2xXBzXwcY9Fq","David Boji?","","","","","cs","","","","","2015-01-28 17:29:02","9","2015-04-11 07:53:34","1","2015-04-11 07:53:34","9");
INSERT INTO `users`  VALUES ( "17","1","broker","ivo@belartis.cz","$2y$10$CABCWdp/KDOwEokRlGLC6efNIon57PnQohOJYFi4mk62yNSPI4zHa","Ivo Gregov","","","","","cs","","","","","2015-02-04 16:14:12","9","2015-02-20 15:16:51","1","","");
INSERT INTO `users`  VALUES ( "18","1","broker","ivana@belartis.cz","$2y$10$QLLr4tIj3WPCbfTPZSeYOOL1g88zVroDZAXxUA9VurXqAkbcR3tMu","PhDr. Ivana Rezkov�","","","","","cs","","","","","2015-02-13 10:17:06","9","2015-02-13 10:18:02","9","2015-02-13 10:18:02","");
INSERT INTO `users`  VALUES ( "19","2","broker","danielmarhan@gmail.com","$2y$10$g38pY6CvyteuEK5jgeKfuOVwbV0ijahsF7iLbJZYeZjMP2bIgn7Pe","Demo Makl�?","2015-05-20 18:04:08","::1","","","cs","","","","","2015-03-10 15:05:58","8","2015-05-20 18:04:08","","","");
INSERT INTO `users`  VALUES ( "20","2","admin","demo.spravce@strankyrealitky.cz","$2y$10$g38pY6CvyteuEK5jgeKfuOVwbV0ijahsF7iLbJZYeZjMP2bIgn7Pe","Demo Spr�vce","2015-04-14 16:46:46","185.36.160.19","","","cs","","","","","2015-03-10 15:15:38","8","2015-04-14 16:46:46","","","");
INSERT INTO `users`  VALUES ( "21","2","superAdmin","info@strankyrealitky.cz","$2y$10$ckguAeonssnxdwJrdQ5Tz.VFNPOmNN0vPFJRp2gPapt1BKoNTTY4e","Str�nkyRealitky.Cz","2015-05-13 16:22:39","::1","","","cs","","","","","2015-03-30 15:22:38","8","2015-05-13 16:22:39","","","");
INSERT INTO `users`  VALUES ( "22","2","broker","sallam@centrum.cz","$2y$10$dnE7pbIafiKni0Zvq4ZK0.8y1DWPxUlZ7FJWVpmveb1E58jIDVdI.","Tom� Makl�?","","","","","cs","","","","","2015-03-30 18:15:57","20","2015-03-30 18:48:17","20","","");
INSERT INTO `users`  VALUES ( "23","2","broker","danielmarhan@gmail.com","$2y$10$JXN8ImugvQj/hw7iUMSx6uR2vm1iqY2IAiQmPlFhLd6UYZg5ynxqm","test","","","","","cs","","","","","2015-03-30 18:20:14","1","2015-03-30 18:28:49","","2015-03-30 18:28:43","1");
INSERT INTO `users`  VALUES ( "24","2","broker","danielmarhan+test@gmail.com","$2y$10$/c9ZgFT6mDwQcfKX/4HTJe8Iz1UXgzDhbZKPYXA.Nq7fDXoTFcPZi","test","","","","","cs","","","","","2015-03-30 18:20:40","1","2015-03-30 18:28:23","","2015-03-30 18:28:18","1");
INSERT INTO `users`  VALUES ( "25","2","broker","danielmarhan+test2@gmail.com","$2y$10$URpGGZAFFraZlPp9iSLIzuKsanBuTVB8JPo4Lo2cK9ZGTzssMhv3y","test","","","","","cs","","","","","2015-03-30 18:21:45","1","2015-03-30 18:27:22","","2015-03-30 18:27:17","1");
INSERT INTO `users`  VALUES ( "26","2","broker","danielmarhan+test3@gmail.com","$2y$10$L/yiRYRiqXYHBBhUVBAe3OxXBXgTnnLzG5ZYFxNEVWTo.yEFnpS26","test","","","","","cs","","","","","2015-03-30 18:26:18","1","2015-03-30 18:26:39","","2015-03-30 18:26:34","1");
INSERT INTO `users`  VALUES ( "27","2","broker","tomas.husa@terragroup.cz","$2y$10$vBPZiSxGDOFf29tdDXT7kuYx6zZ3S7oYWruf3xyRDLFLPN1WfTyI6","Martina Makl�?ka","","","","","cs","","","","","2015-03-30 18:47:01","20","2015-03-30 18:48:32","20","","");
INSERT INTO `users`  VALUES ( "28","2","broker","manager@soulcox.com","$2y$10$L5iOoJB4Fke2nEObyiFP3erjO14jf8FLRIFaQC3V73coKmMUgXVzi","Dalibor Makl�?sk�","","","","","cs","","","","","2015-03-30 18:50:12","20","","","","");


--
-- Tabel structure for table `users_admin`
--
DROP TABLE  IF EXISTS `users_admin`;
CREATE TABLE `users_admin` (
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users_admin`  VALUES ( "1","0000-00-00 00:00:00","","2015-01-19 15:59:04","","","");
INSERT INTO `users_admin`  VALUES ( "8","2015-01-19 19:23:47","1","2015-01-23 20:58:22","","","");
INSERT INTO `users_admin`  VALUES ( "9","2015-01-19 19:27:31","1","2015-02-18 09:20:47","","","");


--
-- Tabel structure for table `users_broker`
--
DROP TABLE  IF EXISTS `users_broker`;
CREATE TABLE `users_broker` (
  `user_id` int(11) NOT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `broker_phone` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users_broker`  VALUES ( "1","","123456789","2015-01-19 16:32:17","","","","","");
INSERT INTO `users_broker`  VALUES ( "2","","12345","2015-01-19 16:32:17","","2015-01-12 17:04:41","1","","");
INSERT INTO `users_broker`  VALUES ( "5","","1","2015-01-12 19:56:48","1","","","","");
INSERT INTO `users_broker`  VALUES ( "11","","+420 602 102 123","2015-01-19 20:08:55","8","2015-02-20 15:17:12","1","","");
INSERT INTO `users_broker`  VALUES ( "13","","+420 602 460 891","2015-01-21 16:33:59","12","2015-02-20 15:17:09","1","","");
INSERT INTO `users_broker`  VALUES ( "14","","+420 731 471 988","2015-01-22 08:49:09","9","2015-02-20 15:17:06","1","","");
INSERT INTO `users_broker`  VALUES ( "15","","+420 602 603 604","2015-01-22 21:06:46","9","2015-04-04 08:58:13","9","","");
INSERT INTO `users_broker`  VALUES ( "16","","+420 721 211 338","2015-01-28 17:29:02","9","2015-02-20 15:16:55","1","","");
INSERT INTO `users_broker`  VALUES ( "17","","+420 608 888 870","2015-02-04 16:14:12","9","2015-02-20 15:16:51","1","","");
INSERT INTO `users_broker`  VALUES ( "18","","736158291","2015-02-13 10:17:06","9","","","","");
INSERT INTO `users_broker`  VALUES ( "19","","+420 123 456 789","2015-03-10 15:05:59","8","2015-04-28 10:55:38","21","","");
INSERT INTO `users_broker`  VALUES ( "22","","+420 606 258 369","2015-03-30 18:15:57","20","2015-03-30 18:48:17","20","","");
INSERT INTO `users_broker`  VALUES ( "23","","123456789","2015-03-30 18:20:15","1","","","","");
INSERT INTO `users_broker`  VALUES ( "24","","123456789","2015-03-30 18:20:40","1","","","","");
INSERT INTO `users_broker`  VALUES ( "25","","123456789","2015-03-30 18:21:46","1","","","","");
INSERT INTO `users_broker`  VALUES ( "26","","123456789","2015-03-30 18:26:18","1","","","","");
INSERT INTO `users_broker`  VALUES ( "27","","+420 606 258 369","2015-03-30 18:47:01","20","2015-03-30 18:48:32","20","","");
INSERT INTO `users_broker`  VALUES ( "28","","+420 606 258 369","2015-03-30 18:50:12","20","","","","");


SET FOREIGN_KEY_CHECKS = 1 ; 
COMMIT ; 
SET AUTOCOMMIT = 1 ; 
