/*
Navicat MySQL Data Transfer

Source Server         : _LOCALHOST
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : estateapp

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-13 16:26:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `addressbook_contacts`
-- ----------------------------
DROP TABLE IF EXISTS `addressbook_contacts`;
CREATE TABLE `addressbook_contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_ic` varchar(255) DEFAULT NULL,
  `contact_dic` varchar(255) DEFAULT NULL,
  `contact_address_street` varchar(255) DEFAULT NULL,
  `contact_address_no` varchar(255) DEFAULT NULL,
  `contact_address_city` varchar(255) DEFAULT NULL,
  `contact_address_postcode` varchar(255) DEFAULT NULL,
  `contact_address_country` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addressbook_contacts
-- ----------------------------
INSERT INTO `addressbook_contacts` VALUES ('1', '2', null, null, null, null, null, null, null, null, null, null, '2015-03-30 16:14:14', '19', null, null, null, null);

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `article_published` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', 'projects', '1', '2', '2015-03-30 00:00:00', null, '2015-03-30 16:15:46', '19', '2015-04-13 12:43:12', '19', null, null);
INSERT INTO `articles` VALUES ('2', 'projects', '2', '2', '2015-03-31 00:00:00', '22', '2015-03-30 18:55:05', '20', '2015-03-30 18:57:12', '20', null, null);

-- ----------------------------
-- Table structure for `articles_cs`
-- ----------------------------
DROP TABLE IF EXISTS `articles_cs`;
CREATE TABLE `articles_cs` (
  `article_id` int(11) NOT NULL,
  `article_title` varchar(255) DEFAULT NULL,
  `article_snippet` text,
  `article_content` longtext,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articles_cs
-- ----------------------------
INSERT INTO `articles_cs` VALUES ('1', 'Nové technologie v našem projektu', '', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent quis  erat nisi. Curabitur eros nibh, placerat ac pulvinar in, rutrum iaculis  ante. Cras ut justo molestie, hendrerit enim sit amet, finibus mauris.  Sed ut eleifend purus, a ullamcorper tortor. Morbi sit amet sem nisl.  Proin fermentum mi quis nibh porta, id feugiat odio posuere. Nulla  facilisi. Maecenas ex magna, iaculis at sem et, luctus malesuada orci.  Morbi varius cursus nibh quis malesuada. Donec vel vehicula tortor. Nam  ut nulla ut ipsum sagittis dignissim. Vivamus augue augue, volutpat ut  vehicula vel, malesuada non mi. Phasellus fringilla, quam sed faucibus  interdum, ex tellus imperdiet ex, ullamcorper semper lectus arcu sit  amet lectus. Aliquam ullamcorper tortor in efficitur volutpat. Proin ut  orci nisi. Quisque congue, nibh vel pretium vulputate, mi nisi gravida  est, in elementum risus ex vel purus.</p><p> Duis tincidunt finibus lacus, id efficitur dolor aliquet hendrerit.  Nullam dictum, eros in dignissim auctor, augue felis dapibus velit, nec  vehicula augue mauris sed dolor. Fusce bibendum tellus vel fermentum  hendrerit. Vestibulum at vehicula lacus. Praesent ultrices lorem odio,  ut efficitur ligula pretium in. Interdum et malesuada fames ac ante  ipsum primis in faucibus. Pellentesque porta, nulla vitae aliquam  tempor, metus turpis mattis mi, facilisis vulputate eros ligula ut  massa. Nunc lobortis semper felis, ut posuere massa vestibulum ut.  Aenean quam turpis, commodo faucibus mattis et, posuere commodo urna.  Aenean suscipit tortor ipsum. Etiam dignissim erat non nulla vestibulum,  in finibus nibh elementum. Mauris ultrices sollicitudin elit at  convallis. Duis sollicitudin lobortis metus non elementum. Vestibulum  elementum justo vel leo tristique, nec pretium felis pellentesque.  Praesent in ullamcorper neque.</p>');
INSERT INTO `articles_cs` VALUES ('2', 'Nízká energetická náročnost prokázána', '', '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed suscipit  ultrices mollis. Curabitur et augue et erat commodo sodales. Quisque ac  volutpat est. Suspendisse potenti. Quisque cursus efficitur facilisis.  Vivamus ut congue eros. Curabitur egestas mi nec metus varius, porta  ullamcorper tortor tristique. Vivamus eget mi lacus. Sed in erat lectus.  Fusce malesuada eleifend metus non mattis. Etiam ac blandit dui. Nulla  facilisi.</p><p> Nunc iaculis, est sit amet sagittis bibendum, erat dui pellentesque  odio, id lobortis sem eros ac tellus. Quisque tristique feugiat justo ut  vestibulum. Donec consectetur nisl eu risus pretium congue. Nullam  sagittis orci ultricies finibus pharetra. Phasellus nec tellus  condimentum, auctor felis nec, pretium tellus. In malesuada mauris nibh,  id scelerisque tellus euismod ac. Sed mi sapien, semper eu enim sit  amet, hendrerit iaculis lectus. Maecenas condimentum hendrerit rhoncus.  Donec vitae viverra enim. Quisque et dictum nunc. Proin massa turpis,  volutpat eu posuere et, ultricies non sem. Fusce auctor a turpis quis  dictum. Sed tristique, metus ornare bibendum interdum, augue diam  gravida nisl, ut tempus augue tellus et quam. Etiam interdum ultrices  urna, eget accumsan quam posuere nec. Vivamus venenatis ex vitae lacus  maximus viverra. Etiam accumsan, sem vel condimentum scelerisque, nisi  leo vulputate ex, at venenatis nisl leo id leo.</p>');

-- ----------------------------
-- Table structure for `brokerprofiles`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles`;
CREATE TABLE `brokerprofiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `profile_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles
-- ----------------------------
INSERT INTO `brokerprofiles` VALUES ('19', '2', '1', '2015-04-13 11:46:05', '19', '2015-04-15 10:34:36', '20', null, null);

-- ----------------------------
-- Table structure for `brokerprofiles_cs`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles_cs`;
CREATE TABLE `brokerprofiles_cs` (
  `user_id` int(11) NOT NULL,
  `profile_title` varchar(255) DEFAULT NULL,
  `profile_position` varchar(255) DEFAULT NULL,
  `profile_description` longtext,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles_cs
-- ----------------------------
INSERT INTO `brokerprofiles_cs` VALUES ('19', 'Demo Makléř', 'Realitní expert pro Českou Republiku', '<p class=\"lead\">\n	S realitní činností jsem začala poprvé po škole v jedné větší realitní kanceláři. Bylo to na začátku samé školení a pozitivní ladění, jenže bohužel praxe byla trochu jinde. Začal být velký tlak spíše na kvatitu, nežli na kvalitu práce a tak i přesto, že jsem byla velmi vděčná za vše co mne naučili a některé postupy, které mi ukázali, naše cesty se rozešli.</p><p>\n	Nyní pracuji systémem, který přináší mým klientům kvalitu a přímé jednání, pomáhám vše řešit lidským přístupem a nezatěžuji je věcmi, které mohu řešit sama. Vždy je ovšem informuji a mý klienti přesně vědí co zrovna dělám a mohou toho být i součástí.</p>');

-- ----------------------------
-- Table structure for `brokerprofiles_testimonials`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles_testimonials`;
CREATE TABLE `brokerprofiles_testimonials` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `testimonial_author` varchar(255) DEFAULT NULL,
  `testimonial_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles_testimonials
-- ----------------------------
INSERT INTO `brokerprofiles_testimonials` VALUES ('1', '19', '2', 'Spokojený klient, Neratovice', null, '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `brokerprofiles_testimonials` VALUES ('2', '19', '2', 'Jan Novák, Mělník', null, '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `brokerprofiles_testimonials` VALUES ('3', '19', '2', 'Jméno Příjmení, Praha', null, '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);

-- ----------------------------
-- Table structure for `brokerprofiles_testimonials_cs`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles_testimonials_cs`;
CREATE TABLE `brokerprofiles_testimonials_cs` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_text` text,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles_testimonials_cs
-- ----------------------------
INSERT INTO `brokerprofiles_testimonials_cs` VALUES ('1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. Curabitur rutrum venenatis sem, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. Nam porta pellentesque varius. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>');
INSERT INTO `brokerprofiles_testimonials_cs` VALUES ('2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. Curabitur rutrum venenatis sem, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. <em>Nam porta pellentesque varius</em>. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>');
INSERT INTO `brokerprofiles_testimonials_cs` VALUES ('3', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel enim imperdiet mi feugiat interdum sed nec diam. Nulla tristique nisl auctor dignissim congue. Proin eget leo ac lacus hendrerit cursus. <strong>Curabitur rutrum venenatis sem</strong>, ut ullamcorper arcu. Pellentesque ultricies mattis ligula a accumsan. Etiam eu dapibus enim. Duis viverra erat quis facilisis fermentum. Maecenas interdum dignissim nibh vel gravida. Nam porta pellentesque varius. Vestibulum sagittis sem nec elit vehicula, vel malesuada libero vulputate. Curabitur ut sodales odio, et semper lectus. Sed faucibus rhoncus vehicula. Maecenas placerat libero vitae auctor auctor.</p>');

-- ----------------------------
-- Table structure for `brokerprofiles_videos`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles_videos`;
CREATE TABLE `brokerprofiles_videos` (
  `video_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `video_address` varchar(255) DEFAULT NULL,
  `video_code` varchar(100) DEFAULT NULL,
  `video_rank` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles_videos
-- ----------------------------
INSERT INTO `brokerprofiles_videos` VALUES ('1', '19', '2', 'Poděbrady', 'RIcKaKe5ves', null, '2015-04-13 12:37:32', '19', '2015-04-15 10:34:36', '20', null, null);

-- ----------------------------
-- Table structure for `brokerprofiles_videos_cs`
-- ----------------------------
DROP TABLE IF EXISTS `brokerprofiles_videos_cs`;
CREATE TABLE `brokerprofiles_videos_cs` (
  `video_id` int(11) NOT NULL,
  `video_title` varchar(255) DEFAULT NULL,
  `video_description` text,
  PRIMARY KEY (`video_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brokerprofiles_videos_cs
-- ----------------------------
INSERT INTO `brokerprofiles_videos_cs` VALUES ('1', 'Prodej bytu', '<p>Naše videoprezentace bytu v Poděbradech k prodeji</p>');

-- ----------------------------
-- Table structure for `clients`
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_title` varchar(255) DEFAULT NULL,
  `client_address` varchar(255) DEFAULT NULL,
  `client_office_address` varchar(255) DEFAULT NULL,
  `client_office_latitude` double DEFAULT NULL,
  `client_office_longitude` double DEFAULT NULL,
  `client_ic` varchar(255) DEFAULT NULL,
  `client_dic` varchar(255) DEFAULT NULL,
  `client_incorporation` varchar(255) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `client_admin_properties` text,
  `client_subdomain` varchar(255) DEFAULT NULL,
  `client_domain` varchar(255) DEFAULT NULL,
  `client_expire` timestamp NULL DEFAULT NULL,
  `source_id` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `sreality_client_id` varchar(255) DEFAULT NULL,
  `sreality_password` varchar(255) DEFAULT NULL,
  `sreality_key` varchar(255) DEFAULT NULL,
  `idnes_ftp_user` varchar(255) DEFAULT NULL,
  `idnes_ftp_password` varchar(255) DEFAULT NULL,
  `idnes_import_login` varchar(255) DEFAULT NULL,
  `idnes_import_password` varchar(255) DEFAULT NULL,
  `client_dir` varchar(255) DEFAULT NULL,
  `export_settings` text,
  `realitymix_export_counter` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('1', 'BELARTIS s.r.o.', 'Dušní 112/16, Praha 1 - Staré Město, 110 00', 'Dušní 112/16, Praha 1 - Staré Město, 110 00', '50.0907497', '14.4206115', '2934299', 'CZ02934299', 'C 225585 vedená u Městského soudu v Praze', '+420 602 603 604', 'info@belartis.cz', '{\"logoPath\":\"/theme/www-belartis-cz/img/belartis-logotype.png\", \"faviconPath\":\"/theme/www-belartis-cz/img/favicon.ico\"}', 'belartis', 'www.belartis.cz', null, null, '2015-01-05 16:04:03', null, '2015-04-29 18:23:45', '21', null, null, '', '', '', 'external_export_test', 'XYC3p_71', '28948157', '93dF4EeF1F', 'theme/www-belartis-cz', '{\"sreality\":true,\"sreality_client_id\":\"13001\",\"sreality_password\":\"db9349ade56a325bb948e89ddc3af0b0\",\"sreality_key\":\"bela-451-ad\",\"idnes\":true,\"idnes_ftp_user\":\"external_export_test\",\"idnes_ftp_password\":\"XYC3p_71\",\"idnes_import_login\":\"28948157\",\"idnes_import_password\":\"93dF4EeF1F\",\"realitymix\":false,\"realitymix_client_id\":\"\",\"realitymix_password\":\"\",\"realitymix_key\":\"\",\"realcity\":false,\"realcity_client_id\":\"\",\"realcity_password\":\"\",\"realcity_key\":\"\"}', '0');
INSERT INTO `clients` VALUES ('2', 'StránkyRealitky.Cz', 'Na Výsluní 1468, 277 11 Neratovice', 'U Závor 1362, 277 11 Neratovice', '50.2543394', '14.5057423', '28948157', 'CZ28948157', 'C 155109 vedená u Městského soudu v Praze', '+420 725 814 440', 'info@strankyrealitky.cz', '{\"logoPath\":\"/theme/www-demo-cz/img/strankyrealitky-logotype.svg\", \"faviconPath\":\"/theme/www-demo-cz/img/favicon.ico\"}', 'demo', null, null, null, '2015-03-09 19:17:30', null, '2015-04-29 18:23:28', '21', null, null, '13490', '3685227c64f66ea8888deca4984ed442', 'sreality-test', 'external_export_test', 'XYC3p_71', '28948157', '93dF4EeF1F', 'theme/www-demo-cz', '{\"sreality\":true,\"sreality_client_id\":\"13490\",\"sreality_password\":\"3685227c64f66ea8888deca4984ed442\",\"sreality_key\":\"sreality-test\",\"idnes\":true,\"idnes_ftp_user\":\"external_export_test\",\"idnes_ftp_password\":\"XYC3p_71\",\"idnes_import_login\":\"28948157\",\"idnes_import_password\":\"93dF4EeF1F\",\"realitymix\":false,\"realitymix_client_id\":\"5\",\"realitymix_password\":\"5c2cc95a\",\"realitymix_key\":\"test_1_0\",\"realcity\":false,\"realcity_client_id\":\"140784\",\"realcity_password\":\"2028ceb06602c9525b0c5bb220d1fe6f\",\"realcity_key\":\"rcpro\"}', '1');
INSERT INTO `clients` VALUES ('3', 'Elite Living - premium estates by Aktivreality', '28.Října 919, 277 11 Neratovice\n277 11 Neratovice', '28.Října 919, 277 11 Neratovice', null, null, '70796068', 'CZ8308070144', null, null, 'info@eliteliving.cz', '{\"logoPath\":\"/theme/www-eliteliving-cz/img/eliteliving-logotype.svg\", \"faviconPath\":\"/theme/www-eliteliving-cz/img/favicon.ico\"}', 'eliteliving', 'www.eliteliving.cz', null, null, '2015-04-15 10:38:18', null, '2015-04-29 11:52:13', null, null, null, null, null, null, null, null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for `demands`
-- ----------------------------
DROP TABLE IF EXISTS `demands`;
CREATE TABLE `demands` (
  `demand_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `demand_client_title` varchar(100) NOT NULL,
  `demand_client_phone` varchar(20) NOT NULL,
  `demand_client_email` varchar(100) NOT NULL,
  `advert_type` tinyint(1) NOT NULL,
  `advert_function` tinyint(1) NOT NULL,
  `advert_subtype` text,
  `demand_locality` varchar(255) DEFAULT NULL,
  `demand_locality_radius` int(11) DEFAULT NULL,
  `demand_locality_latitude` double DEFAULT NULL,
  `demand_locality_longitude` double DEFAULT NULL,
  `demand_area_from` int(11) DEFAULT NULL,
  `demand_area_to` int(11) DEFAULT NULL,
  `demand_price_from` int(11) DEFAULT NULL,
  `demand_price_to` int(11) DEFAULT NULL,
  `demand_description` text,
  `demand_status` tinyint(4) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`demand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of demands
-- ----------------------------
INSERT INTO `demands` VALUES ('1', '1', null, 'Jan Salák', '12345678', 'j.salak@seznam.cz', '1', '1', '[2,3]', 'Neratovice', '3', '50.2592611', '14.5176026', null, null, null, '100000', 'požadavky', '0', '2015-01-07 15:35:09', '1', '2015-01-07 15:43:17', '1', '2015-01-07 15:43:17', '1');
INSERT INTO `demands` VALUES ('2', '1', '2', 'Jan Salák', '12345678', 'j.salak@seznam.cz', '1', '1', '[2,3]', 'Mělník', '3', '50.3539017', '14.4817813', '50', null, null, '1000000', 'požadavky', '1', '2015-01-07 15:53:11', '1', null, null, '2015-01-21 17:35:32', '9');
INSERT INTO `demands` VALUES ('3', '1', null, 'Jan Salák', '123456789', 'j.salak@seznam.cz', '1', '1', '[2,3]', 'Neratovice', '2', '50.2592611', '14.5176026', '50', null, null, '2000000', 'jiné', '2', '2015-01-07 16:20:22', '1', null, null, null, null);
INSERT INTO `demands` VALUES ('4', '1', '5', 'dfs', 'sfa', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '2', '50.2592611', '14.5176026', null, null, null, null, null, '1', '2015-01-12 20:23:05', '1', '2015-01-19 17:04:20', '1', '2015-01-19 17:04:25', '1');
INSERT INTO `demands` VALUES ('5', '1', null, 'Testovaíc', 'dfjlsj', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '2', '50.2592611', '14.5176026', null, null, null, null, null, '0', '2015-01-20 15:54:23', null, null, null, '2015-01-20 16:02:02', '1');
INSERT INTO `demands` VALUES ('6', '1', null, 'Dafd', 'dafds', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '2', '50.2592611', '14.5176026', null, null, null, null, null, '0', '2015-01-20 15:58:55', '1', null, null, '2015-01-20 16:01:59', '1');
INSERT INTO `demands` VALUES ('7', '1', null, 'DSFDS', 'dafds', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '2', '50.2592611', '14.5176026', null, null, null, null, null, '0', '2015-01-20 15:59:57', '1', null, null, '2015-01-20 16:01:55', '1');
INSERT INTO `demands` VALUES ('8', '1', null, 'test', '1', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '2', '50.2592611', '14.5176026', null, null, null, null, null, '0', '2015-01-20 16:07:03', '1', null, null, '2015-01-20 16:12:16', '1');
INSERT INTO `demands` VALUES ('9', '1', null, 'dfs', 'fsadd', 'danielmarhan@gmail.com', '1', '1', null, 'Neratovice', '1', '50.2592611', '14.5176026', null, null, null, null, null, '0', '2015-01-20 16:11:42', '1', null, null, '2015-01-20 16:12:20', '1');

-- ----------------------------
-- Table structure for `emails`
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email_from_name` varchar(255) DEFAULT NULL,
  `email_from_email` varchar(255) DEFAULT NULL,
  `email_to` text,
  `email_cc` text,
  `email_bcc` text,
  `email_subject` varchar(255) DEFAULT NULL,
  `email_message` longtext,
  `email_attachments` text,
  `email_sent` timestamp NULL DEFAULT NULL,
  `email_priority` tinyint(4) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emails
-- ----------------------------
INSERT INTO `emails` VALUES ('1', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>info@creativeprojects.cz</strong><br>\n	Dočasné pčihlašovací heslo: <strong>yvsnod</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-01-19 19:24:22', '0', '2015-01-19 19:23:47', '1', '2015-01-19 19:24:22', '1', null, null);
INSERT INTO `emails` VALUES ('2', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>info@belartis.cz</strong><br>\n	Dočasné pčihlašovací heslo: <strong>ul9u9v</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-01-19 21:14:20', '0', '2015-01-19 19:27:31', '1', '2015-01-19 21:14:20', null, null, null);
INSERT INTO `emails` VALUES ('4', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"helena@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>helena@belartis.cz</strong><br>\n	Dočasné pčihlašovací heslo: <strong>w3ttb1</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-01-19 21:14:20', '0', '2015-01-19 20:08:55', '8', '2015-01-19 21:14:20', null, null, null);
INSERT INTO `emails` VALUES ('5', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>lukas@belartis.cz</strong><br>\n	Dočasné pčihlašovací heslo: <strong>ycxizu</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-01-19 23:13:43', '0', '2015-01-19 21:51:25', '9', '2015-01-19 23:13:43', '9', null, null);
INSERT INTO `emails` VALUES ('6', '11', 'stwe', 'helenpisova@gmail.com', '[{\"email\":\"helena@belartis.cz\",\"name\":\"Helena P\\u00ed\\u0161ov\\u00e1\"}]', null, null, 'Dotaz na nemovitost ev. č. 5', '<h1>Dotaz k nemovitosti č. 5</h1>\n<p>\n	wečtwč\n</p>', null, '2015-01-21 12:05:57', '0', '2015-01-21 11:57:08', '11', '2015-01-21 12:05:57', null, null, null);
INSERT INTO `emails` VALUES ('7', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=Qresbq7XDbXRESZeEjnNCRnsjtOk3w\">http://belartis.strankyrealitky.cz/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=Qresbq7XDbXRESZeEjnNCRnsjtOk3w</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-01-23 20:10:55', '0', '2015-01-23 20:07:16', null, '2015-01-23 20:10:55', null, null, null);
INSERT INTO `emails` VALUES ('8', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=YJkocx43eoDz7VBTaeVl9aQkHAMxxk\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=YJkocx43eoDz7VBTaeVl9aQkHAMxxk</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-01-23 20:57:55', '0', '2015-01-23 20:27:29', '8', '2015-01-23 20:57:55', null, null, null);
INSERT INTO `emails` VALUES ('9', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"david@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>david@belartis.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>mmu6wu</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-01-28 17:29:56', '0', '2015-01-28 17:29:02', '9', '2015-01-28 17:29:56', null, null, null);
INSERT INTO `emails` VALUES ('10', null, 'hokus pokus', 'lukas.matocha@gmail.com', '[{\"email\":\"josef@belartis.cz\",\"name\":\"Ing. Josef \\u017d\\u00e1ra\"}]', null, null, 'Dotaz na nemovitost ev. č. 2', '<h1>Dotaz k nemovitosti č. 2</h1>\n<p>\n	rad bych se zeptal k nemovitosti kotoucov. kam ted dotaz dojde?\n</p>', null, '2015-01-28 20:13:55', '0', '2015-01-28 20:13:02', null, '2015-01-28 20:13:55', null, null, null);
INSERT INTO `emails` VALUES ('11', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Ing. Lukáš Matocha</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=35dhkSgI1izET3Rfa6c7XEB5QohQ5c\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=35dhkSgI1izET3Rfa6c7XEB5QohQ5c</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-01-29 18:24:59', '0', '2015-01-29 18:24:20', null, '2015-01-29 18:24:59', null, null, null);
INSERT INTO `emails` VALUES ('12', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"ivo@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>ivo@belartis.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>ydqmsw</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-02-09 11:05:05', '0', '2015-02-04 16:14:12', '9', '2015-02-09 11:05:05', null, null, null);
INSERT INTO `emails` VALUES ('13', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"ivana@belartis.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://belartis.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://belartis.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://belartis.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://belartis.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>ivana@belartis.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>6d4ti9</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-02-13 10:17:10', '0', '2015-02-13 10:17:06', '9', '2015-02-13 10:17:10', null, null, null);
INSERT INTO `emails` VALUES ('14', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>BELARTIS s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=2JlWjdYBn9yqKqRvhWDVtmGLJNcHgd\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=2JlWjdYBn9yqKqRvhWDVtmGLJNcHgd</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-17 17:13:00', '0', '2015-02-17 17:12:41', null, '2015-02-17 17:13:00', null, null, null);
INSERT INTO `emails` VALUES ('15', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>BELARTIS s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=6al6XpzpQa369Uikx5FRJJQO5TeDQb\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=info@belartis.cz&amp;user_recovery_code=6al6XpzpQa369Uikx5FRJJQO5TeDQb</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-17 20:55:00', '0', '2015-02-17 20:54:13', null, '2015-02-17 20:55:00', null, null, null);
INSERT INTO `emails` VALUES ('16', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"helena@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Ing. Helena Píšová</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=helena@belartis.cz&amp;user_recovery_code=E9tOuT74uRMSYmRhzTt7FZgaY0t2Mi\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=helena@belartis.cz&amp;user_recovery_code=E9tOuT74uRMSYmRhzTt7FZgaY0t2Mi</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-20 09:57:00', '0', '2015-02-20 09:56:41', null, '2015-02-20 09:57:00', null, null, null);
INSERT INTO `emails` VALUES ('17', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://belartis.strankyrealitky.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Ing. Lukáš Matocha</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=F5eWf0tMGL9ct9GTxIPMwWklWbhonj\">http://belartis.strankyrealitky.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=F5eWf0tMGL9ct9GTxIPMwWklWbhonj</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-22 10:33:00', '0', '2015-02-22 10:32:39', null, '2015-02-22 10:33:00', null, null, null);
INSERT INTO `emails` VALUES ('18', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://www.belartis.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Ing. Lukáš Matocha</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=SepMnTyzMJmNDa1KY2m5m8KqxpyNOT\">http://www.belartis.cz/admin/users/set-new-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=SepMnTyzMJmNDa1KY2m5m8KqxpyNOT</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-22 10:36:00', '0', '2015-02-22 10:35:27', null, '2015-02-22 10:36:00', null, null, null);
INSERT INTO `emails` VALUES ('19', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://www.belartis.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=aoWqOUDTSvGtBYl5ZFVMmsg8hyFsWX\">http://www.belartis.cz/admin/users/set-new-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=aoWqOUDTSvGtBYl5ZFVMmsg8hyFsWX</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-22 14:45:00', null, '2015-02-22 14:44:36', null, '2015-02-22 14:45:00', null, null, null);
INSERT INTO `emails` VALUES ('20', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"info@creativeprojects.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://www.belartis.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Creative Projects s.r.o.</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/user/reset-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=CtnOW74MjRD8l515bMJmrPxiYv7wq0\">http://www.belartis.cz/admin/user/reset-password/?user_email=info@creativeprojects.cz&amp;user_recovery_code=CtnOW74MjRD8l515bMJmrPxiYv7wq0</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-22 14:50:01', null, '2015-02-22 14:49:43', null, '2015-02-22 14:50:01', null, null, null);
INSERT INTO `emails` VALUES ('21', null, 'BELARTIS s.r.o.', 'info@belartis.cz', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"\"}]', null, null, 'Žádost o reset hesla na http://www.belartis.cz', '\n\n<h1>\n  Zapoměli jste heslo? \n  <small>Dostali jsme žádost o změnu hesla k Vašemu účtu</small>\n</h1>\n\n<p>\n    Někdo, pravděpodobně Vy zažádal(a) o změnu hesla k účtu <strong>Ing. Lukáš Matocha</strong>. Pokud jste to o reset hesla nežádal(a) tento email ignorujte. V opačném případě si můžete heslo změnit na níže uvedené adrese.\n</p>\n\n<p class=\"description\">\n  <span>Adresa pro změnu Vašeho hesla</span> \n  <strong>\n    <a href=\"http://www.belartis.cz/admin/user/reset-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=bX3bXDgGk3JtkwUvbekJHKm5ds1kP2\">http://www.belartis.cz/admin/user/reset-password/?user_email=lukas@belartis.cz&amp;user_recovery_code=bX3bXDgGk3JtkwUvbekJHKm5ds1kP2</a>\n   </strong>\n</p>\n\n<p class=\"text-center\">\n  Na výše uvedeném odkazu naleznete formulář pro zadání nového hesla k Vašemu účtu.\n</p>\n', null, '2015-02-22 21:55:00', null, '2015-02-22 21:54:48', '9', '2015-02-22 21:55:00', null, null, null);
INSERT INTO `emails` VALUES ('22', null, 'zorro mstitel', 'helenpisova@gmail.com', '[{\"email\":\"lukas@belartis.cz\",\"name\":\"Ing. Luk\\u00e1\\u0161 Matocha\"}]', null, null, 'Dotaz na nemovitost ev. č. 8', '<h1>Dotaz k nemovitosti č. 8</h1>\n<p>\n	grrrr\n</p>', null, '2015-02-23 10:09:00', null, '2015-02-23 10:08:26', null, '2015-02-23 10:09:00', null, null, null);
INSERT INTO `emails` VALUES ('23', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"demo.makler@strankyrealitky.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>demo.makler@strankyrealitky.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>2g2hnr</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-10 15:06:00', null, '2015-03-10 15:05:59', '8', '2015-03-10 15:06:00', null, null, null);
INSERT INTO `emails` VALUES ('24', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"demo.spravce@strankyrealitky.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>demo.spravce@strankyrealitky.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>u1h36z</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-10 15:16:00', null, '2015-03-10 15:15:38', '8', '2015-03-10 15:16:00', null, null, null);
INSERT INTO `emails` VALUES ('25', null, 'Jan Salák', 'j.salak@seznam.cz', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-13 17:54:00', null, '2015-03-13 17:53:34', '1', '2015-03-13 17:54:00', null, null, null);
INSERT INTO `emails` VALUES ('26', null, 'Jan Salák', 'j.salak@seznam.cz', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-13 17:55:00', null, '2015-03-13 17:54:28', '1', '2015-03-13 17:55:00', null, null, null);
INSERT INTO `emails` VALUES ('27', null, 'Jan Salák', 'j.salak@seznam.cz', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-13 18:42:00', null, '2015-03-13 18:41:43', '1', '2015-03-13 18:42:00', null, null, null);
INSERT INTO `emails` VALUES ('28', null, 'Jan Salák', 'j.salak@seznam.cz', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-13 18:46:01', null, '2015-03-13 18:45:24', '1', '2015-03-13 18:46:01', null, null, null);
INSERT INTO `emails` VALUES ('29', null, 'Jan Salák', 'j.salak@seznam.cz', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-13 18:47:00', null, '2015-03-13 18:46:55', '1', '2015-03-13 18:47:00', null, null, null);
INSERT INTO `emails` VALUES ('36', null, 'daniel', 'danielmarhan@gmail.com', null, null, null, 'Dotaz z webových stránek', 'test', null, '2015-03-14 18:07:00', null, '2015-03-14 18:06:20', null, '2015-03-14 18:07:00', null, null, null);
INSERT INTO `emails` VALUES ('37', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"info@strankyrealitky.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://demo.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://demo.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>info@strankyrealitky.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>oscbiu</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 15:37:01', null, '2015-03-30 15:22:38', '8', '2015-03-30 15:37:01', null, null, null);
INSERT INTO `emails` VALUES ('38', null, 'Demo Makléř', 'demo.makler@strankyrealitky.cz', '[{\"email\":\"sallam@centrum.cz\",\"name\":\"\"}]', null, null, 'Karta nemovitosti Prodej bytu 4+1, Neratovice', '<p>Dobrý den</p><p>\n	V příloze Vám zasíláme kartu nemovitosti <strong>Prodej bytu 4+1, Neratovice</strong>.</p><p>\n	S pozdravem</p><p>fuck</p>', '[\"\\/theme\\/www-demo-cz\\/files\\/estates_pdf\\/prodej-bytu-4-1-neratovice.pdf\"]', '2015-03-30 16:11:01', null, '2015-03-30 16:10:09', '19', '2015-03-30 16:11:01', null, null, null);
INSERT INTO `emails` VALUES ('39', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"sallam@centrum.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://demo.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://demo.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>sallam@centrum.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>g4leim</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:16:01', null, '2015-03-30 18:15:57', '20', '2015-03-30 18:16:01', null, null, null);
INSERT INTO `emails` VALUES ('40', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"danielmarhan@gmail.com\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>danielmarhan@gmail.com</strong><br>\n	Dočasné přihlašovací heslo: <strong>qrb2e3</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:21:00', null, '2015-03-30 18:20:14', '1', '2015-03-30 18:21:00', null, null, null);
INSERT INTO `emails` VALUES ('41', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"danielmarhan+test@gmail.com\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>danielmarhan+test@gmail.com</strong><br>\n	Dočasné přihlašovací heslo: <strong>hyvc3t</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:21:00', null, '2015-03-30 18:20:40', '1', '2015-03-30 18:21:00', null, null, null);
INSERT INTO `emails` VALUES ('42', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"danielmarhan+test2@gmail.com\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>danielmarhan+test2@gmail.com</strong><br>\n	Dočasné přihlašovací heslo: <strong>0mqwdm</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:22:00', null, '2015-03-30 18:21:46', '1', '2015-03-30 18:22:00', null, null, null);
INSERT INTO `emails` VALUES ('43', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"danielmarhan+test3@gmail.com\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://localhost', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://localhost. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://localhost/admin\" title=\"Přihlášení\">http://localhost/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>danielmarhan+test3@gmail.com</strong><br>\n	Dočasné přihlašovací heslo: <strong>w9w76l</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:27:01', null, '2015-03-30 18:26:18', '1', '2015-03-30 18:27:01', null, null, null);
INSERT INTO `emails` VALUES ('44', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"tomas.husa@terragroup.cz\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://demo.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://demo.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>tomas.husa@terragroup.cz</strong><br>\n	Dočasné přihlašovací heslo: <strong>qq7nv9</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:47:07', null, '2015-03-30 18:47:01', '20', '2015-03-30 18:47:07', null, null, null);
INSERT INTO `emails` VALUES ('45', null, 'StránkyRealitky.Cz', 'info@strankyrealitky.cz', '[{\"email\":\"manager@soulcox.com\",\"name\":\"\"}]', null, null, 'Vytvořen uživatelský účet na http://demo.strankyrealitky.cz', '<h1>\n	Vytvořen uživatelský účet\n</h1>\n<p>\n	Byl Vám vytvořen uživatelský účet na realitních stránkách http://demo.strankyrealitky.cz. Přihlásit se můžete ne adrese:\n</p>\n<p>\n	<a href=\"http://demo.strankyrealitky.cz/admin\" title=\"Přihlášení\">http://demo.strankyrealitky.cz/admin</a>\n</p>\n<p>\n	Přihlašovací email: <strong>manager@soulcox.com</strong><br>\n	Dočasné přihlašovací heslo: <strong>dn1d7z</strong><br>\n	<em>Dočasné heslo doporučujeme ihned po přihlášení změnit</em>\n</p>', null, '2015-03-30 18:51:00', null, '2015-03-30 18:50:12', '20', '2015-03-30 18:51:00', null, null, null);
INSERT INTO `emails` VALUES ('46', null, 'tomáš husa', 'sallam@centrum.cz', null, null, null, 'Dotaz z webových stránek', 'qw§peijfeorihjgoidfwjg§psj g jfdpj g§pwedj§gfpjsdfmg', null, '2015-04-07 15:47:01', null, '2015-04-07 15:46:56', null, '2015-04-07 15:47:01', null, null, null);
INSERT INTO `emails` VALUES ('47', null, 'fdgfhgfhf', 'sallam@centrum.cz', null, null, null, 'Dotaz z webových stránek', 'ohfphsdfjglsg', null, '2015-04-07 15:48:01', null, '2015-04-07 15:47:27', null, '2015-04-07 15:48:01', null, null, null);

-- ----------------------------
-- Table structure for `estates`
-- ----------------------------
DROP TABLE IF EXISTS `estates`;
CREATE TABLE `estates` (
  `estate_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `advert_function` tinyint(4) DEFAULT NULL,
  `advert_lifetime` tinyint(4) DEFAULT NULL,
  `advert_price` double DEFAULT NULL,
  `advert_price_original` double DEFAULT NULL,
  `advert_price_currency` tinyint(4) DEFAULT NULL,
  `advert_price_unit` tinyint(4) DEFAULT NULL,
  `advert_type` tinyint(4) DEFAULT NULL,
  `locality_city` varchar(255) DEFAULT NULL,
  `locality_zip` varchar(255) DEFAULT NULL,
  `locality_inaccuracy_level` int(11) DEFAULT NULL,
  `advert_id` int(11) DEFAULT NULL,
  `advert_rkid` varchar(255) DEFAULT NULL,
  `advert_room_count` tinyint(4) DEFAULT NULL,
  `advert_subtype` tinyint(4) DEFAULT NULL,
  `balcony` tinyint(4) DEFAULT NULL,
  `basin` tinyint(4) DEFAULT NULL,
  `building_condition` tinyint(4) DEFAULT NULL,
  `building_type` tinyint(4) DEFAULT NULL,
  `cellar` tinyint(4) DEFAULT NULL,
  `estate_area` int(11) DEFAULT NULL,
  `floor_number` int(11) DEFAULT NULL,
  `garage` tinyint(4) DEFAULT NULL,
  `locality_latitude` double DEFAULT NULL,
  `locality_longitude` double DEFAULT NULL,
  `locality_ruian` int(11) DEFAULT NULL,
  `locality_ruian_level` int(11) DEFAULT NULL,
  `locality_uir` int(11) DEFAULT NULL,
  `locality_uir_level` int(11) DEFAULT NULL,
  `loggia` tinyint(4) DEFAULT NULL,
  `object_type` tinyint(4) DEFAULT NULL,
  `ownership` tinyint(4) DEFAULT NULL,
  `parking_lots` tinyint(4) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `project_rkid` varchar(255) DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `seller_rkid` varchar(255) DEFAULT NULL,
  `terrace` tinyint(4) DEFAULT NULL,
  `usable_area` int(11) DEFAULT NULL,
  `acceptance_year` int(11) DEFAULT NULL,
  `advert_code` varchar(255) DEFAULT NULL,
  `advert_low_energy` tinyint(4) DEFAULT NULL,
  `advert_price_charge` tinyint(4) DEFAULT NULL,
  `advert_price_commission` tinyint(4) DEFAULT NULL,
  `advert_price_legal_services` tinyint(4) DEFAULT NULL,
  `advert_price_negotiation` tinyint(4) DEFAULT NULL,
  `advert_price_vat` tinyint(4) DEFAULT NULL,
  `annuity` int(11) DEFAULT NULL,
  `auction_advertisement_pdf` text,
  `auction_date` datetime DEFAULT NULL,
  `auction_date_tour` datetime DEFAULT NULL,
  `auction_date_tour2` datetime DEFAULT NULL,
  `auction_kind` tinyint(4) DEFAULT NULL,
  `auction_place` varchar(255) DEFAULT NULL,
  `auction_review_pdf` text,
  `balcony_area` int(11) DEFAULT NULL,
  `basin_area` int(11) DEFAULT NULL,
  `beginning_date` date DEFAULT NULL,
  `building_area` int(11) DEFAULT NULL,
  `ceiling_height` double DEFAULT NULL,
  `cellar_area` int(11) DEFAULT NULL,
  `cost_of_living` varchar(255) DEFAULT NULL,
  `easy_access` tinyint(4) DEFAULT NULL,
  `electricity` text,
  `elevator` tinyint(4) DEFAULT NULL,
  `energy_efficiency_rating` tinyint(4) DEFAULT NULL,
  `energy_performance_attachment` text,
  `energy_performance_certificate` tinyint(4) DEFAULT NULL,
  `energy_performance_summary` double DEFAULT NULL,
  `extra_info` tinyint(4) DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `first_tour_date` datetime DEFAULT NULL,
  `first_tour_date_to` datetime DEFAULT NULL,
  `flat_class` tinyint(4) DEFAULT NULL,
  `floor_area` int(11) DEFAULT NULL,
  `floors` int(11) DEFAULT NULL,
  `furnished` tinyint(4) DEFAULT NULL,
  `garage_count` int(11) DEFAULT NULL,
  `garden_area` int(11) DEFAULT NULL,
  `garret` tinyint(4) DEFAULT NULL,
  `gas` text,
  `gully` text,
  `heating` text,
  `locality_citypart` varchar(255) DEFAULT NULL,
  `locality_co` varchar(255) DEFAULT NULL,
  `locality_cp` varchar(255) DEFAULT NULL,
  `locality_street` varchar(255) DEFAULT NULL,
  `loggia_area` int(11) DEFAULT NULL,
  `mortgage` tinyint(4) DEFAULT NULL,
  `mortgage_percent` double DEFAULT NULL,
  `nolive_total_area` int(11) DEFAULT NULL,
  `object_age` int(11) DEFAULT NULL,
  `object_kind` tinyint(4) DEFAULT NULL,
  `object_location` tinyint(4) DEFAULT NULL,
  `offices_area` int(11) DEFAULT NULL,
  `parking` int(11) DEFAULT NULL,
  `personal` tinyint(4) DEFAULT NULL,
  `price_auction_principal` double DEFAULT NULL,
  `price_expert_report` double DEFAULT NULL,
  `price_minimum_bid` double DEFAULT NULL,
  `production_area` int(11) DEFAULT NULL,
  `protection` tinyint(4) DEFAULT NULL,
  `ready_date` date DEFAULT NULL,
  `reconstruction_year` int(11) DEFAULT NULL,
  `road_type` text,
  `sale_date` date DEFAULT NULL,
  `shop_area` int(11) DEFAULT NULL,
  `spor_percent` double DEFAULT NULL,
  `steps` varchar(255) DEFAULT NULL,
  `store_area` int(11) DEFAULT NULL,
  `surroundings_type` tinyint(4) DEFAULT NULL,
  `telecommunication` text,
  `terrace_area` int(11) DEFAULT NULL,
  `transport` text,
  `underground_floors` int(11) DEFAULT NULL,
  `usable_area_ground` int(11) DEFAULT NULL,
  `user_status` tinyint(4) DEFAULT NULL,
  `water` text,
  `workshop_area` int(11) DEFAULT NULL,
  `youtube_video` varchar(255) DEFAULT NULL,
  `advert_status` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `advert_price_hidden` tinyint(1) NOT NULL,
  `locality_district` varchar(255) DEFAULT NULL,
  `id_kraj` int(11) DEFAULT NULL,
  `id_okres` int(11) DEFAULT NULL,
  `id_obec` int(11) DEFAULT NULL,
  `kraj` varchar(255) DEFAULT NULL,
  `okres` varchar(255) DEFAULT NULL,
  `advert_onhomepage` tinyint(1) NOT NULL,
  `estate_pdf_path` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estates
-- ----------------------------
INSERT INTO `estates` VALUES ('1', '1', '11', null, '1', null, '2500000', '19000', '1', '1', '2', 'Kotoučov', '25163', '1', null, null, '1', '37', null, null, '1', '1', null, null, '3', '1', '42.8412344', '73.9872382', null, null, null, null, null, '1', '1', null, null, null, null, null, null, '80', null, '56', null, '1', '1', '1', null, '1', '3333125', '1419937407.pdf', null, null, null, '1', null, '', null, null, null, null, null, null, null, '1', '[2]', '1', '1', '', '1', null, '1', null, null, null, '1', null, null, '1', '1', null, null, '[2]', '[3]', '[8]', null, null, '1', 'Kotoucov', null, '1', null, null, null, '4', '1', null, null, '1', null, null, null, null, '1', null, null, '[3]', null, null, '10555', null, null, '1', '[1,2,3,4,5]', null, null, null, null, null, '[2]', null, 'mr2_wjcyK7s', '1', '2014-12-30 12:00:53', '0', '2015-01-20 09:30:41', '9', '2015-01-20 09:30:48', '12', '0', null, null, null, null, null, null, '0', null);
INSERT INTO `estates` VALUES ('2', '1', '11', null, '2', null, '19000', null, '1', '2', '1', 'Praha', '140000', null, null, null, '1', '6', null, null, '1', '1', null, null, null, null, '50.0444343', '14.4354763', null, null, null, null, null, '1', '1', null, null, null, null, null, null, '80', null, null, null, '2', '1', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, '1', '1', null, '1', null, null, null, null, null, '1', null, null, '1', null, null, null, null, null, null, null, null, '123', 'Jeremenkova', null, null, null, null, null, '1', '1', null, null, '1', null, null, null, null, '1', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '2015-01-11 20:30:01', '0', '2015-01-11 22:04:40', '1', '2015-01-19 22:06:50', '9', '0', null, null, null, null, null, null, '0', null);
INSERT INTO `estates` VALUES ('3', '1', '15', null, '2', '1', '21000', '19000', '1', '1', '1', 'Praha 4', '14700', '1', null, null, '1', '6', null, null, '1', '2', '1', null, '2', null, '50.0466835', '14.4160549', null, null, null, null, '1', '1', '1', '1', null, null, null, null, null, '80', null, '1', null, '2', '1', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '2', '[2]', '1', null, null, '1', null, null, null, null, null, null, null, null, '1', null, null, null, '[2]', '[1]', '[7]', null, '7', '920', null, null, null, null, null, null, '1', '1', null, null, '1', null, null, null, null, '1', null, null, '[3]', null, null, null, null, null, '1', '[1,2,4,5]', null, '[1,2,3,4,5]', null, null, null, '[2]', null, null, '1', '2015-01-20 10:25:51', '11', '2015-04-09 09:27:14', '9', null, null, '0', 'Podolí', '19', '19', '554782', 'Hlavní město Praha', 'Hlavní město Praha', '0', null);
INSERT INTO `estates` VALUES ('4', '1', '15', null, '1', '1', '1690000', '1790000', '1', '1', '2', 'Bohdaneč', '28522', '1', null, null, '4', '37', null, '1', '1', '7', '1', '2205', null, null, '49.7702564', '15.231228', null, null, null, null, null, '1', '1', null, null, null, null, null, null, '100', null, '2', null, '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '[2,4]', '2', '3', null, '1', null, null, null, null, null, '1', null, null, '1', null, null, null, null, '[2]', '[5]', null, null, '1', 'Kotoučov', null, null, null, null, null, '4', '2', null, null, '1', null, null, null, null, '1', null, null, '[3]', null, null, null, null, null, '1', '[1,2,3]', null, '[1,2,3,5]', null, null, null, '[1]', null, null, '1', '2015-01-20 11:36:35', '11', '2015-04-03 12:10:03', '9', null, null, '0', null, '27', '3205', '533980', 'Středočeský kraj', 'Kutná Hora', '1', null);
INSERT INTO `estates` VALUES ('5', '1', '15', null, '1', '1', '2300000', '1900000', '1', '1', '2', 'Úněšov', '330 38', '1', null, null, '5', '37', null, null, '1', '2', '1', '3004', null, '1', '49.9185347942', '13.1118383831', null, null, null, null, null, '2', '1', null, null, null, null, null, null, '250', null, '3', null, '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '2', '[2,4]', '2', '5', null, '1', null, null, null, null, null, '1', null, null, '1', null, null, null, null, '[3]', '[5]', null, null, '3', 'Vojtěšín', null, '1', null, null, null, '4', '2', null, null, '1', null, null, null, null, '1', null, null, null, null, null, null, null, null, '1', '[1,2]', null, '[3,5]', null, null, null, '[1]', null, null, '1', '2015-01-20 20:53:21', '11', '2015-04-03 12:11:46', '9', null, null, '0', null, '43', '3407', '559563', 'Plzeňský kraj', 'Plzeň-sever', '1', null);
INSERT INTO `estates` VALUES ('6', '1', '15', null, '1', '1', '1200000', null, '1', '1', '2', 'Pitín', '68771', '1', null, null, '3', '37', null, null, '1', '7', null, '16595', null, null, '49.0367214', '17.8559227', null, null, null, null, null, '1', '1', '1', null, null, null, null, null, '95', null, '4', null, '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, '2', '3', null, '1', null, null, null, null, null, '1', null, null, '1', null, null, null, null, null, null, null, null, '125', 'Pitín', null, '1', null, null, null, '4', '1', null, null, '1', null, null, null, null, '1', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '2015-01-20 21:04:32', '11', '2015-03-22 10:29:01', '9', null, null, '0', null, '141', '3711', '592498', 'Zlínský kraj', 'Uherské Hradiště', '0', null);
INSERT INTO `estates` VALUES ('7', '1', '15', null, '1', '1', '12000000', '1', '1', '1', '1', 'Praha', '16200', '1', null, null, '1', '16', null, null, '1', '2', null, null, '4', null, '50.0949115', '14.3727936', null, null, null, null, null, '1', '1', '1', null, null, null, null, null, '120', null, '5', null, '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '[2,4]', '1', '1', null, '1', null, null, '1970-01-01', null, null, '1', null, null, '1', null, '850', null, '[2]', '[1]', '[1]', null, null, '77', 'Na Ořechovce', null, '1', null, null, null, '1', '1', null, null, '1', null, null, null, null, '1', null, null, '[3]', null, null, null, null, null, '1', '[1,2,3,4,5]', null, '[1,2,3,4,5]', null, null, null, '[2]', null, null, '1', '2015-01-21 10:27:56', '11', '2015-04-03 12:13:18', '9', null, null, '0', null, '19', '19', '554782', 'Hlavní město Praha', 'Hlavní město Praha', '0', null);
INSERT INTO `estates` VALUES ('8', '1', '15', null, '1', '1', '1500000', null, '1', '1', '2', 'Kněžmost', '29402', '1', null, null, '4', '33', null, null, '1', '1', null, '386', null, '1', '50.4917852', '15.0741517', null, null, null, null, null, '2', '1', null, null, null, null, null, '1', '86', null, '6', null, '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, '46', null, null, null, '2', '[2,4]', '2', null, null, '1', null, null, null, null, null, '1', '86', null, '1', null, '340', null, null, '[3]', '[2]', null, null, '82', 'Drhleny', null, null, null, null, null, '4', '4', null, null, '1', null, null, null, null, '1', null, null, '[4]', null, null, null, null, null, '1', null, null, '[3,5]', null, null, null, '[1]', null, null, '2', '2015-01-21 17:09:57', '11', '2015-04-09 07:49:40', '9', null, null, '0', null, '27', '3207', '536041', 'Středočeský kraj', 'Mladá Boleslav', '0', null);
INSERT INTO `estates` VALUES ('9', '1', '15', null, '1', '1', '120000', null, '1', '1', '3', 'Nová Ves', '27752', '1', null, null, '1', '23', null, null, '1', '1', null, '1231', null, null, '50.364284', '13.6688222', null, null, null, null, null, '1', '1', null, null, null, null, null, null, null, null, '7', null, '2', '2', '2', null, '2', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, '1', '1', null, '1', null, null, null, null, null, '1', null, null, '1', null, null, null, null, null, null, null, null, '7', 'Nová Ves', null, null, null, null, null, '1', '1', null, null, '1', null, null, null, null, '1', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '2015-01-22 21:57:23', '11', '2015-03-22 19:43:12', '9', null, null, '0', null, '27', '3206', '535117', 'Středočeský kraj', 'Mělník', '0', null);
INSERT INTO `estates` VALUES ('10', '1', '11', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '2015-02-20 09:57:57', '11', '2015-02-21 09:10:12', null, '2015-02-21 09:10:12', '9', '0', null, null, null, null, null, null, '0', null);
INSERT INTO `estates` VALUES ('11', '1', '11', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '2015-02-21 08:30:18', '11', '2015-02-21 09:10:19', null, '2015-02-21 09:10:19', '9', '0', null, null, null, null, null, null, '0', null);
INSERT INTO `estates` VALUES ('12', '1', '15', null, '1', '1', '8500000', '6500000', '1', '1', '2', 'Skorkov', '25901', '1', null, null, '5', '39', null, null, '6', '1', null, '1133', null, '1', '50.2086100745', '14.745390255', null, null, null, null, null, '2', '1', null, null, null, null, null, '1', '151', null, '8', '1', '1', '2', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '[2,4]', '2', null, null, '1', null, null, null, null, null, null, null, '2', '1', null, null, null, '[1]', '[1]', '[6,8]', null, null, '84', null, null, '1', null, null, null, '4', '4', null, null, '1', null, null, null, null, null, null, null, '[3]', null, null, null, null, null, '1', '[1,2,3,4,5]', null, '[1,2,3,4,5]', null, null, null, '[1]', null, null, '1', '2015-02-22 21:57:59', '15', '2015-03-22 10:29:41', '9', null, null, '0', 'Otradovice', '27', '3201', '530905', 'Středočeský kraj', 'Benešov', '0', null);
INSERT INTO `estates` VALUES ('13', '2', '19', null, '1', '1', '890000', null, '1', '1', '1', 'Neratovice', '27711', '1', null, null, '1', '9', null, null, '2', '5', '1', null, '2', '1', '50.2555993896', '14.5129071698', null, null, null, null, '1', '1', '1', '1', '1', null, null, null, null, '75', null, 'PB001', null, '1', '1', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, '3', '2', null, '1', '[2]', '1', null, null, '1', null, null, null, null, null, null, null, '4', '1', '1', null, '1', '[2]', '[1]', '[4]', null, null, '918', '28. října', '10', null, null, null, null, '1', '1', null, '2', '1', null, null, null, null, null, null, null, '[3]', null, null, null, null, null, '1', '[2,4]', null, '[1,3,4,5]', null, null, null, '[2]', null, 'ef05D0RnOlI', '1', '2015-03-10 15:30:10', '19', '2015-04-24 15:20:29', '21', null, null, '0', null, '27', '3206', '535087', 'Středočeský kraj', 'Mělník', '0', '/theme/www-demo-cz/files/estates_pdf/prodej-bytu-4-1-neratovice.pdf');
INSERT INTO `estates` VALUES ('14', '2', '19', null, '1', '1', '3800000', null, '1', '1', '2', 'Neratovice', '27711', '1', null, null, '5', '37', null, '1', '6', '2', null, '1200', null, '1', '50.2590012', '14.5063665', null, null, null, null, null, '2', '1', null, '2', null, null, null, '1', '150', null, 'PD001', '1', '1', '1', '1', '1', '1', null, null, null, null, null, '1', null, null, null, '12', null, '75', null, null, null, '2', '[2]', '2', '1', null, '1', null, null, null, null, null, null, '70', '1', '1', '2', '1000', null, '[2]', '[1]', '[2]', 'Neratovice', null, '1294', 'Polní', null, '1', null, null, null, '4', '2', null, null, '1', null, null, null, null, null, null, null, '[2]', null, null, null, null, null, '1', '[2,3,4]', '30', null, '1', null, null, '[2]', null, null, '1', '2015-03-10 16:19:42', '19', '2015-04-29 17:23:48', '21', null, null, '0', null, '27', '3206', '535087', 'Středočeský kraj', 'Mělník', '1', null);
INSERT INTO `estates` VALUES ('15', '2', '19', null, '2', '1', '19000', null, '1', '2', '4', 'Neratovice', '27711', '1', null, null, '1', '25', null, null, '1', '2', null, '2000', '1', null, '50.2543394', '14.5057423', null, null, null, null, null, '1', '1', '1', '2', null, null, null, null, '100', null, 'NK001', null, '1', '1', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', '[2,4]', '2', '5', null, '1', null, null, null, null, null, null, null, '1', '1', null, null, null, null, '[1,3]', '[3]', null, null, '1362', 'U Závor', null, null, null, null, null, '4', '2', '100', '20', '1', null, null, null, null, null, null, null, '[3]', null, null, null, null, null, '1', '[2]', null, null, null, null, null, '[2]', null, null, '1', '2015-03-10 16:30:42', '19', '2015-03-30 20:56:25', '19', null, null, '0', null, '27', '3206', '535087', 'Středočeský kraj', 'Mělník', '1', null);
INSERT INTO `estates` VALUES ('16', '2', '19', null, '1', '1', '123500000', null, '1', '1', '3', 'Neratovice', '27711', '3', null, null, '1', '19', null, null, '1', '1', null, '2000000', null, null, '50.2592611', '14.5176026', null, null, null, null, null, '1', '1', null, '1', null, null, null, null, null, null, 'PP001', null, '1', '1', '1', null, '1', null, null, null, null, null, '1', null, null, null, null, null, null, null, null, null, '1', null, '1', null, null, '1', null, null, null, null, null, null, null, null, '1', null, null, null, null, null, null, null, null, '1468', null, null, null, null, null, null, '1', '7', null, null, '1', null, null, null, null, null, null, null, null, null, null, null, null, null, '6', null, null, null, null, null, null, null, null, null, '1', '2015-03-10 16:35:50', '19', '2015-04-28 15:59:07', '21', null, null, '0', null, '27', '3206', '535087', 'Středočeský kraj', 'Mělník', '1', 's');
INSERT INTO `estates` VALUES ('17', '2', '19', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '2015-04-08 16:44:03', '19', '2015-04-08 16:45:27', null, '2015-04-08 16:45:27', '20', '0', null, null, null, null, null, null, '0', null);
INSERT INTO `estates` VALUES ('18', '2', '19', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', '2015-04-13 15:11:26', '19', null, null, null, null, '0', null, null, null, null, null, null, '0', null);

-- ----------------------------
-- Table structure for `estates_cs`
-- ----------------------------
DROP TABLE IF EXISTS `estates_cs`;
CREATE TABLE `estates_cs` (
  `estate_id` int(11) NOT NULL,
  `estate_title` varchar(255) DEFAULT NULL,
  `estate_description` text,
  `advert_price_text_note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estates_cs
-- ----------------------------
INSERT INTO `estates_cs` VALUES ('1', 'Byt, 1+kk, osobní vlastnictví', 'Exkluzivně nabízíme k prodeji rodinný dům v malebné vesničce Kotoučov, okres Kutná Hora. Dům je po kompletní rekonstrukci, chybí pouze fasáda. Skládá se z obytné části (cca 100 m2) a z hospodářské části – chlévy (cca 100 m2). Obytnou plochu lze zvětšit dobudováním půdní vestavby (projekt je k dispozici). Obytná část je postavena z cihel, hospodářská část je postavena ze smíšeného zdiva z cihel a kamene. Na celé budově je nová střecha z roku 2007. V obytné části jsou nová špaletová okna, nové ústřední topení, kotel se zásobníkem na uhlí (ořech 2), nový el. bojler, který je současně napojen na ohřev z kotle. Obytnou část tvoří veranda, předsíň, kuchyň a obývací pokoj, 2 ložnice, koupelna a toaleta zvlášť. Odpady jsou svedeny do vlastní ČOV. Pitná voda je čerpána z vlastní studny.\nNa pozemku se nachází velká stodola o výměře cca 200 m2 se sklepem cca 40 m2. K objektu náleží pozemek o velikosti 2205 m2. K relaxaci slouží bazén a předzahrádka s domácí udírnou a příjemným posezením.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO `estates_cs` VALUES ('2', null, 'Krásný byt k pronájmu 3+kk o velikosti 80 m2 s lodžií, který prošel nákladnou rekonstrukcí se nachází ve druhém patře cihlové budovy v ulici Jeremenkova, v rezidenční čtvrti Podolí. Byt je velice útulný a luxusně vybavený. Kuchyňská linka se všemi spotřebiči. V okolí naleznete veškerou občanskou vybavenost, lokalita plná zeleně, parků a míst k relaxaci. Bezproblémová dopravní dostupnost do centra města, zastávka tramvaje minutu chůze. V blízkosti bytu je mnoho sportovního vyžití – golfové hřiště, žluté lázně, cyklostezka apod. Měsíční nájem činí 19tis. Poplatky za služby činí 500,- Kč/osobu měsíčně, elektřina a plyn se převádí na nájemce. Preferujeme dlouhodobý pronájem.', 'Poplatky za služby činí 500,- Kč/osobu měsíčně');
INSERT INTO `estates_cs` VALUES ('3', 'Pronájem bytu 3+kk, Praha', 'Krásný byt k pronájmu 3+kk o velikosti 80 m2 s lodžií, který prošel nákladnou rekonstrukcí se nachází ve druhém patře cihlové budovy v ulici Jeremenkova, v rezidenční čtvrti Podolí. Byt je velice útulný a luxusně vybavený. Kuchyňská linka se všemi spotřebiči. V okolí naleznete veškerou občanskou vybavenost, lokalita plná zeleně, parků a míst k relaxaci. Bezproblémová dopravní dostupnost do centra města, zastávka tramvaje minutu chůze. V blízkosti bytu je mnoho sportovního vyžití – golfové hřiště, žluté lázně, cyklostezka apod. Měsíční nájem činí 21tis. Poplatky za služby činí 500,- Kč/osobu měsíčně, elektřina a plyn se převádí na nájemce. Preferujeme dlouhodobý pronájem.\nV případě jakýchkoliv dotazů mě neváhejte kontaktovat.\nWe speak english.\n\n', '');
INSERT INTO `estates_cs` VALUES ('4', 'Rodinný dům, Kotoučov', 'Exkluzivně nabízíme k prodeji rodinný dům v malebné vesničce Kotoučov, okres Kutná Hora. Dům je po kompletní rekonstrukci, chybí pouze fasáda. Skládá se z obytné části (cca 100 m2) a z hospodářské části – chlévy (cca 100 m2). Obytnou plochu lze zvětšit dobudováním půdní vestavby (projekt je k dispozici). Obytná část je postavena z cihel, hospodářská část je postavena ze smíšeného zdiva z cihel a kamene. Na celé budově je nová střecha z roku 2007. V obytné části jsou nová špaletová okna, nové ústřední topení, kotel se zásobníkem na uhlí (ořech 2), nový el. bojler, který je současně napojen na ohřev z kotle. Obytnou část tvoří veranda, předsíň, kuchyň a obývací pokoj, 2 ložnice, koupelna a toaleta zvlášť. Odpady jsou svedeny do vlastní ČOV. Pitná voda je čerpána z vlastní studny.\nNa pozemku se nachází velká stodola o výměře cca 200 m2 se sklepem cca 40 m2. K objektu náleží pozemek o velikosti 2205 m2. K relaxaci slouží bazén a předzahrádka s domácí udírnou a příjemným posezením.\nV případě zájmu mě neváhejte kontaktovat, prohlídka je možná kdykoliv.', '');
INSERT INTO `estates_cs` VALUES ('5', 'Rodinný dům, Vojtěšín', 'Exkluzivně nabízíme k prodeji prostorný patrový zčásti podsklepený rodinný dům se zastavěnou plochou 250m2 nacházející se na velmi krásném, klidném místě v bezprostřední blízkosti lesa v obci Vojtěšín (Plzeň-sever). Dům je vystavěn na vlastním rozlehlém pozemku 3004m2 (zčásti slouží jako výběh pro koně, druhá část zahrady k relaxaci se vzrostlými ovocnými a okrasnými stromy) domluven odkup případně pronájem dalších 1000m2. Součástí objektu jsou hospodářské budovy. Stavba je napojena na elektrickou energii, kvalitní voda z vlastní studny. Momentálně probíhá kompletní nákladná rekonstrukce, voda, kanalizace, elektro, nová dřevěná eurookna, nové dřevěné dveře, rozvody ústředního topení - nový kotel Viadrus na tuhá paliva s akumulační nádobou 1000l, krbová kamna, nová fasáda - kontaktní zateplovací systém, koupelna s rohovou vanou 150x150, vybavení Hansgrohe, Villeroy & Boch, v přízemí 4 místnosti, v 1.patře příprava pro výstavbu 4 místností, prostorná půda. Internet 4G. Okolní zástavbu tvoří rodinné domy a chalupy se skvělými sousedy, kteří si vzájemně pomáhají - v dnešní době vzácné. Vojtěšín je v ojedinělou oázou klidu a pohody. Veškerá občanská vybavenost je v dosahu, obec Úněšov 7km, Plzeň 25km. Jedná se skutečně o krásné místo s malebnou přírodou, vhodné k rekreaci, houbaření, cyklistice, turistice. Možno financovat hypotékou. \nV případě jakýchkoliv dotazů mě neváhejte kontaktovat.', '');
INSERT INTO `estates_cs` VALUES ('6', 'Prodej rodinného domu Pitín', 'Exkluzivně nabízíme k prodeji přízemní rodinný dům 3+1 s příslušenstvím, navazující kůlnou, stodolou a půdou. Součástí domu je vstupní hala, komora, kuchyň, dva pokoje, koupelna a samostatná toaleta. K domu přímo přísluší nádvoří a ovocná zahrada.\nDům je napojen na veřejný vodovod, kanalizaci, elektřinu a plyn.\nCelková užitná plocha domu je 95 m2. Z nádvoří  119 m2 je přístupná kůlna a stodola s půdou. Na nádvoří navazuje zahrada s ovocnými stromy o výměře 591 m2. K domu dále náleží pozemky (pole a lesy) o celkové výměře 16 595 m2. \nDům je umístěn v klidné části obce s veškerou občanskou vybaveností – mateřská škola, základní škola, knihovna, hřiště, tenisové kurty, koupaliště, obchody, restaurace.\nDům je ihned k dispozici, možnost prohlídky.\n\n', '');
INSERT INTO `estates_cs` VALUES ('7', 'Prodej mezonetového bytu Praha 6', 'Exkluzivně nabízíme k prodeji půdní mezonetový byt o celkové ploše 120m², na lukrativním místě v Praze 6 na Ořechovce. Designový mezonetový apartmán situovaný v 3. a 4.NP, který vznikne v půdní vestavbě kompletně zrekonstruovaného prvorepublikového domu.\n\nDispozice první úrovně nabízí otevřený a vzdušný koncept obývacího pokoje s kuchyní, prostorem pro jídelní stůl a schodištěm vedoucím na galerii, centrální halu, koupelnu a samostatnou toaletu. Druhá úroveň je pak tvořena ložnicí a hlavní koupelnou s vanou a toaletou.\nVysoký standard bytu odpovídá současným představám o moderním, nadčasovém bydlení, které vkusně kombinuje původní architektonické prvky a plně uspokojí i náročnější klienty. Vybavení zahrnuje dřevěné palubkové podlahy, ateliérová, střešní okna, koupelnové zařizovací předměty Hansgrohe, Villeroy & Boch, klimatizaci, podlahové vytápění v koupelnách, intercom. Plánované dokončení 2015/2016.\n\nDům stojí v klidné, slepé ulici, s možností užívání vlastní zahrady. V okolí se nachází množství zeleně, kavárny, restaurace, školy, Ústřední vojenská nemocnice. Parkovat lze bezproblému před domem.', '');
INSERT INTO `estates_cs` VALUES ('8', 'Chata v Českém ráji', 'Exkluzivně nabízíme k prodeji krásnou chatu ve vyhledávané rekreační oblasti Drhleny v Českém ráji. Dvoupatrová zděná chata s prostornou terasou na jih, garáží a zahradou v chatové lokalitě na okraji lesa. Dispozice: obývací pokoj se vstupem na terasu, kuchyň, 2x ložnice, koupelna s vanou, WC. Prodej včetně veškerého vybavení. Koupání, rybolov, cyklostezky, turistika.', '');
INSERT INTO `estates_cs` VALUES ('9', 'Zahrada 1231m2', 'Exkluzivně nabízíme prodeji pozemek o celkové ploše 1231m2 vedený v katastru nemovitostí jako zahrada. ', '');
INSERT INTO `estates_cs` VALUES ('10', null, null, null);
INSERT INTO `estates_cs` VALUES ('11', null, null, null);
INSERT INTO `estates_cs` VALUES ('12', 'Vila, Otradovice', 'Exkluzivně nabízíme k prodeji samostatně stojící nízkoenergetickou vilu 6+1, v nadstandardním provedení, zastavěná plocha 151m2, pozemek 1133m2, garáž, v žádané lokalitě, 15minut severně od Prahy. V přízemí hala s vestavěným nábytkem, prostorný obývák spojený s jídelnou, velkorysá kuchyň na míru, koupelna se sprchovým koutem a toaletou, pracovna, technická místnost – tepelné čerpadlo vzduch/voda, elektrokotel. V prvním patře ložnice s šatnou, koupelnou se sprchovým koutem, toaletou a vstupem na velkou jižní terasu, dva dětské pokoje, koupelna s vanou a toaletou.\nDům je postaven v nové zástavbě na rovinatém pozemku v klidné části obce na okraji lesa. Ideální bydlení v přírodě se skvělou dostupností do centra Prahy. V místě veškerá občanská vybavenost. Zajistíme pro Vás výhodné financování. Volné ihned. \n', '');
INSERT INTO `estates_cs` VALUES ('13', 'Prodej bytu 4+1, Neratovice', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.\n\nDEMO - Neexistující ukázková nemovitost', '');
INSERT INTO `estates_cs` VALUES ('14', 'Prodej rodinného domu 7+2, Neratovice', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.', '');
INSERT INTO `estates_cs` VALUES ('15', 'Kancelářské prostory k pronájmu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.', 'cena zahrnuje i připojení k internetu');
INSERT INTO `estates_cs` VALUES ('16', 'Prodej údolí 2 mil. m2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel odio turpis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec laoreet volutpat facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse rutrum magna ac hendrerit dapibus. Morbi vel enim eget quam convallis convallis vel ac enim. Suspendisse congue quis nisl vitae varius. Duis pretium vitae enim eget fringilla. Fusce eget erat sed turpis lobortis accumsan. Ut sodales diam nec sapien suscipit, at finibus odio porttitor. Duis non urna ac arcu cursus accumsan. Aliquam ac eros vitae leo efficitur maximus sit amet ac sem. Donec nec dolor tortor. Nam ut urna imperdiet ex ornare aliquam. Maecenas velit erat, fringilla eget dolor eu, efficitur eleifend justo.', '');
INSERT INTO `estates_cs` VALUES ('17', null, null, null);
INSERT INTO `estates_cs` VALUES ('18', null, null, null);

-- ----------------------------
-- Table structure for `estates_en`
-- ----------------------------
DROP TABLE IF EXISTS `estates_en`;
CREATE TABLE `estates_en` (
  `estate_id` int(11) NOT NULL,
  `estate_title` varchar(255) DEFAULT NULL,
  `estate_description` text,
  `advert_price_text_note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estates_en
-- ----------------------------
INSERT INTO `estates_en` VALUES ('1', 'dsf', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque molestie vel metus ac venenatis. Fusce finibus in velit et luctus. Suspendisse ac urna nulla. Vestibulum justo dui, fringilla in mauris ut, consectetur cursus neque. Suspendisse interdum justo arcu, rhoncus pellentesque mauris molestie in. Nulla neque velit, posuere at sem eu, ullamcorper faucibus nunc. Maecenas nec nisl eu odio convallis tincidunt et in purus.\n\nDonec sed metus sollicitudin, imperdiet tellus ac, elementum arcu. Integer a pretium metus, id finibus lacus. Etiam tellus leo, sodales et erat et, pharetra interdum quam. Etiam sed lacinia lacus. Quisque odio nisl, aliquet nec dui ut, ornare interdum ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et imperdiet lorem. Cras tincidunt posuere urna. Sed vitae purus libero. Sed at ex eu dolor porttitor eleifend. Etiam eget tortor id augue rutrum venenatis. Maecenas neque diam, efficitur eget accumsan nec, fringilla at tortor. Integer ut posuere arcu, non dignissim augue. Donec a quam efficitur, venenatis ipsum eget, consectetur tellus.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.');
INSERT INTO `estates_en` VALUES ('2', null, null, null);

-- ----------------------------
-- Table structure for `export`
-- ----------------------------
DROP TABLE IF EXISTS `export`;
CREATE TABLE `export` (
  `estate_id` int(11) NOT NULL AUTO_INCREMENT,
  `images` timestamp NULL DEFAULT NULL,
  `images_error` varchar(255) DEFAULT NULL,
  `sreality` timestamp NULL DEFAULT NULL,
  `sreality_error` varchar(255) DEFAULT NULL,
  `idnes` timestamp NULL DEFAULT NULL,
  `idnes_error` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`estate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of export
-- ----------------------------
INSERT INTO `export` VALUES ('6', '2015-03-22 10:30:03', null, '2015-03-22 10:30:08', null, null, null, '2015-02-14 10:52:41', '1', '2015-03-22 10:30:08', '9', null, null);
INSERT INTO `export` VALUES ('7', '2015-04-03 12:14:03', null, '2015-04-03 12:14:03', null, '2015-04-03 12:14:03', null, '2015-02-14 11:41:52', '1', '2015-04-03 12:14:03', '9', null, null);
INSERT INTO `export` VALUES ('8', '2015-04-09 07:50:02', null, '2015-04-09 07:50:05', null, null, null, '2015-02-14 11:41:53', '1', '2015-04-09 07:50:05', '9', null, null);
INSERT INTO `export` VALUES ('9', '2015-03-22 19:44:01', null, '2015-03-22 19:44:01', null, '2015-03-22 19:44:01', null, '2015-02-14 11:41:55', '1', '2015-03-22 19:44:01', '9', null, null);
INSERT INTO `export` VALUES ('14', '2015-03-30 19:02:00', null, null, null, null, null, '2015-03-10 16:28:17', '19', '2015-03-30 19:02:00', '19', null, null);
INSERT INTO `export` VALUES ('4', '2015-04-03 12:11:05', null, '2015-04-03 12:11:12', null, null, null, '2015-02-14 11:21:54', '1', '2015-04-03 12:11:12', '9', null, null);
INSERT INTO `export` VALUES ('5', '2015-04-03 12:12:03', null, '2015-04-03 12:12:08', null, null, null, '2015-02-14 11:41:28', '1', '2015-04-03 12:12:08', '9', null, null);
INSERT INTO `export` VALUES ('15', '2015-03-30 20:57:00', null, null, null, null, null, '2015-03-10 16:35:09', '19', '2015-03-30 20:57:00', '19', null, null);
INSERT INTO `export` VALUES ('16', '2015-04-13 12:42:01', null, null, null, null, null, '2015-03-10 16:54:57', '19', '2015-04-13 12:42:01', '19', null, null);
INSERT INTO `export` VALUES ('3', '2015-04-09 09:28:03', null, '2015-04-09 09:28:03', null, '2015-04-09 09:28:04', null, '2015-02-14 11:41:24', '1', '2015-04-09 09:28:04', '9', null, null);
INSERT INTO `export` VALUES ('12', '2015-03-22 10:32:04', null, '2015-03-22 10:32:04', null, '2015-03-22 10:32:05', null, '2015-02-22 22:02:57', '15', '2015-03-22 10:32:05', '9', null, null);
INSERT INTO `export` VALUES ('13', null, null, null, null, null, null, '2015-03-10 15:33:19', '19', '2015-04-16 12:24:42', '21', null, null);

-- ----------------------------
-- Table structure for `export_brokers`
-- ----------------------------
DROP TABLE IF EXISTS `export_brokers`;
CREATE TABLE `export_brokers` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `broker_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `export_timestamp` timestamp NULL DEFAULT NULL,
  `last_export_timestamp` timestamp NULL DEFAULT NULL,
  `message` text,
  `url` varchar(255) DEFAULT NULL,
  `export` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`export_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of export_brokers
-- ----------------------------
INSERT INTO `export_brokers` VALUES ('2', '19', '1', '2015-04-28 10:56:11', '2015-04-28 10:56:11', '200 - OK', null, '1', '2015-04-21 12:21:41', '1', '2015-04-28 10:56:11', '21', null, null);
INSERT INTO `export_brokers` VALUES ('4', '19', '3', null, null, null, null, '1', '2015-04-24 16:16:19', '21', '2015-04-28 10:55:39', '21', null, null);
INSERT INTO `export_brokers` VALUES ('5', '19', '4', null, '2015-04-24 16:19:26', '200 - OK', null, '1', '2015-04-24 16:16:19', '21', '2015-04-28 10:55:39', '21', null, null);

-- ----------------------------
-- Table structure for `export_portals`
-- ----------------------------
DROP TABLE IF EXISTS `export_portals`;
CREATE TABLE `export_portals` (
  `export_id` int(11) NOT NULL AUTO_INCREMENT,
  `estate_id` int(11) DEFAULT NULL,
  `portal_id` int(11) DEFAULT NULL,
  `export_timestamp` timestamp NULL DEFAULT NULL,
  `last_export_timestamp` timestamp NULL DEFAULT NULL,
  `message` text,
  `export` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`export_id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of export_portals
-- ----------------------------
INSERT INTO `export_portals` VALUES ('16', '13', '1', '2015-03-14 16:42:20', '2015-04-27 11:23:27', '200 - OK', '1', '2015-04-16 12:55:53', '21', '2015-04-28 16:42:23', '21', null, null, 'http://www.sreality.cz/detail/prodej/byt/4+1/neratovice-neratovice-28--rijna/4294705244');
INSERT INTO `export_portals` VALUES ('17', '13', '2', null, '2015-04-23 16:55:08', 'OK Nemovitost byla úspěšně aktualizována (id= 65504) / Čas zpracování 0.207s POZOR! Detekovali jsme reimport a inzerát vrátili na původní místo ve výpisu. Chcete-li se dostat na přední pozice, použijte HOPOVÁNÍ. POZOR! Následující atributy vykazují nesrovnalosti oproti požadavkům: ( [makler_mobil] => údaj neodpovídá tel. číslu mobilního operátora ) ', '1', '2015-04-16 12:55:53', '21', '2015-04-24 15:20:30', '21', null, null, 'http://rk.mobil.cz/detail/prodej/byt/4+1/neratovice-28-rijna/65504');
INSERT INTO `export_portals` VALUES ('18', '14', '1', null, '2015-04-28 17:34:23', '200 - OK', '0', '2015-04-16 15:58:04', '21', '2015-04-29 17:23:48', '21', null, null, 'http://www.sreality.cz/detail/prodej/dum/rodinny/neratovice-neratovice-polni/1845231708');
INSERT INTO `export_portals` VALUES ('19', '14', '2', null, '2015-04-23 16:55:15', 'OK Nemovitost byla úspěšně aktualizována (id= 65505) / Čas zpracování 0.621s POZOR! Následující atributy vykazují nesrovnalosti oproti požadavkům: ( [makler_mobil] => údaj neodpovídá tel. číslu mobilního operátora ) ', '0', '2015-04-16 15:58:04', '21', '2015-04-29 17:23:49', '21', null, null, 'http://rk.mobil.cz/detail/prodej/dum/samostatny/neratovice-polni/65505');
INSERT INTO `export_portals` VALUES ('22', '13', '4', '2015-03-22 17:36:46', null, null, '0', '2015-04-24 15:20:12', '21', '2015-04-28 17:36:48', '21', null, null, null);
INSERT INTO `export_portals` VALUES ('23', '14', '4', null, '2015-04-28 17:41:41', '200 - OK', '0', '2015-04-24 15:20:43', '21', '2015-04-29 17:23:49', '21', null, null, 'http://www.realcity.cz/nemovitost/prodej-domu-70-m-neratovice-polni/3040152');
INSERT INTO `export_portals` VALUES ('24', '16', '1', null, null, null, '0', '2015-04-28 14:51:19', '21', '2015-04-28 15:59:07', '21', null, null, null);
INSERT INTO `export_portals` VALUES ('25', '16', '2', null, null, null, '0', '2015-04-28 14:51:19', '21', '2015-04-28 15:59:07', '21', null, null, null);
INSERT INTO `export_portals` VALUES ('26', '16', '3', '2015-04-28 16:00:44', '2015-04-28 16:00:44', '200 - OK', '0', '2015-04-28 14:51:19', '21', '2015-04-28 16:00:44', '21', null, null, null);
INSERT INTO `export_portals` VALUES ('20', '13', '3', '2015-03-14 10:23:52', null, '452 - Povinná položka building_area chybí a nebo je prázdná.', '1', '2015-04-21 17:05:26', '1', '2015-04-29 10:23:54', '21', null, null, null);
INSERT INTO `export_portals` VALUES ('21', '14', '3', null, '2015-04-29 12:01:03', '200 - OK', '0', '2015-04-22 15:55:43', '1', '2015-04-29 17:23:49', '21', null, null, 'nourl');
INSERT INTO `export_portals` VALUES ('27', '16', '4', null, null, null, '0', '2015-04-28 14:51:19', '21', '2015-04-28 15:59:07', '21', null, null, null);

-- ----------------------------
-- Table structure for `files`
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `file_src` varchar(255) NOT NULL,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `file_rank` int(11) DEFAULT NULL,
  `file_main` tinyint(1) NOT NULL,
  `file_rel` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES ('1', '1', 'theme/www-belartis-cz/files/estates/1420992875.jpg', 'estates', '1', '2', '1', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', null, '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('2', '1', 'theme/www-belartis-cz/files/estates/1420997523.JPG', 'estates', '1', '1', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('3', '1', 'theme/www-belartis-cz/files/estates/1420997523_1.JPG', 'estates', '1', '2', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('4', '1', 'theme/www-belartis-cz/files/estates/1420997523_2.JPG', 'estates', '1', '3', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('5', '1', 'theme/www-belartis-cz/files/estates/1420997523_3.JPG', 'estates', '1', '4', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('6', '1', 'theme/www-belartis-cz/files/estates/1420997523_4.JPG', 'estates', '1', '5', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('7', '1', 'theme/www-belartis-cz/files/estates/1420997523_5.JPG', 'estates', '1', '6', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('8', '1', 'theme/www-belartis-cz/files/estates/1420997523_6.JPG', 'estates', '1', '7', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('9', '1', 'theme/www-belartis-cz/files/estates/1421000392.JPG', 'estates', '1', '8', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('10', '1', 'theme/www-belartis-cz/files/estates/1421000392_1.JPG', 'estates', '1', '9', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('11', '1', 'theme/www-belartis-cz/files/estates/1421000392_2.JPG', 'estates', '1', '10', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('12', '1', 'theme/www-belartis-cz/files/estates/1421000392_3.JPG', 'estates', '1', '11', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('13', '1', 'theme/www-belartis-cz/files/estates/1421000392_4.JPG', 'estates', '1', '12', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('14', '1', 'theme/www-belartis-cz/files/estates/1421000392_5.JPG', 'estates', '1', '13', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('15', '1', 'theme/www-belartis-cz/files/estates/1421000392_6.JPG', 'estates', '1', '14', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('16', '1', 'theme/www-belartis-cz/files/estates/1421000392_7.JPG', 'estates', '1', '15', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('17', '1', 'theme/www-belartis-cz/files/estates/1421000392_8.JPG', 'estates', '1', '16', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('18', '1', 'theme/www-belartis-cz/files/estates/1421000392_9.JPG', 'estates', '1', '17', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('19', '1', 'theme/www-belartis-cz/files/estates/1421000392_10.JPG', 'estates', '1', '18', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('20', '1', 'theme/www-belartis-cz/files/estates/1421000392_11.JPG', 'estates', '1', '19', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('21', '1', 'theme/www-belartis-cz/files/estates/1421000392_12.JPG', 'estates', '1', '20', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('22', '1', 'theme/www-belartis-cz/files/estates/1421000392_13.JPG', 'estates', '1', '21', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('23', '1', 'theme/www-belartis-cz/files/estates/1421000392_14.JPG', 'estates', '1', '22', '0', 'images', '2015-01-20 09:30:48', '0', '2015-02-12 12:29:57', '9', '2015-01-20 09:30:48', '12');
INSERT INTO `files` VALUES ('24', '1', 'theme/www-belartis-cz/files/estates/1421004931.JPG', 'estates', '2', '2', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('25', '1', 'theme/www-belartis-cz/files/estates/1421004931_1.JPG', 'estates', '2', '3', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('26', '1', 'theme/www-belartis-cz/files/estates/1421004931_2.jpg', 'estates', '2', '4', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('27', '1', 'theme/www-belartis-cz/files/estates/1421004931_3.JPG', 'estates', '2', '5', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('28', '1', 'theme/www-belartis-cz/files/estates/1421004931_4.JPG', 'estates', '2', '6', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('29', '1', 'theme/www-belartis-cz/files/estates/1421004931_5.JPG', 'estates', '2', '7', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('30', '1', 'theme/www-belartis-cz/files/estates/1421004931_6.jpg', 'estates', '2', '8', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('31', '1', 'theme/www-belartis-cz/files/estates/1421004931_7.JPG', 'estates', '2', '9', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('32', '1', 'theme/www-belartis-cz/files/estates/1421004931_8.JPG', 'estates', '2', '10', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('33', '1', 'theme/www-belartis-cz/files/estates/1421004931_9.JPG', 'estates', '2', '11', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('34', '1', 'theme/www-belartis-cz/files/estates/1421004931_10.JPG', 'estates', '2', '12', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('35', '1', 'theme/www-belartis-cz/files/estates/1421004946.JPG', 'estates', '2', '13', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('36', '1', 'theme/www-belartis-cz/files/estates/1421004946_1.JPG', 'estates', '2', '14', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('37', '1', 'theme/www-belartis-cz/files/estates/1421004946_2.JPG', 'estates', '2', '15', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('38', '1', 'theme/www-belartis-cz/files/estates/1421004946_3.JPG', 'estates', '2', '16', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('39', '1', 'theme/www-belartis-cz/files/estates/1421004946_4.JPG', 'estates', '2', '17', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('40', '1', 'theme/www-belartis-cz/files/estates/1421004946_5.JPG', 'estates', '2', '18', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('41', '1', 'theme/www-belartis-cz/files/estates/1421004946_6.JPG', 'estates', '2', '19', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('42', '1', 'theme/www-belartis-cz/files/estates/1421004946_7.JPG', 'estates', '2', '20', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('43', '1', 'theme/www-belartis-cz/files/estates/1421004946_8.JPG', 'estates', '2', '21', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('44', '1', 'theme/www-belartis-cz/files/estates/1421004946_9.JPG', 'estates', '2', '22', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('45', '1', 'theme/www-belartis-cz/files/estates/1421004946_10.JPG', 'estates', '2', '23', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('46', '1', 'theme/www-belartis-cz/files/estates/1421004946_11.JPG', 'estates', '2', '24', '0', 'images', '2015-01-19 22:06:50', '0', '2015-02-12 12:29:57', null, '2015-01-19 22:06:50', '9');
INSERT INTO `files` VALUES ('47', '1', 'theme/www-belartis-cz/files/estates/private/1421075060.png', 'users', '2', '0', '0', 'profile_picture', '2015-01-12 16:04:20', '0', '2015-02-12 12:29:57', null, null, null);
INSERT INTO `files` VALUES ('48', '1', 'theme/www-belartis-cz/files/brokers/1423087796.jpg', 'users_broker', '11', '1', '0', 'portrait', '2015-02-04 23:10:12', '8', '2015-02-20 15:17:12', '1', null, null);
INSERT INTO `files` VALUES ('49', '0', '', null, null, null, '0', null, '2015-01-20 10:26:05', '11', null, null, null, null);
INSERT INTO `files` VALUES ('50', '0', 'theme/www-belartis-cz/files/estates/1421778599.JPG', 'estates', '3', '1', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('51', '0', 'theme/www-belartis-cz/files/estates/1421778629.JPG', 'estates', '3', '2', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('52', '0', 'theme/www-belartis-cz/files/estates/1421778643.JPG', 'estates', '3', '3', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('53', '0', 'theme/www-belartis-cz/files/estates/1421778676.JPG', 'estates', '3', '4', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('54', '0', 'theme/www-belartis-cz/files/estates/1421778704.JPG', 'estates', '3', '5', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('55', '0', 'theme/www-belartis-cz/files/estates/1421778733.JPG', 'estates', '3', '6', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('56', '0', 'theme/www-belartis-cz/files/estates/1421778763.JPG', 'estates', '3', '7', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('57', '0', 'theme/www-belartis-cz/files/estates/1421778801.JPG', 'estates', '3', '8', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('58', '0', 'theme/www-belartis-cz/files/estates/1421778823.JPG', 'estates', '3', '9', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', '2015-04-09 09:27:14', '9');
INSERT INTO `files` VALUES ('59', '0', 'theme/www-belartis-cz/files/estates/1421783001.JPG', 'estates', '4', '1', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('60', '0', 'theme/www-belartis-cz/files/estates/1421783016.JPG', 'estates', '4', '2', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('61', '0', 'theme/www-belartis-cz/files/estates/1421783049.JPG', 'estates', '4', '3', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('62', '0', 'theme/www-belartis-cz/files/estates/1421783099.JPG', 'estates', '4', '4', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('63', '0', 'theme/www-belartis-cz/files/estates/1421783124.JPG', 'estates', '4', '5', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('64', '0', 'theme/www-belartis-cz/files/estates/1421783142.JPG', 'estates', '4', '6', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('65', '0', 'theme/www-belartis-cz/files/estates/1421783175.JPG', 'estates', '4', '7', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('66', '0', 'theme/www-belartis-cz/files/estates/1421783196.JPG', 'estates', '4', '8', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('67', '0', 'theme/www-belartis-cz/files/estates/1421783216.JPG', 'estates', '4', '9', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('68', '0', 'theme/www-belartis-cz/files/estates/1421783235.JPG', 'estates', '4', '10', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('69', '0', 'theme/www-belartis-cz/files/estates/1421783251.JPG', 'estates', '4', '11', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('70', '0', 'theme/www-belartis-cz/files/estates/1421783267.JPG', 'estates', '4', '12', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('71', '0', 'theme/www-belartis-cz/files/estates/1421783286.JPG', 'estates', '4', '13', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('72', '0', 'theme/www-belartis-cz/files/estates/1421783302.JPG', 'estates', '4', '14', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', '2015-04-03 12:10:03', '9');
INSERT INTO `files` VALUES ('73', '0', 'theme/www-belartis-cz/files/estates/1421783817.jpg', 'estates', '5', '1', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('74', '0', 'theme/www-belartis-cz/files/estates/1421783848.JPG', 'estates', '5', '2', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('75', '0', 'theme/www-belartis-cz/files/estates/1421783874.JPG', 'estates', '5', '3', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('76', '0', 'theme/www-belartis-cz/files/estates/1421783901.JPG', 'estates', '5', '4', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('77', '0', 'theme/www-belartis-cz/files/estates/1421783923.JPG', 'estates', '5', '5', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('78', '0', 'theme/www-belartis-cz/files/estates/1421783959.JPG', 'estates', '5', '6', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('79', '0', 'theme/www-belartis-cz/files/estates/1421783995.JPG', 'estates', '5', '7', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('80', '0', 'theme/www-belartis-cz/files/estates/1421784043.JPG', 'estates', '5', '8', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('81', '0', 'theme/www-belartis-cz/files/estates/1421784075.JPG', 'estates', '5', '9', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('82', '0', 'theme/www-belartis-cz/files/estates/1421784108.JPG', 'estates', '5', '10', '0', 'images', '2015-01-28 12:58:26', '11', '2015-04-03 12:11:46', '9', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('83', '0', 'theme/www-belartis-cz/files/estates/1421831236.png', 'estates', '6', '1', '0', 'images', '2015-01-25 18:32:20', '8', '2015-03-22 10:29:01', null, '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('84', '0', 'theme/www-belartis-cz/files/estates/1421833906.JPG', 'estates', '7', '1', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('85', '0', 'theme/www-belartis-cz/files/estates/1421834290.jpg', 'estates', '7', '2', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('86', '0', 'theme/www-belartis-cz/files/estates/1421834886.jpg', 'estates', '7', '3', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('87', '0', 'theme/www-belartis-cz/files/estates/1421834942.jpg', 'estates', '7', '4', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('88', '0', 'theme/www-belartis-cz/files/estates/1421838200.jpg', 'estates', '7', '5', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('89', '0', 'theme/www-belartis-cz/files/estates/1421838229.jpg', 'estates', '7', '6', '0', 'images', '2015-01-25 18:44:17', '11', '2015-04-03 12:13:18', '11', '2015-04-03 12:13:18', '9');
INSERT INTO `files` VALUES ('90', '0', 'theme/www-belartis-cz/files/estates/1421838373.jpg', 'estates', '6', '1', '0', 'images', '2015-01-25 18:32:20', '11', '2015-03-22 10:29:01', null, '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('91', '0', 'theme/www-belartis-cz/files/estates/1421838513.jpg', 'estates', '6', '1', '0', 'images', '2015-01-25 18:32:20', '11', '2015-03-22 10:29:01', '12', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('92', '0', 'theme/www-belartis-cz/files/estates/1421855124.jpg', 'estates', '6', '1', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('93', '0', 'theme/www-belartis-cz/files/estates/1421855185.jpg', 'estates', '6', '2', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('94', '0', 'theme/www-belartis-cz/files/estates/1421855202.jpg', 'estates', '6', '3', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('95', '0', 'theme/www-belartis-cz/files/estates/1421855232.jpg', 'estates', '6', '4', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('96', '0', 'theme/www-belartis-cz/files/estates/1421855247.jpg', 'estates', '6', '5', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('97', '0', 'theme/www-belartis-cz/files/estates/1421855266.jpg', 'estates', '6', '6', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('98', '0', 'theme/www-belartis-cz/files/estates/1421855300.jpg', 'estates', '6', '7', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('99', '0', 'theme/www-belartis-cz/files/estates/1421855370.jpg', 'estates', '6', '8', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('100', '0', 'theme/www-belartis-cz/files/estates/1421855318.jpg', 'estates', '6', '9', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('101', '0', 'theme/www-belartis-cz/files/estates/1421855332.jpg', 'estates', '6', '10', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('102', '0', 'theme/www-belartis-cz/files/estates/1421855391.jpg', 'estates', '6', '11', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', '2015-03-22 10:29:01', '9');
INSERT INTO `files` VALUES ('103', '0', 'theme/www-belartis-cz/files/estates/1421858664.jpg', 'estates', '8', '1', '0', 'images', '2015-01-25 18:45:39', '11', '2015-04-09 07:49:40', '9', '2015-04-09 07:49:40', '9');
INSERT INTO `files` VALUES ('104', '0', 'theme/www-belartis-cz/files/estates/1421859179.JPG', 'estates', '8', '2', '0', 'images', '2015-01-25 18:45:39', '11', '2015-04-09 07:49:40', '9', '2015-04-09 07:49:40', '9');
INSERT INTO `files` VALUES ('105', '0', '', null, '13', '1', '0', null, '2015-01-22 14:29:08', '9', null, null, '2015-01-22 14:29:08', '9');
INSERT INTO `files` VALUES ('106', '0', '', null, '14', '1', '0', null, '2015-01-22 14:28:47', '9', null, null, null, null);
INSERT INTO `files` VALUES ('107', '0', '', null, '13', '1', '0', null, '2015-01-22 14:29:08', '9', null, null, null, null);
INSERT INTO `files` VALUES ('108', '0', 'theme/www-belartis-cz/files/estates/1421939382.JPG', 'estates', '5', '11', '1', 'images', '2015-01-28 12:58:26', '12', '2015-04-03 12:11:46', null, '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('109', '0', 'theme/www-belartis-cz/files/estates/1421960623.JPG', 'estates', '9', '1', '0', 'images', '2015-01-25 19:08:56', '11', '2015-03-22 19:43:12', '9', '2015-03-22 19:43:12', '9');
INSERT INTO `files` VALUES ('110', '0', 'theme/www-belartis-cz/files/estates/1421960652.JPG', 'estates', '9', '2', '0', 'images', '2015-01-25 19:08:56', '11', '2015-03-22 19:43:12', '9', '2015-03-22 19:43:12', '9');
INSERT INTO `files` VALUES ('111', '0', 'theme/www-belartis-cz/files/estates/1421960672.JPG', 'estates', '9', '1', '0', 'images', '2015-01-25 19:08:56', '11', '2015-03-22 19:43:12', '9', '2015-03-22 19:43:12', '9');
INSERT INTO `files` VALUES ('112', '0', 'theme/www-belartis-cz/files/estates/1421960698.JPG', 'estates', '9', '2', '0', 'images', '2015-01-25 19:08:56', '11', '2015-03-22 19:43:12', '9', '2015-03-22 19:43:12', '9');
INSERT INTO `files` VALUES ('113', '0', 'theme/www-belartis-cz/files/estates/1421960717.jpg', 'estates', '9', '3', '0', 'images', '2015-01-25 19:08:56', '11', '2015-03-22 19:43:12', '9', '2015-03-22 19:43:12', '9');
INSERT INTO `files` VALUES ('114', '1', 'theme/www-belartis-cz/files/brokers/1422994292.png', 'users_broker', '13', '1', '0', 'portrait', '2015-02-03 21:11:35', '8', '2015-02-20 15:17:09', '1', null, null);
INSERT INTO `files` VALUES ('115', '0', 'theme/www-belartis-cz/files/estates/1422204253.JPG', 'estates', '3', '1', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('116', '0', 'theme/www-belartis-cz/files/estates/1422204398.JPG', 'estates', '3', '2', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('117', '0', 'theme/www-belartis-cz/files/estates/1422204412.JPG', 'estates', '3', '3', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('118', '0', 'theme/www-belartis-cz/files/estates/1422204421.JPG', 'estates', '3', '4', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('119', '0', 'theme/www-belartis-cz/files/estates/1422204440.JPG', 'estates', '3', '5', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('120', '0', 'theme/www-belartis-cz/files/estates/1422204447.JPG', 'estates', '3', '6', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('121', '0', 'theme/www-belartis-cz/files/estates/1422204457.JPG', 'estates', '3', '7', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('122', '0', 'theme/www-belartis-cz/files/estates/1422204490.JPG', 'estates', '3', '8', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('123', '0', 'theme/www-belartis-cz/files/estates/1422204516.JPG', 'estates', '3', '9', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('124', '0', 'theme/www-belartis-cz/files/estates/1422204534.JPG', 'estates', '3', '10', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('125', '0', 'theme/www-belartis-cz/files/estates/1422204550.JPG', 'estates', '3', '11', '0', 'images', '2015-02-04 23:11:06', '9', '2015-04-09 09:27:14', '9', null, null);
INSERT INTO `files` VALUES ('126', '0', 'theme/www-belartis-cz/files/estates/1422205797.JPG', 'estates', '4', '1', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('127', '0', 'theme/www-belartis-cz/files/estates/1422205797_1.JPG', 'estates', '4', '2', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('128', '0', 'theme/www-belartis-cz/files/estates/1422205797_2.JPG', 'estates', '4', '3', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('129', '0', 'theme/www-belartis-cz/files/estates/1422205797_3.JPG', 'estates', '4', '4', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('130', '0', 'theme/www-belartis-cz/files/estates/1422205797_4.JPG', 'estates', '4', '5', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('131', '0', 'theme/www-belartis-cz/files/estates/1422205797_5.JPG', 'estates', '4', '6', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('132', '0', 'theme/www-belartis-cz/files/estates/1422205797_6.JPG', 'estates', '4', '7', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('133', '0', 'theme/www-belartis-cz/files/estates/1422205797_7.JPG', 'estates', '4', '8', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('134', '0', 'theme/www-belartis-cz/files/estates/1422205797_8.JPG', 'estates', '4', '9', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('135', '0', 'theme/www-belartis-cz/files/estates/1422205797_9.JPG', 'estates', '4', '10', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('136', '0', 'theme/www-belartis-cz/files/estates/1422205797_10.JPG', 'estates', '4', '11', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('137', '0', 'theme/www-belartis-cz/files/estates/1422205797_11.JPG', 'estates', '4', '12', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('138', '0', 'theme/www-belartis-cz/files/estates/1422205797_12.JPG', 'estates', '4', '13', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('139', '0', 'theme/www-belartis-cz/files/estates/1422205797_13.JPG', 'estates', '4', '14', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('140', '0', 'theme/www-belartis-cz/files/estates/1422205797_14.JPG', 'estates', '4', '15', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('141', '0', 'theme/www-belartis-cz/files/estates/1422205797_15.JPG', 'estates', '4', '16', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('142', '0', 'theme/www-belartis-cz/files/estates/1422205797_16.JPG', 'estates', '4', '17', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('143', '0', 'theme/www-belartis-cz/files/estates/1422205797_17.JPG', 'estates', '4', '18', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('144', '0', 'theme/www-belartis-cz/files/estates/1422205797_18.JPG', 'estates', '4', '19', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('145', '0', 'theme/www-belartis-cz/files/estates/1422205797_19.JPG', 'estates', '4', '20', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('146', '0', 'theme/www-belartis-cz/files/estates/1422205930.JPG', 'estates', '4', '21', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('147', '0', 'theme/www-belartis-cz/files/estates/1422205930_1.JPG', 'estates', '4', '22', '0', 'images', '2015-01-28 12:58:49', '9', '2015-04-03 12:10:03', '9', null, null);
INSERT INTO `files` VALUES ('148', '0', 'theme/www-belartis-cz/files/estates/1422206056.JPG', 'estates', '5', '1', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', null, '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('149', '0', 'theme/www-belartis-cz/files/estates/1422206056_1.JPG', 'estates', '5', '15', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('150', '0', 'theme/www-belartis-cz/files/estates/1422206056_2.JPG', 'estates', '5', '16', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('151', '0', 'theme/www-belartis-cz/files/estates/1422206056_3.JPG', 'estates', '5', '3', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('152', '0', 'theme/www-belartis-cz/files/estates/1422206056_4.JPG', 'estates', '5', '5', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', null, '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('153', '0', 'theme/www-belartis-cz/files/estates/1422206056_5.JPG', 'estates', '5', '6', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', null, '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('154', '0', 'theme/www-belartis-cz/files/estates/1422206056_6.JPG', 'estates', '5', '4', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('155', '0', 'theme/www-belartis-cz/files/estates/1422206056_7.JPG', 'estates', '5', '5', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('156', '0', 'theme/www-belartis-cz/files/estates/1422206056.jpg', 'estates', '5', '1', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '1', '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('157', '0', 'theme/www-belartis-cz/files/estates/1422206056_8.JPG', 'estates', '5', '2', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('158', '0', 'theme/www-belartis-cz/files/estates/1422206056_9.JPG', 'estates', '5', '11', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', null, '2015-04-03 12:11:46', '9');
INSERT INTO `files` VALUES ('159', '0', 'theme/www-belartis-cz/files/estates/1422206928_3.JPG', 'estates', '5', '6', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('160', '0', 'theme/www-belartis-cz/files/estates/1422206257.JPG', 'estates', '5', '7', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('161', '0', 'theme/www-belartis-cz/files/estates/1422206257_4.JPG', 'estates', '5', '8', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('162', '0', 'theme/www-belartis-cz/files/estates/1422206257_7.JPG', 'estates', '5', '9', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('163', '0', 'theme/www-belartis-cz/files/estates/1422206257_8.JPG', 'estates', '5', '10', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('164', '0', 'theme/www-belartis-cz/files/estates/1422206257_10.JPG', 'estates', '5', '11', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('165', '0', 'theme/www-belartis-cz/files/estates/1422206257_11.JPG', 'estates', '5', '12', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('166', '0', 'theme/www-belartis-cz/files/estates/1422206928.JPG', 'estates', '5', '13', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('167', '0', 'theme/www-belartis-cz/files/estates/1422206928_1.JPG', 'estates', '5', '14', '0', 'images', '2015-01-28 12:58:26', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('168', '0', 'theme/www-belartis-cz/files/estates/1422207053.jpg', 'estates', '6', '1', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('169', '0', 'theme/www-belartis-cz/files/estates/1422207053_1.jpg', 'estates', '6', '2', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('170', '0', 'theme/www-belartis-cz/files/estates/1422207053_2.jpg', 'estates', '6', '3', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('171', '0', 'theme/www-belartis-cz/files/estates/1422207053_3.jpg', 'estates', '6', '4', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('172', '0', 'theme/www-belartis-cz/files/estates/1422207053_4.jpg', 'estates', '6', '5', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('173', '0', 'theme/www-belartis-cz/files/estates/1422207053_5.jpg', 'estates', '6', '6', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('174', '0', 'theme/www-belartis-cz/files/estates/1422207053_6.jpg', 'estates', '6', '7', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('175', '0', 'theme/www-belartis-cz/files/estates/1422207053_7.jpg', 'estates', '6', '8', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('176', '0', 'theme/www-belartis-cz/files/estates/1422207053_8.jpg', 'estates', '6', '9', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('177', '0', 'theme/www-belartis-cz/files/estates/1422207053_9.jpg', 'estates', '6', '10', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('178', '0', 'theme/www-belartis-cz/files/estates/1422207053_10.jpg', 'estates', '6', '11', '0', 'images', '2015-01-25 18:32:20', '9', '2015-03-22 10:29:01', '9', null, null);
INSERT INTO `files` VALUES ('179', '0', 'theme/www-belartis-cz/files/estates/1422207844.JPG', 'estates', '7', '1', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('180', '0', 'theme/www-belartis-cz/files/estates/1422207431.jpg', 'estates', '7', '2', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('181', '0', 'theme/www-belartis-cz/files/estates/1422207431_1.jpg', 'estates', '7', '3', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('182', '0', 'theme/www-belartis-cz/files/estates/1422207431_2.jpg', 'estates', '7', '4', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('183', '0', 'theme/www-belartis-cz/files/estates/1422207431_3.jpg', 'estates', '7', '5', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('184', '0', 'theme/www-belartis-cz/files/estates/1422207431_4.jpg', 'estates', '7', '6', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('185', '0', 'theme/www-belartis-cz/files/estates/1422207431_5.jpg', 'estates', '7', '7', '0', 'images', '2015-01-25 18:44:17', '9', '2015-04-03 12:13:18', '9', null, null);
INSERT INTO `files` VALUES ('187', '0', 'theme/www-belartis-cz/files/estates/1422207928.JPG', 'estates', '8', '1', '0', 'images', '2015-01-25 18:45:39', '9', '2015-04-09 07:49:40', '9', null, null);
INSERT INTO `files` VALUES ('188', '0', 'theme/www-belartis-cz/files/estates/1422209187.JPG', 'estates', '9', '1', '0', 'images', '2015-01-25 19:08:56', '9', '2015-03-22 19:43:12', '9', null, null);
INSERT INTO `files` VALUES ('189', '0', 'theme/www-belartis-cz/files/estates/1422209234.JPG', 'estates', '9', '2', '0', 'images', '2015-01-25 19:08:56', '9', '2015-03-22 19:43:12', '9', null, null);
INSERT INTO `files` VALUES ('190', '0', 'theme/www-belartis-cz/files/estates/1422209256.JPG', 'estates', '9', '3', '0', 'images', '2015-01-25 19:08:56', '9', '2015-03-22 19:43:12', '9', null, null);
INSERT INTO `files` VALUES ('191', '0', 'theme/www-belartis-cz/files/estates/1422209281.JPG', 'estates', '9', '4', '0', 'images', '2015-01-25 19:08:56', '9', '2015-03-22 19:43:12', '9', null, null);
INSERT INTO `files` VALUES ('192', '1', 'theme/www-belartis-cz/files/brokers/1428130689.png', 'users_broker', '15', '1', '0', 'portrait', '2015-02-03 21:12:01', '9', '2015-04-04 08:58:13', '9', null, null);
INSERT INTO `files` VALUES ('193', '1', 'theme/www-belartis-cz/files/brokers/1422994303.png', 'users_broker', '14', '1', '0', 'portrait', '2015-02-03 21:11:46', '9', '2015-02-20 15:17:06', '1', null, null);
INSERT INTO `files` VALUES ('194', '0', '', null, '16', '1', '0', null, '2015-01-28 17:29:02', '9', '2015-02-20 15:16:55', null, '2015-02-20 15:16:55', '1');
INSERT INTO `files` VALUES ('195', '0', '', null, '17', '1', '0', null, '2015-02-04 16:14:12', '9', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('196', '0', '', null, '17', '1', '0', null, '2015-02-10 16:44:36', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('197', '0', '', null, '17', '1', '0', null, '2015-02-10 18:12:17', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('198', '0', 'theme/www-belartis-cz/files/estates/1423818570.jpg', 'estates', '5', '1', '0', 'images', '2015-02-13 10:09:55', '9', '2015-04-03 12:11:46', '9', null, null);
INSERT INTO `files` VALUES ('199', '0', '', null, '18', '1', '0', null, '2015-02-13 10:17:06', '9', null, null, null, null);
INSERT INTO `files` VALUES ('200', '0', '', null, '17', '1', '0', null, '2015-02-14 10:30:17', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('201', '0', '', null, '16', '1', '0', null, '2015-02-14 10:30:23', '1', '2015-02-20 15:16:55', null, '2015-02-20 15:16:55', '1');
INSERT INTO `files` VALUES ('202', '0', '', null, '17', '1', '0', null, '2015-02-14 10:31:08', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('203', '0', '', null, '17', '1', '0', null, '2015-02-14 10:31:54', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('204', '0', '', null, '16', '1', '0', null, '2015-02-20 11:17:27', '1', '2015-02-20 15:16:55', null, '2015-02-20 15:16:55', '1');
INSERT INTO `files` VALUES ('205', '0', '', null, '17', '1', '0', null, '2015-02-20 11:17:32', '1', '2015-02-20 15:16:51', null, '2015-02-20 15:16:51', '1');
INSERT INTO `files` VALUES ('206', '0', '', null, '17', '1', '0', null, '2015-02-20 15:16:51', '1', null, null, null, null);
INSERT INTO `files` VALUES ('207', '0', '', null, '16', '1', '0', null, '2015-02-20 15:16:55', '1', null, null, null, null);
INSERT INTO `files` VALUES ('208', '0', 'theme/www-belartis-cz/files/estates/1424683981.jpg', 'estates', '12', '2', '0', 'images', '2015-02-23 10:35:17', '8', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('209', '0', 'theme/www-belartis-cz/files/estates/1424689052.jpg', 'estates', '12', '1', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('210', '0', 'theme/www-belartis-cz/files/estates/1424689052_1.jpg', 'estates', '12', '3', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('211', '0', 'theme/www-belartis-cz/files/estates/1424689052_2.jpg', 'estates', '12', '4', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('212', '0', 'theme/www-belartis-cz/files/estates/1424689053.jpg', 'estates', '12', '5', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('213', '0', 'theme/www-belartis-cz/files/estates/1424689053_1.jpg', 'estates', '12', '6', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('214', '0', 'theme/www-belartis-cz/files/estates/1424689053_2.jpg', 'estates', '12', '7', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('215', '0', 'theme/www-belartis-cz/files/estates/1424689053_3.jpg', 'estates', '12', '8', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('216', '0', 'theme/www-belartis-cz/files/estates/1424689053_4.jpg', 'estates', '12', '9', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('217', '0', 'theme/www-belartis-cz/files/estates/1424689053_5.jpg', 'estates', '12', '10', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('218', '0', 'theme/www-belartis-cz/files/estates/1424689053_6.jpg', 'estates', '12', '11', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('219', '0', 'theme/www-belartis-cz/files/estates/1424689053_7.jpg', 'estates', '12', '12', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('220', '0', 'theme/www-belartis-cz/files/estates/1424689053_8.jpg', 'estates', '12', '13', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('221', '0', 'theme/www-belartis-cz/files/estates/1424689053_9.jpg', 'estates', '12', '14', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('222', '0', 'theme/www-belartis-cz/files/estates/1424689053_10.jpg', 'estates', '12', '15', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('223', '0', 'theme/www-belartis-cz/files/estates/1424689053_11.jpg', 'estates', '12', '16', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('224', '0', 'theme/www-belartis-cz/files/estates/1424689053_12.jpg', 'estates', '12', '17', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('225', '0', 'theme/www-belartis-cz/files/estates/1424689053_13.jpg', 'estates', '12', '18', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', null, '2015-03-22 10:29:41', '9');
INSERT INTO `files` VALUES ('226', '0', 'theme/www-belartis-cz/files/estates/1424689053_14.jpg', 'estates', '12', '18', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('227', '0', 'theme/www-belartis-cz/files/estates/1424689053_15.jpg', 'estates', '12', '20', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', null, '2015-03-22 10:29:41', '9');
INSERT INTO `files` VALUES ('228', '0', 'theme/www-belartis-cz/files/estates/1424689053_16.jpg', 'estates', '12', '19', '0', 'images', '2015-02-23 12:03:21', '15', '2015-03-22 10:29:41', '9', null, null);
INSERT INTO `files` VALUES ('229', '2', 'theme/www-demo-cz/files/brokers/1425996352.jpg', 'users_broker', '19', '1', '0', 'portrait', '2015-03-10 15:05:59', '8', '2015-04-28 10:55:39', '21', null, null);
INSERT INTO `files` VALUES ('230', '0', 'theme/www-demo-cz/files/estates/1425998426.jpg', 'estates', '13', '1', '0', 'images', '2015-03-10 15:41:12', '19', '2015-04-22 15:55:11', null, '2015-04-22 15:55:11', '1');
INSERT INTO `files` VALUES ('231', '2', 'theme/www-demo-cz/files/estates/1425999564.jpg', 'estates', '13', '1', '0', 'images', '2015-03-10 15:59:32', '19', '2015-04-22 16:22:51', '1', '2015-04-22 16:22:51', '1');
INSERT INTO `files` VALUES ('232', '2', 'theme/www-demo-cz/files/estates/1426000934.jpg', 'estates', '14', '1', '0', 'images', '2015-03-10 16:28:17', '19', '2015-04-29 17:23:49', '21', null, null);
INSERT INTO `files` VALUES ('233', '0', 'theme/www-demo-cz/files/estates/1426001544.jpg', 'estates', '15', '1', '0', 'images', '2015-03-10 16:35:09', '19', '2015-03-30 20:56:25', '19', null, null);
INSERT INTO `files` VALUES ('234', '2', 'theme/www-demo-cz/files/estates/1426002868.jpg', 'estates', '16', '1', '0', 'images', '2015-03-10 16:54:57', '19', '2015-04-28 15:59:07', '21', null, null);
INSERT INTO `files` VALUES ('235', '0', 'theme/www-demo-cz/files/projects/1427723078.jpg', 'projects', '1', '1', '0', 'images', '2015-03-30 15:47:06', '19', '2015-04-13 12:41:34', '19', null, null);
INSERT INTO `files` VALUES ('236', '0', 'theme/www-demo-cz/files/articles/1427725049.jpg', 'articles', '1', '1', '0', 'images', '2015-03-30 16:17:36', '19', '2015-04-13 12:43:12', '19', null, null);
INSERT INTO `files` VALUES ('237', '0', 'theme/www-demo-cz/files/projects/1427730630.jpg', 'projects', '2', '1', '0', 'images', '2015-03-30 17:50:51', '19', '2015-04-13 12:42:10', '19', null, null);
INSERT INTO `files` VALUES ('238', '0', '', null, '22', '1', '0', null, '2015-03-30 18:15:57', '20', null, null, null, null);
INSERT INTO `files` VALUES ('239', '0', '', null, '23', '1', '0', null, '2015-03-30 18:20:15', '1', null, null, null, null);
INSERT INTO `files` VALUES ('240', '0', '', null, '24', '1', '0', null, '2015-03-30 18:20:40', '1', null, null, null, null);
INSERT INTO `files` VALUES ('241', '0', '', null, '25', '1', '0', null, '2015-03-30 18:21:46', '1', null, null, null, null);
INSERT INTO `files` VALUES ('242', '0', '', null, '26', '1', '0', null, '2015-03-30 18:26:18', '1', null, null, null, null);
INSERT INTO `files` VALUES ('243', '2', 'theme/www-demo-cz/files/brokers/1427733975.jpg', 'users_broker', '22', '1', '0', 'portrait', '2015-03-30 18:37:12', '20', '2015-03-30 18:48:17', '20', null, null);
INSERT INTO `files` VALUES ('244', '2', 'theme/www-demo-cz/files/brokers/1427734012.jpg', 'users_broker', '27', '1', '0', 'portrait', '2015-03-30 18:47:01', '20', '2015-03-30 18:48:32', '20', null, null);
INSERT INTO `files` VALUES ('245', '2', 'theme/www-demo-cz/files/brokers/1427734193.jpg', 'users_broker', '28', '1', '0', 'portrait', '2015-03-30 18:50:12', '20', null, null, null, null);
INSERT INTO `files` VALUES ('246', '0', 'theme/www-demo-cz/files/projects/1427734380.JPG', 'projects', '3', '1', '0', 'images', '2015-03-30 18:53:28', '20', '2015-03-30 20:37:32', '19', null, null);
INSERT INTO `files` VALUES ('247', '0', 'theme/www-demo-cz/files/articles/1427734627.jpg', 'articles', '2', '1', '0', 'images', '2015-03-30 18:57:12', '20', null, null, null, null);
INSERT INTO `files` VALUES ('248', '0', 'theme/www-belartis-cz/files/estates/1428057424.JPG', 'estates', '8', '2', '0', 'images', '2015-04-03 12:43:59', '9', '2015-04-09 07:49:40', '9', null, null);
INSERT INTO `files` VALUES ('249', '0', 'theme/www-belartis-cz/files/estates/1428057560.JPG', 'estates', '8', '3', '0', 'images', '2015-04-03 12:43:59', '9', '2015-04-09 07:49:40', '9', null, null);
INSERT INTO `files` VALUES ('250', '0', 'theme/www-belartis-cz/files/estates/1428057638.JPG', 'estates', '8', '4', '0', 'images', '2015-04-03 12:43:59', '9', '2015-04-09 07:49:40', '9', null, null);
INSERT INTO `files` VALUES ('251', '0', '', null, null, null, '0', null, '2015-04-13 12:09:28', '19', null, null, null, null);
INSERT INTO `files` VALUES ('252', '0', '', null, null, null, '0', null, '2015-04-13 12:24:26', '19', null, null, null, null);
INSERT INTO `files` VALUES ('253', '0', '', null, null, null, '0', null, '2015-04-13 12:26:37', '19', null, null, null, null);
INSERT INTO `files` VALUES ('254', '0', '', null, null, null, '0', null, '2015-04-13 12:27:51', '19', null, null, null, null);
INSERT INTO `files` VALUES ('255', '0', '', null, null, null, '0', null, '2015-04-13 12:29:10', '19', null, null, null, null);
INSERT INTO `files` VALUES ('256', '2', '', null, '16', null, '0', null, '2015-04-13 12:41:52', '19', null, null, null, null);
INSERT INTO `files` VALUES ('257', '2', '', null, '13', null, '0', null, '2015-04-13 13:17:40', '20', null, null, null, null);
INSERT INTO `files` VALUES ('258', '2', 'theme/www-demo-cz/files/broker-profiles/1429024637.jpg', 'brokerprofiles', '19', '1', '1', 'broker', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('259', '2', 'theme/www-demo-cz/files/broker-profiles/1429024637_1.jpg', 'brokerprofiles', '19', '2', '0', 'broker', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('260', '2', 'theme/www-demo-cz/files/broker-profiles/1429024637_2.jpg', 'brokerprofiles', '19', '3', '0', 'broker', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('261', '2', 'theme/www-demo-cz/files/broker-profiles/1429024637_3.jpg', 'brokerprofiles', '19', '4', '0', 'broker', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('262', '2', 'theme/www-demo-cz/files/broker-profiles/1429024277.jpg', 'brokerprofiles', '19', '1', '0', 'estates', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('263', '2', 'theme/www-demo-cz/files/broker-profiles/1429024277_1.jpg', 'brokerprofiles', '19', '2', '0', 'estates', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('264', '2', 'theme/www-demo-cz/files/broker-profiles/1429024277_2.jpg', 'brokerprofiles', '19', '3', '0', 'estates', '2015-04-14 17:18:56', '20', '2015-04-15 10:34:36', '20', null, null);
INSERT INTO `files` VALUES ('265', '2', '', null, '13', null, '0', null, '2015-04-16 12:24:42', '21', null, null, null, null);
INSERT INTO `files` VALUES ('266', '2', '', null, '13', null, '0', null, '2015-04-16 12:53:47', '21', null, null, null, null);
INSERT INTO `files` VALUES ('267', '2', '', null, '13', null, '0', null, '2015-04-16 12:55:54', '21', null, null, null, null);
INSERT INTO `files` VALUES ('268', '2', '', null, '13', null, '0', null, '2015-04-16 15:55:37', '21', null, null, null, null);
INSERT INTO `files` VALUES ('269', '2', '', null, '14', null, '0', null, '2015-04-16 15:56:21', '21', null, null, null, null);
INSERT INTO `files` VALUES ('270', '2', '', null, '13', null, '0', null, '2015-04-16 15:56:54', '21', null, null, null, null);
INSERT INTO `files` VALUES ('271', '2', '', null, '14', null, '0', null, '2015-04-16 15:58:04', '21', null, null, null, null);
INSERT INTO `files` VALUES ('272', '2', '', null, '13', null, '0', null, '2015-04-16 17:46:16', '21', null, null, null, null);
INSERT INTO `files` VALUES ('273', '2', '', null, '14', null, '0', null, '2015-04-16 17:46:28', '21', null, null, null, null);
INSERT INTO `files` VALUES ('274', '2', '', null, '13', null, '0', null, '2015-04-17 11:32:46', '21', null, null, null, null);
INSERT INTO `files` VALUES ('275', '2', '', null, '13', null, '0', null, '2015-04-17 11:35:18', '21', null, null, null, null);
INSERT INTO `files` VALUES ('276', '2', '', null, '13', null, '0', null, '2015-04-17 11:35:44', '21', null, null, null, null);
INSERT INTO `files` VALUES ('277', '2', '', null, '13', null, '0', null, '2015-04-17 12:54:37', '21', null, null, null, null);
INSERT INTO `files` VALUES ('278', '2', '', null, '14', null, '0', null, '2015-04-20 11:17:05', '1', null, null, null, null);
INSERT INTO `files` VALUES ('279', '2', '', null, '13', null, '0', null, '2015-04-20 11:17:33', '1', null, null, null, null);
INSERT INTO `files` VALUES ('280', '2', '', null, '13', null, '0', null, '2015-04-20 11:52:34', '1', null, null, null, null);
INSERT INTO `files` VALUES ('281', '2', '', null, '13', null, '0', null, '2015-04-20 12:07:27', '1', null, null, null, null);
INSERT INTO `files` VALUES ('282', '2', '', null, '14', null, '0', null, '2015-04-20 12:07:36', '1', null, null, null, null);
INSERT INTO `files` VALUES ('283', '2', '', null, '13', null, '0', null, '2015-04-20 12:21:20', '1', null, null, null, null);
INSERT INTO `files` VALUES ('284', '2', '', null, '14', null, '0', null, '2015-04-20 12:23:10', '1', null, null, null, null);
INSERT INTO `files` VALUES ('285', '2', '', null, '13', null, '0', null, '2015-04-20 12:23:46', '1', null, null, null, null);
INSERT INTO `files` VALUES ('286', '2', '', null, '13', null, '0', null, '2015-04-20 12:24:29', '1', null, null, null, null);
INSERT INTO `files` VALUES ('287', '2', '', null, '13', null, '0', null, '2015-04-20 12:26:10', '1', null, null, null, null);
INSERT INTO `files` VALUES ('288', '2', '', null, '14', null, '0', null, '2015-04-20 12:26:15', '1', null, null, null, null);
INSERT INTO `files` VALUES ('289', '2', '', null, '14', null, '0', null, '2015-04-20 12:26:36', '1', null, null, null, null);
INSERT INTO `files` VALUES ('290', '2', '', null, '14', null, '0', null, '2015-04-20 12:27:34', '1', null, null, null, null);
INSERT INTO `files` VALUES ('291', '2', '', null, '14', null, '0', null, '2015-04-20 12:27:48', '1', null, null, null, null);
INSERT INTO `files` VALUES ('292', '2', '', null, '13', null, '0', null, '2015-04-20 12:28:15', '1', null, null, null, null);
INSERT INTO `files` VALUES ('293', '2', '', null, '13', null, '0', null, '2015-04-20 12:28:49', '1', null, null, null, null);
INSERT INTO `files` VALUES ('294', '2', '', null, '13', null, '0', null, '2015-04-20 12:29:19', '1', null, null, null, null);
INSERT INTO `files` VALUES ('295', '2', '', null, '13', null, '0', null, '2015-04-20 12:29:34', '1', null, null, null, null);
INSERT INTO `files` VALUES ('296', '2', '', null, '13', null, '0', null, '2015-04-20 12:30:07', '1', null, null, null, null);
INSERT INTO `files` VALUES ('297', '2', '', null, '13', null, '0', null, '2015-04-20 12:30:25', '1', null, null, null, null);
INSERT INTO `files` VALUES ('298', '2', '', null, '13', null, '0', null, '2015-04-20 12:30:50', '1', null, null, null, null);
INSERT INTO `files` VALUES ('299', '2', '', null, '13', null, '0', null, '2015-04-20 12:31:06', '1', null, null, null, null);
INSERT INTO `files` VALUES ('300', '2', '', null, '13', null, '0', null, '2015-04-20 12:54:28', '1', null, null, null, null);
INSERT INTO `files` VALUES ('301', '2', '', null, '13', null, '0', null, '2015-04-20 15:30:43', '1', null, null, null, null);
INSERT INTO `files` VALUES ('302', '2', '', null, '13', null, '0', null, '2015-04-20 15:32:46', '1', null, null, null, null);
INSERT INTO `files` VALUES ('303', '2', '', null, '13', null, '0', null, '2015-04-20 15:33:23', '1', null, null, null, null);
INSERT INTO `files` VALUES ('304', '2', '', null, '13', null, '0', null, '2015-04-20 15:34:02', '1', null, null, null, null);
INSERT INTO `files` VALUES ('305', '2', '', null, '13', null, '0', null, '2015-04-20 15:34:20', '1', null, null, null, null);
INSERT INTO `files` VALUES ('306', '2', '', null, '13', null, '0', null, '2015-04-21 10:42:52', '1', null, null, null, null);
INSERT INTO `files` VALUES ('307', '2', '', null, '13', null, '0', null, '2015-04-21 17:05:26', '1', null, null, null, null);
INSERT INTO `files` VALUES ('308', '2', '', null, '13', null, '0', null, '2015-04-21 17:27:04', '1', null, null, null, null);
INSERT INTO `files` VALUES ('309', '2', '', null, '13', null, '0', null, '2015-04-22 15:55:11', '1', null, null, null, null);
INSERT INTO `files` VALUES ('310', '2', '', null, '14', null, '0', null, '2015-04-22 15:55:43', '1', null, null, null, null);
INSERT INTO `files` VALUES ('311', '2', '', null, '14', null, '0', null, '2015-04-22 15:57:17', '1', null, null, null, null);
INSERT INTO `files` VALUES ('312', '2', '', null, '14', null, '0', null, '2015-04-23 16:28:12', '21', null, null, null, null);
INSERT INTO `files` VALUES ('313', '0', '', null, null, null, '0', null, '2015-04-23 16:34:31', '21', null, null, null, null);
INSERT INTO `files` VALUES ('314', '0', '', null, null, null, '0', null, '2015-04-23 16:34:51', '21', null, null, null, null);
INSERT INTO `files` VALUES ('315', '0', '', null, null, null, '0', null, '2015-04-23 16:35:15', '21', null, null, null, null);
INSERT INTO `files` VALUES ('316', '2', '', null, '14', null, '0', null, '2015-04-23 16:35:37', '21', null, null, null, null);
INSERT INTO `files` VALUES ('317', '0', '', null, null, null, '0', null, '2015-04-23 16:36:43', '21', null, null, null, null);
INSERT INTO `files` VALUES ('318', '0', '', null, null, null, '0', null, '2015-04-23 16:37:30', '21', null, null, null, null);
INSERT INTO `files` VALUES ('319', '0', '', null, null, null, '0', null, '2015-04-23 16:38:01', '21', null, null, null, null);
INSERT INTO `files` VALUES ('320', '0', '', null, null, null, '0', null, '2015-04-23 16:39:36', '21', null, null, null, null);
INSERT INTO `files` VALUES ('321', '0', '', null, null, null, '0', null, '2015-04-23 16:40:25', '21', null, null, null, null);
INSERT INTO `files` VALUES ('322', '0', '', null, null, null, '0', null, '2015-04-23 16:44:38', '21', null, null, null, null);
INSERT INTO `files` VALUES ('323', '0', '', null, null, null, '0', null, '2015-04-23 16:45:17', '21', null, null, null, null);
INSERT INTO `files` VALUES ('324', '0', '', null, null, null, '0', null, '2015-04-23 16:48:27', '21', null, null, null, null);
INSERT INTO `files` VALUES ('325', '0', '', null, null, null, '0', null, '2015-04-23 16:48:43', '21', null, null, null, null);
INSERT INTO `files` VALUES ('326', '0', '', null, null, null, '0', null, '2015-04-23 16:49:19', '21', null, null, null, null);
INSERT INTO `files` VALUES ('327', '0', '', null, null, null, '0', null, '2015-04-23 16:49:53', '21', null, null, null, null);
INSERT INTO `files` VALUES ('328', '0', '', null, null, null, '0', null, '2015-04-23 16:50:27', '21', null, null, null, null);
INSERT INTO `files` VALUES ('329', '0', '', null, null, null, '0', null, '2015-04-23 16:50:47', '21', null, null, null, null);
INSERT INTO `files` VALUES ('330', '0', '', null, null, null, '0', null, '2015-04-23 16:52:04', '21', null, null, null, null);
INSERT INTO `files` VALUES ('331', '0', '', null, null, null, '0', null, '2015-04-23 16:52:28', '21', null, null, null, null);
INSERT INTO `files` VALUES ('332', '0', '', null, null, null, '0', null, '2015-04-23 16:54:46', '21', null, null, null, null);
INSERT INTO `files` VALUES ('333', '2', '', 'estates', '14', null, '0', null, '2015-04-23 16:55:01', '21', null, null, null, null);
INSERT INTO `files` VALUES ('334', '0', '', null, null, null, '0', null, '2015-04-23 16:55:56', '21', null, null, null, null);
INSERT INTO `files` VALUES ('335', '2', '', 'estates', '14', null, '0', null, '2015-04-23 16:57:31', '21', null, null, null, null);
INSERT INTO `files` VALUES ('336', '2', '', 'estates', '14', null, '0', null, '2015-04-23 17:16:46', '21', null, null, null, null);
INSERT INTO `files` VALUES ('337', '0', '', null, null, null, '0', null, '2015-04-24 15:13:50', '21', null, null, null, null);
INSERT INTO `files` VALUES ('338', '0', '', null, null, null, '0', null, '2015-04-24 15:20:12', '21', null, null, null, null);
INSERT INTO `files` VALUES ('339', '0', '', null, null, null, '0', null, '2015-04-24 15:20:30', '21', null, null, null, null);
INSERT INTO `files` VALUES ('340', '2', '', 'estates', '14', null, '0', null, '2015-04-24 15:20:43', '21', null, null, null, null);
INSERT INTO `files` VALUES ('341', '2', '', 'estates', '14', null, '0', null, '2015-04-27 11:51:29', '21', null, null, null, null);
INSERT INTO `files` VALUES ('342', '2', '', 'estates', '14', null, '0', null, '2015-04-27 11:55:07', '21', null, null, null, null);
INSERT INTO `files` VALUES ('343', '2', '', 'estates', '14', null, '0', null, '2015-04-27 12:30:45', '21', null, null, null, null);
INSERT INTO `files` VALUES ('344', '2', '', 'estates', '16', null, '0', null, '2015-04-28 14:51:19', '21', null, null, null, null);
INSERT INTO `files` VALUES ('345', '2', '', 'estates', '16', null, '0', null, '2015-04-28 15:17:53', '21', null, null, null, null);
INSERT INTO `files` VALUES ('346', '2', '', 'estates', '16', null, '0', null, '2015-04-28 15:59:07', '21', null, null, null, null);
INSERT INTO `files` VALUES ('347', '2', '', 'estates', '14', null, '0', null, '2015-04-28 16:11:21', '21', null, null, null, null);
INSERT INTO `files` VALUES ('348', '2', 'theme/www-demo-cz/files/estates/1430230305.jpg', 'estates', '14', '2', '0', 'images', '2015-04-28 16:12:42', '21', '2015-04-29 17:23:49', null, '2015-04-29 17:23:49', '21');
INSERT INTO `files` VALUES ('349', '2', '', 'estates', '14', null, '0', null, '2015-04-28 16:12:42', '21', null, null, null, null);
INSERT INTO `files` VALUES ('350', '2', '', 'estates', '14', null, '0', null, '2015-04-28 17:27:18', '21', null, null, null, null);
INSERT INTO `files` VALUES ('351', '2', 'theme/www-demo-cz/files/estates/1430235040.jpg', 'estates', '14', '2', '0', 'images', '2015-04-28 17:30:58', '21', '2015-04-29 17:23:49', null, '2015-04-29 17:23:49', '21');
INSERT INTO `files` VALUES ('352', '2', '', 'estates', '14', null, '0', null, '2015-04-28 17:30:59', '21', null, null, null, null);
INSERT INTO `files` VALUES ('353', '2', '', 'estates', '14', null, '0', null, '2015-04-28 17:34:11', '21', null, null, null, null);
INSERT INTO `files` VALUES ('354', '2', 'theme/www-demo-cz/files/estates/1430235632.jpg', 'estates', '14', '2', '0', 'images', '2015-04-28 17:40:47', '21', '2015-04-29 17:23:49', null, '2015-04-29 17:23:49', '21');
INSERT INTO `files` VALUES ('355', '2', '', 'estates', '14', null, '0', null, '2015-04-28 17:40:48', '21', null, null, null, null);
INSERT INTO `files` VALUES ('356', '2', '', 'estates', '14', null, '0', null, '2015-04-28 17:41:26', '21', null, null, null, null);
INSERT INTO `files` VALUES ('357', '2', '', 'estates', '14', null, '0', null, '2015-04-29 17:23:49', '21', null, null, null, null);

-- ----------------------------
-- Table structure for `files_cs`
-- ----------------------------
DROP TABLE IF EXISTS `files_cs`;
CREATE TABLE `files_cs` (
  `file_id` int(11) NOT NULL,
  `file_alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of files_cs
-- ----------------------------
INSERT INTO `files_cs` VALUES ('1', 'Česká popisek');
INSERT INTO `files_cs` VALUES ('2', 'První obrázek');
INSERT INTO `files_cs` VALUES ('3', '');
INSERT INTO `files_cs` VALUES ('4', '');
INSERT INTO `files_cs` VALUES ('5', '');
INSERT INTO `files_cs` VALUES ('6', '');
INSERT INTO `files_cs` VALUES ('7', '');
INSERT INTO `files_cs` VALUES ('8', '');
INSERT INTO `files_cs` VALUES ('9', '');
INSERT INTO `files_cs` VALUES ('10', '');
INSERT INTO `files_cs` VALUES ('11', '');
INSERT INTO `files_cs` VALUES ('12', '');
INSERT INTO `files_cs` VALUES ('13', '');
INSERT INTO `files_cs` VALUES ('14', '');
INSERT INTO `files_cs` VALUES ('15', '');
INSERT INTO `files_cs` VALUES ('16', '');
INSERT INTO `files_cs` VALUES ('17', '');
INSERT INTO `files_cs` VALUES ('18', '');
INSERT INTO `files_cs` VALUES ('19', '');
INSERT INTO `files_cs` VALUES ('20', '');
INSERT INTO `files_cs` VALUES ('21', '');
INSERT INTO `files_cs` VALUES ('22', '');
INSERT INTO `files_cs` VALUES ('23', '');
INSERT INTO `files_cs` VALUES ('24', null);
INSERT INTO `files_cs` VALUES ('25', null);
INSERT INTO `files_cs` VALUES ('26', null);
INSERT INTO `files_cs` VALUES ('27', null);
INSERT INTO `files_cs` VALUES ('28', null);
INSERT INTO `files_cs` VALUES ('29', null);
INSERT INTO `files_cs` VALUES ('30', null);
INSERT INTO `files_cs` VALUES ('31', null);
INSERT INTO `files_cs` VALUES ('32', null);
INSERT INTO `files_cs` VALUES ('33', null);
INSERT INTO `files_cs` VALUES ('34', null);
INSERT INTO `files_cs` VALUES ('35', null);
INSERT INTO `files_cs` VALUES ('36', null);
INSERT INTO `files_cs` VALUES ('37', null);
INSERT INTO `files_cs` VALUES ('38', null);
INSERT INTO `files_cs` VALUES ('39', null);
INSERT INTO `files_cs` VALUES ('40', null);
INSERT INTO `files_cs` VALUES ('41', null);
INSERT INTO `files_cs` VALUES ('42', null);
INSERT INTO `files_cs` VALUES ('43', null);
INSERT INTO `files_cs` VALUES ('44', null);
INSERT INTO `files_cs` VALUES ('45', null);
INSERT INTO `files_cs` VALUES ('46', null);
INSERT INTO `files_cs` VALUES ('48', '');
INSERT INTO `files_cs` VALUES ('49', null);
INSERT INTO `files_cs` VALUES ('50', '');
INSERT INTO `files_cs` VALUES ('51', '');
INSERT INTO `files_cs` VALUES ('52', '');
INSERT INTO `files_cs` VALUES ('53', '');
INSERT INTO `files_cs` VALUES ('54', '');
INSERT INTO `files_cs` VALUES ('55', '');
INSERT INTO `files_cs` VALUES ('56', '');
INSERT INTO `files_cs` VALUES ('57', '');
INSERT INTO `files_cs` VALUES ('58', '');
INSERT INTO `files_cs` VALUES ('59', '');
INSERT INTO `files_cs` VALUES ('60', '');
INSERT INTO `files_cs` VALUES ('61', '');
INSERT INTO `files_cs` VALUES ('62', '');
INSERT INTO `files_cs` VALUES ('63', '');
INSERT INTO `files_cs` VALUES ('64', '');
INSERT INTO `files_cs` VALUES ('65', '');
INSERT INTO `files_cs` VALUES ('66', '');
INSERT INTO `files_cs` VALUES ('67', '');
INSERT INTO `files_cs` VALUES ('68', '');
INSERT INTO `files_cs` VALUES ('69', '');
INSERT INTO `files_cs` VALUES ('70', '');
INSERT INTO `files_cs` VALUES ('71', '');
INSERT INTO `files_cs` VALUES ('72', '');
INSERT INTO `files_cs` VALUES ('73', '');
INSERT INTO `files_cs` VALUES ('74', '');
INSERT INTO `files_cs` VALUES ('75', '');
INSERT INTO `files_cs` VALUES ('76', '');
INSERT INTO `files_cs` VALUES ('77', '');
INSERT INTO `files_cs` VALUES ('78', '');
INSERT INTO `files_cs` VALUES ('79', '');
INSERT INTO `files_cs` VALUES ('80', '');
INSERT INTO `files_cs` VALUES ('81', '');
INSERT INTO `files_cs` VALUES ('82', '');
INSERT INTO `files_cs` VALUES ('83', null);
INSERT INTO `files_cs` VALUES ('84', '');
INSERT INTO `files_cs` VALUES ('85', '');
INSERT INTO `files_cs` VALUES ('86', '');
INSERT INTO `files_cs` VALUES ('87', '');
INSERT INTO `files_cs` VALUES ('88', '');
INSERT INTO `files_cs` VALUES ('89', '');
INSERT INTO `files_cs` VALUES ('90', null);
INSERT INTO `files_cs` VALUES ('91', '');
INSERT INTO `files_cs` VALUES ('92', '');
INSERT INTO `files_cs` VALUES ('93', '');
INSERT INTO `files_cs` VALUES ('94', '');
INSERT INTO `files_cs` VALUES ('95', '');
INSERT INTO `files_cs` VALUES ('96', '');
INSERT INTO `files_cs` VALUES ('97', '');
INSERT INTO `files_cs` VALUES ('98', '');
INSERT INTO `files_cs` VALUES ('99', '');
INSERT INTO `files_cs` VALUES ('100', '');
INSERT INTO `files_cs` VALUES ('101', '');
INSERT INTO `files_cs` VALUES ('102', '');
INSERT INTO `files_cs` VALUES ('103', '');
INSERT INTO `files_cs` VALUES ('104', '');
INSERT INTO `files_cs` VALUES ('105', null);
INSERT INTO `files_cs` VALUES ('106', null);
INSERT INTO `files_cs` VALUES ('107', null);
INSERT INTO `files_cs` VALUES ('108', null);
INSERT INTO `files_cs` VALUES ('109', '');
INSERT INTO `files_cs` VALUES ('110', '');
INSERT INTO `files_cs` VALUES ('111', '');
INSERT INTO `files_cs` VALUES ('112', '');
INSERT INTO `files_cs` VALUES ('113', '');
INSERT INTO `files_cs` VALUES ('114', '');
INSERT INTO `files_cs` VALUES ('115', '');
INSERT INTO `files_cs` VALUES ('116', '');
INSERT INTO `files_cs` VALUES ('117', '');
INSERT INTO `files_cs` VALUES ('118', '');
INSERT INTO `files_cs` VALUES ('119', '');
INSERT INTO `files_cs` VALUES ('120', '');
INSERT INTO `files_cs` VALUES ('121', '');
INSERT INTO `files_cs` VALUES ('122', '');
INSERT INTO `files_cs` VALUES ('123', '');
INSERT INTO `files_cs` VALUES ('124', '');
INSERT INTO `files_cs` VALUES ('125', '');
INSERT INTO `files_cs` VALUES ('126', '');
INSERT INTO `files_cs` VALUES ('127', '');
INSERT INTO `files_cs` VALUES ('128', '');
INSERT INTO `files_cs` VALUES ('129', '');
INSERT INTO `files_cs` VALUES ('130', '');
INSERT INTO `files_cs` VALUES ('131', '');
INSERT INTO `files_cs` VALUES ('132', '');
INSERT INTO `files_cs` VALUES ('133', '');
INSERT INTO `files_cs` VALUES ('134', '');
INSERT INTO `files_cs` VALUES ('135', '');
INSERT INTO `files_cs` VALUES ('136', '');
INSERT INTO `files_cs` VALUES ('137', '');
INSERT INTO `files_cs` VALUES ('138', '');
INSERT INTO `files_cs` VALUES ('139', '');
INSERT INTO `files_cs` VALUES ('140', '');
INSERT INTO `files_cs` VALUES ('141', '');
INSERT INTO `files_cs` VALUES ('142', '');
INSERT INTO `files_cs` VALUES ('143', '');
INSERT INTO `files_cs` VALUES ('144', '');
INSERT INTO `files_cs` VALUES ('145', '');
INSERT INTO `files_cs` VALUES ('146', '');
INSERT INTO `files_cs` VALUES ('147', '');
INSERT INTO `files_cs` VALUES ('148', null);
INSERT INTO `files_cs` VALUES ('149', '');
INSERT INTO `files_cs` VALUES ('150', '');
INSERT INTO `files_cs` VALUES ('151', '');
INSERT INTO `files_cs` VALUES ('152', null);
INSERT INTO `files_cs` VALUES ('153', null);
INSERT INTO `files_cs` VALUES ('154', '');
INSERT INTO `files_cs` VALUES ('155', '');
INSERT INTO `files_cs` VALUES ('156', '');
INSERT INTO `files_cs` VALUES ('157', '');
INSERT INTO `files_cs` VALUES ('158', null);
INSERT INTO `files_cs` VALUES ('159', '');
INSERT INTO `files_cs` VALUES ('160', '');
INSERT INTO `files_cs` VALUES ('161', '');
INSERT INTO `files_cs` VALUES ('162', '');
INSERT INTO `files_cs` VALUES ('163', '');
INSERT INTO `files_cs` VALUES ('164', '');
INSERT INTO `files_cs` VALUES ('165', '');
INSERT INTO `files_cs` VALUES ('166', '');
INSERT INTO `files_cs` VALUES ('167', '');
INSERT INTO `files_cs` VALUES ('168', '');
INSERT INTO `files_cs` VALUES ('169', '');
INSERT INTO `files_cs` VALUES ('170', '');
INSERT INTO `files_cs` VALUES ('171', '');
INSERT INTO `files_cs` VALUES ('172', '');
INSERT INTO `files_cs` VALUES ('173', '');
INSERT INTO `files_cs` VALUES ('174', '');
INSERT INTO `files_cs` VALUES ('175', '');
INSERT INTO `files_cs` VALUES ('176', '');
INSERT INTO `files_cs` VALUES ('177', '');
INSERT INTO `files_cs` VALUES ('178', '');
INSERT INTO `files_cs` VALUES ('179', '');
INSERT INTO `files_cs` VALUES ('180', '');
INSERT INTO `files_cs` VALUES ('181', '');
INSERT INTO `files_cs` VALUES ('182', '');
INSERT INTO `files_cs` VALUES ('183', '');
INSERT INTO `files_cs` VALUES ('184', '');
INSERT INTO `files_cs` VALUES ('185', '');
INSERT INTO `files_cs` VALUES ('187', '');
INSERT INTO `files_cs` VALUES ('188', '');
INSERT INTO `files_cs` VALUES ('189', '');
INSERT INTO `files_cs` VALUES ('190', '');
INSERT INTO `files_cs` VALUES ('191', '');
INSERT INTO `files_cs` VALUES ('192', '');
INSERT INTO `files_cs` VALUES ('193', '');
INSERT INTO `files_cs` VALUES ('194', null);
INSERT INTO `files_cs` VALUES ('195', null);
INSERT INTO `files_cs` VALUES ('196', null);
INSERT INTO `files_cs` VALUES ('197', null);
INSERT INTO `files_cs` VALUES ('198', '');
INSERT INTO `files_cs` VALUES ('199', null);
INSERT INTO `files_cs` VALUES ('200', null);
INSERT INTO `files_cs` VALUES ('201', null);
INSERT INTO `files_cs` VALUES ('202', null);
INSERT INTO `files_cs` VALUES ('203', null);
INSERT INTO `files_cs` VALUES ('204', null);
INSERT INTO `files_cs` VALUES ('205', null);
INSERT INTO `files_cs` VALUES ('206', null);
INSERT INTO `files_cs` VALUES ('207', null);
INSERT INTO `files_cs` VALUES ('208', '');
INSERT INTO `files_cs` VALUES ('209', '');
INSERT INTO `files_cs` VALUES ('210', '');
INSERT INTO `files_cs` VALUES ('211', '');
INSERT INTO `files_cs` VALUES ('212', '');
INSERT INTO `files_cs` VALUES ('213', '');
INSERT INTO `files_cs` VALUES ('214', '');
INSERT INTO `files_cs` VALUES ('215', '');
INSERT INTO `files_cs` VALUES ('216', '');
INSERT INTO `files_cs` VALUES ('217', '');
INSERT INTO `files_cs` VALUES ('218', '');
INSERT INTO `files_cs` VALUES ('219', '');
INSERT INTO `files_cs` VALUES ('220', '');
INSERT INTO `files_cs` VALUES ('221', '');
INSERT INTO `files_cs` VALUES ('222', '');
INSERT INTO `files_cs` VALUES ('223', '');
INSERT INTO `files_cs` VALUES ('224', '');
INSERT INTO `files_cs` VALUES ('225', null);
INSERT INTO `files_cs` VALUES ('226', '');
INSERT INTO `files_cs` VALUES ('227', null);
INSERT INTO `files_cs` VALUES ('228', '');
INSERT INTO `files_cs` VALUES ('229', '');
INSERT INTO `files_cs` VALUES ('230', null);
INSERT INTO `files_cs` VALUES ('231', '');
INSERT INTO `files_cs` VALUES ('232', '');
INSERT INTO `files_cs` VALUES ('233', '');
INSERT INTO `files_cs` VALUES ('234', '');
INSERT INTO `files_cs` VALUES ('235', 'Psí bouda');
INSERT INTO `files_cs` VALUES ('236', '');
INSERT INTO `files_cs` VALUES ('237', '');
INSERT INTO `files_cs` VALUES ('238', null);
INSERT INTO `files_cs` VALUES ('239', null);
INSERT INTO `files_cs` VALUES ('240', null);
INSERT INTO `files_cs` VALUES ('241', null);
INSERT INTO `files_cs` VALUES ('242', null);
INSERT INTO `files_cs` VALUES ('243', '');
INSERT INTO `files_cs` VALUES ('244', '');
INSERT INTO `files_cs` VALUES ('245', null);
INSERT INTO `files_cs` VALUES ('246', '');
INSERT INTO `files_cs` VALUES ('247', null);
INSERT INTO `files_cs` VALUES ('248', '');
INSERT INTO `files_cs` VALUES ('249', '');
INSERT INTO `files_cs` VALUES ('250', '');
INSERT INTO `files_cs` VALUES ('251', null);
INSERT INTO `files_cs` VALUES ('252', null);
INSERT INTO `files_cs` VALUES ('253', null);
INSERT INTO `files_cs` VALUES ('254', null);
INSERT INTO `files_cs` VALUES ('255', null);
INSERT INTO `files_cs` VALUES ('256', null);
INSERT INTO `files_cs` VALUES ('257', null);
INSERT INTO `files_cs` VALUES ('258', '');
INSERT INTO `files_cs` VALUES ('259', '');
INSERT INTO `files_cs` VALUES ('260', '');
INSERT INTO `files_cs` VALUES ('261', '');
INSERT INTO `files_cs` VALUES ('262', '');
INSERT INTO `files_cs` VALUES ('263', '');
INSERT INTO `files_cs` VALUES ('264', '');
INSERT INTO `files_cs` VALUES ('265', null);
INSERT INTO `files_cs` VALUES ('266', null);
INSERT INTO `files_cs` VALUES ('267', null);
INSERT INTO `files_cs` VALUES ('268', null);
INSERT INTO `files_cs` VALUES ('269', null);
INSERT INTO `files_cs` VALUES ('270', null);
INSERT INTO `files_cs` VALUES ('271', null);
INSERT INTO `files_cs` VALUES ('272', null);
INSERT INTO `files_cs` VALUES ('273', null);
INSERT INTO `files_cs` VALUES ('274', null);
INSERT INTO `files_cs` VALUES ('275', null);
INSERT INTO `files_cs` VALUES ('276', null);
INSERT INTO `files_cs` VALUES ('277', null);
INSERT INTO `files_cs` VALUES ('278', null);
INSERT INTO `files_cs` VALUES ('279', null);
INSERT INTO `files_cs` VALUES ('280', null);
INSERT INTO `files_cs` VALUES ('281', null);
INSERT INTO `files_cs` VALUES ('282', null);
INSERT INTO `files_cs` VALUES ('283', null);
INSERT INTO `files_cs` VALUES ('284', null);
INSERT INTO `files_cs` VALUES ('285', null);
INSERT INTO `files_cs` VALUES ('286', null);
INSERT INTO `files_cs` VALUES ('287', null);
INSERT INTO `files_cs` VALUES ('288', null);
INSERT INTO `files_cs` VALUES ('289', null);
INSERT INTO `files_cs` VALUES ('290', null);
INSERT INTO `files_cs` VALUES ('291', null);
INSERT INTO `files_cs` VALUES ('292', null);
INSERT INTO `files_cs` VALUES ('293', null);
INSERT INTO `files_cs` VALUES ('294', null);
INSERT INTO `files_cs` VALUES ('295', null);
INSERT INTO `files_cs` VALUES ('296', null);
INSERT INTO `files_cs` VALUES ('297', null);
INSERT INTO `files_cs` VALUES ('298', null);
INSERT INTO `files_cs` VALUES ('299', null);
INSERT INTO `files_cs` VALUES ('300', null);
INSERT INTO `files_cs` VALUES ('301', null);
INSERT INTO `files_cs` VALUES ('302', null);
INSERT INTO `files_cs` VALUES ('303', null);
INSERT INTO `files_cs` VALUES ('304', null);
INSERT INTO `files_cs` VALUES ('305', null);
INSERT INTO `files_cs` VALUES ('306', null);
INSERT INTO `files_cs` VALUES ('307', null);
INSERT INTO `files_cs` VALUES ('308', null);
INSERT INTO `files_cs` VALUES ('309', null);
INSERT INTO `files_cs` VALUES ('310', null);
INSERT INTO `files_cs` VALUES ('311', null);
INSERT INTO `files_cs` VALUES ('312', null);
INSERT INTO `files_cs` VALUES ('313', null);
INSERT INTO `files_cs` VALUES ('314', null);
INSERT INTO `files_cs` VALUES ('315', null);
INSERT INTO `files_cs` VALUES ('316', null);
INSERT INTO `files_cs` VALUES ('317', null);
INSERT INTO `files_cs` VALUES ('318', null);
INSERT INTO `files_cs` VALUES ('319', null);
INSERT INTO `files_cs` VALUES ('320', null);
INSERT INTO `files_cs` VALUES ('321', null);
INSERT INTO `files_cs` VALUES ('322', null);
INSERT INTO `files_cs` VALUES ('323', null);
INSERT INTO `files_cs` VALUES ('324', null);
INSERT INTO `files_cs` VALUES ('325', null);
INSERT INTO `files_cs` VALUES ('326', null);
INSERT INTO `files_cs` VALUES ('327', null);
INSERT INTO `files_cs` VALUES ('328', null);
INSERT INTO `files_cs` VALUES ('329', null);
INSERT INTO `files_cs` VALUES ('330', null);
INSERT INTO `files_cs` VALUES ('331', null);
INSERT INTO `files_cs` VALUES ('332', null);
INSERT INTO `files_cs` VALUES ('333', null);
INSERT INTO `files_cs` VALUES ('334', null);
INSERT INTO `files_cs` VALUES ('335', null);
INSERT INTO `files_cs` VALUES ('336', null);
INSERT INTO `files_cs` VALUES ('337', null);
INSERT INTO `files_cs` VALUES ('338', null);
INSERT INTO `files_cs` VALUES ('339', null);
INSERT INTO `files_cs` VALUES ('340', null);
INSERT INTO `files_cs` VALUES ('341', null);
INSERT INTO `files_cs` VALUES ('342', null);
INSERT INTO `files_cs` VALUES ('343', null);
INSERT INTO `files_cs` VALUES ('344', null);
INSERT INTO `files_cs` VALUES ('345', null);
INSERT INTO `files_cs` VALUES ('346', null);
INSERT INTO `files_cs` VALUES ('347', null);
INSERT INTO `files_cs` VALUES ('348', null);
INSERT INTO `files_cs` VALUES ('349', null);
INSERT INTO `files_cs` VALUES ('350', null);
INSERT INTO `files_cs` VALUES ('351', null);
INSERT INTO `files_cs` VALUES ('352', null);
INSERT INTO `files_cs` VALUES ('353', null);
INSERT INTO `files_cs` VALUES ('354', null);
INSERT INTO `files_cs` VALUES ('355', null);
INSERT INTO `files_cs` VALUES ('356', null);
INSERT INTO `files_cs` VALUES ('357', null);

-- ----------------------------
-- Table structure for `files_en`
-- ----------------------------
DROP TABLE IF EXISTS `files_en`;
CREATE TABLE `files_en` (
  `file_id` int(11) NOT NULL,
  `file_alt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  CONSTRAINT `files_en_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `files` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of files_en
-- ----------------------------
INSERT INTO `files_en` VALUES ('1', 'Anglicky');
INSERT INTO `files_en` VALUES ('2', null);
INSERT INTO `files_en` VALUES ('3', null);
INSERT INTO `files_en` VALUES ('4', null);
INSERT INTO `files_en` VALUES ('5', null);
INSERT INTO `files_en` VALUES ('6', null);
INSERT INTO `files_en` VALUES ('7', null);
INSERT INTO `files_en` VALUES ('8', null);
INSERT INTO `files_en` VALUES ('9', null);
INSERT INTO `files_en` VALUES ('10', null);
INSERT INTO `files_en` VALUES ('11', null);
INSERT INTO `files_en` VALUES ('12', null);
INSERT INTO `files_en` VALUES ('13', null);
INSERT INTO `files_en` VALUES ('14', null);
INSERT INTO `files_en` VALUES ('15', null);
INSERT INTO `files_en` VALUES ('16', null);
INSERT INTO `files_en` VALUES ('17', null);
INSERT INTO `files_en` VALUES ('18', null);
INSERT INTO `files_en` VALUES ('19', null);
INSERT INTO `files_en` VALUES ('20', null);
INSERT INTO `files_en` VALUES ('21', null);
INSERT INTO `files_en` VALUES ('22', null);
INSERT INTO `files_en` VALUES ('23', null);
INSERT INTO `files_en` VALUES ('24', null);
INSERT INTO `files_en` VALUES ('25', null);
INSERT INTO `files_en` VALUES ('26', null);
INSERT INTO `files_en` VALUES ('27', null);
INSERT INTO `files_en` VALUES ('28', null);
INSERT INTO `files_en` VALUES ('29', null);
INSERT INTO `files_en` VALUES ('30', null);
INSERT INTO `files_en` VALUES ('31', null);
INSERT INTO `files_en` VALUES ('32', null);
INSERT INTO `files_en` VALUES ('33', null);
INSERT INTO `files_en` VALUES ('34', null);
INSERT INTO `files_en` VALUES ('35', null);
INSERT INTO `files_en` VALUES ('36', null);
INSERT INTO `files_en` VALUES ('37', null);
INSERT INTO `files_en` VALUES ('38', null);
INSERT INTO `files_en` VALUES ('39', null);
INSERT INTO `files_en` VALUES ('40', null);
INSERT INTO `files_en` VALUES ('41', null);
INSERT INTO `files_en` VALUES ('42', null);
INSERT INTO `files_en` VALUES ('43', null);
INSERT INTO `files_en` VALUES ('44', null);
INSERT INTO `files_en` VALUES ('45', null);
INSERT INTO `files_en` VALUES ('46', null);

-- ----------------------------
-- Table structure for `menu_items`
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `menu` varchar(255) DEFAULT NULL,
  `item_rank` int(11) DEFAULT NULL,
  `slug_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES ('1', '1', 'main', '1', '2', '2015-02-06 10:36:02', null, null, null, null, null);
INSERT INTO `menu_items` VALUES ('2', '1', 'main', '2', '1', '2015-02-06 10:36:02', null, null, null, null, null);
INSERT INTO `menu_items` VALUES ('3', '1', 'main', '3', '3', '2015-02-06 10:36:02', null, null, null, null, null);
INSERT INTO `menu_items` VALUES ('4', '1', 'main', '7', '4', '2015-02-06 10:36:02', null, '2015-04-07 15:54:06', '8', null, null);
INSERT INTO `menu_items` VALUES ('5', '1', 'main', null, '5', '2015-02-06 10:36:02', null, '2015-02-09 12:18:19', null, '2014-12-01 17:02:28', null);
INSERT INTO `menu_items` VALUES ('6', '1', 'main', '8', '6', '2015-02-06 10:36:02', null, '2015-04-07 15:53:51', '8', null, null);
INSERT INTO `menu_items` VALUES ('7', '1', 'main', '4', '7', '2015-02-06 10:36:02', null, '2015-04-07 15:54:14', null, '2015-04-07 15:54:14', '8');
INSERT INTO `menu_items` VALUES ('8', '2', 'header', '1', '91', '2015-03-10 11:09:07', '8', null, null, null, null);
INSERT INTO `menu_items` VALUES ('9', '2', 'header', '2', '1', '2015-03-10 11:09:14', '8', null, null, null, null);
INSERT INTO `menu_items` VALUES ('10', '2', 'header', '3', '92', '2015-03-10 11:10:16', '8', null, null, null, null);
INSERT INTO `menu_items` VALUES ('11', '2', 'header', '4', '5', '2015-03-10 11:10:29', '8', null, null, null, null);
INSERT INTO `menu_items` VALUES ('12', '2', 'header', '6', '6', '2015-03-10 11:10:37', '8', '2015-03-30 15:24:49', '8', null, null);
INSERT INTO `menu_items` VALUES ('13', '2', 'header', '5', '97', '2015-03-30 15:24:46', '8', '2015-03-30 15:24:49', '8', null, null);
INSERT INTO `menu_items` VALUES ('14', '1', null, '1', '84', '2015-04-07 15:52:57', '8', null, null, null, null);
INSERT INTO `menu_items` VALUES ('15', '1', 'main', '6', '85', '2015-04-07 15:53:14', '8', '2015-04-07 15:54:10', '8', null, null);
INSERT INTO `menu_items` VALUES ('16', '1', 'main', '5', '84', '2015-04-07 15:53:22', '8', '2015-04-07 15:54:10', '8', null, null);

-- ----------------------------
-- Table structure for `notifications`
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_key` varchar(255) DEFAULT NULL,
  `notification_table_name` varchar(255) DEFAULT NULL,
  `notification_table_key` int(11) DEFAULT NULL,
  `notification_deadline` timestamp NULL DEFAULT NULL,
  `notification_done` timestamp NULL DEFAULT NULL,
  `notification_url` varchar(255) DEFAULT NULL,
  `notification_content` text,
  `user_id` int(11) DEFAULT NULL,
  `role` text,
  `client_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES ('4', 'newDemand', 'demands', '8', '2015-01-21 16:07:03', '2015-01-20 16:07:18', 'belartis.strankyrealitky.cz/demands/edit?demand_id=8', 'Byla vytvořena nová poptávka na nemovitost v obci Neratovice', null, 'admin', '1', '2015-01-20 16:07:03', '1', '2015-01-20 16:07:18', null, null, null);
INSERT INTO `notifications` VALUES ('5', 'newDemand', 'demands', '9', '2015-01-21 16:11:42', '2015-01-20 16:12:00', 'http://belartis.strankyrealitky.cz/admin/demands/edit?demand_id=9', 'Byla vytvořena nová poptávka na nemovitost v obci Neratovice', null, 'admin', '1', '2015-01-20 16:11:42', '1', '2015-01-20 16:12:00', null, null, null);
INSERT INTO `notifications` VALUES ('6', 'changePassword', null, null, null, null, 'http://belartis.strankyrealitky.cz/admin/users/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '16', null, '1', '2015-01-28 17:29:02', '9', null, null, null, null);
INSERT INTO `notifications` VALUES ('7', 'changePassword', null, null, null, null, 'http://belartis.strankyrealitky.cz/admin/users/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '17', null, '1', '2015-02-04 16:14:12', '9', null, null, null, null);
INSERT INTO `notifications` VALUES ('8', 'changePassword', null, null, null, null, 'http://belartis.strankyrealitky.cz/admin/users/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '18', null, '1', '2015-02-13 10:17:06', '9', null, null, null, null);
INSERT INTO `notifications` VALUES ('9', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '19', null, '2', '2015-03-10 15:05:59', '8', null, null, null, null);
INSERT INTO `notifications` VALUES ('10', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '20', null, '2', '2015-03-10 15:15:38', '8', null, null, null, null);
INSERT INTO `notifications` VALUES ('11', 'changePassword', null, null, null, '2015-03-30 15:36:34', 'http://demo.strankyrealitky.cz/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '21', null, '2', '2015-03-30 15:22:38', '8', '2015-03-30 15:36:34', '21', null, null);
INSERT INTO `notifications` VALUES ('12', 'changePassword', null, null, null, null, 'http://demo.strankyrealitky.cz/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '22', null, '2', '2015-03-30 18:15:57', '20', null, null, null, null);
INSERT INTO `notifications` VALUES ('13', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '23', null, '2', '2015-03-30 18:20:15', '1', null, null, null, null);
INSERT INTO `notifications` VALUES ('14', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '24', null, '2', '2015-03-30 18:20:40', '1', null, null, null, null);
INSERT INTO `notifications` VALUES ('15', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '25', null, '2', '2015-03-30 18:21:46', '1', null, null, null, null);
INSERT INTO `notifications` VALUES ('16', 'changePassword', null, null, null, null, 'http://localhost/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '26', null, '2', '2015-03-30 18:26:18', '1', null, null, null, null);
INSERT INTO `notifications` VALUES ('17', 'changePassword', null, null, null, null, 'http://demo.strankyrealitky.cz/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '27', null, '2', '2015-03-30 18:47:01', '20', null, null, null, null);
INSERT INTO `notifications` VALUES ('18', 'changePassword', null, null, null, null, 'http://demo.strankyrealitky.cz/admin/user/change-password/', 'Změňte si vaše heslo pro přístup do administrace.', '28', null, '2', '2015-03-30 18:50:12', '20', null, null, null, null);

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `page_published` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('1', '1', '1', '2015-01-09 10:13:03', '1', '2015-04-01 11:09:05', '9', null, null);
INSERT INTO `pages` VALUES ('2', '1', '1', '2015-01-09 12:01:05', '1', '2015-04-07 17:33:09', '8', null, null);
INSERT INTO `pages` VALUES ('3', '1', '1', '2015-01-09 12:05:30', '1', '2015-01-20 16:28:00', '8', '2015-01-20 16:31:35', '8');
INSERT INTO `pages` VALUES ('4', '1', '1', '2015-01-09 12:06:19', '1', '2015-01-20 16:29:52', '8', '2015-01-20 16:31:40', '8');
INSERT INTO `pages` VALUES ('5', '1', '1', '2015-01-09 12:07:13', '1', '2015-01-20 16:30:28', '8', '2015-01-20 16:31:44', '8');
INSERT INTO `pages` VALUES ('6', '1', '1', '2015-01-09 12:07:59', '1', '2015-04-08 22:17:15', '8', null, null);
INSERT INTO `pages` VALUES ('7', '1', '0', '2015-01-09 16:14:37', '1', '2015-04-06 11:04:51', '8', null, null);
INSERT INTO `pages` VALUES ('8', '1', '1', '2015-01-19 17:01:51', '1', '2015-01-19 17:02:05', '1', '2015-01-19 17:03:12', '1');
INSERT INTO `pages` VALUES ('9', '1', '1', '2015-01-20 10:47:14', '9', '2015-01-23 17:42:58', '8', null, null);
INSERT INTO `pages` VALUES ('10', '1', '1', '2015-01-20 16:22:12', '8', '2015-01-23 17:43:34', '8', null, null);
INSERT INTO `pages` VALUES ('11', '1', '1', '2015-01-20 16:22:43', '8', '2015-01-23 17:45:05', '8', null, null);
INSERT INTO `pages` VALUES ('12', '1', '1', '2015-01-20 16:23:54', '8', '2015-01-20 16:24:18', '8', '2015-01-20 16:36:06', '8');
INSERT INTO `pages` VALUES ('13', '1', '1', '2015-01-20 16:25:11', '8', '2015-01-23 17:48:46', '8', null, null);
INSERT INTO `pages` VALUES ('14', '1', '1', '2015-01-20 16:25:46', '8', '2015-04-10 08:06:12', '9', null, null);
INSERT INTO `pages` VALUES ('15', '1', '1', '2015-01-20 16:26:12', '8', '2015-04-10 08:05:03', '9', null, null);
INSERT INTO `pages` VALUES ('16', '1', '1', '2015-02-02 10:05:26', '9', '2015-04-06 20:12:30', '9', null, null);
INSERT INTO `pages` VALUES ('17', '2', '1', '2015-03-10 11:00:03', '8', '2015-03-13 16:30:35', '1', null, null);
INSERT INTO `pages` VALUES ('18', '2', '1', '2015-03-10 11:09:27', '8', '2015-03-13 16:34:52', '1', null, null);

-- ----------------------------
-- Table structure for `pages_blocks`
-- ----------------------------
DROP TABLE IF EXISTS `pages_blocks`;
CREATE TABLE `pages_blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `block_key` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_blocks
-- ----------------------------
INSERT INTO `pages_blocks` VALUES ('1', '1', 'main', '2015-01-09 10:13:03', '1', '2015-04-01 11:09:05', '9', null, null);
INSERT INTO `pages_blocks` VALUES ('2', '2', 'main', '2015-01-09 12:01:05', '1', '2015-04-07 17:33:09', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('3', '3', 'main', '2015-01-09 12:05:30', '1', '2015-01-20 16:31:35', '8', '2015-01-20 16:31:35', '8');
INSERT INTO `pages_blocks` VALUES ('4', '4', 'main', '2015-01-09 12:06:19', '1', '2015-01-20 16:31:40', '8', '2015-01-20 16:31:40', '8');
INSERT INTO `pages_blocks` VALUES ('5', '5', 'main', '2015-01-09 12:07:13', '1', '2015-01-20 16:31:44', '8', '2015-01-20 16:31:44', '8');
INSERT INTO `pages_blocks` VALUES ('6', '6', 'main', '2015-01-09 12:07:59', '1', '2015-04-08 22:17:15', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('7', '7', 'main', '2015-01-09 16:14:37', '1', '2015-02-02 10:25:11', '9', null, null);
INSERT INTO `pages_blocks` VALUES ('8', '8', 'main', '2015-01-19 17:01:51', '1', '2015-01-19 17:03:12', '1', '2015-01-19 17:03:12', '1');
INSERT INTO `pages_blocks` VALUES ('9', '9', 'main', '2015-01-20 10:47:14', '9', '2015-01-23 17:42:58', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('10', '10', 'main', '2015-01-20 16:22:12', '8', '2015-01-23 17:43:34', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('11', '11', 'main', '2015-01-20 16:22:43', '8', '2015-01-23 17:45:05', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('12', '12', 'main', '2015-01-20 16:23:55', '8', '2015-01-20 16:36:06', '8', '2015-01-20 16:36:06', '8');
INSERT INTO `pages_blocks` VALUES ('13', '13', 'main', '2015-01-20 16:25:11', '8', '2015-01-23 17:48:46', '8', null, null);
INSERT INTO `pages_blocks` VALUES ('14', '14', 'main', '2015-01-20 16:25:46', '8', '2015-04-10 08:06:12', '9', null, null);
INSERT INTO `pages_blocks` VALUES ('15', '15', 'main', '2015-01-20 16:26:12', '8', '2015-04-10 08:05:03', '9', null, null);
INSERT INTO `pages_blocks` VALUES ('16', '16', 'main', '2015-02-02 10:05:26', '9', '2015-04-06 20:12:30', '9', null, null);
INSERT INTO `pages_blocks` VALUES ('17', '17', 'main', '2015-03-10 11:00:04', '8', '2015-03-13 16:30:35', '1', null, null);
INSERT INTO `pages_blocks` VALUES ('18', '18', 'main', '2015-03-10 11:09:27', '8', '2015-03-13 16:34:53', '1', null, null);

-- ----------------------------
-- Table structure for `pages_blocks_cs`
-- ----------------------------
DROP TABLE IF EXISTS `pages_blocks_cs`;
CREATE TABLE `pages_blocks_cs` (
  `block_id` int(11) NOT NULL,
  `block_html` longtext,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_blocks_cs
-- ----------------------------
INSERT INTO `pages_blocks_cs` VALUES ('1', '<h1>O nás</h1><p class=\"lead\">\n	Reality nás baví, proto jsme se rozhodli nabídnout Vám naši pomoc a dlouholeté zkušenosti na realitním trhu při prodeji či pronájmu Vaší nemovitosti.</p><hr><p>\n	Jsme si vědomi, že otázka bydlení je v životě každého člověka jednou z priorit. Víme, že čas je to nejdražší, co máme, proto jsme připraveni věnovat se Vám se 100% nasazením, 24hodin, 7dní v týdnu.</p><p>\n	Garantujeme Vám rychlý, bezpečný a v neposlední řadě také příjemný průběh celého obchodu.</p><p>\n	Zajistíme pro Vás prodej či pronájem vašeho domu, bytu, chaty, chalupy, pozemku za nejlepší možnou cenu, v co nejkratším čase.</p>\n<div class=\"row\">\n	<div class=\"col-sm-6\">\n		<h2>Etický kodex<br>\n		<small>makléře BELARTIS</small></h2>\n		<ul>\n			<li>Makléř vždy vykonává svoji činnost profesionálně, čestně, v souladu s dobrými  mravy, obchodní etikou a platnými právními normami České republiky.</li>\n			<li>Makléř hájí zájmy a práva svého klienta dle svého nejlepšího vědomí a svědomí.</li>\n			<li>Makléř vždy jedná se svým klientem korektně, srozumitelně, dodržuje domluvené dohody a je povinen dostát všem svým závazkům.</li>\n			<li>Makléř jedná pouze v rozsahu, který mu byl klientem určen, nikdy nepřekračuje své kompetence.</li>\n			<li>Makléř zachovává mlčenlivost vůči třetím osobám o obchodních případech, a to i po jejich ukončení.</li>\n			<li>Makléř si je vědom své odpovědnosti v procesu obchodování s nemovitostmi, a proto se neustále zdokonaluje ve všech oblastech realitního trhu.</li>\n			<li>Makléř je vizitkou firmy, buduje dobré jméno a zvyšuje prestiž realitní kanceláře BELARTIS.</li>\n		</ul>\n	</div>	<div class=\"col-sm-6\">\n		<h2>Centrála společnosti<br>\n		<small>Dušní 112/16, Praha 1 - Staré Město, 110 00</small></h2>\n		<div class=\"row\">\n			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/dum.jpg\" title=\"Zvětšit obrázek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/dum.jpg\" alt=\"BELARTIS - Reality nás baví\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar.jpg\" title=\"Zvětšit obrázek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar.jpg\" alt=\"BELARTIS - Reality nás baví\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar1.jpg\" title=\"Zvětšit obrázek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar1.jpg\" alt=\"BELARTIS - Reality nás baví\">\n				</a>\n			</div>			<div class=\"col-xs-6\">\n				<a class=\"colorbox image\" href=\"{$instancePath}/img/kancelar/kancelar2.jpg\" title=\"Zvětšit obrázek\" data-rel=\"office\">\n				<img class=\"img-thumbnail\" src=\"{$instancePath}/img/kancelar/kancelar2.jpg\" alt=\"BELARTIS - Reality nás baví\">\n				</a>\n			</div></div></div></div><p class=\"text-center lead\">\n	<br>\n	<a n:href=\"contacts:default\" title=\"Zobrazit podrobné kontaktní informace\"><i class=\"fa fa-chevron-right\"></i> Kontaktní informace</a></p>');
INSERT INTO `pages_blocks_cs` VALUES ('2', '<h1>Naše služby</h1>\n<div class=\"row services\">\n	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 9\" title=\"Více informací o prodeji nemovitostí\">\n		<i class=\"fa fa-home\"></i><strong>Prodej nemovitostí</strong><small>na nás se můžete spolehnout</small></a></h2>\n		<p>\n			Proces prodeje nemovitosti se skládá z celé řady dílčích úkonů a služeb. Provedeme Vás celým procesem prodeje či pronájmu nemovitosti tak, aby vše proběhlo k Vaší plné spokojenosti.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 10\" title=\"Více informací o pronájmu nemovitostí\">\n		<i class=\"fa fa-building\"></i><strong>Pronájem nemovitostí</strong><small>vše k Vaší spokojenosti</small></a></h2>\n		<p>\n			Pokud pro svoji nemovitost sháníte nájemce, rádi Vám s celým procesem pomůžeme.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2><a href=\"http://belartis.strankyrealitky.cz/nase-sluzby/rekonstrukce-nemovitosti\">\n		<i class=\"fa fa-arrow-circle-up\"></i>\n		<strong>Rekonstrukce nemovitostí</strong>&nbsp;</a><a href=\"Pages:default 10\"><small>s námi to zvládnete</small></a></h2>\n		<p>\n			<a href=\"Pages:default 11\"></a>\n			Zabýváme se kompletními rekonstrukcemi bytů, rodinných a bytových&nbsp;domů a projektovou činností.&nbsp;\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 11\" title=\"Více informací o správě nemovitostí\">\n		<i class=\"fa fa-gear\"></i><strong>Správa nemovitostí</strong><small>jako by byla naše vlastní</small></a></h2>\n		<p>\n			Vlastníte nemovitost, ale nemáte čas či chuť starat se o její správu? Svěřte svoji nemovitost do našich rukou, postaráme se o ni jako by byla naše vlastní.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 14\" title=\"Více informací o finančních službách\">\n		<i class=\"fa fa-money\"></i><strong>Finanční služby</strong><small>vyřídíme&nbsp;hypoteční úvěr</small></a></h2>\n		<p>\n			Pro většinu lidí je nákup nemovitosti jedním z největších a nejdražších rozhodnutí v životě. Uvažujete-li o koupi nemovitosti a chybí Vám potřebná finanční částka, můžete situaci řešit pomocí hypotečního úvěru nebo úvěru ze stavebního spoření.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 15\" title=\"Více informací o právních službách\">\n		<i class=\"fa fa-files-o\"></i><strong>Právní služby</strong><small>pro Váš klid a bezpečí</small></a></h2>\n		<p>\n			Realitní kancelář BELARTIS&nbsp;spolupracuje s renomovanou advokátní kanceláří. Advokátní kancelář se zabývá všemi úkony spojenými s převodem a nájmem nemovitostí od vypracování smlouvy o smlouvě budoucí až po návrh na vklad do katastru nemovitostí.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<a n:href=\"Pages:default 13\" title=\"Více informací o službě Homestaging\">\n		<i class=\"fa fa-camera\"></i><strong>Homestaging</strong><small>zvýšíme šanci na prodej</small></a></h2>\n		<p>\n			Aby byl celkový dojem z prezentace Vaší nemovitosti co nejlepší, je nutné Váš dům či byt před samotným focením upravit. Pomůžeme Vám s úklidem a vhodným rozmístěním nábytku tak, aby co nejvíce vynikl potenciál prostoru.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<i class=\"fa fa-eye\"></i><strong>Ocenění nemovitostí</strong><small>zajistíme tržní odhad</small></h2>\n		<p>\n			 Naše realitní kancelář pro vás zajistí tržní odhad Vaší nemovitosti i odhad soudním znalcem pro potřeby finančního úřadu.\n		</p>\n	</div>	<div class=\"col-sm-6 col-md-4\">\n		<h2>\n		<i class=\"fa fa-thumbs-o-up\"></i><strong>Ostatní služby</strong><small>pro Vás cokoliv</small></h2>\n		<p>\n			Ve spolupráci s odborníky z daných oblastí, poskytujeme i další služby, mezi které patří například architektonické návrhy, projekční práce, geodetické práce či&nbsp;energetické audity.\n		</p>\n	</div></div>');
INSERT INTO `pages_blocks_cs` VALUES ('3', '<h1>Prodej nemovitostí</h1>\n<p>\n	Proces prodeje nemovitosti se skládá z celé řady dílčích úkonů a služeb. Provedeme Vás celým procesem prodeje či pronájmu nemovitosti tak, aby vše proběhlo k Vaší plné spokojenosti. Mezi naše základní služby patří zejména:\n</p>\n<ul class=\"lead\">\n	<li>Prohlídka Vaší nemovitosti</li>\n	<li>Doporučení prodejní ceny a strategie prodeje</li>\n	<li>Poradíme Vám co zlepšit aby se Vaše nemovitost co nejrychleji a co nejvýhodněji prodala</li>\n	<li>Pořídíme profesionální fotografie </li>\n	<li>Oslovíme klienty z vlastní rozsáhlé databáze</li>\n	<li>Zajistíme kompletní prezentaci a rozsáhlou inzerci Vaší nemovitosti</li>\n	<li>Veškerou komunikaci a prohlídky se zájemci řešíme za Vás</li>\n	<li>Postaráme se o veškerý právní servis</li>\n	<li>Předáme Vaši nemovitost novému majiteli</li>\n</ul>');
INSERT INTO `pages_blocks_cs` VALUES ('4', '<h1>Pronájem nemovitostí</h1>\n<p>\n	Pokud pro svoji nemovitost sháníte nájemce, rádi Vám s celým procesem pomůžeme. Mezi naše základní služby při pronájmu nemovitostí patří zejména:\n</p>\n<ul class=\"lead\">\n	<li>Prohlídka Vaší nemovitosti</li>\n	<li>Doporučení odpovídajícího nájemného pro Vaši nemovitost</li>\n	<li>Poradíme co zlepšit aby se Vaše nemovitost co nejrychleji a co nejvýhodněji pronajala</li>\n	<li>Pořídíme profesionální fotografie Vaší nemovitosti</li>\n	<li>Oslovíme klienty hledající pronájem z vlastní rozsáhlé databáze</li>\n	<li>Zajistíme kompletní prezentaci a rozsáhlou inzerci Vaší nemovitosti</li>\n	<li>Veškerou komunikaci a prohlídky se zájemci řešíme za Vás</li>\n	<li>Poradíme s výběrem vhodného nájemníka pro Vaši nemovitost</li>\n	<li>Předáme Vaši nemovitost novému nájemníkovi</li>\n	<li>Zajistíme veškerý právní servis</li>\n	<li>Postaráme se o všechny záležitosti a komunikaci s nájemníkem po celou dobu trvání nájemního vztahu</li>\n</ul>');
INSERT INTO `pages_blocks_cs` VALUES ('5', '<h1>Správa nemovitostí</h1>\n<p>\n	Vlastníte nemovitost, ale nemáte čas či chuť starat se o její správu? Svěřte svoji nemovitost do našich rukou, postaráme se o ni jako by byla naše vlastní. Mezi naše základní služby při správě nemovitostí patří zejména:\n</p>\n<ul class=\"lead\">\n	<li>Převzetí objektu do správy</li>\n	<li>Zajištění smluvních dodávek médií</li>\n	<li>Úklid nemovitosti</li>\n	<li>Běžnou preventivní údržbu nemovitosti s důrazem na předcházení vzniku závad</li>\n	<li>Drobné řemeslné práce</li>\n	<li>Havarijní služby</li>\n</ul>');
INSERT INTO `pages_blocks_cs` VALUES ('6', '<h1>Kariéra</h1><p class=\"lead\">\n	Baví Vás reality stejně jako nás? Rádi Vás přivítáme v našem týmu!</p><hr>\n<div class=\"row\">\n	<div class=\"col-sm-6\">\n		<h2>Co nabízíme?</h2>\n		<ul>\n			<li>Unikátní provizní systém – 80% z provize pro Vás</li>\n			<li>Zázemí moderní kanceláře v centru Prahy</li>\n			<li>Firemní vůz za bezkonkurenční cenu</li>\n			<li>Mobilní telefon s neomezeným tarifem</li>\n			<li>Možnost práce z domova </li>\n			<li>Zaškolení pro nové makléře zdarma</li>\n			<li>Průběžné obchodní a odborné vzdělávání v oboru</li>\n			<li>Kariérní růst s možností vybudování vlastního týmu makléřů</li>\n			<li>Finanční nezávislost</li>\n		</ul>\n	</div>	<div class=\"col-sm-6\">\n		<h2>Co požadujeme?</h2>\n		<ul>\n			<li>Minimálně středoškolské vzdělání s maturitou</li>\n			<li>Vysoké pracovní nasazení a časovou flexibilitu</li>\n			<li>Komunikační a vyjednávací dovednosti</li>\n			<li>Seriózní vzhled a vystupování</li>\n			<li>Zodpovědnost, spolehlivost</li>\n			<li>Znalost práce na PC</li>\n			<li>Řidičský průkaz skupiny B</li>\n		</ul>\n	</div></div><p class=\"text-center\">\n	<br>\nPokud Vás naše nabídka zaujala, neváhejte nás kontaktovat:</p><p class=\"text-center lead\">\nna email:&nbsp;\n	<a href=\"mailto:mailto:info@belartis.cz\">info@belartis.cz</a> \n	nebo volejte na tel. číslo \n	<strong>+420 602 603 604</strong></p>');
INSERT INTO `pages_blocks_cs` VALUES ('7', '<h1>Realizace</h1><p class=\"text-center\">\n	<img style=\"width: 30%\" src=\"{$instancePath}/img/belartis-logotype.png\" alt=\"BELARTIS - Reality nás baví\"></p><p class=\"lead text-center\">\n	Připravujeme</p>');
INSERT INTO `pages_blocks_cs` VALUES ('8', '<p>dsfslkdjdflkds</p><p>sdfsdf</p>');
INSERT INTO `pages_blocks_cs` VALUES ('9', '<h1>Prodej nemovitostí</h1><p class=\"lead\">\n	Proces prodeje nemovitosti se skládá z celé řady dílčích úkonů a služeb.</p><hr><p>\nProvedeme Vás celým procesem prodeje či pronájmu nemovitosti tak, aby vše proběhlo k Vaší plné spokojenosti. Mezi naše základní služby patří zejména:</p><ul>\n	\n<li>Prohlídka Vaší nemovitosti</li>	\n<li>Doporučení prodejní ceny a strategie prodeje</li>	\n<li>Poradíme Vám co zlepšit aby se Vaše nemovitost co nejrychleji a co nejvýhodněji prodala</li>	\n<li>Pořídíme profesionální fotografie </li>	\n<li>Oslovíme klienty z vlastní rozsáhlé databáze</li>	\n<li>Zajistíme kompletní prezentaci a rozsáhlou inzerci Vaší nemovitosti</li>	\n<li>Veškerou komunikaci a prohlídky se zájemci řešíme za Vás</li>	\n<li>Postaráme se o veškerý právní servis</li>	\n<li>Předáme Vaši nemovitost novému majiteli</li></ul>');
INSERT INTO `pages_blocks_cs` VALUES ('10', '<h1>Pronájem nemovitostí</h1><p class=\"lead\">\nPokud pro svoji nemovitost sháníte nájemce, rádi Vám s celým procesem pomůžeme.</p><hr><p>Mezi naše základní služby při pronájmu nemovitostí patří zejména:</p><ul>\n	\n<li>Prohlídka Vaší nemovitosti</li>	\n<li>Doporučení odpovídajícího nájemného pro Vaši nemovitost</li>	\n<li>Poradíme co zlepšit aby se Vaše nemovitost co nejrychleji a co nejvýhodněji pronajala</li>	\n<li>Pořídíme profesionální fotografie Vaší nemovitosti</li>	\n<li>Oslovíme klienty hledající pronájem z vlastní rozsáhlé databáze</li>	\n<li>Zajistíme kompletní prezentaci a rozsáhlou inzerci Vaší nemovitosti</li>	\n<li>Veškerou komunikaci a prohlídky se zájemci řešíme za Vás</li>	\n<li>Poradíme s výběrem vhodného nájemníka pro Vaši nemovitost</li>	\n<li>Předáme Vaši nemovitost novému nájemníkovi</li>	\n<li>Zajistíme veškerý právní servis</li>	\n<li>Postaráme se o všechny záležitosti a komunikaci s nájemníkem po celou dobu trvání nájemního vztahu</li></ul>');
INSERT INTO `pages_blocks_cs` VALUES ('11', '<h1>Správa nemovitostí</h1>\n<p class=\"lead\">\n	Vlastníte nemovitost, ale nemáte čas či chuť starat se o její správu?&nbsp;\n</p>\n<hr>\n<p>\n	Svěřte svoji nemovitost do našich rukou, postaráme se o ni jako by byla naše vlastní. Mezi naše základní služby při správě nemovitostí patří zejména:\n</p>\n<ul>\n	<li>Převzetí objektu do správy</li>\n	<li>Zajištění smluvních dodávek médií</li>\n	<li>Úklid nemovitosti</li>\n	<li>Běžnou preventivní údržbu nemovitosti s důrazem na předcházení vzniku závad</li>\n	<li>Drobné řemeslné práce</li>\n	<li>Havarijní služby</li>\n</ul>');
INSERT INTO `pages_blocks_cs` VALUES ('12', '<h1>Ocenění nemovitostí</h1>\n<p>\n	Potřebujete zajistit ocenění Vaší nemovitosti? Naše realitní kancelář pro vás zajistí :\n</p>\n<ul class=\"lead\">\n	<li>Tržní odhad Vaší nemovitosti</li>\n	<li>Odhad soudním znalcem pro potřeby finančního úřadu</li>\n</ul>');
INSERT INTO `pages_blocks_cs` VALUES ('13', '<h1>Homestaging</h1><p class=\"lead\">\n	Aby byl celkový dojem z prezentace Vaší nemovitosti co nejlepší, je nutné Váš dům či byt před samotným focením upravit.&nbsp;</p><hr>\n<p>\n	Pomůžeme Vám s úklidem a vhodným rozmístěním nábytku tak, aby co nejvíce vynikl potenciál prostoru. Byt či dům sladíme v líbivý celek, který bude působit vzdušně, moderně a přitažlivě.&nbsp;</p><p>\n	Významně tím nemovitost zatraktivníme v očích potenciálních zájemců a zvýšíme tak šanci na rychlý a výhodný prodej.</p>');
INSERT INTO `pages_blocks_cs` VALUES ('14', '<h1>Finanční služby</h1><p class=\"lead\">\n	Pro většinu lidí je nákup nemovitosti jedním z největších a nejdražších rozhodnutí v životě.</p><hr>\n<p>\n	Uvažujete-li o koupi nemovitosti a chybí Vám potřebná finanční částka, můžete situaci řešit pomocí hypotečního úvěru nebo úvěru ze stavebního spoření. Naše realitní kancelář Vám pomůže s vyřízením hypotečního úvěru nebo úvěru ze stavebního spoření u všech bankovních ústavů a stavebních spořitelen působících na českém trhu za mimořádně výhodných podmínek.</p><h2>BELARTIS hypotéka – exkluzivně pro Vás</h2><ul>\n	<li>sjednejte si s námi schůzku na které se dozvíte vše potřebné pro sjednání nejvýhodnější hypotéky</li>	<li>spolupracujeme se všemi bankovními ústavy poskytujícími hypoteční úvěry v ČR – máte tak možnost relevantního srovnání nabízených podmínek a služeb z kterých si snadno vyberete</li>	<li>naši hypoteční specialisté pro Vás vše zařídí a zároveň se o Vás starají po celou dobu čerpání úvěru.</li></ul><p>\n	Pro více informací jsme Vám k dispozici na tel. čísle +420&nbsp;602 603 604 nebo na e-mailu <a href=\"mailto:info@belartis.cz\">info@belartis.cz</a></p>');
INSERT INTO `pages_blocks_cs` VALUES ('15', '<h1>Právní služby</h1><p class=\"lead\">\n	Realitní kancelář BELARTIS dlouhodobě spolupracuje s renomovanou advokátní kanceláří.&nbsp;</p><hr><ul>\n	\n<li>Advokátní kancelář se zabývá všemi úkony spojenými s převodem a nájmem nemovitostí od vypracování smlouvy o smlouvě budoucí až po návrh na vklad do katastru nemovitostí.</li>	\n<li>Kromě veškerých smluv zajišťuje advokátní kancelář pro naše klienty i advokátní úschovu kupní ceny.</li></ul><p>\n	Pro naše klienty zpracovává veškerou smluvní dokumentaci a realizuje advokátní úschovu kupní ceny JUDr. Jakub Zámyslický, advokát se specializací na právo nemovitostí a stavebnictví.</p><h2>JUDr. Jakub Zámyslický</h2><ul><li>Univerzita Karlova, Právnická fakulta, Praha (2010), titul JUDr.</li><li>Univerzita Karlova, Právnická fakulta, Praha (2009), titul Mgr.</li><li>National and Kapodistrian University of Athens, Atény (2008)</li><li>advokát ev.č. ČAK 14916</li></ul><p>\n	Zaměření: občanské právo, bytové právo (vlastnictví, nájem, SVJ), obchodní právo, stavební právo</p>');
INSERT INTO `pages_blocks_cs` VALUES ('16', '<h1>Rekonstrukce nemovitostí</h1><h3>Kompletní služby při rekonstrukci Vaší nemovitosti</h3><hr><p>Rekonstrukce bytů, rodinných a bytových domů, projektová činnost</p><ul><li>Zabýváme se kompletními rekonstrukcemi bytů, rodinných a bytových domů a projektovou činností. Byt či dům který projde rekonstrukcí se stane hodnotnějším, luxusnějším a získá větší množství potenciálních zájemců o koupi či pronájem. Pokud potřebujete jen drobné opravy, úpravy či kompletní přestavbu na kterou sami nestačíte, neváhejte se na nás obrátit.&nbsp;</li><li>Zdarma vypracujeme návrh možného řešení a podrobnou cenovou kalkulaci.&nbsp;</li><li>Samozřejmostí jsou kompletně zhotovené projekty, revizní zprávy a protokoly, které souvisejí s prováděnou rekonstrukcí a které jsou nezbytné k následné kolaudaci.</li></ul>');
INSERT INTO `pages_blocks_cs` VALUES ('17', '<h1>O nás</h1><p class=\"lead\">\n	</p><p>Na StrankyRealitky.Cz se můžete obrátit ve všem, co bude potřeba pro Vaše podnikání vytvořit. Je dobré umět správně napsat stránku O nás, tak aby zaujala, dobře se četla a přinesla informace, které chcete klientům o sobě sdělit. Lidé mají rádi příběhy, málo jich zajímají suchá čísla, nebo Vaše minulé úspěchy, je proto důležité umět sdělit co můžete udělat právě pro ně. Co můžete dělat pro klienty v budoucnu, čím jim pomoci a jak k nim budete přistupovat.</p><hr>\n<p>My, tým StrankyRealitky.Cz, jsme se dali na cestu tohoto projektu, protože vnímáme, že v naší republice je pořád málo opravdu kvalitních služeb a spousta z těch, kteří je zde provozují, tak často jedou na setrvačník a nijak se nevyvíjí. Když komunikujeme při obchodních jednáních, děláme nějaký projekt, nebo jen něco nakupujeme na internetu, chceme to udělat jednoduše, kvalitně a ještě mít radost, že to bylo příjemné.</p><p>To samé jsme přenesli do tohoto systému, chceme, aby zákazníci našich klientů měli radost, že navštívili právě jejich stránky, že se jim dobře orientovalo a během chvilky našli, co potřebovali. Stejně tak je důležité, aby naši klienti mohli velmi snadno ovládat svůj systém a na pár kliknutí dokázali nastavit, vložit a používat všechno, co je pro ně v daný moment důležité, protože kde jsou lidé spokojeni, tam se i vrací.</p>');
INSERT INTO `pages_blocks_cs` VALUES ('18', '<h1>Naše služby</h1><p class=\"lead\">\n	</p><p>StrankyRealitky.Cz poskytují komplexní servis začínajícím i zavedeným realitním kancelářím, moderním samostatným makléřům a posunují jejich možnosti na prodej nemovitosti na novou úroveň.</p><hr>\n<p>Chcete být vidět? Chcete, aby se o Vás mluvilo? K tomu je potřeba být jiný, ideálně lepší než konkurence. My vám zajistíme, že grafika stránek bude jen a čistě Vaše vizitka, kde lidé budou rádi hledat své nové vysněné bydlení, protože bude přehledná a jednoduchá. Ukážeme Vám jak udělat prezentace nemovitostí, na které lidé nezapomenou a ještě si je mezi sebou budou sdílet na sociálních sítích. Otevřete se novým možnostem a přeneste svoje podnikání na novou úroveň.</p><h2>Co od nás získáte</h2>\n	<div class=\"row services\">\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-eye\"></i>		\n<h3>\n					Moderní Corporate Identity\n				</h3>\n				<p>\n					Zpracujeme logo a celý vizuální styl Vaší realitní kanceláře. Vizitky, tištěné propagační materiály, velkoplošnou reklamu, jednotný vizuální styl Vašich strategií, statistik, smluv a všech ostatních dokumentů.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-paint-brush\"></i>\n									\n<h3>\n					Moderní design na míru\n				</h3>\n				<p>\n					Námi vytvořené stránky pro realitní kanceláře jsou moderní, přehledné a vzbuzují důvěryhodnost což jsou klíčové aspekty pro úspěšné dokončení obchodu. \n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-mobile\"></i>			\n<h3>\n					Správu i na mobilu\n				</h3>\n				<p>\n					Naše stránky, intranet i realitní software můžete pohodlně spravovat i na mobilních zařízeních.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-signal\"></i>\n<h3>\n					\n					Exporty\n				</h3>\n				<p>\n					Automaticky exportujeme zvolené inzeráty i makléře na největší české realitní portály.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-users\"></i>\n							\n<h3>\n					Makléřské účty a profily\n				</h3>\n				<p>\n					Správa makléřů, včetně fotografií, přehledů i profiových stránek makléřů Vám pomůže dostat Vaši realitní kancelář na přední místa ve vyhledávačích.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n<i class=\"fa fa-comment\"></i>\n									\n<h3>\n					Poptávky a jejich párování\n				</h3>\n				<p>\n					Evidence poptávek Vašich makléřu i sběr poptávek od návštěvníků Vašich stránek Vám poskytne přehled a zajistí, že o žádný potenciální obchod nepřijdete.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-camera\"></i><h3>\n					Profesionální fotografie a homestagging\n				</h3>\n				<p>\n					Zajistíme profesionální přípravu, nasvícení a nafocení Vašich zakázek a poskytneme tam Vašim klientům profesionální prezentaci nemovitostí.\n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-video-camera\"></i><h3>\n					Profesionální videoprezentace\n				</h3>\n				<p>\n					Připravíme, natočíme, sestříháme profesionální videoprezentace nemovitostí včetně úvodní znělky Vaší realitní kanceláře a dynamické vizitky makléře v závěru videa a následně publikujeme na světových videoportálech a také připravíme komprese prezentací pro české realitní portály. \n				</p>\n			</div>\n			<div class=\"col-xs-6 col-sm-4\">\n				<i class=\"fa fa-cubes\"></i><h3>\n					Developerské projekty\n				</h3>\n				<p>\n					Nabízíme také komponentu pro správu Vašich developerských projektů. Přímo na stránkách Vaší realitní kanceláře tak můžete prezentovat Vámi prodávané developerské projekty včetně aktualizací stavů. Pro jednotlivé projekty také vytváříme vlastní webové prezentace, včetně všech náležitostí, jako jsou vizuální identity projektů, ceníky, dispozice, 3D vizualizace, videoprezentace apod.\n				</p>\n			</div>\n</div>');

-- ----------------------------
-- Table structure for `pages_blocks_en`
-- ----------------------------
DROP TABLE IF EXISTS `pages_blocks_en`;
CREATE TABLE `pages_blocks_en` (
  `block_id` int(11) NOT NULL,
  `block_html` longtext,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_blocks_en
-- ----------------------------
INSERT INTO `pages_blocks_en` VALUES ('1', null, '2015-01-09 10:13:03', '1', '2015-01-09 11:49:49', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('2', null, '2015-01-09 12:01:05', '1', '2015-01-09 12:29:04', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('3', null, '2015-01-09 12:05:30', '1', '2015-01-09 12:06:14', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('4', null, '2015-01-09 12:06:19', '1', '2015-01-09 12:07:10', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('5', null, '2015-01-09 12:07:13', '1', '2015-01-09 12:07:54', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('6', null, '2015-01-09 12:07:59', '1', '2015-01-09 12:44:15', '1', null, null);
INSERT INTO `pages_blocks_en` VALUES ('7', null, '2015-01-09 16:14:37', '1', '2015-01-09 16:14:57', '1', null, null);

-- ----------------------------
-- Table structure for `pages_cs`
-- ----------------------------
DROP TABLE IF EXISTS `pages_cs`;
CREATE TABLE `pages_cs` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_slug` varchar(255) DEFAULT NULL,
  `page_html` longtext,
  `page_template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_cs
-- ----------------------------
INSERT INTO `pages_cs` VALUES ('1', 'O nás', 'o-nas', null, null);
INSERT INTO `pages_cs` VALUES ('2', 'Naše služby', 'nase-sluzby', null, null);
INSERT INTO `pages_cs` VALUES ('3', 'Prodej nemovitostí', 'prodej-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('4', 'Pronájem nemovitostí', 'pronajem-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('5', 'Správa nemovitostí', 'sprava-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('6', 'Kariéra', 'kariera', null, '');
INSERT INTO `pages_cs` VALUES ('7', 'Realizace', 'realizace', null, null);
INSERT INTO `pages_cs` VALUES ('8', 'Testovací stránky', 'testovaci-stranky', null, null);
INSERT INTO `pages_cs` VALUES ('9', 'Prodej nemovitostí', 'prodej-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('10', 'Pronájem nemovitostí', 'pronajem-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('11', 'Správa nemovitostí', 'sprava-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('12', 'Ocenění nemovitostí', 'oceneni-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('13', 'Homestaging', 'homestaging', null, null);
INSERT INTO `pages_cs` VALUES ('14', 'Finanční služby', 'financni-sluzby', null, '');
INSERT INTO `pages_cs` VALUES ('15', 'Právní služby', 'pravni-sluzby', null, '');
INSERT INTO `pages_cs` VALUES ('16', 'Rekonstrukce nemovitostí', 'rekonstrukce-nemovitosti', null, null);
INSERT INTO `pages_cs` VALUES ('17', 'O nás', null, null, null);
INSERT INTO `pages_cs` VALUES ('18', 'Naše služby', null, null, null);

-- ----------------------------
-- Table structure for `pages_en`
-- ----------------------------
DROP TABLE IF EXISTS `pages_en`;
CREATE TABLE `pages_en` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `page_slag` varchar(255) DEFAULT NULL,
  `page_html` longtext,
  `page_template` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_en
-- ----------------------------
INSERT INTO `pages_en` VALUES ('1', 'About us', 'about-us', null, null, '2015-01-09 10:13:03', '1', '2015-01-09 11:49:49', '1', null, null);
INSERT INTO `pages_en` VALUES ('2', 'Our services', 'our-services', null, null, '2015-01-09 12:01:05', '1', '2015-01-09 12:29:04', '1', null, null);
INSERT INTO `pages_en` VALUES ('3', null, null, null, null, '2015-01-09 12:05:30', '1', '2015-01-09 12:06:14', '1', null, null);
INSERT INTO `pages_en` VALUES ('4', null, null, null, null, '2015-01-09 12:06:19', '1', '2015-01-09 12:07:10', '1', null, null);
INSERT INTO `pages_en` VALUES ('5', null, null, null, null, '2015-01-09 12:07:13', '1', '2015-01-09 12:07:54', '1', null, null);
INSERT INTO `pages_en` VALUES ('6', null, null, null, null, '2015-01-09 12:07:59', '1', '2015-01-09 12:44:15', '1', null, null);
INSERT INTO `pages_en` VALUES ('7', null, null, null, null, '2015-01-09 16:14:37', '1', '2015-01-09 16:14:57', '1', null, null);

-- ----------------------------
-- Table structure for `pages_structure`
-- ----------------------------
DROP TABLE IF EXISTS `pages_structure`;
CREATE TABLE `pages_structure` (
  `structure_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_structure_id` int(11) DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `structure_rank` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`structure_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages_structure
-- ----------------------------

-- ----------------------------
-- Table structure for `portals`
-- ----------------------------
DROP TABLE IF EXISTS `portals`;
CREATE TABLE `portals` (
  `portal_id` int(11) NOT NULL AUTO_INCREMENT,
  `portal_title` varchar(255) DEFAULT NULL,
  `portal_classname` varchar(255) DEFAULT NULL,
  `portal_url` varchar(255) DEFAULT NULL,
  `portal_active` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `export_broker` tinyint(4) DEFAULT NULL,
  `portal_settings` text,
  `portal_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`portal_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of portals
-- ----------------------------
INSERT INTO `portals` VALUES ('1', 'Sreality', 'srealityExport', 'www.sreality.cz', '1', '2015-04-15 18:22:28', null, '2015-04-23 11:59:12', null, null, null, '1', '[\"sreality_client_id\",\"sreality_password\",\"sreality_key\"]', 'sreality');
INSERT INTO `portals` VALUES ('2', 'Reality Idnes', 'idnesExport', 'reality.idnes.cz', '1', '2015-04-15 18:23:14', null, '2015-04-23 11:59:16', null, null, null, '0', '[\"idnes_ftp_user\",\"idnes_ftp_password\",\"idnes_import_login\",\"idnes_import_password\"]', 'idnes');
INSERT INTO `portals` VALUES ('3', 'Reality MIX', 'realityMixExport', 'www.realitymix.cz', '1', '2015-04-21 16:59:36', null, '2015-04-23 17:12:48', null, null, null, '1', '[\"realitymix_client_id\",\"realitymix_password\",\"realitymix_key\"]', 'realitymix');
INSERT INTO `portals` VALUES ('4', 'Real City', 'realCityExport', 'www.realcity.cz', '1', '2015-04-24 15:16:23', null, '2015-04-24 15:19:01', null, null, null, '1', '[\"realcity_client_id\",\"realcity_password\",\"realcity_key\"]', 'realcity');

-- ----------------------------
-- Table structure for `projects`
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `project_address` varchar(255) DEFAULT NULL,
  `project_latitude` double DEFAULT NULL,
  `project_longitude` double DEFAULT NULL,
  `project_established` date DEFAULT NULL,
  `project_finished` date DEFAULT NULL,
  `project_status` varchar(255) NOT NULL DEFAULT 'new',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `project_rank` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('1', '2', 'U závor 1367, Neratovice', '50.2544773', '14.5043104', '2015-03-05', '2015-05-08', 'progress', '2015-03-30 15:41:53', '19', '2015-04-13 12:41:34', '19', null, null, '1');
INSERT INTO `projects` VALUES ('2', '2', null, '49.8174366199', '15.474335291', '2015-03-07', '2015-06-19', 'prepare', '2015-03-30 17:31:26', '19', '2015-04-13 12:42:10', '19', null, null, '2');
INSERT INTO `projects` VALUES ('3', '2', 'Houston', '29.7604267', '95.3698028', '2015-03-19', '2015-06-11', 'sale', '2015-03-30 17:50:59', '19', '2015-03-30 20:37:32', '19', null, null, null);
INSERT INTO `projects` VALUES ('4', '2', null, null, null, null, null, 'prepare', '2015-04-13 15:08:54', '20', '2015-04-14 16:38:26', null, '2015-04-14 16:38:26', '8', '3');

-- ----------------------------
-- Table structure for `projects_brokers`
-- ----------------------------
DROP TABLE IF EXISTS `projects_brokers`;
CREATE TABLE `projects_brokers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of projects_brokers
-- ----------------------------
INSERT INTO `projects_brokers` VALUES ('1', '1', '19', '2015-03-30 15:47:06', '19', '2015-04-13 12:41:34', '19', null, null);
INSERT INTO `projects_brokers` VALUES ('2', '1', '22', '2015-03-30 18:47:44', '20', '2015-04-13 12:41:34', '19', null, null);
INSERT INTO `projects_brokers` VALUES ('3', '3', '28', '2015-03-30 18:53:28', '20', '2015-03-30 20:37:32', '19', null, null);
INSERT INTO `projects_brokers` VALUES ('4', '3', '27', '2015-03-30 18:53:28', '20', '2015-03-30 20:37:32', '19', null, null);

-- ----------------------------
-- Table structure for `projects_cs`
-- ----------------------------
DROP TABLE IF EXISTS `projects_cs`;
CREATE TABLE `projects_cs` (
  `project_id` int(11) NOT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_description` longtext,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of projects_cs
-- ----------------------------
INSERT INTO `projects_cs` VALUES ('1', 'Lucky Gardens', '<p> Curabitur tempor feugiat nisi, at finibus sapien. Curabitur ultrices  imperdiet mauris ut accumsan. Sed euismod, dolor ac eleifend dictum, leo  dui hendrerit dui, id consectetur orci quam in orci. Etiam lobortis  augue a venenatis suscipit. Proin placerat ullamcorper porta. Quisque  rutrum id nunc at egestas. Fusce erat risus, commodo vel mauris id,  pellentesque congue erat. Nunc in hendrerit orci, eget vestibulum elit.  Mauris maximus nunc ac augue fermentum, eu dapibus elit ornare. Duis  viverra bibendum auctor. Etiam in euismod magna, accumsan placerat nisl.  Quisque lacinia rhoncus justo, eget lobortis nulla. Phasellus vel  feugiat nulla.</p><p> Nullam aliquam ipsum est. Praesent ac lobortis ipsum. Etiam bibendum,  augue vitae sagittis cursus, felis neque mattis urna, sed aliquet ipsum  justo non massa. Fusce faucibus nisi justo, vitae facilisis metus  vestibulum vitae. Integer laoreet metus semper tempus ultrices. Nam in  enim vitae nibh efficitur faucibus vitae eu metus. Duis maximus porta  est eu elementum. Suspendisse pretium sollicitudin lacus, at accumsan  est fermentum sit amet. Vestibulum consequat at ligula non suscipit.  Vestibulum et eleifend enim, interdum sodales felis. Ut nisi ligula,  accumsan vitae finibus quis, luctus ut eros. Morbi venenatis, turpis sed  ultrices cursus, erat ipsum pharetra risus, dapibus vestibulum arcu  lacus ac felis. Nulla ac eros quis lacus vestibulum varius. Praesent  eget nunc dolor. Pellentesque id mi dolor.</p><p> Donec viverra dui nisl, eget faucibus libero vestibulum at. Suspendisse  vehicula augue mauris, vitae ultrices quam fringilla ut. Pellentesque  mauris orci, tristique a libero in, tempus ullamcorper risus. Nulla  facilisi. Mauris eget fringilla lectus. Proin semper arcu nunc, quis  maximus tortor congue id. Nulla volutpat augue orci, ac vulputate ligula  accumsan et. Ut sit amet porttitor nisl, eu maximus erat. Suspendisse  sit amet urna a purus eleifend congue.</p>');
INSERT INTO `projects_cs` VALUES ('2', 'Luxury town', '<p> Suspendisse nec dignissim mauris. Curabitur eget ante ut enim rhoncus  sodales. Pellentesque viverra lorem urna, eget lacinia eros lobortis  non. Donec commodo ultricies aliquam. Aliquam sed accumsan nisl, in  ultricies erat. Morbi ut tortor ac nibh feugiat condimentum sed non leo.  Nam a risus justo. Proin id scelerisque dolor. Proin id fermentum elit.  Etiam at arcu sit amet quam volutpat gravida sed et ex. Phasellus  tristique eget magna ac venenatis. In hac habitasse platea dictumst.  Proin fermentum arcu nibh, sed blandit turpis tempus non. Sed sed  consectetur velit. Pellentesque feugiat sapien sed mauris ultrices, non  dignissim nisi faucibus. Nulla varius ligula nisl, eu blandit quam  congue in.</p><p> Mauris sed est dignissim, venenatis libero lacinia, placerat turpis.  Vivamus suscipit, nisl eget cursus aliquam, ex odio fermentum metus, sit  amet semper felis tellus et enim. Pellentesque habitant morbi tristique  senectus et netus et malesuada fames ac turpis egestas. Nunc congue dui  nisi, ut luctus nibh scelerisque a. Aliquam lobortis gravida  consectetur. Maecenas sit amet enim ornare, volutpat risus in, rhoncus  tellus. Vestibulum viverra convallis augue, ut lobortis risus fermentum  eu. Proin blandit, metus non tempor consequat, odio risus imperdiet  massa, in tincidunt nisl nisi ac nibh. Donec quis malesuada erat.  Aliquam sed velit ut odio rutrum aliquam. Donec et auctor nulla, sit  amet efficitur ex. Sed ut mi ac turpis aliquet faucibus ut id ante.</p>');
INSERT INTO `projects_cs` VALUES ('3', 'Billy Hills', '<p>Tento projekt představuje možnost pohodlného bydlení v nádherné přírodní scenérii vrchoviny Billy Hills</p>');
INSERT INTO `projects_cs` VALUES ('4', 'Nový projekt', null);

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role` varchar(30) NOT NULL,
  `parent_role` varchar(30) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('admin', 'superAdmin', '2014-11-04 10:47:45', '0', null, null, null, null);
INSERT INTO `roles` VALUES ('broker', 'admin', '2014-11-04 10:47:45', '0', null, null, null, null);
INSERT INTO `roles` VALUES ('guest', 'broker', '2014-11-04 10:47:45', '0', null, null, null, null);
INSERT INTO `roles` VALUES ('superAdmin', null, '2014-11-04 10:47:45', '0', null, null, null, null);

-- ----------------------------
-- Table structure for `roles_cs`
-- ----------------------------
DROP TABLE IF EXISTS `roles_cs`;
CREATE TABLE `roles_cs` (
  `role` varchar(30) NOT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles_cs
-- ----------------------------
INSERT INTO `roles_cs` VALUES ('admin', 'Správce');
INSERT INTO `roles_cs` VALUES ('broker', 'Makléř');
INSERT INTO `roles_cs` VALUES ('guest', 'Host');
INSERT INTO `roles_cs` VALUES ('superAdmin', 'Programátor');

-- ----------------------------
-- Table structure for `roles_en`
-- ----------------------------
DROP TABLE IF EXISTS `roles_en`;
CREATE TABLE `roles_en` (
  `role` varchar(30) NOT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) NOT NULL,
  `changed` timestamp NULL DEFAULT NULL,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles_en
-- ----------------------------
INSERT INTO `roles_en` VALUES ('Admin', 'Administrator', '2014-11-04 10:48:22', '0', null, null, null, null);
INSERT INTO `roles_en` VALUES ('broker', 'Broker', '2014-11-04 10:48:22', '0', null, null, null, null);
INSERT INTO `roles_en` VALUES ('guest', 'Guest', '2014-11-04 10:48:22', '0', null, null, null, null);
INSERT INTO `roles_en` VALUES ('superAdmin', 'Programer', '2014-11-04 10:48:22', '0', null, null, null, null);

-- ----------------------------
-- Table structure for `slugs`
-- ----------------------------
DROP TABLE IF EXISTS `slugs`;
CREATE TABLE `slugs` (
  `slug_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_table` varchar(255) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `slug_action` varchar(255) DEFAULT NULL,
  `slug_parameters` text,
  `slug_available` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `slug_meta_author` varchar(255) DEFAULT NULL,
  `slug_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slugs
-- ----------------------------
INSERT INTO `slugs` VALUES ('1', null, null, 'Estates:default', null, '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:39', null, null, null, null, null, 'ccd52a8e8a4b59587a5e7fd2ed6bf9d2');
INSERT INTO `slugs` VALUES ('2', 'pages', '1', 'Pages:default', '{\"page_id\":1}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:39', null, null, null, '1', null, '0c044734422a02875322048c03bcf4aa');
INSERT INTO `slugs` VALUES ('3', 'pages', '2', 'Pages:default', '{\"page_id\":2}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:39', null, null, null, '1', null, '577860d0d46917d106d86c4e1f2b52d1');
INSERT INTO `slugs` VALUES ('4', 'pages', '6', 'Pages:default', '{\"page_id\":6}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:39', null, null, null, '1', null, '7f0b7965df41281d6ea67fc517145591');
INSERT INTO `slugs` VALUES ('5', null, null, 'Demands:default', null, '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:40', null, null, null, null, null, '03ec03d59d0e9594cd6a92b08ce59cd4');
INSERT INTO `slugs` VALUES ('6', null, null, 'Contacts:default', null, '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:40', null, null, null, null, null, '03dfc94c5e6a759dc7091f4b07ecb57b');
INSERT INTO `slugs` VALUES ('7', 'pages', '7', 'Pages:default', '{\"page_id\":7}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:40', null, null, null, '1', null, '049aad9800ae4a7a30eca00aeca061cd');
INSERT INTO `slugs` VALUES ('8', 'pages', '5', 'Pages:default', '{\"page_id\":5}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:40', null, null, null, null, null, '5b31a32ebdf7040a9e580a6e3d25f488');
INSERT INTO `slugs` VALUES ('9', 'pages', '6', 'Pages:default', '{\"page_id\":6}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:40', null, null, null, null, null, '7f0b7965df41281d6ea67fc517145591');
INSERT INTO `slugs` VALUES ('10', 'pages', '7', 'Pages:default', '{\"page_id\":7}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:41', null, null, null, null, null, '049aad9800ae4a7a30eca00aeca061cd');
INSERT INTO `slugs` VALUES ('11', null, null, 'Estates:default', '{\"advert_type\":1}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:41', null, null, null, null, null, '437e45f4f9e7d39feec7e46f5c0c8d4b');
INSERT INTO `slugs` VALUES ('12', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_function\":1}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:41', null, null, null, null, null, '151a78e61108af19ac603edec51bb277');
INSERT INTO `slugs` VALUES ('13', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_function\":2}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:41', null, null, null, null, null, 'defee534ae2c00473088d97bc2c11ac1');
INSERT INTO `slugs` VALUES ('14', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_function\":3}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, '610b68d857d15337fd1e3572b3cf6a27');
INSERT INTO `slugs` VALUES ('15', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":2}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, 'a106ac7c27b7c9f1ee2ac359c49d6356');
INSERT INTO `slugs` VALUES ('16', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":3}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, '9a0b278324be95e404c302275c11bab6');
INSERT INTO `slugs` VALUES ('17', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":4}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, 'faac0f1879beb07bb36416cc93306998');
INSERT INTO `slugs` VALUES ('18', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":5}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, '34b00aa8a48c496ecaced728863b0ead');
INSERT INTO `slugs` VALUES ('19', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":6}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:42', null, null, null, null, null, 'f9e2b28637a78a46f11a8dcf2b8b94e6');
INSERT INTO `slugs` VALUES ('20', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":7}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:43', null, null, null, null, null, 'afc1da9dac9876ad3d1d31fc69ad9d13');
INSERT INTO `slugs` VALUES ('21', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":8}', '1', '2015-01-22 16:21:51', null, '2015-05-11 15:10:43', null, null, null, null, null, '82f8a5805934484e9079c1a9b869f78b');
INSERT INTO `slugs` VALUES ('22', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":9}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:43', null, null, null, null, null, '395d0315302bc2bec0eb1777bf7ca58c');
INSERT INTO `slugs` VALUES ('23', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":10}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:43', null, null, null, null, null, '7c576cb7f42c76aebe0c0a77cb8e437d');
INSERT INTO `slugs` VALUES ('24', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":11}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:43', null, null, null, null, null, 'a0267b27cd5e78b92b5f2dacc73c8671');
INSERT INTO `slugs` VALUES ('25', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":12}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:43', null, null, null, null, null, 'cd2a699e554fb6dbd471138df12d1826');
INSERT INTO `slugs` VALUES ('26', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":16}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:43', null, null, null, null, null, '7ce95a337d8521d04c9283209a61a437');
INSERT INTO `slugs` VALUES ('27', null, null, 'Estates:default', '{\"advert_type\":1,\"advert_subtype\":47}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, 'f6dc9f410c454f7ffc4e1d040118c25d');
INSERT INTO `slugs` VALUES ('28', null, null, 'Estates:default', '{\"advert_type\":2}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, 'dd4833f1034b69e842c0b5cbb2bad00a');
INSERT INTO `slugs` VALUES ('29', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_function\":1}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, '3946e8ba518323c88a17d4a0c15ed36a');
INSERT INTO `slugs` VALUES ('30', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_function\":2}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, 'ae5061ab13fe544b606bbf043a791389');
INSERT INTO `slugs` VALUES ('31', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_function\":3}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, '227d351711a70c07cc69982b30dd1d95');
INSERT INTO `slugs` VALUES ('32', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":33}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:44', null, null, null, null, null, '2e10884bb835b777b1725b6bd0c8ddd2');
INSERT INTO `slugs` VALUES ('33', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":35}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:45', null, null, null, null, null, '79d26526e29c2d0e1b8892439e58642b');
INSERT INTO `slugs` VALUES ('34', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":37}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:45', null, null, null, null, null, '755e2feaac81a0b5cbf5703b527024e2');
INSERT INTO `slugs` VALUES ('35', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":39}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, '88ca386edfde538a0cb1af8b9603ae3a');
INSERT INTO `slugs` VALUES ('36', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":40}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, '1cbb0920f2dbb2d0f2035505a849e5ce');
INSERT INTO `slugs` VALUES ('37', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":43}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, 'aa6382ad22b0afd6955127ef4ea2ff07');
INSERT INTO `slugs` VALUES ('38', null, null, 'Estates:default', '{\"advert_type\":2,\"advert_subtype\":44}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, '689e685c6a2622debd3254dd1ea1a6a7');
INSERT INTO `slugs` VALUES ('39', null, null, 'Estates:default', '{\"advert_type\":3}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, '26a07bcbfd9c424f70d1952263ce55f4');
INSERT INTO `slugs` VALUES ('40', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_function\":1}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, 'bb79e5048b7992200299f122dba26203');
INSERT INTO `slugs` VALUES ('41', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_function\":2}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, '733828fc6feed1bd710de98828059dd6');
INSERT INTO `slugs` VALUES ('42', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_function\":3}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:46', null, null, null, null, null, 'b5c49a4558c41c921b0934126a68bbae');
INSERT INTO `slugs` VALUES ('43', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":18}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, '0126cce4c52815f3b1a20d5646161bed');
INSERT INTO `slugs` VALUES ('44', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":19}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, 'fbe32f9287649afde3c364e04dbe731b');
INSERT INTO `slugs` VALUES ('45', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":20}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, '7ff861f45167980cca011235a3ea6a03');
INSERT INTO `slugs` VALUES ('46', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":21}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, '5c96f149db736d9247f0e8f97feb6222');
INSERT INTO `slugs` VALUES ('47', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":22}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, '2b001aec70670705d5cb7a45d718594e');
INSERT INTO `slugs` VALUES ('48', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":23}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, 'cd048b3a3a46eaf221c3ad93a7f61472');
INSERT INTO `slugs` VALUES ('49', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":24}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:47', null, null, null, null, null, '5dadcc716de666b968c75779c08b577a');
INSERT INTO `slugs` VALUES ('50', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":46}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, 'b747eed2471b75a1a2504773b70cb590');
INSERT INTO `slugs` VALUES ('51', null, null, 'Estates:default', '{\"advert_type\":3,\"advert_subtype\":48}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, 'a5a1556cb43950c5b638c81a3bdd1326');
INSERT INTO `slugs` VALUES ('52', null, null, 'Estates:default', '{\"advert_type\":4}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, 'ff60651dd8aac1c1255e37159cc18e64');
INSERT INTO `slugs` VALUES ('53', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_function\":1}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, '9287ccc04442ca9fddd19cece6a0b383');
INSERT INTO `slugs` VALUES ('54', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_function\":2}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, '568080e9e462260712fcc5b8140b9fa7');
INSERT INTO `slugs` VALUES ('55', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_function\":3}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, 'f3276ca414a684bb126938cb0fe2ab1d');
INSERT INTO `slugs` VALUES ('56', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":25}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, 'beca7fd9034cef5efca2b4549a661a61');
INSERT INTO `slugs` VALUES ('57', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":26}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:48', null, null, null, null, null, '42c10e5481b6cb90a4841c67ff44f2be');
INSERT INTO `slugs` VALUES ('58', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":27}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'bcc24dfe5f10550655b46a20d89b99f5');
INSERT INTO `slugs` VALUES ('59', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":28}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'e9896eeb6431e8426b3bb2dc9aa15b42');
INSERT INTO `slugs` VALUES ('60', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":29}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'ac51dfda7a782ac4c1337e5c478fd0c1');
INSERT INTO `slugs` VALUES ('61', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":30}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, '6e8ee87d32ca0cebd736f998a9d2ccab');
INSERT INTO `slugs` VALUES ('62', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":31}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'aeaaf503f3ce709ed99e8959b7e2126b');
INSERT INTO `slugs` VALUES ('63', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":32}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'cd0eb2967ae30ac7fbd16c1c218585fc');
INSERT INTO `slugs` VALUES ('64', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":38}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, '4a09f21d68fe20d1a03e860f38f348b1');
INSERT INTO `slugs` VALUES ('65', null, null, 'Estates:default', '{\"advert_type\":4,\"advert_subtype\":49}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:49', null, null, null, null, null, 'b17dd652eb29b64750d3ae81ce2ab50c');
INSERT INTO `slugs` VALUES ('66', null, null, 'Estates:default', '{\"advert_type\":5}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:50', null, null, null, null, null, 'ea9b336b44f6b8083f0b1927324f312e');
INSERT INTO `slugs` VALUES ('67', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_function\":1}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:50', null, null, null, null, null, 'a84bd15af34f5d9157794bb99fd6c91a');
INSERT INTO `slugs` VALUES ('68', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_function\":2}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:50', null, null, null, null, null, '240b0abfb5935d5d0ae74d4f130c137d');
INSERT INTO `slugs` VALUES ('69', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_function\":3}', '1', '2015-01-22 16:21:52', null, '2015-05-11 15:10:50', null, null, null, null, null, '15885ddde94f4e2924b37c23f31b571a');
INSERT INTO `slugs` VALUES ('70', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":34}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:50', null, null, null, null, null, '39a83d9dd22d85ab16ef5dcf6356e793');
INSERT INTO `slugs` VALUES ('71', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":36}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:50', null, null, null, null, null, '1578aa65e16ddbb0b251413bc7d4a82f');
INSERT INTO `slugs` VALUES ('72', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":50}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:50', null, null, null, null, null, 'cccfa1b8aef1fe0fb3cfeaa51e6ca66d');
INSERT INTO `slugs` VALUES ('73', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":51}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:51', null, null, null, null, null, 'da95df29ce03d4eefad2444b4f704388');
INSERT INTO `slugs` VALUES ('74', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":52}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:51', null, null, null, null, null, '0fcc041e2173c3a520bfb5f799b51f43');
INSERT INTO `slugs` VALUES ('75', null, null, 'Estates:default', '{\"advert_type\":5,\"advert_subtype\":53}', '1', '2015-01-22 16:21:53', null, '2015-05-11 15:10:51', null, null, null, null, null, 'b738081ef0b6bfdb9119a322be4bc1d5');
INSERT INTO `slugs` VALUES ('76', 'estates', '3', 'Estates:detail', '{\"estate_id\":3}', '0', '2015-01-22 21:07:38', '9', '2015-05-11 15:10:51', null, null, null, '1', null, '69c7952bf4a9564361ef029a6deb51e5');
INSERT INTO `slugs` VALUES ('77', 'estates', '5', 'Estates:detail', '{\"estate_id\":5}', '0', '2015-01-22 21:18:17', '9', '2015-05-11 15:10:51', null, null, null, '1', null, '3c93fe5d591778dfc0ac504de5361a71');
INSERT INTO `slugs` VALUES ('78', 'estates', '9', 'Estates:detail', '{\"estate_id\":9}', '0', '2015-01-22 22:03:14', '11', '2015-05-11 15:10:51', null, null, null, '1', null, '2c5877300d4407c0af2a8a264a7f13e4');
INSERT INTO `slugs` VALUES ('79', 'estates', '6', 'Estates:detail', '{\"estate_id\":6}', '0', '2015-01-23 13:29:04', '9', '2015-05-11 15:10:51', null, null, null, '1', null, 'f32e60ef0b84f16a556c755f043e84aa');
INSERT INTO `slugs` VALUES ('80', 'pages', '9', 'Pages:default', '{\"page_id\":9}', '1', '2015-01-23 15:47:25', '8', '2015-05-11 15:10:52', null, null, null, '1', null, 'e8a2ac597aae4437cb00e3228085dc20');
INSERT INTO `slugs` VALUES ('81', 'pages', '10', 'Pages:default', '{\"page_id\":10}', '1', '2015-01-23 15:49:30', '8', '2015-05-11 15:10:52', null, null, null, '1', null, '448bf998ed9686a17992f7ba020add38');
INSERT INTO `slugs` VALUES ('82', 'pages', '11', 'Pages:default', '{\"page_id\":11}', '1', '2015-01-23 17:45:05', '8', '2015-05-11 15:10:52', null, null, null, '1', null, '311a4c1e5441ebf4b45da2854c84f739');
INSERT INTO `slugs` VALUES ('83', 'pages', '13', 'Pages:default', '{\"page_id\":13}', '1', '2015-01-23 17:48:23', '8', '2015-05-11 15:10:52', null, null, null, '1', null, 'd27b838daa16bbe42a8907caa3f596a8');
INSERT INTO `slugs` VALUES ('84', 'pages', '14', 'Pages:default', '{\"page_id\":14}', '1', '2015-01-23 17:49:54', '8', '2015-05-11 15:10:52', null, null, null, '1', null, 'e10d0656ad3da71c90826bd3ce692bc8');
INSERT INTO `slugs` VALUES ('85', 'pages', '15', 'Pages:default', '{\"page_id\":15}', '1', '2015-01-23 17:50:37', '8', '2015-05-11 15:10:52', null, null, null, '1', null, 'aa27208578aa12860c3efaead7a61871');
INSERT INTO `slugs` VALUES ('86', 'estates', '8', 'Estates:detail', '{\"estate_id\":8}', '0', '2015-01-23 18:25:35', '9', '2015-05-11 15:10:52', null, null, null, '1', null, '04c7a90403c3b133ab0b7088cb1cf35d');
INSERT INTO `slugs` VALUES ('87', 'estates', '4', 'Estates:detail', '{\"estate_id\":4}', '0', '2015-01-23 18:46:10', '8', '2015-05-11 15:10:52', null, null, null, '1', null, '95901dd4034ac4be8ae1f74b63c091cf');
INSERT INTO `slugs` VALUES ('88', 'estates', '7', 'Estates:detail', '{\"estate_id\":7}', '0', '2015-01-25 18:44:17', '9', '2015-05-11 15:10:53', null, null, null, '1', null, '7658160dfa20194836527f4bd9dffad4');
INSERT INTO `slugs` VALUES ('89', 'pages', '16', 'Pages:default', '{\"page_id\":16}', '1', '2015-02-02 10:05:26', '9', '2015-05-11 15:10:53', null, null, null, '1', null, '1cc809e56de09e967af5ee09109c2679');
INSERT INTO `slugs` VALUES ('90', 'estates', '12', 'Estates:detail', '{\"estate_id\":12}', '0', '2015-02-22 22:02:57', '15', '2015-05-11 15:10:53', null, null, null, '1', null, 'bf9e0717a8018571d2a94bfc59293a50');
INSERT INTO `slugs` VALUES ('91', 'pages', '17', 'Pages:default', '{\"page_id\":17}', '1', '2015-03-10 11:00:04', '8', '2015-05-11 15:10:53', null, null, null, '2', null, '3f3ee05ab11ebe6768ccc9b443a10112');
INSERT INTO `slugs` VALUES ('92', 'pages', '18', 'Pages:default', '{\"page_id\":18}', '1', '2015-03-10 11:09:28', '8', '2015-05-11 15:10:53', null, null, null, '2', null, 'c02fa361665584506c5175677176b6a3');
INSERT INTO `slugs` VALUES ('93', 'estates', '13', 'Estates:detail', '{\"estate_id\":13}', '0', '2015-03-10 15:33:19', '19', '2015-04-22 16:22:51', '1', '2015-04-22 16:22:51', '1', '2', null, null);
INSERT INTO `slugs` VALUES ('94', 'estates', '14', 'Estates:detail', '{\"estate_id\":14}', '0', '2015-03-10 16:28:17', '19', '2015-05-11 15:10:58', null, null, null, '2', null, '9693d45b4df66c488caf4e8dcd07c80f');
INSERT INTO `slugs` VALUES ('95', 'estates', '15', 'Estates:detail', '{\"estate_id\":15}', '0', '2015-03-10 16:35:09', '19', '2015-05-11 15:10:58', null, null, null, '2', null, '6714750f8ecbd7bd7960078716e500ac');
INSERT INTO `slugs` VALUES ('96', 'estates', '16', 'Estates:detail', '{\"estate_id\":16}', '0', '2015-03-10 16:54:57', '19', '2015-05-11 15:10:59', null, null, null, '2', null, 'ea8c7cfbeca22a8a8eb253ef046a1463');
INSERT INTO `slugs` VALUES ('97', 'projects', null, 'Projects:default', null, '1', '2015-03-30 15:23:41', null, '2015-05-11 15:10:54', null, null, null, null, null, 'e739ed60dc7c92e164c19f7cd4c56d79');
INSERT INTO `slugs` VALUES ('98', 'projects', '1', 'Projects:detail', '{\"project_id\":1}', '0', '2015-03-30 15:41:53', '19', '2015-05-11 15:10:54', null, null, null, '2', null, '9d4859c0da6098c1567c571079429d8c');
INSERT INTO `slugs` VALUES ('99', 'articles', '1', 'Articles:detail', '{\"article_id\":1}', '0', '2015-03-30 16:15:46', '19', '2015-05-11 15:10:54', null, null, null, '2', null, '1e4ac85e10917bf47d252c5437146fcf');
INSERT INTO `slugs` VALUES ('100', 'projects', '2', 'Projects:detail', '{\"project_id\":2}', '0', '2015-03-30 17:31:26', '19', '2015-05-11 15:10:54', null, null, null, '2', null, 'a2bae6b65a9325ccf46239c29596b1df');
INSERT INTO `slugs` VALUES ('101', 'projects', '3', 'Projects:detail', '{\"project_id\":3}', '0', '2015-03-30 17:50:59', '19', '2015-05-11 15:10:54', null, null, null, '2', null, '0c24150d1e443587b383c4a93de73a4a');
INSERT INTO `slugs` VALUES ('102', 'articles', '2', 'Articles:detail', '{\"article_id\":2}', '0', '2015-03-30 18:55:05', '20', '2015-05-11 15:10:54', null, null, null, '2', null, 'c485a273eb78f783dc10168fbefe31b4');
INSERT INTO `slugs` VALUES ('103', 'brokerprofiles', '19', 'BrokerProfiles:detail', '{\"user_id\":19}', '0', '2015-04-13 11:46:05', '19', '2015-05-11 15:10:54', null, null, null, '2', null, 'ecdd858f5ab45798b0981473cba69773');
INSERT INTO `slugs` VALUES ('104', 'projects', '4', 'Projects:detail', '{\"project_id\":4}', '0', '2015-04-13 15:08:54', '20', '2015-05-11 15:10:54', null, null, null, '2', null, 'edddc3b7b0a5e4aa0e853f5f2681838f');

-- ----------------------------
-- Table structure for `slugs_cs`
-- ----------------------------
DROP TABLE IF EXISTS `slugs_cs`;
CREATE TABLE `slugs_cs` (
  `slug_id` int(11) NOT NULL,
  `slug_url` varchar(255) DEFAULT NULL,
  `slug_label` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `slug_meta_keywords` varchar(255) DEFAULT NULL,
  `slug_meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`),
  CONSTRAINT `slugs_cs_ibfk_1` FOREIGN KEY (`slug_id`) REFERENCES `slugs` (`slug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slugs_cs
-- ----------------------------
INSERT INTO `slugs_cs` VALUES ('1', 'nemovitosti', 'Nemovitosti', 'Naše nemovitosti', '', '');
INSERT INTO `slugs_cs` VALUES ('2', 'o-nas', 'O nás', '', '', '');
INSERT INTO `slugs_cs` VALUES ('3', 'nase-sluzby', 'Naše služby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('4', 'kariera', 'Kariéra', '', '', '');
INSERT INTO `slugs_cs` VALUES ('5', 'poptavame', 'Poptáváme', 'Poptáváte nemovitost? Obraťte se na nás', '', '');
INSERT INTO `slugs_cs` VALUES ('6', 'kontakty', 'Kontakty', 'Kontaktní informace společnosti BELARTIS', '', '');
INSERT INTO `slugs_cs` VALUES ('7', 'realizace', 'Realizace', '', '', '');
INSERT INTO `slugs_cs` VALUES ('8', '', '', '', '', '');
INSERT INTO `slugs_cs` VALUES ('9', null, null, null, null, null);
INSERT INTO `slugs_cs` VALUES ('10', null, null, null, null, null);
INSERT INTO `slugs_cs` VALUES ('11', 'byty', 'Byty', '', '', '');
INSERT INTO `slugs_cs` VALUES ('12', 'byty/prodej', 'Byty, Prodej', '', '', '');
INSERT INTO `slugs_cs` VALUES ('13', 'byty/pronajem', 'Byty, Pronájem', '', '', '');
INSERT INTO `slugs_cs` VALUES ('14', 'byty/drazby', 'Byty, Dražby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('15', 'byty/1-kk', 'Byty, 1+kk', '', '', '');
INSERT INTO `slugs_cs` VALUES ('16', 'byty/1-1', 'Byty, 1+1', '', '', '');
INSERT INTO `slugs_cs` VALUES ('17', 'byty/2-kk', 'Byty, 2+kk', '', '', '');
INSERT INTO `slugs_cs` VALUES ('18', 'byty/2-1', 'Byty, 2+1', '', '', '');
INSERT INTO `slugs_cs` VALUES ('19', 'byty/3-kk', 'Byty, 3+kk', '', '', '');
INSERT INTO `slugs_cs` VALUES ('20', 'byty/3-1', 'Byty, 3+1', '', '', '');
INSERT INTO `slugs_cs` VALUES ('21', 'byty/4-kk', 'Byty, 4+kk', '', '', '');
INSERT INTO `slugs_cs` VALUES ('22', 'byty/4-1', 'Byty, 4+1', '', '', '');
INSERT INTO `slugs_cs` VALUES ('23', 'byty/5-kk', 'Byty, 5+kk', '', '', '');
INSERT INTO `slugs_cs` VALUES ('24', 'byty/5-1', 'Byty, 5+1', '', '', '');
INSERT INTO `slugs_cs` VALUES ('25', 'byty/6-a-vice', 'Byty, 6 a více', '', '', '');
INSERT INTO `slugs_cs` VALUES ('26', 'byty/atypicky', 'Byty, Atypický', '', '', '');
INSERT INTO `slugs_cs` VALUES ('27', 'byty/pokoj', 'Byty, Pokoj', '', '', '');
INSERT INTO `slugs_cs` VALUES ('28', 'domy', 'Domy', '', '', '');
INSERT INTO `slugs_cs` VALUES ('29', 'domy/prodej', 'Domy, Prodej', '', '', '');
INSERT INTO `slugs_cs` VALUES ('30', 'domy/pronajem', 'Domy, Pronájem', '', '', '');
INSERT INTO `slugs_cs` VALUES ('31', 'domy/drazby', 'Domy, Dražby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('32', 'domy/chata', 'Domy, Chata', '', '', '');
INSERT INTO `slugs_cs` VALUES ('33', 'domy/pamatka/jine', 'Domy, Památka/jiné', '', '', '');
INSERT INTO `slugs_cs` VALUES ('34', 'domy/rodinny', 'Domy, Rodinný', '', '', '');
INSERT INTO `slugs_cs` VALUES ('35', 'domy/vila', 'Domy, Vila', '', '', '');
INSERT INTO `slugs_cs` VALUES ('36', 'domy/na-klic', 'Domy, Na klíč', '', '', '');
INSERT INTO `slugs_cs` VALUES ('37', 'domy/chalupa', 'Domy, Chalupa', '', '', '');
INSERT INTO `slugs_cs` VALUES ('38', 'domy/zemedelska-usedlost', 'Domy, Zemedělská usedlost', '', '', '');
INSERT INTO `slugs_cs` VALUES ('39', 'pozemky', 'Pozemky', '', '', '');
INSERT INTO `slugs_cs` VALUES ('40', 'pozemky/prodej', 'Pozemky, Prodej', '', '', '');
INSERT INTO `slugs_cs` VALUES ('41', 'pozemky/pronajem', 'Pozemky, Pronájem', '', '', '');
INSERT INTO `slugs_cs` VALUES ('42', 'pozemky/drazby', 'Pozemky, Dražby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('43', 'pozemky/komercni', 'Pozemky, Komerční', '', '', '');
INSERT INTO `slugs_cs` VALUES ('44', 'pozemky/bydleni', 'Pozemky, Bydlení', '', '', '');
INSERT INTO `slugs_cs` VALUES ('45', 'pozemky/pole', 'Pozemky, Pole', '', '', '');
INSERT INTO `slugs_cs` VALUES ('46', 'pozemky/lesy', 'Pozemky, Lesy', '', '', '');
INSERT INTO `slugs_cs` VALUES ('47', 'pozemky/louky', 'Pozemky, Louky', '', '', '');
INSERT INTO `slugs_cs` VALUES ('48', 'pozemky/zahrady', 'Pozemky, Zahrady', '', '', '');
INSERT INTO `slugs_cs` VALUES ('49', 'pozemky/ostatni', 'Pozemky, Ostatní', '', '', '');
INSERT INTO `slugs_cs` VALUES ('50', 'pozemky/rybniky', 'Pozemky, Rybníky', '', '', '');
INSERT INTO `slugs_cs` VALUES ('51', 'pozemky/sady/vinice', 'Pozemky, Sady/vinice', '', '', '');
INSERT INTO `slugs_cs` VALUES ('52', 'komercni', 'Komerční', '', '', '');
INSERT INTO `slugs_cs` VALUES ('53', 'komercni/prodej', 'Komerční, Prodej', '', '', '');
INSERT INTO `slugs_cs` VALUES ('54', 'komercni/pronajem', 'Komerční, Pronájem', '', '', '');
INSERT INTO `slugs_cs` VALUES ('55', 'komercni/drazby', 'Komerční, Dražby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('56', 'komercni/kancelare', 'Komerční, Kanceláře', '', '', '');
INSERT INTO `slugs_cs` VALUES ('57', 'komercni/sklady', 'Komerční, Sklady', '', '', '');
INSERT INTO `slugs_cs` VALUES ('58', 'komercni/vyroba', 'Komerční, Výroba', '', '', '');
INSERT INTO `slugs_cs` VALUES ('59', 'komercni/obchodni-prostory', 'Komerční, Obchodní prostory', '', '', '');
INSERT INTO `slugs_cs` VALUES ('60', 'komercni/ubytovani', 'Komerční, Ubytování', '', '', '');
INSERT INTO `slugs_cs` VALUES ('61', 'komercni/restaurace', 'Komerční, Restaurace', '', '', '');
INSERT INTO `slugs_cs` VALUES ('62', 'komercni/zemedelsky', 'Komerční, Zemedělský', '', '', '');
INSERT INTO `slugs_cs` VALUES ('63', 'komercni/ostatni', 'Komerční, Ostatní', '', '', '');
INSERT INTO `slugs_cs` VALUES ('64', 'komercni/cinzovni-dum', 'Komerční, Cinžovní dům', '', '', '');
INSERT INTO `slugs_cs` VALUES ('65', 'komercni/virtualni-kancelar', 'Komerční, Virtuální kancelář', '', '', '');
INSERT INTO `slugs_cs` VALUES ('66', 'ostatni', 'Ostatní', '', '', '');
INSERT INTO `slugs_cs` VALUES ('67', 'ostatni/prodej', 'Ostatní, Prodej', '', '', '');
INSERT INTO `slugs_cs` VALUES ('68', 'ostatni/pronajem', 'Ostatní, Pronájem', '', '', '');
INSERT INTO `slugs_cs` VALUES ('69', 'ostatni/drazby', 'Ostatní, Dražby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('70', 'ostatni/garaz', 'Ostatní, Garáž', '', '', '');
INSERT INTO `slugs_cs` VALUES ('71', 'ostatni/ostatni', 'Ostatní, Ostatní', '', '', '');
INSERT INTO `slugs_cs` VALUES ('72', 'ostatni/vinny-sklep', 'Ostatní, Vinný sklep', '', '', '');
INSERT INTO `slugs_cs` VALUES ('73', 'ostatni/pudni-prostor', 'Ostatní, Půdní prostor', '', '', '');
INSERT INTO `slugs_cs` VALUES ('74', 'ostatni/garazove-stani', 'Ostatní, Garážové stání', '', '', '');
INSERT INTO `slugs_cs` VALUES ('75', 'ostatni/mobilheim', 'Ostatní, Mobilheim', '', '', '');
INSERT INTO `slugs_cs` VALUES ('76', 'nemovitosti/pronajem/byty/pronajem-bytu-3-kk-praha', 'Pronájem bytu 3+kk, Praha', '', '', '');
INSERT INTO `slugs_cs` VALUES ('77', 'nemovitosti/prodej/domy/rodinny-dum-vojtesin', 'Rodinný dům, Vojtěšín', '', '', '');
INSERT INTO `slugs_cs` VALUES ('78', 'nemovitosti/prodej/pozemky/zahrada-1231m2', 'Zahrada 1231m2', '', '', '');
INSERT INTO `slugs_cs` VALUES ('79', 'nemovitosti/prodej/domy/prodej-rodinneho-domu-pitin', 'Prodej rodinného domu Pitín', '', '', '');
INSERT INTO `slugs_cs` VALUES ('80', 'nase-sluzby/prodej-nemovitosti', 'Prodej nemovitostí', '', '', '');
INSERT INTO `slugs_cs` VALUES ('81', 'nase-sluzby/pronajem-nemovitosti', 'Pronájem nemovitostí', '', '', '');
INSERT INTO `slugs_cs` VALUES ('82', 'nase-sluzby/sprava-nemovitosti', 'Správa nemovitostí', '', '', '');
INSERT INTO `slugs_cs` VALUES ('83', 'nase-sluzby/homestaging', 'Homestaging', '', '', '');
INSERT INTO `slugs_cs` VALUES ('84', 'nase-sluzby/financni-sluzby', 'Finanční služby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('85', 'nase-sluzby/pravni-sluzby', 'Právní služby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('86', 'nemovitosti/prodej/domy/chata-v-ceskem-raji', 'Chata v Českém ráji', '', '', '');
INSERT INTO `slugs_cs` VALUES ('87', 'nemovitosti/prodej/domy/rodinny-dum-kotoucov', 'Rodinný dům, Kotoučov', '', '', '');
INSERT INTO `slugs_cs` VALUES ('88', 'nemovitosti/prodej/byty/prodej-mezonetoveho-bytu-praha-6', 'Prodej mezonetového bytu Praha 6', '', '', '');
INSERT INTO `slugs_cs` VALUES ('89', 'nase-sluzby/rekonstrukce-nemovitosti', 'Rekonstrukce nemovitostí', '', '', '');
INSERT INTO `slugs_cs` VALUES ('90', 'nemovitosti/prodej/domy/vila-otradovice', 'Vila, Otradovice', '', '', '');
INSERT INTO `slugs_cs` VALUES ('91', 'o-nas', 'O nás', '', '', '');
INSERT INTO `slugs_cs` VALUES ('92', 'nase-sluzby', 'Naše služby', '', '', '');
INSERT INTO `slugs_cs` VALUES ('93', 'nemovitosti/prodej/byty/prodej-bytu-4-1-neratovice', 'Prodej bytu 4+1, Neratovice', '', '', '');
INSERT INTO `slugs_cs` VALUES ('94', 'nemovitosti/prodej/domy/prodej-rodinneho-domu-7-2-neratovice', 'Prodej rodinného domu 7+2, Neratovice', '', '', '');
INSERT INTO `slugs_cs` VALUES ('95', 'nemovitosti/pronajem/komercni/kancelarske-prostory-k-pronajmu', 'Kancelářské prostory k pronájmu', '', '', '');
INSERT INTO `slugs_cs` VALUES ('96', 'nemovitosti/prodej/pozemky/prodej-udoli-2-mil-m2', 'Prodej údolí 2 mil. m2', '', '', '');
INSERT INTO `slugs_cs` VALUES ('97', 'nase-projekty', 'Naše projekty', '', '', '');
INSERT INTO `slugs_cs` VALUES ('98', 'projekty/lucky-gardens', 'Lucky Gardens', '', '', '');
INSERT INTO `slugs_cs` VALUES ('99', 'clanky/nove-technologie-v-nasem-projektu', 'Nové technologie v našem projektu', '', '', '');
INSERT INTO `slugs_cs` VALUES ('100', 'projekty/luxury-town', 'Luxury town', '', '', '');
INSERT INTO `slugs_cs` VALUES ('101', 'projekty/billy-hills', 'Billy Hills', '', '', '');
INSERT INTO `slugs_cs` VALUES ('102', 'clanky/nizka-energeticka-narocnost-prokazana', 'Nízká energetická náročnost prokázána', '', '', '');
INSERT INTO `slugs_cs` VALUES ('103', 'profil/demo-makler', 'Demo Makléř', '', '', '');
INSERT INTO `slugs_cs` VALUES ('104', 'projekty/novy-projekt', 'Nový projekt', '', '', '');

-- ----------------------------
-- Table structure for `slugs_en`
-- ----------------------------
DROP TABLE IF EXISTS `slugs_en`;
CREATE TABLE `slugs_en` (
  `slug_id` int(11) NOT NULL,
  `slug_url` varchar(255) DEFAULT NULL,
  `slug_label` varchar(255) DEFAULT NULL,
  `slug_title` varchar(255) DEFAULT NULL,
  `slug_meta_keywords` varchar(255) DEFAULT NULL,
  `slug_meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`slug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slugs_en
-- ----------------------------
INSERT INTO `slugs_en` VALUES ('1', 'estates', 'Estates', 'Our Estates', null, null);
INSERT INTO `slugs_en` VALUES ('2', 'about-us', 'O nás', 'O společnosti BELARTIS', null, null);
INSERT INTO `slugs_en` VALUES ('3', 'nase-sluzby', 'Naše služby', 'Informace o našich službách', null, null);
INSERT INTO `slugs_en` VALUES ('4', 'kariera', 'Kariéra', 'Pracovní příležitosti', null, null);
INSERT INTO `slugs_en` VALUES ('5', 'poptavame', 'Poptáváme', 'Poptáváte nemovitost? Obraťte se na nás', null, null);
INSERT INTO `slugs_en` VALUES ('6', 'kontakty', 'Kontakty', 'Kontaktní informace společnosti BELARTIS', null, null);
INSERT INTO `slugs_en` VALUES ('7', 'reference', 'Reference', 'Reference a realizované projekty', null, null);
INSERT INTO `slugs_en` VALUES ('97', 'our-projects', 'Our Projects', null, null, null);

-- ----------------------------
-- Table structure for `sources`
-- ----------------------------
DROP TABLE IF EXISTS `sources`;
CREATE TABLE `sources` (
  `source_id` int(11) NOT NULL AUTO_INCREMENT,
  `source_title` varchar(100) NOT NULL,
  `source_method` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sources
-- ----------------------------

-- ----------------------------
-- Table structure for `test`
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_bool` tinyint(1) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------

-- ----------------------------
-- Table structure for `test_cs`
-- ----------------------------
DROP TABLE IF EXISTS `test_cs`;
CREATE TABLE `test_cs` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_title_edit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test_cs
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_title` varchar(255) DEFAULT NULL,
  `user_lastloged` timestamp NULL DEFAULT NULL,
  `user_lastloged_ip` varchar(255) DEFAULT NULL,
  `user_activation_code` varchar(255) DEFAULT NULL,
  `user_recovery_code` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `user_redirect_onlogin` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'admin', 'danielmarhan@gmail.com', '$2y$10$8MC1ov9dIR.SlrJFDz.Mo.HHrbzx6tngKoGXQpS7ubp7kmiqXTAly', 'Daniel Marhan', '2015-02-26 11:32:11', '185.36.160.19', null, 'BUsFWwHSTtktaTqPlOkoFQJyZ7SEUS', 'cs', null, null, null, null, '2014-12-19 10:54:24', null, '2015-02-26 11:32:11', null, null, null);
INSERT INTO `users` VALUES ('5', '1', 'broker', 'pisova@belartis.cz', '$2y$10$CJ/F4Vi5Qo6DfZJjjO6rqe.1RK0XdgK/PfJLKNUg0.iDnf4z88wna', 'Helena Píšová', null, null, null, null, 'cs', null, null, null, null, '2015-01-12 19:56:48', '1', '2015-01-19 20:11:49', '8', '2015-01-19 20:11:49', null);
INSERT INTO `users` VALUES ('8', null, 'superAdmin', 'info@creativeprojects.cz', '$2y$10$cG/bxxQCInQaiofoCktXWOBJS5y0oEakoqiPI09ESeocTc/rQ/.bm', 'Creative Projects s.r.o.', '2015-04-14 16:50:49', '185.36.160.19', null, null, 'cs', null, null, null, null, '2015-01-19 19:23:47', '1', '2015-04-14 16:50:49', null, null, null);
INSERT INTO `users` VALUES ('9', '1', 'admin', 'info@belartis.cz', '$2y$10$eA4N45HlgC3tyIYoZQxkWOr4GV6XFaeiBd6Y1ZUS.eZvNtrcNUdNi', 'BELARTIS s.r.o.', '2015-04-13 11:21:08', '185.36.160.19', null, null, 'cs', null, null, null, null, '2015-01-19 19:27:31', '1', '2015-04-13 11:21:08', null, null, null);
INSERT INTO `users` VALUES ('11', '1', 'broker', 'helena@belartis.cz', '$2y$10$nXKiOc6ix809DT9qgq6FI.f8eHCzvz9GSodNRqVJACAw7mmJvx5U.', 'Ing. Helena Píšová', '2015-02-21 08:26:16', null, null, null, 'cs', null, null, null, null, '2015-01-19 20:08:55', '8', '2015-02-21 16:38:01', null, '2015-02-21 16:38:01', '8');
INSERT INTO `users` VALUES ('13', '1', 'broker', 'hana@belartis.cz', '$2y$10$Ajamw0XBV/L8qHAbIVPmMecSpr/w/4eLrKYb0/s4tbP0AX5W/wpla', 'Ing. Hana Vejvodová', null, null, null, null, 'cs', null, null, null, null, '2015-01-21 16:33:59', '12', '2015-02-20 15:17:09', '1', null, null);
INSERT INTO `users` VALUES ('14', '1', 'broker', 'josef@belartis.cz', '$2y$10$i68AwAToVm1Jab7tj4wVbOQYMjbdckw7nbY9GRVSAppYEpeT36r8y', 'Ing. Josef Žára', null, null, null, null, 'cs', null, null, null, null, '2015-01-22 08:49:09', '9', '2015-02-20 15:17:06', '1', null, null);
INSERT INTO `users` VALUES ('15', '1', 'broker', 'lukas@belartis.cz', '$2y$10$JCPcwDIwdPN1nvPv2Z8NDOkEqXuKwqGW6iTuHnDDV5YKg4XRAR5VO', 'Ing. Lukáš Matocha', '2015-02-24 19:24:54', '46.135.10.15', null, null, 'cs', null, null, null, null, '2015-01-22 21:06:46', '9', '2015-04-04 08:58:13', '9', null, null);
INSERT INTO `users` VALUES ('16', '1', 'broker', 'david@belartis.cz', '$2y$10$RgB4bR4VwHDcrPfsaTmxAOpLmLvjDpO8JMQsqZJOV2xXBzXwcY9Fq', 'David Bojič', null, null, null, null, 'cs', null, null, null, null, '2015-01-28 17:29:02', '9', '2015-04-11 07:53:34', '1', '2015-04-11 07:53:34', '9');
INSERT INTO `users` VALUES ('17', '1', 'broker', 'ivo@belartis.cz', '$2y$10$CABCWdp/KDOwEokRlGLC6efNIon57PnQohOJYFi4mk62yNSPI4zHa', 'Ivo Gregov', null, null, null, null, 'cs', null, null, null, null, '2015-02-04 16:14:12', '9', '2015-02-20 15:16:51', '1', null, null);
INSERT INTO `users` VALUES ('18', '1', 'broker', 'ivana@belartis.cz', '$2y$10$QLLr4tIj3WPCbfTPZSeYOOL1g88zVroDZAXxUA9VurXqAkbcR3tMu', 'PhDr. Ivana Rezková', null, null, null, null, 'cs', null, null, null, null, '2015-02-13 10:17:06', '9', '2015-02-13 10:18:02', '9', '2015-02-13 10:18:02', null);
INSERT INTO `users` VALUES ('19', '2', 'broker', 'danielmarhan@gmail.com', '$2y$10$g38pY6CvyteuEK5jgeKfuOVwbV0ijahsF7iLbJZYeZjMP2bIgn7Pe', 'Demo Makléř', '2015-04-13 15:10:18', '185.36.160.19', null, null, 'cs', null, null, null, null, '2015-03-10 15:05:58', '8', '2015-04-28 10:55:38', '21', null, null);
INSERT INTO `users` VALUES ('20', '2', 'admin', 'demo.spravce@strankyrealitky.cz', '$2y$10$g38pY6CvyteuEK5jgeKfuOVwbV0ijahsF7iLbJZYeZjMP2bIgn7Pe', 'Demo Správce', '2015-04-14 16:46:46', '185.36.160.19', null, null, 'cs', null, null, null, null, '2015-03-10 15:15:38', '8', '2015-04-14 16:46:46', null, null, null);
INSERT INTO `users` VALUES ('21', '2', 'superAdmin', 'info@strankyrealitky.cz', '$2y$10$ckguAeonssnxdwJrdQ5Tz.VFNPOmNN0vPFJRp2gPapt1BKoNTTY4e', 'StránkyRealitky.Cz', '2015-05-13 16:22:39', '::1', null, null, 'cs', null, null, null, null, '2015-03-30 15:22:38', '8', '2015-05-13 16:22:39', null, null, null);
INSERT INTO `users` VALUES ('22', '2', 'broker', 'sallam@centrum.cz', '$2y$10$dnE7pbIafiKni0Zvq4ZK0.8y1DWPxUlZ7FJWVpmveb1E58jIDVdI.', 'Tomáš Makléř', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:15:57', '20', '2015-03-30 18:48:17', '20', null, null);
INSERT INTO `users` VALUES ('23', '2', 'broker', 'danielmarhan@gmail.com', '$2y$10$JXN8ImugvQj/hw7iUMSx6uR2vm1iqY2IAiQmPlFhLd6UYZg5ynxqm', 'test', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:20:14', '1', '2015-03-30 18:28:49', null, '2015-03-30 18:28:43', '1');
INSERT INTO `users` VALUES ('24', '2', 'broker', 'danielmarhan+test@gmail.com', '$2y$10$/c9ZgFT6mDwQcfKX/4HTJe8Iz1UXgzDhbZKPYXA.Nq7fDXoTFcPZi', 'test', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:20:40', '1', '2015-03-30 18:28:23', null, '2015-03-30 18:28:18', '1');
INSERT INTO `users` VALUES ('25', '2', 'broker', 'danielmarhan+test2@gmail.com', '$2y$10$URpGGZAFFraZlPp9iSLIzuKsanBuTVB8JPo4Lo2cK9ZGTzssMhv3y', 'test', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:21:45', '1', '2015-03-30 18:27:22', null, '2015-03-30 18:27:17', '1');
INSERT INTO `users` VALUES ('26', '2', 'broker', 'danielmarhan+test3@gmail.com', '$2y$10$L/yiRYRiqXYHBBhUVBAe3OxXBXgTnnLzG5ZYFxNEVWTo.yEFnpS26', 'test', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:26:18', '1', '2015-03-30 18:26:39', null, '2015-03-30 18:26:34', '1');
INSERT INTO `users` VALUES ('27', '2', 'broker', 'tomas.husa@terragroup.cz', '$2y$10$vBPZiSxGDOFf29tdDXT7kuYx6zZ3S7oYWruf3xyRDLFLPN1WfTyI6', 'Martina Makléřka', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:47:01', '20', '2015-03-30 18:48:32', '20', null, null);
INSERT INTO `users` VALUES ('28', '2', 'broker', 'manager@soulcox.com', '$2y$10$L5iOoJB4Fke2nEObyiFP3erjO14jf8FLRIFaQC3V73coKmMUgXVzi', 'Dalibor Makléřský', null, null, null, null, 'cs', null, null, null, null, '2015-03-30 18:50:12', '20', null, null, null, null);

-- ----------------------------
-- Table structure for `users_admin`
-- ----------------------------
DROP TABLE IF EXISTS `users_admin`;
CREATE TABLE `users_admin` (
  `user_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_admin
-- ----------------------------
INSERT INTO `users_admin` VALUES ('1', '0000-00-00 00:00:00', null, '2015-01-19 15:59:04', null, null, null);
INSERT INTO `users_admin` VALUES ('8', '2015-01-19 19:23:47', '1', '2015-01-23 20:58:22', null, null, null);
INSERT INTO `users_admin` VALUES ('9', '2015-01-19 19:27:31', '1', '2015-02-18 09:20:47', null, null, null);

-- ----------------------------
-- Table structure for `users_broker`
-- ----------------------------
DROP TABLE IF EXISTS `users_broker`;
CREATE TABLE `users_broker` (
  `user_id` int(11) NOT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `broker_phone` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_user_id` int(11) DEFAULT NULL,
  `changed` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `changed_user_id` int(11) DEFAULT NULL,
  `removed` timestamp NULL DEFAULT NULL,
  `removed_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users_broker
-- ----------------------------
INSERT INTO `users_broker` VALUES ('1', null, '123456789', '2015-01-19 16:32:17', null, null, null, null, null);
INSERT INTO `users_broker` VALUES ('2', null, '12345', '2015-01-19 16:32:17', null, '2015-01-12 17:04:41', '1', null, null);
INSERT INTO `users_broker` VALUES ('5', null, '1', '2015-01-12 19:56:48', '1', null, null, null, null);
INSERT INTO `users_broker` VALUES ('11', null, '+420 602 102 123', '2015-01-19 20:08:55', '8', '2015-02-20 15:17:12', '1', null, null);
INSERT INTO `users_broker` VALUES ('13', null, '+420 602 460 891', '2015-01-21 16:33:59', '12', '2015-02-20 15:17:09', '1', null, null);
INSERT INTO `users_broker` VALUES ('14', null, '+420 731 471 988', '2015-01-22 08:49:09', '9', '2015-02-20 15:17:06', '1', null, null);
INSERT INTO `users_broker` VALUES ('15', null, '+420 602 603 604', '2015-01-22 21:06:46', '9', '2015-04-04 08:58:13', '9', null, null);
INSERT INTO `users_broker` VALUES ('16', null, '+420 721 211 338', '2015-01-28 17:29:02', '9', '2015-02-20 15:16:55', '1', null, null);
INSERT INTO `users_broker` VALUES ('17', null, '+420 608 888 870', '2015-02-04 16:14:12', '9', '2015-02-20 15:16:51', '1', null, null);
INSERT INTO `users_broker` VALUES ('18', null, '736158291', '2015-02-13 10:17:06', '9', null, null, null, null);
INSERT INTO `users_broker` VALUES ('19', null, '+420 123 456 789', '2015-03-10 15:05:59', '8', '2015-04-28 10:55:38', '21', null, null);
INSERT INTO `users_broker` VALUES ('22', null, '+420 606 258 369', '2015-03-30 18:15:57', '20', '2015-03-30 18:48:17', '20', null, null);
INSERT INTO `users_broker` VALUES ('23', null, '123456789', '2015-03-30 18:20:15', '1', null, null, null, null);
INSERT INTO `users_broker` VALUES ('24', null, '123456789', '2015-03-30 18:20:40', '1', null, null, null, null);
INSERT INTO `users_broker` VALUES ('25', null, '123456789', '2015-03-30 18:21:46', '1', null, null, null, null);
INSERT INTO `users_broker` VALUES ('26', null, '123456789', '2015-03-30 18:26:18', '1', null, null, null, null);
INSERT INTO `users_broker` VALUES ('27', null, '+420 606 258 369', '2015-03-30 18:47:01', '20', '2015-03-30 18:48:32', '20', null, null);
INSERT INTO `users_broker` VALUES ('28', null, '+420 606 258 369', '2015-03-30 18:50:12', '20', null, null, null, null);
