
$(document).ready(function(){
	if($("select[name='advert_type']").length) updateAdvertSubtype.apply($("select[name='advert_type']"));
});

function updateAdvertSubtype(){
	var options = {
		1: [2,3,4,5,6,7,8,9,10,11,12,16,47],
		3: [18,19,20,21,22,23,24,46,48],
		4: [25,26,27,28,29,30,31,32,38,49],
		2: [33,35,37,39,40,43,44],
		5: [34,36,50,51,52,53]
	};

	$(this).each(function(){

		var $this = $(this);

		var optionElements = new Object();

		var $form = $this.parents("form");

		var $subTypeElement = $form.find("select[name='advert_subtype']");

		var $prompt = $subTypeElement.find("option[value='']");

		if(!(incomingValue = $subTypeElement.find("option:selected").attr("value"))) incomingValue = "";

		for(advert_type in options){
			if($form.find("select[name='advert_subtype'] option[value='_all']").length){
				options[advert_type].unshift("_all");
			}
			
			for(advert_subtype in options[advert_type]){
				var element = $form.find("select[name='advert_subtype'] option[value='"+options[advert_type][advert_subtype]+"']");
					
				if(typeof optionElements[advert_type] != "object") optionElements[advert_type] = [];

				optionElements[advert_type][advert_subtype] = element;
			}
		}

		$this.change(function(){
			var value = $(this).val();

			$subTypeElement.find("option").remove();
			
			$subTypeElement.append(optionElements[value]);

			if(value == "" && $prompt.length){
				$subTypeElement.prepend($prompt);
			}

			$subTypeElement.val(incomingValue);

			$form.find(".advert_subtype .checkboxlist label").each(function(){
				var $this = $(this);

				var val = $this.find("input").val();

				if(in_array(val, options[value]) || val == "_all"){
					$this.show();
				} else {
					$this.hide();
				}
			});

			incomingValue = "";
		});

		$(this).trigger("change");
	});
}