$(document).ready(function(){
	//Aktualizace mapy podle zadaní fieldů adresy nemovitosti
	$("#locality_address").each(initializeLocalityAddressMap);
	
	//Přepínaš skryté ceny nemovitosti
	$("input[name='advert_price_hidden']").each(updateAdvertPriceHidden);
});

$.nette.ext('brokerProfilesVideos', {
	success: function () {
		var snippet = $("#snippet--videosSnippet");

		snippet.find("input.youtube").youtubeCode();
	}
});

function updateAdvertPriceHidden(){
	
	var $this = $(this);

	var small = $("small.advert_price_hidden");

	$this.change(function(e){
		changeSmall.apply(this);
	});

	var changeSmall = function(){
		var text = small.data("label-" + ($(this).is(":checked") ? "1" : "0"));

		small.text(text);
	}

	changeSmall.apply($this);
}

function initializeLocalityAddressMap(){
	var $this = $(this);

	var $triggers = $("input[name='locality_street'], input[name='locality_cp'], input[name='locality_co'], input[name='locality_city']");

	$triggers.change(changeAddress);

	$triggers.keyup(function(){
		keyUpDelay(changeAddress, 1000);
	});

	var changeAddress = function(){
		var address = $("input[name='locality_street']").val() + " " + $("input[name='locality_cp']").val() + " " + $("input[name='locality_co']").val() + ", " + $("input[name='locality_city']").val(); 

		$this.find("input.address").val(address).change();
	};

	if(!($("input[name='locality[latitude]']").val().length && $("input[name='locality[longitude]']").val().length)){
		$("input[name='locality_street']").trigger("change");
	}
}