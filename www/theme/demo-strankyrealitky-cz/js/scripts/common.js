$(document).ready(function(){
	//Collapsible na výchozí třídě collapsible
	$(".collapsible").collapsible();

	$(".brokerprofiles .images.estates").each(function(){
		var container = $(this).find(".row").removeClass("row");

		container.find("> div").removeAttr("class");

		container.owlCarousel({
			autoPlay: 3000,
			items : 5, //10 items above 1000px browser width
			itemsDesktop : [1000,5], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
			itemsTablet: [600,3], //2 items between 600 and 0;
			itemsMobile : [480, 2], // itemsMobile disabled - inherit from itemsTablet option
		  	afterInit: function(elem){
				elem.find(".image img").thb({
					url: app.basePath + "/files/createthb"
				});
			}
		  });
	});

	$(".estates.detail .images").removeClass("row").find(" > div").attr("class", "item");

	$(".estates.detail .images").owlCarousel({
		autoPlay: 3000,
		items : 3, //10 items above 1000px browser width
//		itemsDesktop : [1000,5], //5 items between 1000px and 901px
//		itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
//		itemsTablet: [600,3], //2 items between 600 and 0;
		itemsMobile : [480, 2], // itemsMobile disabled - inherit from itemsTablet option
		afterInit: function(elem){
			elem.find(".image img").thb("enable").thb("checkThb");
		}
	});

	$("#homepage-more").click(function(e){
		e.preventDefault();

		$('html,body').animate({
          scrollTop: $("#lists").offset().top/* - $("#header").outerHeight()*/
        }, 1000);

        return false;
	});

	$("html, body").on("scroll", function(){
		console.log($(this).scrollTop());
		if($(this).scrollTop() > 300){
			$("#header").fadeOut();
		}
	});
});