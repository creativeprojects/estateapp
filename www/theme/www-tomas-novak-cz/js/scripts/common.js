$(document).ready(function(){
	$(".estates.detail .images").each(function(){
		$(this).owlCarousel({
			autoPlay: 3000,
			autoHeight : true,
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,3], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
			itemsTablet: [600,3], //2 items between 600 and 0;
			itemsMobile : [480,2], // itemsMobile disabled - inherit from itemsTablet option
			afterInit: function(elem){
				elem.find(".image img").thb({
					url: app.basePath + "/files/createthb"
				});
			}
		  });
	});

	$("body#homepage").scrollspy({target: "#mainmenu", offset: $("header").outerHeight() + 1});

	$("body").anchorScrollAnimation({
		offset: $("header").outerHeight()
	});

	$(".service-trigger").click(function(){
		if($(this).next(".collapsible").collapsible("expanded") == false){
			$(this).next(".collapsible").collapsible("expanded", true);
		}
	});

	//introAnimation();

});

function introAnimation()
{
	$("#intro").each(function(){
		var duration = parseInt($(this).css("transition-duration").replace("s","")) * 1000;

		introZoomIn(duration);
	});
}

function introZoomIn(duration){
	$("#intro").css("background-size", "110%");

	setTimeout(function(){
		introZoomOut(duration);
	}, duration);
}

function introZoomOut(duration){
	$("#intro").css("background-size", "100%");

	setTimeout(function(){
		introZoomIn(duration);
	}, duration / 10);
}