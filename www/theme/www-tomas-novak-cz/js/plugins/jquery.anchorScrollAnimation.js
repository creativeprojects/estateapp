(function($) {
	var defaults = {
		offset: 0
	},
	settings = {},
	methods = {
		init: function(options){
			settings = $.extend(true, defaults, options);

			return this.each(function(){
				settings.offset = settings.offset || $(this).data("anchor-scroll-offset");

				$(this).find('a[href*=#]:not([href=#])').click(function() {
					if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
						var target = $(this.hash);
							
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
							
						if (target.length) {
							var scrollTo = target.offset().top - settings.offset;

							console.log(scrollTo);

							$('html,body').animate({
								scrollTop: scrollTo
							}, 1000);
								
							return false;
						}
					}
				});
			});
		}
	};
	
	$.fn.anchorScrollAnimation = function(method){
		if(methods[method]){
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if (typeof method === 'object' || ! method){
			return methods.init.apply(this, arguments);
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.anchorScrollAnimation' );
		}    
	};
})(jQuery);