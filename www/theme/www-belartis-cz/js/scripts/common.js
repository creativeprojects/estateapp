$(document).ready(function(){
	//Zapnout nejl=pe až bude více makléřů
	$(".estates.brokers").each(function(){
		$(this).owlCarousel({
			autoPlay: 3000,
			items : 6, //10 items above 1000px browser width
			itemsDesktop : [1000,4], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
			itemsTablet: [600,2], //2 items between 600 and 0;
			itemsMobile : [480,1], // itemsMobile disabled - inherit from itemsTablet option
			afterInit: function(elem){
				elem.find(".image img").thb("enable").thb("checkThb");
			}
		  });
	});

	$(".estates.detail .images").removeClass("row").find(" > div").attr("class", "item");

	$(".estates.detail .images").owlCarousel({
		autoPlay: 3000,
		items : 3, //10 items above 1000px browser width
//		itemsDesktop : [1000,5], //5 items between 1000px and 901px
//		itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
//		itemsTablet: [600,3], //2 items between 600 and 0;
//		itemsMobile : [480, 2], // itemsMobile disabled - inherit from itemsTablet option
		  afterInit: function(elem){
			elem.find(".image img").thb("enable").thb("checkThb");
		}
	});
});