var anchorScrollOffset = 0;

$(document).ready(function(){
	$('body').each(function(){
		anchorScrollOffset = anchorScrollOffset || $(this).data("anchor-scroll-offset");

		$(this).find('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
					
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					
				if (target.length) {
					var scrollTo = target.offset().top - anchorScrollOffset;

					$('html,body').animate({
						scrollTop: scrollTo
					}, 1000);
						
					return false;
				}
			}
		});
	});
});