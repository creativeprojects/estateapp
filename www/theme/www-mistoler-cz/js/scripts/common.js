$(document).ready(function(){
	$(".estates.detail .images").each(function(){
		$(this).owlCarousel({
			autoPlay: 3000,
			autoHeight : true,
			items : 3, //10 items above 1000px browser width
			itemsDesktop : [1000,3], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
			itemsTablet: [600,3], //2 items between 600 and 0;
			itemsMobile : [480,2], // itemsMobile disabled - inherit from itemsTablet option
			afterInit: function(elem){
				elem.find(".image img").data("disable-autothb", 0).thb("checkThb");
			}
		  });
	});

	$(".videos").each(function(){
		var $this = $(this);

		var $modal = $("#video-modal").on('hidden.bs.modal', function () {
			$(this).find("iframe").attr("src", "");
		});

		$this.removeClass("row").find("> div").removeClass("col-sm-6").addClass("item");

		$this.find(".item").click(function(e){
			e.preventDefault();

			$modal.find(".modal-title, .modal-body h3").text($(this).find("h3").text());
			$modal.find(".modal-body p").html($(this).find("p").html());
			$modal.find("iframe").attr("src", "https://www.youtube.com/embed/"+$(this).data("video")+"?rel=0&vq=hd1080");

			$modal.modal('show');

			return false;
		});

		$(".owl-carousel .item:first").trigger("click");

		$(this).owlCarousel({
			autoPlay: 5000,
			//autoHeight : true,
			items : 2, //10 items above 1000px browser width
			itemsDesktop : [1000,2], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
			itemsTablet: [600,2], //2 items between 600 and 0;
			itemsMobile : [480,1], // itemsMobile disabled - inherit from itemsTablet option
			video:true,
			lazyLoad:true
		});
	});

	$("body#homepage").scrollspy({target: "#mainmenu"});

	anchorScrollOffset = $("#header").outerHeight();
});