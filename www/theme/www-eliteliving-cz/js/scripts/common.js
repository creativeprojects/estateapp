$(document).ready(function(){
	$(".estates.detail .images").each(function(){
		$(this).owlCarousel({
			autoPlay: 3000,
			autoHeight : true,
			items : 4, //10 items above 1000px browser width
			itemsDesktop : [1000,4], //5 items between 1000px and 901px
			itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
			itemsTablet: [600,2], //2 items between 600 and 0;
			itemsMobile: [480,1], // itemsMobile disabled - inherit from itemsTablet option
			afterInit: function(elem){
				elem.find(".image img").thb("enable").thb("checkThb");
			}
		  });
	});

	var intro = $("#intro");

	intro.removeAttr("data-ride").removeClass("carousel slide carousel-fade").find("> div").removeClass("carousel-inner").find(".carousel-indicators").remove();

	if($(window).width() > 900){
		
		var navigation = $('<nav class="roundabout-navigation" />').appendTo(intro);
		
		navigation
		.append('<a class="roundabout-prev" href="javascript:void(0);" title="Předchozí"><i class="fa fa-chevron-left"></i></a>')
		.append('<a class="roundabout-next" href="javascript:void(0);" title="Další"><i class="fa fa-chevron-right"></i></a>')

		var intro = $("#intro > div");

		intro.addClass("roundabout");

		intro.find(".item:not(.active) .estate-description").hide();

		$("#intro > div").addClass("roundabout").roundabout({
			//debug: true,
			responsive: true,
			minOpacity: 100,
			maxScale: 1,
			minScale: 0.3,
			tilt: 0,
			childSelector: ".item",
			autoplay: true,
			autoplayDuration: 5000,
			autoplayPauseOnHover: true,
			btnPrev: ".roundabout-prev",
			btnNext: ".roundabout-next"
		}).bind('animationStart', function(){
			$(this).find(".estate-description").fadeOut();
		}).bind('animationEnd', function(){
			var $this = $(this).find(".roundabout-in-focus .estate-description");

			$this.fadeIn();
		});
	} else {
		intro.addClass("carousel slide carousel-fade").attr("data-ride", "carousel").find("> div").addClass("carousel-inner");

		var ol = $('<ol class="carousel-indicators">').appendTo(intro);

		intro.find(".item").each(function(i){
			if(i == 0)
				$(this).addClass("active");

			ol.append('<li data-target="#intro" data-slide-to="'+i+'" '+(i == 0 ? 'class="active"' : '')+'></li>');
		});

		intro.carousel({
			interval: 5000,
		});

		intro.carousel('cycle');
	}
});