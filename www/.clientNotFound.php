<?php

header("HTTP/1.0 404 Not Found");

$locale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

?>

<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta name="generator" content="StránkyRealitky.Cz">
	<style>
		body { color: #333; background: white; width: 500px; margin: 100px auto;text-align: center; }
		img {display: block; margin: auto;width: 30vw;}
		h1 { font: 200 47px/1.5 sans-serif; margin: .6em 0; color: #2075BC; margin: 1.5em 0;}
		a, a:visited, a:focus {color: #2075BC;}
		a:hover {color: #1065AC;}
		h1 small {display: block; color: #777;font-size: 0.5em;}
		p { font: 21px/1.5 sans-serif; margin: 1.5em 0; }
	</style>

<?php

switch ($locale) {
	case "cs":
?>

	<title>Stránky na doméně neexistují</title>
</head>
<body>
	<img src="/theme/www-strankyrealitky-cz/img/strankyrealitky-logotype.svg">

	<h1>Omlouváme se <small>požadovaná doména neexistuje</small></h1>

	<p>Stránky na požadované doméně <strong><?php echo domain(); ?></strong> neexistují.</p>

	<p>
		<a href="http://www.strankyrealitky.cz" title="StránkyRealitky.Cz - služby pro moderní realitní kanceláře">www.strankyrealitky.cz</a>
	</p>

</body>

<?php

		break;
	default:

?>
	<title>Domain not found</title>
</head>
<body>
	<img src="/theme/www-strankyrealitky-cz/img/strankyrealitky-logotype.svg">

	<h1>We are sorry <small>domain not found</small></h1>

	<p>Website on domain <strong><?php echo domain(); ?></strong> not found.</p>

	<p>
		<a href="http://www.strankyrealitky.cz" title="StrankyRealitky.Cz - services for modern real estates">www.strankyrealitky.cz</a>
	</p>
</body>

<?php

}

exit;
