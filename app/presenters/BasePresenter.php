<?php

namespace App\Presenters;

abstract class BasePresenter extends \CP\Presenters\BasePresenter
{
	public $estatesParameters;
	
	public function startup()
	{
		parent::startup();

		$this->estatesParameters = $this->getContext()->parameters['estatesParameters'];
	}

	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->estatesParameters = $this->estatesParameters;
	}
}