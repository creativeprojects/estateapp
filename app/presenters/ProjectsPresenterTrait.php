<?php

namespace App\Presenters;

trait ProjectsPresenterTrait
{
	/** @var \App\Services\UsersService @inject */
	public $usersService;

	/** @var \App\Services\ProjectsService @inject */
	public $projectsService;

	/** @var \App\Services\Projects\ArticlesService @inject */
	public $articlesService;

	/** @var \App\Collection\Projects\ArticlesCollection */
	public $articlesCollection;

	/** @var \App\Collections\ProjectsCollection */
	public $projectsCollection;

	/** @var \App\Entities\ProjectEntity */
	public $projectEntity;

	/** @var \App\Entities\Projects\ArticleEntity */
	public $articleEntity;
}