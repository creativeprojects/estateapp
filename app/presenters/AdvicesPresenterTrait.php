<?php

namespace App\Presenters;

use App\FrontModule\Forms\Advices\QuestionForm;

trait AdvicesPresenterTrait
{
	/** @var \App\Services\Advices\ArticlesService @inject */
	public $advicesArticlesService;

	/** @var \App\Entities\Advices\ArticleEntity */
	public $advicesArticleEntity;

	/** @var \App\Services\Advices\QuestionsService @inject */
	public $advicesQuestionsService;

	/** @var \App\Entities\Advices\QuestionEntity */
	public $advicesQuestionEntity;

	/** @var \App\Collections\Advices\QuestionsCollection */
	public $advicesQuestionsCollection;

	/**
	 * Vytvořit komponentu formuláře otázky
	 * @paran string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentAdvicesQuestionForm($name)
	{
		$form = new QuestionForm($this, $name);

		$form->setEntity($this->advicesQuestionEntity);

		return $form->create();
	}
}