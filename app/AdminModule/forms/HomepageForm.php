<?php

namespace App\AdminModule\Forms;

class HomepageForm extends \CP\Forms\BaseForm
{
	public function create()
	{
		$this->form->addSlug("slug");

		$this->form->addSubmit("save")
			->onClick[] = [$this, "save"];

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->flashMessage("Úvodní stránka byla uložena", "alert-success");

			$this->redirect("Homepage:default");
		} else {
			$this->flashMessage("Při ukládání úvodní stránky došlo k chybě! Zkuste akci opakovat", "alert-danger");
		}
	}
}