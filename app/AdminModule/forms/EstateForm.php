<?php

namespace App\Forms;

use Nette\Forms\Controls\SubmitButton;

class EstateForm extends \CP\Forms\BaseForm
{
	//Generování controlů formuláře
	use EstateForm\FieldsTrait;

	//Obsluha souborů a obrázků
	use EstateForm\FilesTrait;

	//Validate adresy státním číselníkem
	use EstateForm\ValidateAddressTrait;

	public function create()
	{
		//Vygenerovat controly formuláře
		$this->addFields();

		//Pokud se nejedná o novou nemovitosti, uložit vstupní cenu pro porovnání při ukládání
		if ($this->entity->getId()) {
			$this->entity->advert_price_original = $this->entity->advert_price;
		}

		$this->form->addSubmit('save')
			->onClick[] = callback($this, "save");

		//Při odeslání validovat adresu státním číselníkem
		$this->form->onValidate[] = array($this, "validateAddress");

		$this->setDefaults();

		return $this->form;
	}

	public function save(SubmitButton $button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->flashMessage("Nemovitost byla uložena", "alert-success");

			$this->redirect("Estates:default");
		} else {
			$this->flashMessage("Při ukládání nemovitosti došlo k chybě!", "alert-danger");
		}
	}

	/** Utils */

	private function dumpComponents()
	{
		$latteComponents = array_keys((array)$this->form->components);

		$presenterComponents = array_keys($this->presenter->getContext()->parameters['estatesParameters']);

		$postComponents = array_keys($_POST);

		dump(array_diff($latteComponents, $presenterComponents));

		dump(array_diff($latteComponents, $postComponents));
	}
}