<?php

namespace App\AdminModule\Forms\Advices;

use CP\Forms\Form;

class QuestionForm extends \App\Forms\Advices\QuestionForm
{
	/**
	 * Odeslat otásku
	 * @param SubmitButton $button
	 * @return void
	 */
	
	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->presenter->flashMessage("Otázka byla uložena", "alert-success");

			$this->presenter->redirect("Advices:default");
		} else {
			$this->presenter->flashMessage("Otázku se nepodařilo uložit! Zkuste akci zopakovat", "alert-danger");
		}
	}
}