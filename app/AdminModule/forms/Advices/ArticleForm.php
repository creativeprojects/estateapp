<?php

namespace App\AdminModule\Forms\Advices;

class ArticleForm extends \CP\AdminModule\Forms\ArticleForm
{
	/**
	 * Resource pro oveření práv
	 * @var string
	 */

	protected $resource = "Admin:Advices";

	/**
	 * Výchozí adresář pro ukládání obrázků článku blogu
	 * @var string
	 */

	protected $defaultImagesDir = "files/advices/articles";

	/**
	 * Přesměrování po uložení článku
	 * @return void
	 */

	protected function onSave()
	{
		return $this->redirect("Advices:default");
	}
}