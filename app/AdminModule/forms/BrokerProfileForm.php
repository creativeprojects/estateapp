<?php

namespace App\AdminModule\Forms;

use CP\Forms\Form;

class BrokerProfileForm extends \CP\Forms\BaseForm
{
	use BrokerProfileForm\TestimonialsTrait;

	use BrokerProfileForm\VideosTrait;

	use \CP\Forms\SlugForm\SlugPart;

	protected $imagesDir;

	/**
	 * Nastavit adresář pro ukládání obrázků makléřů
	 * @param string $imagesDir
	 * @return this
	 */

	public function setImagesDir($imagesDir)
	{
		$this->imagesDir = $imagesDir;

		return $this;
	}

	/**
	 * získat cestu k adresáři pro ukládání obráků makléře
	 * @return string
	 */

	public function getImagesDir()
	{
		if (!$this->imagesDir) {
			$this->setImagesDir($this->presenter->thisDomainEntity->getPath() . "/files/broker-profiles");
		}

		return $this->imagesDir;
	}

	/**
	 * Vytvořit formulář
	 * @return CP\Forms\Form
	 */

	public function create()
	{
		$this->form->addHidden("id");

		$this->form->addHidden("profile_rank");

		$this->form->addHidden("domainId");

		$profile_title = $this->form->addContainer("profile_title");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$profile_title->addText($locale)
				->setRequired("Prosím zadejte název makléřského profilu");
		}

		$profile_position = $this->form->addContainer("profile_position");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$profile_position->addText($locale);
		}

		$profile_description = $this->form->addContainer("profile_description");

		foreach ($this->presenter->translator->getAvailableLocales() AS $locale) {
			$profile_description->addTextArea($locale);
		}

		$this->form->addGallery("profile_broker_images", null, $this->getImagesDir());

		$this->form->addGallery("profile_estates_images", null, $this->getImagesDir());

		$this->form->addSlug("profile_slug");

		$this->addTestimonials();

		$this->addVideos();

		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->flashMessage("Profil makléře byl uložen", "alert-success");

			if($this->presenter->thisDomainEntity->isComponentAllowed("Users", $this->presenter->user) && $this->presenter->user->isAllowed("Admin:Users", "default")){
				$this->redirect("Users:default");
			} else{
				$this->redirect("User:default");
			}
		} else {
			$this->flashMessage("Při ukládání profiu makléře došlo k chybě!", "alert-danger");
		}
	}
}