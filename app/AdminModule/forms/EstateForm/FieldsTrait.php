<?php

namespace App\Forms\EstateForm;

use CP\Forms\Form;

trait FieldsTrait
{
	protected function addFields()
	{
		$this->form->addHidden("estate_id");

		$this->form->addSlug("estate_slug");

		$this->addImages();

		$this->addExports();

		foreach ($this->presenter->estatesParameters as $name => $params) {
			switch ($params["type"]) {
				case "address": $this->addAddress($name, $params); break;
				case "hidden": $this->form->addHidden($name); break;
				case "codebook": $this->addCodebook($name, $params); break;
				case "double": $this->addFloat($name, $params); break;
				case "text": $this->addTextArea($name, $params); break;
				case "video": $this->form->addText($name); break;
				case "string": $this->addText($name, $params);break;
				case "int": $this->addInteger($name, $params); break;
				case "bool": $this->form->addCheckbox($name); break;
				case "multiselect": $this->form->addCheckboxList($name, NULL, $params["options"]); break;
				case "date":
				case "datetime":
				case "time": $this->addDateTime($name, $params); break;
				case "multilevelcodebook": $this->addMultilevelCodebook($name, $params); break;
				case "base64": $this->addAttachments($name, $params); break;
				case "advert_price_original": $this->form->addHidden($name); break;
				case 'broker': $this->addBroker($name, $params); break;
				case "project": $this->addProject($name, $params); break;
			}
		}
	}

	protected function sanitizeField($field, $params)
	{
		if(isset($params["required"])){
			$this->setRequired($field, $params);
		}

		if($field instanceof \Nette\Forms\Controls\BaseControl){
			$field->setOption("id", $field->getName());
		}

		return $field;
	}

	protected function setRequired($field, $params)
	{
		if (is_string($params["required"])) { //Required zadáno jako konretní text, použít pro alert
			$message = $params["required"];
		} else { //Required zadáno pouze jako TRUE či jako array pomíněných hodnot, vygenerovat alert
			$message = "Prosím vyplňte pole " . $params["title"];
		} 

		//Pokud se jedná o lokalizovaný field, přidat Required pro všechny jazyky
		if(isset($params["locale"])){
			foreach($this->presenter->translator->getAvailableLocales() AS $locale){
				$this->addRequired($field[$locale], $params, $message);
			}
		} else {
			$this->addRequired($field, $params, $message);
		}
	}

	protected function addRequired($field, $params, $message)
	{
		if(is_array($params["required"])){
			foreach ($params["required"] as $key => $value) {
				$field->addConditionOn($this->form[$key], Form::EQUAL, $value)
					->setRequired($message);
			}
		} else {
			$field->setRequired($message);
		}
	}

	protected function addAddress($name, $params)
	{
		$field = $this->form->addAddress($name);

		return $this->sanitizeField($field, $params);
	}

	protected function addBroker($name, $params)
	{
		if($this->presenter->user->isAllowed("Admin:Estates", "setUser")){
			$options = $this->presenter->usersService->getBrokerOptions();

			if(!in_array($this->values[$name], array_keys($options))){
				$this->values[$name] = null;
			}

			$field = $this->form->addSelect($name, null, $this->presenter->usersService->getBrokerOptions())
				->setPrompt("Zvolte makléře ...")
				->setRequired("Nemovitost je třeba přiřadit jednomu z makléřů");
		} else {
			$field = $this->form->addHidden($name);
		}

		return $this->sanitizeField($field, $params);
	}

	protected function addCodebook($name, $params)
	{
		$field = $this->form->addSelect($name, NULL, $params["options"]);

		if (isset($params["toggle"])) {
			foreach ($params['toggle'] as $key => $values) {
				$condition = $field->addCondition(Form::EQUAL, $key);

				foreach ($values as $child) {
					$condition = $condition->toggle($child);
				}

				$condition = $condition->endCondition();
			}
		}

		return $this->sanitizeField($field, $params);
	}

	protected function addDateTime($name, $params)
	{
		$field = $this->form->addText($name)
			->setAttribute("class", $params["type"]);

		return $this->sanitizeField($field, $params);
	}

	protected function addFloat($name, $params)
	{
		$field = $this->form->addText($name);

		$field->addCondition(Form::FILLED)
			->addRule(Form::FLOAT, "Hodnota musí být číslo");

		return $this->sanitizeField($field, $params);
	}

	protected function addInteger($name, $params)
	{
		$field = $this->form->addText($name);

		$field->addCondition(Form::FILLED)
			->addRule(Form::INTEGER, "Hodnota musí být celé číslo");

		return $this->sanitizeField($field, $params);
	}

	protected function addMultilevelCodebook($name, $params)
	{
		$field = $this->form->addSelect($name, NULL, $this->presenter->estatesService->getAdvertSubtypeOptions())
			->setPrompt("Prosím vyberte ...");

		return $this->sanitizeField($field, $params);
	}

	/**
	 * Vytvořit selectbox s výběrem developerského projektu
	 * @param string $name
	 * @param mixed $params
	 * @return ...
	 */

	protected function addProject($name, $params)
	{
		if ($this->presenter->thisDomainEntity->isComponentAllowed("Projects", $this->presenter->user)) {
			$options = $this->presenter->projectsService->projectsCollection()->whereDomain()->find()->getOptions();

			$field = $this->form->addSelect($name, null, $options)
				->setPrompt("Nepřiřazeno k žádnému developerskému projektu");

			return $this->sanitizeField($field, $params);
		}
	}

	protected function addTextArea($name, $params)
	{
		if (isset($params["locale"])) {
			$field = $container = $this->form->addContainer($name);

			foreach ($this->presenter->translator->getavailablelocales() as $locale) {
				$container->addTextArea($locale);
			}
		} else {
			$field = $this->form->addTextArea($name);
		}

		$field = $this->sanitizeField($field, $params);

		return $field;
	}

	protected function addText($name, $params)
	{
		if (isset($params["locale"])) {
			$field = $container = $this->form->addContainer($name);

			foreach ($this->presenter->translator->getavailablelocales() as $locale) {
				$container->addText($locale);
			}
		} else {
			$field = $this->form->addText($name);
		}

		$field = $this->sanitizeField($field, $params);

		return $field;
	}

	protected function addExports()
	{
		$portals = $this->form->addContainer("portals");

		foreach ($this->presenter->template->portalsCollection->getAll() AS $portalEntity) {
			if (is_array($this->presenter->thisDomainEntity->getExportSettings()) && array_key_exists($portalEntity->portal_key, $this->presenter->thisDomainEntity->getExportSettings()) && $this->presenter->thisDomainEntity->getExportSettings()[$portalEntity->portal_key] == 1) {
				$container = $portals->addContainer($portalEntity->portal_id);
				
				$container->addHidden("export_id");
				
				$container->addCheckbox("export");
			}
		}
	}
}