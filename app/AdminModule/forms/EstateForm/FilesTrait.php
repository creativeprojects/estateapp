<?php

namespace App\Forms\EstateForm;

use Nette\Utils\Image;
use CP\Utils\Files;

trait FilesTrait
{
	/**
	 * Adresář pro obrázky nemovitosti
	 * @var string
	 */

	protected $imagesDir;

	/**
	 * Adresář pro verze obrázků pro export na realitní servery
	 * @var string
	 */

	protected $imagesExportDir;

	/**
	 * Adresář pro přílohy nemovitostí
	 * @var string
	 */

	protected $attachmentsDir;

	/**
	 * Nastavit adresář pro obrázky nemovitosti
	 * @param string $dir
	 * @return this
	 */

	public function setImagesDir($dir)
	{
		$this->imagesDir = $dir;

		Files::validateDir($this->imagesDir);

		return $dir;
	}

	/**
	 * Získat cestu k adresáři s obrázky nemovitosti
	 * Pokud nebyl dříve nastaven, nastavit výchozí
	 * @return string
	 */

	public function getImagesDir()
	{
		if (!$this->imagesDir) {
			$this->setImagesDir($this->presenter->thisDomainEntity->getPath() . "/files/estates/images");
		}

		return $this->imagesDir;
	}

	/**
	 * Nastavit adresář pro verze obrázků pro export
	 * @param string $dir
	 * @return this
	 */

	public function setImagesExportDir($dir)
	{
		$this->imagesExportDir = $dir;

		Files::validateDir($this->imagesExportDir);

		return $this;
	}

	/**
	 * Získat cestu k adresáři s verzemi obrázků pro export
	 * Pokud nebyl dříve nastaven, nastavit výchozí
	 * @return string
	 */

	public function getImagesExportDir()
	{
		if (!$this->imagesExportDir) {
			$this->setImagesExportDir($this->presenter->thisDomainEntity->getPath() . "/files/estates/for-export");
		}

		return $this->imagesExportDir;
	}

	/**
	 * Nastavit adresář pro přílohy nemovitosti
	 * @param string $dir
	 * @return this
	 */

	public function setAttachmentsDir($dir)
	{
		$this->attachmentsDir = $dir;

		Files::validateDir($this->attachmentsDir);

		return $this;
	}

	/**
	 * Získat cestu k adresáři s přílohami nemovitosti
	 * Pokud nebyl dříve nastaven, nastavit výchozí
	 * @return string
	 */

	public function getAttachmentsDir()
	{
		if (!$this->attachmentsDir) {
			$this->setAttachmentsDir($this->presenter->thisDomainEntity->getPath() . "/files/estates/private");
		}

		return $this->attachmentsDir;
	}

	/**
	 * Přidat správu obrázků nemovitosti
	 * @return this
	 */

	protected function addImages()
	{
		$this->form->addGallery("estate_images", null, $this->getImagesDir())
			->addCallback(callback($this, "resizeImagesForExport"))
			->addWatermark(TRUE);

		return $this;
	}

	/**
	 * Přidat správu příloh nemovitosti
	 * @param string $name jméno pole příloh (tuto metodu požíváme vícekrát pro různá pole příloh)
	 * @param array $params vlastnosti daného pole příloh
	 * @return CP\Forms\Controls\AttachmentsControl $field
	 */

	protected function addAttachments($name, $params)
	{
		$field = $this->form->addAttachments($name, null, $this->getAttachmentsDir());

		return $field;
	}

	/**
	 * Upravit velikost obrázků pro export
	 * @param array $image
	 * TODO - sem posílat ImageEntity
	 * @return bool
	 */

	public function resizeImagesForExport($image)
	{
		$filePath = Files::getAbsolutePath($image["src"]);
		
		$pathInfo = pathinfo($filePath);

		$exportFilePath = $this->getImagesExportDir() . "/" . $pathInfo["basename"];
		
		if (!is_file($exportFilePath)) { //Pokud ještě verze obrázku pro export neexistuje, vytvořit
			$image = Image::fromFile($filePath);
			
			$image->resize(800, 800, Image::FIT);
			
			return $image->save($exportFilePath, 70, Image::JPEG);
		}

		return true;
	}
}