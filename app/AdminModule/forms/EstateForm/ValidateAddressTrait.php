<?php

//TODO - jak řešit pozemky, které nemají ČP?

namespace App\Forms\EstateForm;

trait ValidateAddressTrait
{
	public function validateAddress($form)
	{
		if ($form->submitted->getValidationScope() === NULL) {
			$values = $form->getValues();

			$arr = explode(" ", $values["locality_city"], 2);
			if (mb_strtolower($arr [0]) == "praha") {
				$city = $arr[0];
			} else {
				$city = $values["locality_city"];
			}

			$question = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
				<uir:uir_dotazy xmlns:xsi=\"http://www.w3.org/2000/10/XMLSchema-instance\" xmlns:uir=\"http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2\" xsi:schemaLocation=\"http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2 http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2 /uir_request.xsd\"
					dotaz_datum_cas=\"2003-06-16T12:13:01.000\"
					dotaz_pocet=\"1\"
					dotaz_typ=\"Standard\"
					vystup_format=\"XML\">
					<dotaz>
						<pomocne_id>1</pomocne_id>
						<vypis_ciselnik>obce</vypis_ciselnik>
						<lokalita>
							<psc>" . str_replace(' ', '', $values["locality_zip"]) . "</psc>
						</lokalita>
						<trideni>psc</trideni>

						<max_pocet>100</max_pocet>
					</dotaz>
				</uir:uir_dotazy>
				";

			$pscAnswer = $this->curl($question);

			if ((int)$pscAnswer->odpoved->pocet_zaznamu == 0) {
				$form->addError('Zadané PSČ neexistuje');
			}

			//check city

			$question = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
				<uir:uir_dotazy xmlns:xsi=\"http://www.w3.org/2000/10/XMLSchema-instance\" xmlns:uir=\"http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2\" xsi:schemaLocation=\"http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2 http://forms.mpsv.cz/uirtest/adr/xml/uir_request/v_1_0_2 /uir_request.xsd\"
					dotaz_datum_cas=\"2003-06-16T12:13:01.000\"
					dotaz_pocet=\"1\"
					dotaz_typ=\"Standard\"
					vystup_format=\"XML\">
			<dotaz>
						<pomocne_id>0</pomocne_id>
						<vypis_ciselnik>obce</vypis_ciselnik>
						<lokalita>
							<psc>" . str_replace(' ', '', $values["locality_zip"]) . "</psc>
						</lokalita>
						<trideni>psc</trideni>
						<omezeni_vypisu>
							<vypis_uzel>kod_obce</vypis_uzel>
						</omezeni_vypisu>
						<max_pocet>100</max_pocet>
					</dotaz>
					<dotaz>
						<pomocne_id>2</pomocne_id>
						<vypis_ciselnik>obce</vypis_ciselnik>
						<hledani>
							<hledani_typ>rovna_se</hledani_typ>
							<hledany_text>{$city}</hledany_text>
						</hledani>
						<max_pocet>100</max_pocet>
					</dotaz>
					<dotaz>
						<pomocne_id>3</pomocne_id>
						<vypis_ciselnik>casti_obce</vypis_ciselnik>
						<hledani>
							<hledani_typ>rovna_se</hledani_typ>
							<hledany_text>{$values["locality_district"]}</hledany_text>
						</hledani>
						<max_pocet>100</max_pocet>
					</dotaz>
					<dotaz>
						<pomocne_id>1</pomocne_id>
						<vypis_ciselnik>casti_obce</vypis_ciselnik>
						<hledani>
							<hledani_typ>rovna_se</hledani_typ>
							<hledany_text>{$values["locality_district"]}</hledany_text>
						</hledani>
						<max_pocet>100</max_pocet>
					</dotaz>
				</uir:uir_dotazy>
				";

			$cityAnswer = $this->curl($question);
			$psc = $cityAnswer->odpoved[0];
			foreach ($cityAnswer->odpoved as $answer) {
				if ($answer->pomocne_id > 0) {
					if ((int)$answer->pocet_zaznamu > 0) {
						foreach ($answer->zaznam as $zaznam) {
							foreach ($psc->zaznam as $test) {
								if ((string)$test->kod_obce == (string)$zaznam->kod_obce) {
									$finalAnswer = $zaznam;
								}
							}
						}
					}
				}
			}

			if (!isset($finalAnswer)) {
				$form->addError('Upřesněte adresu');
			} else {
				$form["locality_state_uir"]->setValue((int)$finalAnswer->kod_kraje);
				if ((int)$finalAnswer->kod_kraje == 19) {
					$form["locality_county_uir"]->setValue((int)$finalAnswer->kod_oblasti);
				} else {
					$form["locality_county_uir"]->setValue((int)$finalAnswer->kod_okresu);
				}
				$form["locality_city_uir"]->setValue((int)$finalAnswer->kod_obce);
				$form["locality_county"]->setValue((string)$finalAnswer->nazev_okresu);
				$form["locality_state"]->setValue((string)$finalAnswer->nazev_kraje);
			}
		}
	}

	public function curl($question)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://forms.mpsv.cz/uir/adr/over');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(""));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $question);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// na Linuxu nastavit verzi 3, na Windows ne !
		if (stristr(PHP_OS, 'WIN') === false) {
			curl_setopt($ch, CURLOPT_SSLVERSION, 3);
		}
		$http_answer = curl_exec($ch);

		$curl_error = curl_error($ch);

		curl_close($ch);
		return simplexml_load_string($http_answer
		);
	}
}