<?php

namespace App\AdminModule\Forms;

class DemandForm extends \App\Forms\DemandForm
{
	/** @var string přesměrování po uložení poptávky */
	protected $redirect = "Demands:default";

	/**
	 * Vytvoření nastavení makléře, kterému poptávka patří
	 * @return void
	 */

	protected function addUser()
	{
		if ($this->presenter->user->isAllowed("Admin:Demands", "setUser")) {
			$options = $this->presenter->usersService->getBrokerOptions();

			$this->form->addHidden("oldUserId")
				->setValue($this->entity->getId());

			$this->form->addSelect("userId", null, $options)
				->setPrompt("Vyberte makléře, kterému chcete přiřadit poptávku ...");
		} else {
			$this->form->addHidden("userId")
				->setValue($this->presenter->user->getId());
		}
	}

	/**
	 * Uložit poptávku
	 * @param SubmitButton $button
	 * @return void
	 */

	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->flashMessage("Poptávka byla uložena", "alert-success");

			$this->redirect($this->redirect);
		} else {
			$this->flashMessage("Při ukládání poptávky došlo k chybě! Zkuste akci opakovat", "alert-danger");
		}
	}
}