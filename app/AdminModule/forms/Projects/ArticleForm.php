<?php

namespace App\AdminModule\Forms\Projects;

class ArticleForm extends \CP\AdminModule\Forms\ArticleForm
{
	/**
	 * Resource pro ověření práv
	 * @var string
	 */
	 
	protected $resource = "Admin:Projects:Articles";

	/**
	 * Přesměrovat po uložení
	 * @return void
	 */

	public function onSave()
	{
		$this->redirect("Projects:articles", $this->entity->ownerId);
	}
}