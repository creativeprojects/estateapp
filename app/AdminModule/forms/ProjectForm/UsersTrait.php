<?php

namespace App\AdminModule\Forms\ProjectForm;

use CP\Forms\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

trait UsersTrait
{
	/**
	 * Vytvořit správu uživatelů
	 * @return this
	 */

	private function addUsers()
	{
		$parent = $this;

		$container = $this->form->addDynamic("usersCollection", function (Container $container) use ($parent) {
			$container->addHidden("id");

			$container->addHidden("userId");

			$container->addSubmit("remove")
				->setValidationScope(false)
				->onClick[] = callback($parent, "removeUser");
		});

		$container->addText("userId");

		$container->addSubmit("add")
			->setValidationScope(false)
			->onClick[] = callback($this, "addUser");
	}

	/**
	 * Přidat uživatele
	 * @param SubmitButton $button
	 * @return void
	 */

	public function addUser($button)
	{
		$values = $button->parent->getValues();

		$userEntity = new UserEntity;

		if ($userEntity->find($values->userId)) {
			$values = [
				"id" => null,
				"userId" => $userEntity->getId(),
			];

			$button->parent->createOne()
				->setValues($values);

			$this->flashMessage("Makléř byl přidán", "alert-success");
		} else {
			$this->flashMessage("Makléře se nepodařilo přidat!", "alert-danger");
		}

		$this->invalidateControl();
	}

	/**
	 * Odebrat uživatele
	 * @param SubmiutButton $button
	 * @return void
	 */

	public function removeUser($button)
	{
		$users = $button->parent->parent;

		$users->remove($button->parent, TRUE);

		$this->flashMessage("Makléř byl odebrán", "alert-success");

		$this->invalidateControl();
	}
}