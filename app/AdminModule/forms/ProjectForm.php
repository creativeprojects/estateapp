<?php

namespace App\AdminModule\Forms;

class ProjectForm extends \CP\Forms\BaseForm
{
	/** Makléři projektu */
	use ProjectForm\UsersTrait;

	/**
	 * Adresář pro obrázky projektu
	 * @var string
	 */

	protected $imagesDir;

	/**
	 * Nastavit adersář pro obrázky
	 * @param string $dir
	 * @return this
	 */

	public function setImagesDir($dir)
	{
		$this->imagesDir = $dir;

		return $this;
	}

	/**
	 * Získat adresář pro obrázky
	 * Pokud nebyl dříve zadán, vytvořit výchozí
	 * @return string
	 */

	public function getImagesDir()
	{
		if (empty($this->imagesDir)) {
			$this->setImagesDir($this->presenter->thisDomainEntity->getPath() . "/files/projects");
		}

		return $this->imagesDir;
	}

	/**
	 * Vytvořit formulář projektu
	 * @return CP\Forms\Form
	 */

	public function create()
	{
		$this->form->addHidden("id");

		$title = $this->form->addContainer("title");

		foreach($this->presenter->translator->getAvailableLocales() AS $locale){
			$title->addText($locale)
				->setRequired("Prosím zadejte název projektu");
		}

		$description = $this->form->addContainer("description");

		foreach($this->presenter->translator->getAvailableLocales() AS $locale){
			$description->addTextArea($locale);
		}

		$this->form->addAddress("address");
		
		$this->form->addSelect("status", null, $this->entity->getStatusOptions());

		$this->form->addText("established");

		$this->form->addText("finished");

		$this->addUsers();

		$this->form->addText("youtubeCode");

		$this->form->addGallery("imagesCollection", null, $this->getImagesDir())
			->addWatermark(TRUE);

		$this->form->addSlug("slugEntity");

		$this->form->addSubmit("save")
			->onClick[] = [$this, "save"];

		$this->setDefaults();

		return $this->form;
	}

	/**
	 * Uložit projekt
	 * @param SubmitButton $button
	 * @return void
	 */
	
	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->flashMessage("Projekt byl uložen", "alert-success");

			$this->redirect("Projects:default");
		} else {
			$this->flashMessage("Při ukládání projektu došlo k chybě!", "alert-danger");
		}
	}
}