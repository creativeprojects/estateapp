<?php

namespace App\AdminModule\Forms\BrokerProfileForm;

use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

trait TestimonialsTrait
{
	protected function addTestimonials()
	{
		$parent = $this;

		$container = $this->form->addDynamic("profile_testimonials", function (Container $container) use ($parent) {
			$container->addHidden("testimonial_id");

			$container->addHidden("testimonial_rank");

			$testimonial_text = $container->addContainer("testimonial_text");

			foreach($this->presenter->translator->getAvailableLocales() AS $locale){
				$testimonial_text->addTextArea($locale)
					->setRequired("Zadejte prosím text reference");
			}

			$container->addText("testimonial_author")
				->setRequired("Zadejte prosím autora reference");

			$container->addSubmit("remove")
				->setValidationScope(false)
				->onClick[] = callback($parent, "removeTestimonial");
		});

		$container->addSubmit('add')
			->setValidationScope(false)
			->onClick[] = callback($this, "addTestimonial");
	}

	public function addTestimonial(SubmitButton $button)
	{
		$values = $button->parent->getValues();

		$button->parent->createOne()
			->setValues($values);

		$this->invalidateControl();
	}

	public function removeTestimonial(SubmitButton $button)
	{
		$users = $button->parent->parent;

		$users->remove($button->parent, TRUE);

		$this->flashMessage("Reference byla odebrána", "alert-success");

		$this->invalidateControl();
	}
}