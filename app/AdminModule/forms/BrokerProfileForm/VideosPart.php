<?php

namespace App\AdminModule\Forms\BrokerProfileForm;

use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;

trait VideosTrait
{
	private function addVideos()
	{
		$parent = $this;

		$container = $this->form->addDynamic("profile_videos", function (Container $container) use ($parent) {
			$container->addHidden("video_id");

			$container->addHidden("video_rank");

			$video_title = $container->addContainer("video_title");

			foreach($this->presenter->translator->getAvailableLocales() AS $locale){
				$video_title->addText($locale)
					->setRequired("Zadejte prosím titulek videa");
			}

			$video_description = $container->addContainer("video_description");

			foreach($this->presenter->translator->getAvailableLocales() AS $locale){
				$video_description->addTextArea($locale);
			}

			$container->addText("video_address");

			$container->addText("video_code");

			$container->addSubmit("remove")
				->setValidationScope(false)
				->onClick[] = callback($parent, "removeVideo");
		});

		$container->addSubmit('add')
			->setValidationScope(false)
			->onClick[] = callback($this, "addVideo");
	}

	public function addVideo(SubmitButton $button)
	{
		$videoEntity = $this->presenter->videosService->videoEntity()->blank();

		$button->parent->createOne();
			->setValues($videoEntity->getValues(true));

		$this->flashMessage("Nové video bylo přidáno", "alert-success");

		$this->invalidateControl();
	}

	public function removeVideo(SubmitButton $button)
	{
		$users = $button->parent->parent;

		$users->remove($button->parent, TRUE);

		$this->flashMessage("Video bylo odebráno", "alert-success");

		$this->invalidateControl();
	}
}