<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\HomepageForm;
use Nette\Application\BadRequestException;

class HomepagePresenter extends SecuredPresenter
{
	/** @var \App\Entities\HomepageEntity */
	public $homepageEntity;

	/** @var \App\Services\HomepageService @inject */
	public $homepageService;

	public function renderDefault()
	{
		if($this->homepageEntity = $this->homepageService->getEntity()){
			$this->template->homepageEntity = $this->homepageEntity;
		} else {
			throw new BadRequestException;
		}
	}

	public function actionEdit()
	{
		if($this->homepageEntity = $this->homepageService->getEntity()){
			if($this->homepageEntity->isEditAllowed($this->user)){
				$this->template->homepageEntity = $this->homepageEntity;
			} else {
				$this->notAllowed();
			}
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentHomepageForm($name)
	{
		$form = new HomepageForm($this, $name);

		$form->setEntity($this->homepageEntity);
		
		return $form->create();
	}
}