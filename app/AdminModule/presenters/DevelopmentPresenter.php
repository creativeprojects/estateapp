<?php

namespace App\AdminModule\Presenters;

class DevelopmentPresenter extends \App\AdminModule\Presenters\SecuredPresenter
{
	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	public function actionRemoveEstates($offset = 0)
	{
		$limit = 130;

		$estatesCollection = $this->estatesService->estatesCollection();

		$estatesCollection->query
			->where("advert_status = 3")
			->limit($limit)
			->offset($offset);

		$before = memory_get_usage();
		$bef = time();

		$estatesCollection->find();

		$after = memory_get_usage();
		$aft = time();

		$usage = $after - $before;

		if ($usage > 0) {
			dump("Celková zátěž: " . formatBytes(memory_get_usage()));
			dump("Všechny nemovitosti: " . formatBytes($usage));
			dump("Jedna nemovitost: " . formatBytes($usage / $limit));
			dump("Čas: " . ($aft - $bef));
		}

		dump($estatesCollection->remove());

		dump($this->estatesService->database->query("SELECT COUNT(*) FROM estates WHERE domainId = 5 AND removed IS NULL AND advert_status = 3")->fetch());
		
		die;
	}
}