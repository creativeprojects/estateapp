<?php

namespace App\AdminModule\Presenters;

class MenusPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\MenusPresenterTrait;
}