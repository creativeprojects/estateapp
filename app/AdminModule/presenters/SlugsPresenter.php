<?php

namespace App\AdminModule\Presenters;

class SlugsPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\SlugsPresenterTrait;
}