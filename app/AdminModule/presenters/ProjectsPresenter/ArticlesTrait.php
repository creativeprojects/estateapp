<?php

namespace App\AdminModule\Presenter\ProjectsPresenter;

use App\AdminModule\Forms\Projects\ArticleForm;

trait ArticlesTrait
{
	/**
	 * Správa článků projektu
	 * @param integer $id ID projektu, pro který budeme články zobrazovat
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionArticles($id)
	{
		if (is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			$this->articlesCollection = $this->articlesService->articlesCollection()
				->whereProject($this->projectEntity)
				->attach($this)
				->itemsPerPage(20);
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit správu článků projektu
	 * @return void
	 */

	public function renderArticles()
	{
		$this->template->articlesCollection = $this->articlesCollection->find();
		$this->template->projectEntity = $this->projectEntity;
	}

	/**
	 * Přidat článek
	 * @param integer $id ID projektu, do kterého budeme článek přidávat
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleAddArticle($id)
	{
		if (is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			$articleEntity = $this->articlesService->articleEntity();

			$articleEntity->setOwner($this->projectEntity);
			
			if ($articleEntity->save()) {
				$this->redirect("Projects:editArticle", $this->articleEntity->getId());
			} else {
				$this->flashMessage("Nový článek se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");

				$this->restoreRequest($this->backlink);
				$this->redirect("this");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Upravit článek
	 * @param integer $id ID článku, který chceme upravit
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionEditArticle($id)
	{
		if(is_numeric($id) && ($this->articleEntity = $this->articlesService->articleEntity()->find($id))) {
		 	if(!($this->projectEntity = $this->projectsService->projectEntity()->find($this->articleEntity->ownerId))) {
		 		$this->flashMessage("Developerský projekt pro tento článek již neexistuje!", "alert-danger");

		 		//$this->articleEntity->remove();

		 		$this->handleRedirect("this");
		 	}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit editaci článku
	 * @return void
	 */

	public function renderEditArticle()
	{
		$this->template->projectEntity = $this->projectEntity;
		
		$this->template->articleEntity = $this->articleEntity;
	}

	/**
	 * Získat komponentu formuláře článku
	 * @param string $name jméno formuláře v šabloně
	 * @return CP\Forms\Form
	 */

	protected function createComponentArticleForm($name)
	{
		$form = new ArticleForm($this, $name);

		$form->setEntity($this->articleEntity);

		return $form->create();
	}

	/**
	 * Odebrat článek
	 * @param integer $id ID článku, který chceme odebrat
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleRemoveArticle($id)
	{
		if ($this->articleEntity = $this->articlesService->articleEntity()->find($id)) {
			if ($this->articleEntity->remove()) {
				$this->flashMessage("Článek byl odebrán", "alert-success");
			} else {
				$this->flashMessage("Při odebírání článku došlo k chybě! Zkuste akci opakovat", "alert-danger");
			}

			$this->handleRedirect("this");
		} else {
			throw new BadRequestException;
		}
	}	
}