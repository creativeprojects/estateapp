<?php

namespace App\AdminModule\Presenters;

class AddressBookPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\AddressBookPresenterTrait;
}