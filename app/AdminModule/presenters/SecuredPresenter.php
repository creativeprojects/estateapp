<?php

namespace App\AdminModule\Presenters;

abstract class SecuredPresenter extends BasePresenter
{
	public function startup()
	{
		parent::startup();

		$this->isUserAllowed();
	}
}