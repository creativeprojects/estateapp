<?php

namespace App\AdminModule\Presenters\AdvicesPresenter;

use App\AdminModule\Forms\Advices\QuestionForm;
use Nette\Application\BadRequestException;

trait QuestionsTrait
{
	/**
	 * Přehled všech článků blogu
	 * @return void
	 */

	public function renderDefault()
	{
		$this->template->advicesQuestionsCollection = $this->advicesQuestionsService
			->questionsCollection()
			->newestFirst()
			->attach($this)
			->itemsPerPage(20)
			->find();
	}

	/**
	 * Přidat novou otázku
	 * @return void
	 */

	public function actionAddQuestion()
	{
		$this->advicesQuestionEntity = $this->advicesQuestionsService->questionEntity()->blank();
	}

	/**
	 * Vykreslit přidání nové otázky
	 * @return void
	 */

	public function renderAddQuestion()
	{
		$this->template->questionEntity = $this->advicesQuestionEntity;
	}

	/**
	 * Upravit existující otázku
	 * @param integer $id ID otázky
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionEditQuestion($id)
	{
		if (is_numeric($id) && ($this->advicesQuestionEntity = $this->advicesQuestionsService->questionEntity()->find($id))) {
			$this->template->questionEntity = $this->advicesQuestionEntity;
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Odebrat otázku
	 * @param integer $id ID otázky
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleRemoveQuestion($id)
	{
		if (is_numeric($id) && $this->advicesQuestionEntity = $this->advicesQuestionsService->questionEntity()->find($id)) {
			if($this->advicesQuestionEntity->remove()){
				$this->flashMessage("Otázka byla odebrána", "alert-success");
			} else {
				$this->flashMessage("Otázku se naporařilo odebrat! Zkuste akci zopakovat", "alert-danger");
			}

			$this->handleRedirect("this");
		} else {
			throw new BadRequestException;
		}
	}
}