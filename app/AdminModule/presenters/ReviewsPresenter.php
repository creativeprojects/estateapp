<?php

namespace App\AdminModule\Presenters;

class ReviewsPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\ReviewsPresenterTrait;
}