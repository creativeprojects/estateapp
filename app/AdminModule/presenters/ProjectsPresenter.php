<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\ProjectForm;
use Nette\Application\BadRequestException;

class ProjectsPresenter extends SecuredPresenter
{
	/** Základní vlastnosti */
	use \App\Presenters\ProjectsPresenterTrait;

	/**
	 * Přehled projektů
	 * @return void
	 */
	
	public function renderDefault()
	{
		$this->template->projectsCollection = $this->projectsService->projectsCollection()
			->whereActive()
			->attach($this)
			->itemsPerPage(20)
			->find();
	}

	/**
	 * Archív projektů
	 * @return void
	 */

	public function renderArchive()
	{
		$this->template->projectsCollection = $this->projectsService->projectsCollection()
			->whereFinished()
			->attach($this)
			->itemsPerPage(20)
			->find();
	}

	/**
	 * Vytvořit nový projekt
	 * @return void
	 */

	public function handleAdd()
	{
		$projectEntity = $this->projectsService->projectEntity();

		if ($projectsEntity->save()) {
			$this->redirect("Projects:edit", $projectEntity->getId());
		} else {
			$this->flashMessage("Nový projekt se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");

			$this->restoreRequest($this->backlink);
			$this->redirect("this");
		}
	}

	/**
	 * Upravit existující projekt
	 * @param integer $id ID projektu, který chceme upravit
	 * @return void
	 * @throws BAdRequestException
	 */

	public function actionEdit($id)
	{
		if (is_numeric($id) && $this->projectEntity = $this->projectsService->projectEntity()->find($id)) {
			if($this->projectEntity->isEditAllowed($this->user)){
				
			} else {
				$this->notAllowed("Nemáte oprávnění k úpravě tohoto projektu");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit úpravu projektu
	 * @return void
	 */
	public function renderEdit()
	{
		$this->template->projectEntity = $this->projectEntity;
	}

	/**
	 * Odebrat projekt
	 * @param integer $id
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleRemove($id)
	{
		if (is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			if($this->projectEntity->isRemoveAllowed($this->user)){
				if ($this->projectEntity->remove()) {
					$this->flashMessage("Projekt byl odebrán", "alert-success");
				} else {
					$this->flashMessage("Při odebírání projektu došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění odebrat tento projekt");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Posunout projekt dopředu
	 * @param integer $id ID projektu, který cheme posunout
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleMoveUp($id)
	{
		if (is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			if ($this->projectEntity->isEditAllowed($this->user)) {
				if ($this->projectEntity->moveUp()) {
					$this->flashMessage("Projekt byl v seznamu posunut nahoru", "alert-success");
				} else {
					$this->flashMessage("Při posouvání projektu došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění posunout tento projekt");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Posunout projekt dozadu
	 * @param integer $id ID projektu, který cheme posunout
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleMoveDown($id)
	{
		if(is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			if ($this->projectEntity->isEditAllowed($this->user)) {
				if ($this->projectEntity->moveDown()) {
					$this->flashMessage("Projekt byl v seznamu posunut dolů", "alert-success");
				} else {
					$this->flashMessage("Při posouvání projektu došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění posunout tento projekt");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Získat komponentu formuláře projektu
	 * @param string $name jméno formuláře v šabloně
	 * @return CP\Forms\Form
	 */

	protected function createComponentProjectForm($name)
	{
		$form = new ProjectForm($this, $name);

		$form->setEntity($this->projectEntity);

		return $form->create();
	}

	public function loadFormUsersProperties($form)
	{
		$userIds = [];

		foreach ($form["usersCollection"]->components AS $key => $user) {
			if (is_numeric($key)) {
				$userIds[] = $user->components["userId"]->value;
			}
		}

		$form->properties["userOptions"] = $this->projectsService->getUserOptionsByDomain($userIds);

		$form->properties["usersCollection"] = $this->projectsService->getUsersCollectionByUserIds($userIds)->find();
	}
}