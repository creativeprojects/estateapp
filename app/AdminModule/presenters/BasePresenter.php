<?php

namespace App\AdminModule\Presenters;

abstract class BasePresenter extends \CP\AdminModule\Presenters\BasePresenter
{
	/** @var \App\Services\DemandsService @inject */
	public $demandsService;

	public function beforeRender()
	{
		parent::beforeRender();

		if ($this->user->isAllowed("Admin:Demands")) {
			$this->addMenuNotification("Demands", $this->demandsService->getNewCountByDomain());
		}
	}
}