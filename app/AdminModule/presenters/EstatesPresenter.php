<?php

namespace App\AdminModule\Presenters;

use App\Forms\EstateForm;
use Nette\Application\BadRequestException;

class EstatesPresenter extends SecuredPresenter
{
	/** Vše související s PDF kartou nemovitosti */
	use EstatesPresenter\PdfTrait;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/** @var \App\Services\ProjectsService @inject */
	public $projectsService;

	/** @var \App\Services\Export\PortalsService @inject */
	public $portalsService;

	/** @var \App\Services\UsersService @inject */
	public $usersService;

	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/** @var \App\Entities\EstateEntity */
	public $estateEntity;

	/** @var \CP\Entities\SlugEntity **/
	public $slugEntity;

	/** @var \CP\Collections\SlugsCollection **/
	public $slugsCollection;

	/**
	 * Správa nemovitostí
	 * @return void
	 */

	public function actionDefault()
	{
		//Najít aktivní nemovitosti
		$this->estatesCollection = $this->estatesService->estatesCollection()->whereActive()->whereDomain();

		if ($this->user->isAllowed("Admin:Estates", "showAll")) { //Zobrazit všechny nemovitosti aktuálního klienta (domény)
			
		} elseif($this->user->isAllowed("Admin:Estates", "showMyEstates")) { //Zobrazit nemovitosti aktuálně přihlášeného makléře
			$this->estatesCollection->whereLoggedUser();
		} else { //Pro jistotu jiné případy ošetřit vyjímkou
			$this->notAllowed();
		}

		//Připojit k presenteru kvůli stránkování a filtrům
		$this->estatesCollection->attach($this);
		$this->estatesCollection->pagination->setItemsPerPage(20);
	}

	/**
	 * Vykreslit správu nemovitostí
	 * @return void
	 */

	public function renderDefault()
	{
		$this->template->estatesCollection = $this->estatesCollection->find();
	}

	/**
	 * Archív nemovitostí
	 * @return void
	 */

	public function actionArchive()
	{
		//Najít aktivní nemovitosti
		$this->estatesCollection = $this->estatesService->estatesCollection()->whereArchive();

		if ($this->user->isAllowed("Admin:Estates", "showAllEstates")) { //Nemovitosti aktuálního klienta (domény)
			$this->estatesCollection->whereDomain();
		} elseif($this->user->isAllowed("Admin:Estates", "showMyEstates")) { //Nemovitosti aktuálně přihlášeného makléře
			$this->estatesCollection->whereLoggedUser();
		} else { //Pro jistotu ošetřit ostatní případy vyjímkou
			$this->notAllowed();
		}

		//Připojit k presenteru kvůli stránkování a filtrům
		$this->estatesCollection->attach($this);
		$this->estatesCollection->pagination->setItemsPerPage(20);
	}

	/**
	 * Vykreslit správu nemovitostí
	 * @return void
	 */

	public function renderArchive()
	{
		$this->template->estatesCollection = $this->estatesCollection->find();
	}

	/**
	 * Přidat novou nemovitosti
	 * @return void
	 */

	public function actionAdd()
	{
		$this->estateEntity = $this->estatesService->estateEntity()->blank();

		$this->estateEntity->setUserId($this->user->getIdentity()->getId());

		if ($this->estateEntity->save()) {
			$this->redirect("Estates:edit", $this->estateEntity->getId());
		} else {
			$this->flashMessage("Novou nemovitost se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");

			$this->restoreRequest($this->backlink);
			$this->redirect("Estates:default");
		}
	}

	/**
	 * Upravit existující nemovitost
	 * @param integer $estate_id
	 * @return void
	 */

	public function actionEdit($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			if ($this->estateEntity->isEditAllowed($this->user)) {
				$this->template->estateEntity = $this->estateEntity;

				$this->template->portalsCollection = $this->portalsService->getActiveCollection()->find();

				$this->template->exportSettings = $this->thisDomainEntity->getExportSettings();
			} else {
				$this->notAllowed("Nemáte oprávnění k úpravě této nemovitosti");
			}
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentEstateForm($name)
	{
		$form = new EstateForm($this, $name);

		$form->setEntity($this->estateEntity);

		return $form->create();
	}

	public function handleRemove($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			if ($this->estateEntity->isRemoveAllowed($this->user)) {
				if ($this->estateEntity->remove()) {
					$this->flashMessage("Nemovitost byla odebrána", "alert-success");
				} else {
					$this->flashMessage("Nemovitost se nepodařilo odebrat! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění odebrat tuto nemovitost");
			}
		} else {
			throw new BadRequestException;
		}
	}

	public function handleSetHomepage($estate_id, $advert_onhomepage)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			if ($this->estateEntity->isEditAllowed($this->user)) {
				if ($this->estateEntity->saveOnHomepage($advert_onhomepage)) {
					$this->flashMessage(($advert_onhomepage ? "Nemovitost byla přidána na úvodní stránku" : "Nemovitosti byla odebrána z úvodní stránky"), "alert-success");
				} else {
					$this->flashMessage("Nastavení zobrazení na úvodní stránce se nepodařilo uložit!", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění k úpravě této nemovitosti");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Správa URL adres seznamů nemovitostí
	 * @return void
	 */

	public function actionSlugs()
	{
		$this->slugsCollection = $this->estatesService->getSlugsCollection();

		$this->slugsCollection->attach($this);

		$this->slugsCollection->pagination->setItemsPerPage(50);
	}

	/**
	 * Vykreslit správu URL adres seznamů nemovitostí
	 * @return void
	 */
	
	public function renderSlugs()
	{
		$this->template->slugsCollection = $this->slugsCollection->find();
	}
}