<?php

namespace App\AdminModule\Presenters;

class BlogPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\BlogPresenterTrait;
}