<?php

namespace App\AdminModule\Presenters;

class UserPresenter extends SecuredPresenter
{
	/** @var \App\Services\ExportService @inject */
	public $exportService;

	use \CP\Presenters\UserPresenterTrait;
}