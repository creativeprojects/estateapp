<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\BrokerProfileForm;
use Nette\Application\BadRequestException;
use Nette\Security\AuthenticationException;

class BrokerProfilesPresenter extends SecuredPresenter
{
	/** @var \App\Services\BrokerProfilesService @inject */
	public $brokerProfilesService;

	/** @var \App\Entities\BrokerProfileEntity */
	public $brokerProfileEntity;

	/** @var \App\Services\BrokerProfiles\VideosService @inject */
	public $videosService;

	/** @var \App\Services\BrokerProfiles\TestimonialsService @inject */
	public $testimonialsService;

	/**
	 * Vytvořit profilovou stránku makléře
	 * @param integer $id
	 * @return void
	 */

	public function actionCreate($id)
	{
		
		$userEntity = new \App\Entities\UserEntity;

		if ($userEntity->find($id)) {
			$this->brokerProfileEntity = $this->brokerProfilesService->blank();

			$this->brokerProfileEntity->setUserId($userEntity->getId());

			foreach ($this->translator->getAvailableLocales() AS $locale) {
				$this->brokerProfileEntity->profile_title->{$locale} = $userEntity->getName();
			}

			if ($this->brokerProfileEntity->save()) {
				$this->redirect("BrokerProfiles:edit", $this->brokerProfileEntity->getId());
			} else {
				$this->flashMessage("Makléřský profil se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");
				
				$this->redirect("Dashboard:default");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Upravit profilovou stránku makléře
	 * @param integer $id
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionEdit($id)
	{
		if ($this->brokerProfileEntity = $this->brokerProfilesService->brokerProfileEntity()->find($id)) {
			if ($this->brokerProfileEntity->isEditAllowed($this->user)) {
				$this->template->brokerProfileEntity = $this->brokerProfileEntity;
			} else {
				$this->notAllowed("Nemáte oprávnění upravit tento makléřský profil");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vytvořit komponentu formuláře profilové stránky makléře
	 * @param string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentBrokerProfileForm($name)
	{
		$form = new BrokerProfileForm($this, $name);

		$form->setEntity($this->brokerProfileEntity);
		
		return $form->create();
	}

	/**
	 * Odebrat profilovou stránku makléře
	 * @param integer $id
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleRemove($id)
	{
		if ($this->brokerProfileEntity = $this->brokerProfilesService->brokerProfileEntity()->find($id)) {
			if ($this->brokerProfileEntity->isRemoveAllowed($this->user)) {
				if ($this->brokerProfileEntity->remove()) {
					$this->flashMessage("Profil makléře byl odebrán", "alert-success");
				} else {
					$this->flashMessage("Při odebírání profilu makléře došlo k chybě!", "alert-danger");
				}

				$this->restoreRequest($this->backlink);
				$this->redirect("Dashboard:default");
			} else {
				$this->notAllowed("Nemáte oprávnění odebrat tento makléřský profil");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Posunout v seznamu nahoru
	 */

	public function handleMoveUp($id)
	{
		if ($this->brokerProfileEntity = $this->brokerProfilesService->brokerProfileEntity()->find($id)) {
			if($this->brokerProfileEntity->isMoveAllowed($this->user)){
				if($this->brokerProfileEntity->moveUp()){
					$this->flashMessage("Profil makléře byl v seznamu posunut nahoru");
				} else {
					$this->flashMessage("Profil makléře se nepodařilo posunout nahoru! Zkuste akci opakovat", "alert-danger");
				}

				$this->restoreRequest($this->backlink);
				$this->redirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění posouvat tento makléřský profil");
			}
		} else {
			throw new BadRequestException;
		}
	}

	public function handleMoveDown($id)
	{
		if ($this->brokerProfileEntity = $this->brokerProfilesService->brokerProfileEntity()->find($id)) {
			if($this->brokerProfileEntity->isMoveAllowed($this->user)){
				if($this->brokerProfileEntity->moveDown()){
					$this->flashMessage("Profil makléře byl v seznamu posunut dolů");
				} else {
					$this->flashMessage("Profil makléře se nepodařilo posunout dolů! Zkuste akci opakovat", "alert-danger");
				}

				$this->restoreRequest($this->backlink);
				$this->redirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění posouvat tento makléřský profil");
			}
		} else {
			throw new BadRequestException;
		}
	}
}