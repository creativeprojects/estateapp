<?php

namespace App\AdminModule\Presenters\EstatesPresenter;

use CP\Forms\EmailForm;
use Nette\Application\BadRequestException;

trait PdfTrait
{
	/**
	 * Vytvořit PDF kartu nemovitosti
	 * TODO - Kde se používá?
	 * @param integer $estate_id
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionPdf($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			if ($this->estateEntity->savePDF()) {
				$this->template->estateEntity = $this->estateEntity;
			} else {
				$this->flashMessage("PDF kartu nemovitosti se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");

				$this->restoreRequest($this->backlink);
				$this->redirect("Estates:default");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vývojová metoda pro vytváření designu karet nemovitosti
	 * @param integer $estate_id
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionHtml($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			$this->template->html = $this->estateEntity->getHtml();
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Odeslat kartu nemovitosti
	 * @param integer $estate_id
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionSend($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			$parameters = [
				"estateEntity" => $this->estateEntity
			];

			$this->emailEntity = $this->emailsService->emailEntity()->blank()
				//TODO - přepracovat na localeTemplate
				->setSubjectTemplate("AdminModule/templates/Estates/emails/{$this->translator->getLocale()}/estateCardSubject.latte", $parameters)
				//TODO - přepracovat na localeTemplate
				->setBodyTemplate("AdminModule/templates/Estates/emails/{$this->translator->getLocale()}/estateCardMessage.latte", $parameters)
				->addAttachment($this->estateEntity->estate_pdf)
				->addTo("")
				->setFrom($this->user->getIdentity()->data["email"], $this->user->getIdentity()->data["name"]);
			
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vytvořit komponentu odeslání karty nemovitosti
	 * @param string $name
	 * @return CP\Forms\Form
	 */
	
	protected function createComponentEmailForm($name)
	{
		$form = new EmailForm($this, $name);

		$form->setEntity($this->emailEntity);

		$form->setEditable("to", true)
			->setEditable("subject", true)
			->setEditable("body", true)
			->setEditable("attachments", true)
			->setLabel("send", "Odeslat kartu emailem")
			->setRequired("to", "Zadejte email příjemce")
			->enableHtml()
			->setFlashMessage("Karta nemovitosti byla odeslána", "Kartu nemovitosti se napodařilo odeslat. Zkuste to prosím znovu.");

		return $form->create();
	}
}