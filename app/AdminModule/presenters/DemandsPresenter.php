<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\DemandForm;
use Nette\Application\BadRequestException;

class DemandsPresenter extends SecuredPresenter
{
	/** @var \App\Collections\DemandsCollection */
	public $demandsCollection;

	/** @var \App\Entities\DemandEntity */
	public $demandEntity;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/** @var \App\Services\UsersService @inject */
	public $usersService;

	public function actionDefault()
	{
		if ($this->user->isAllowed("Admin:Demands", "showAllDemands")){
			$this->demandsCollection = $this->demandsService->getCollection();
		} elseif ($this->user->isAllowed("Admin:Demands", "showMyDemands")) {
			$this->demandsCollection = $this->demandsService->getCollectionByLoggedUser();
		} else {
			$this->notAllowed();
		}
	}

	public function actionArchive()
	{
		if ($this->user->isAllowed("Admin:Demands", "showAllDemands")){
			$this->demandsCollection = $this->demandsService->getArchiveCollection();
		} elseif ($this->user->isAllowed("Admin:Demands", "showMyDemands")) {
			$this->demandsCollection = $this->demandsService->getArchiveCollectionByLoggedUser();
		} else {
			$this->notAllowed();
		}
	}

	public function renderDefault()
	{
		$this->demandsCollection->attach($this);

		$this->demandsCollection->pagination->setItemsPerPage(20);

		$this->template->demandsCollection = $this->demandsCollection->find();

		$this->template->slugEntity = $this->slugsService->slugEntity()->findByRequest(["presenter" => "Demands", "action" => "default"]);
	}

	public function renderArchive()
	{
		$this->renderDefault();
	}

	public function actionAdd()
	{
		$this->demandEntity = $this->demandsService->getNewEntity();
		
		$this->template->demandEntity = $this->demandEntity;
	}

	public function actionEdit($demand_id)
	{
		if($this->demandEntity = $this->demandsService->getEntity($demand_id)){
			if($this->demandEntity->isEditAllowed($this->user)){
				$this->template->demandEntity = $this->demandEntity;
			} else {
				$this->notAllowed();
			}
		} else {
			throw new BadRequestException;
		}
	}

	protected function createComponentDemandForm($name)
	{
		$form = new DemandForm($this, $name);

		$form->setEntity($this->demandEntity);
		
		return $form->create();
	}

	public function handleRemove($demand_id)
	{
		if($this->demandEntity = $this->demandsService->getEntity($demand_id)){
			if($this->demandEntity->isRemoveAllowed($this->user)){
				if($this->demandEntity->remove()){
					$this->flashMessage("Poptávka byla odebrána", "alert-success");
				} else {
					$this->flashMessage("Při odebírání poptávky došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed();
			}
		} else {
			throw new BadRequestException;
		}
	}

	public function handleArchive($demand_id)
	{
		if($this->demandEntity = $this->demandsService->getEntity($demand_id)){
			if($this->demandEntity->isEditAllowed($this->user)){
				if($this->demandEntity->saveStatus(2)){
					$this->flashMessage("Poptávka byla přesunuta do archívu", "alert-success");
				} else {
					$this->flashMessage("Při přesouvání poptávky do archívu došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed();
			}
		} else {
			throw new BadRequestException;
		}
	}

	public function handleUnarchive($demand_id)
	{
		if($this->demandEntity = $this->demandsService->getEntity($demand_id)){
			if($this->demandEntity->isEditAllowed($this->user)){
				if($this->demandEntity->saveStatus(0)){
					$this->flashMessage("Poptávka byla obnovena z archívu", "alert-success");
				} else {
					$this->flashMessage("Při obnovování poptávky z archívu došlo k chybě! Zkuste akci opakovat", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed();
			}
		} else {
			throw new BadRequestException;
		}
	}
}