<?php

namespace App\AdminModule\Presenters;

class DashboardPresenter extends SecuredPresenter
{
	/** @var \App\Services\EstatesService @inject */
	public $estateService;

	/** @var \App\Services\DemandsService @inject */
	public $demandService;

	use \CP\AdminModule\Presenters\DashboardPresenterTrait;

	public function renderDefault()
	{
		//Notifikace
		$this->defaultNotifications();

		//Google Analytics
		$this->defaultAnalytics();

		//Statistiky
		$this->template->estatesCount = $this->estateService->getStats();

		if ($this->user->isInRole("admin") || $this->user->isInRole("superAdmin")) {
			$this->template->demandsCount = $this->demandService->getStats();

			$this->template->brokersCount = $this->userService->countBrokers();

			$this->template->brokersStats = $this->userService->getStats();
		}
	}
}