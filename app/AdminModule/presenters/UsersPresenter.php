<?php

namespace App\AdminModule\Presenters;

use Nette\Application\BadRequestException;

class UsersPresenter extends SecuredPresenter
{
	/** @var \App\Services\ExportService @inject */
	public $exportService;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/** @var \App\Services\DemandsService @inject */
	public $demandsService;

	use \CP\Presenters\UsersPresenterTrait;
}