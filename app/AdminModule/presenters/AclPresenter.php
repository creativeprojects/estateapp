<?php

namespace App\AdminModule\Presenters;

class AclPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\AclPresenterTrait;
}