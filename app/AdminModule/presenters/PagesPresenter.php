<?php

namespace App\AdminModule\Presenters;

class PagesPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\PagesPresenterTrait;
}