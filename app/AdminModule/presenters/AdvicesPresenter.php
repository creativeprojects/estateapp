<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\Advices\ArticleForm;
use Nette\Application\BadRequestException;

class AdvicesPresenter extends \App\AdminModule\Presenters\SecuredPresenter
{
	/** Základní vlastnosti presenteru */
	use \App\Presenters\AdvicesPresenterTrait;

	/** Vše související s otázkami */
	use AdvicesPresenter\QuestionsTrait;

	/**
	 * Přehled všech článků poradny
	 * @return void
	 */

	public function renderArticles()
	{
		$this->template->articlesCollection = $this->advicesArticlesService
			->articlesCollection()
			->newestFirst()
			->attach($this)
			->itemsPerPage(20)
			->find();
	}

	/**
	 * Přidat nový článek
	 * @return void
	 */

	public function actionAdd()
	{
		$this->advicesArticleEntity = $this->advicesArticlesService->articleEntity()->blank();
	}

	/**
	 * Vykreslit přidání nového článku
	 * @return void
	 */

	public function renderAdd()
	{
		$this->template->articleEntity = $this->advicesArticleEntity;
	}

	/**
	 * Upravit existující článek
	 * @param integer $id ID článku, který chceme upravit
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionEdit($id)
	{
		if (is_numeric($id) && ($this->advicesArticleEntity = $this->advicesArticlesService->articleEntity()->find($id))) {
			if ($this->advicesArticleEntity->isEditAllowed($this->user)) {
				$this->template->articleEntity = $this->advicesArticleEntity;
			} else {
				$this->notAllowed("Nemáte oprávnění upravovat tento článek");
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vytvořit komponentu formuláře článku
	 * @param string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentArticleForm($name)
	{
		$form = new ArticleForm($this, $name);

		$form->setEntity($this->advicesArticleEntity);

		return $form->create();
	}

	/**
	 * Odebrat článek
	 * @param integer $id ID článku, který chceme odebrat
	 * @return void
	 * @throws BadRequestException
	 */

	public function handleRemove($id)
	{
		if (is_numeric($id) && ($this->advicesArticleEntity = $this->advicesArticlesService->articleEntity()->find($id))) {
			if ($this->advicesArticleEntity->isRemoveAllowed($this->user)) {
				if($this->advicesArticleEntity->remove()){
					$this->flashMessage("Článek byl odebrán", "alert-success");
				} else {
					$this->flashMessage("Při odebírání článku došlo k chybě!", "alert-danger");
				}

				$this->handleRedirect("this");
			} else {
				$this->notAllowed("Nemáte oprávnění odebrat tento článek");
			}
		} else {
			throw new BadRequestException;
		}
	}
}