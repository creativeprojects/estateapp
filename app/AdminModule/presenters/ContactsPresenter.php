<?php

namespace App\AdminModule\Presenters;

class ContactsPresenter extends SecuredPresenter
{
	use \CP\AdminModule\Presenters\ContactsPresenterTrait;
}