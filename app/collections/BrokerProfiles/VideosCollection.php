<?php

namespace App\Collections\BrokerProfiles;

/**
 * @Repository App\Repositories\BrokerProfiles\VideosRepository
 * @Entity App\Entities\BrokerProfiles\VideoEntity
 * @Service brokerProfiles
 */

class VideosCollection extends \CP\Collections\BaseCollection
{
	public function findByParent()
	{
		$videos = $this->service->repository->brokerProfilesVideos->getAllBy(["userId" => $this->parent->getId()]);

		$this->create($videos);

		return $this;
	}

	public function save()
	{
		$savedIds = [];

		foreach ($this->getAll() AS $videoEntity) {
			$videoEntity->setUserId($this->parent->getId());

			if ($videoEntity->save()) {
				$savedIds[] = $videoEntity->getId();
			}
		}

		return $this->remove($savedIds);
	}

	public function remove($excludeIds = [])
	{
		$removeBy = array(
			"userId" => $this->parent->getId()
		);

		if(count($excludeIds)){
			$removeBy["video_id NOT"] = $excludeIds;
		}

		return $this->service->repository->brokerProfilesVideos->removeBy($removeBy);
	}
}