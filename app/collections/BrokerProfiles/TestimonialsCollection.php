<?php

namespace App\Collections\BrokerProfiles;

/**
 * @Repository App\Repositories\BrokerProfiles\TestimonialsRepository
 * @Entity App\Entities\BrokerProfiles\TestimonialEntity
 * @Service brokerProfiles
 */

class TestimonialsCollection extends \CP\Collections\BaseCollection
{
	public function findByParent()
	{
		$testimonials = $this->service->repository->brokerProfilesTestimonials->getAllBy(["userId" => $this->parent->getId()]);

		$this->create($testimonials);

		return $this;
	}

	public function save()
	{
		$savedIds = array();

		foreach ($this->getAll() AS & $testimonialEntity) {
			$testimonialEntity->setUserId($this->parent->getId());
			
			if ($testimonialEntity->save()) {
				$savedIds[] = $testimonialEntity->getId();
			}
		}

		return $this->remove($savedIds);
	}

	public function remove($excludeIds = [])
	{
		$removeBy = [
			"userId" => $this->parent->getId()
		];

		if (count($excludeIds)) {
			$removeBy["testimonial_id NOT"] = $excludeIds;
		}

		return $this->service->repository->brokerProfilesTestimonials->removeBy($removeBy);
	}
}