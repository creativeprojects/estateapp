<?php

namespace App\Collections\Estates;

/**
 * @Repository App\Repositories\Estates\ExportsRepository;
 * @Entity App\Entities\Estates\ExportEntity
 * @Service estates
 */

class ExportsCollection extends \CP\Collections\QueryCollection
{
	public function buildQuery()
	{
		$this->query
			->field("e.*")
			->from("{$this->service->repository->estatesExports} AS e")
			->where("removed IS NULL");
	}

	public function save()
	{
		$savedIds = [];

		foreach ($this->getAll() AS $exportEntity) {
			//$portal["portal_id"] = $key;
			$exportEntity->estate_id = $this->parent->getId();

			$exportEntity->export_timestamp = NULL;

			if($exportEntity->save()){
				$savedIds[] = $exportEntity->getId();
			} else {
				return false;
			}
		}

		return $this->remove($savedIds);
	}

	public function remove($excludeIds = [])
	{
		$by = [
			"estate_id" => $this->parent->getId(),
		];

		if(count($excludeIds)){
			$by["portal_id NOT"] = $excludeIds;
		}

		return $this->service->repository->estatesExports->removeBy($by);
	}
}