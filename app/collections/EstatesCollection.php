<?php

namespace App\Collections;

use CP\Utils\ArrayResult;

/**
 * @Repository App\Repositories\EstatesRepository
 * @Entity App\Entities\EstateEntity
 * @Service estates
 */

class EstatesCollection extends \CP\Collections\QueryCollection
{
	/**
	 * Společné vlastnosti pro všechny kolekce nemovitostí
	 * @return void
	 */

	public function buildQuery()
	{
		$this->query
			->field("r.estate_title")
			->field("r.estate_description")
			->field("e.estate_id")
			->field("e.advert_onhomepage")
			->field("e.advert_price, e.advert_price_hidden, e.advert_price_currency, e.advert_price_unit, r.advert_price_text_note")
			->field("e.advert_status, e.visible")
			->field("e.advert_function, e.advert_type, e.advert_subtype")
			->field("e.advert_code")
			->field("e.usable_area")
			->field("e.locality, e.locality_latitude, e.locality_longitude, e.locality_inaccuracy_level, e.locality_street, e.locality_cp, e.locality_co, e.locality_zip, e.locality_city")
			
			//Toto pořadí JOINovaných tabulek je zde kvůli záhadnému bugu v MySQL, který při kombinaci ORDER BY a LIMIT špatně řadí nemovitosti, pokud nemám mezi fieldy "e.*"
			//Tento hotfix to opravuje, otázka, kde všude máme tento problém s řazením
			->from("{$this->service->repository->estates->getNameLocale()} AS r")
			->leftJoin("{$this->service->repository->estates} AS e ON (e.estate_id = r.estate_id)")
			->where("e.removed IS NULL")
			->cache($this->service->repository->estates);

		$this->addFilters();
	}

	/**
	 * Vytvořit výchozí filtry kolekce nemovitostí
	 * @return void
	 */

	protected function addFilters()
	{
		$this->filter->addSelect("advert_function", "Typ nabídky", $this->service->estatesParameters["advert_function"]["options"])
			->setPrompt("Vyberte typ nabídky ...")
			->validOnly(true);

		$this->filter->addSelect("advert_type", "Typ nemovitosti", $this->service->estatesParameters["advert_type"]["options"])
			->setPrompt("Vyberte typ nemovitosti ...")
			->setRequired("Zvolte typ nemovitosti")
			->validOnly(true);

		$this->filter->addSelect("advert_subtype", "Dispozice", $this->service->getAdvertSubtypeOptions())
			->setPrompt("Nejdříve zvolte typ nemovitosti ...")
			->setAll("Nerozhoduje")
			->validOnly(true);
			
		$this->filter->addLocation("locality", "Lokalita")
			->setPlaceholder("Zadejte požadovanou lokalitu")
			->setDistance(10);
		
		$this->filter->addRange("estate_area", "Plocha od", "Plocha do")
			->setUnit("m<sup>2</sup>");

		$this->filter->addRange("advert_price", "Cena od", "Cena do")
			->setUnit("Kč");
	}

	/**
	 * Vybrat pouze prodané nemovitosti
	 * @return this
	 */

	public function whereSold()
	{
		$this->query
			->where("advert_status = 4");

		return $this;
	}

	/**
	 * Vybrat pouze aktivní nemovitosti
	 * @return $this
	 */

	public function whereActive()
	{
		//TODO - zde spíše ověřovat práva k editaci
		if ($this->service->user && in_array($this->service->user->role, ["broker", "admin", "superAdmin"])) {
			//Načíst nemovitosti, které jsou v přípravě, aktivní, rezervovány a přitom inzerovány, rezervovány
			$this->query->where("e.advert_status IN(0,1,2,3)", "advert_status");
		} else {
			//Načíst pouze nemovitosti, které jsou aktivní, nebo rezervovány a přitom stále inzerovány
			$this->query->where("e.advert_status IN(1,2)", "advert_status");
		}
				
		return $this;
	}

	/**
	 * Vybrat pouze archivní nemovitosti
	 * @return $this
	 */

	public function whereArchive()
	{
		$this->query->where("e.advert_status IN(4,5)");

		return $this;
	}

	/**
	 * Vybrat pouze konkrétní typ nemovitosti
	 * @param integer $advert_type
	 * @param integer $advert_subtype
	 * @return this
	 */

	public function whereType($advert_type = null, $advert_subtype = null)
	{
		if(in_array($advert_type, array_keys($this->service->estatesParameters["advert_type"]["options"]))){
			$this->query->where("advert_type = {$advert_type}");
		}

		if(in_array($advert_subtype, array_keys($this->service->estatesParameters["advert_subtype"]["options"]))){
			$this->query->where("advert_subtype = {$advert_subtype}");
		}

		return $this;
	}

	/**
	 * Vybrat pouze konkrétní typ obchodu (prodej / pronájem ...)
	 * @param integer $advert_function
	 * @return this
	 */

	public function whereFunction($advert_function)
	{
		if (in_array($advert_function, array_keys($this->service->estatesParameters["advert_function"]["options"]))) {
			$this->query->where("advert_function = {$advert_function}");
		}

		return $this;
	}

	/**
	 * Vybrat pouze nemovitosti konkrétního makléře
	 * @param integer $userId
	 * @return this
	 * @throws Exception
	 */

	public function whereUserId($userId)
	{
		if (is_numeric($userId)) {
			$this->query->where("e.userId = " . $userId);

			return $this;
		} else {
			throw new \Exception("Invalid userId: " . print_r($userId, true));
		}
	}

	/**
	 * Vybrat pouze nemovitosti aktuálně přihlášeného makléře
	 * @return this
	 */

	public function whereLoggedUser()
	{
		$this->whereUserId($this->service->user->getId());

		return $this;
	}

	/**
	 * Nemovitosti přihlášeného uživatele
	 * @param Nette\Security\User $user
	 * @return this
	 */

	/*public function whereUser($user)
	{
		if ($user) {
			if (!$user->isAllowed("Admin:Estates", "showAll")) {
				$this->whereUserId($user->getId());
			}
		} else {
			$this->query->where("0 = 1");
		}

		return $this;
	}*/

	/**
	 * Vybrat nemovitosti pouze pro zadaný developerský projekt
	 * @param App\Entities\ProjectEntity $projectEntity
	 * @return this
	 */

	public function whereProject(\App\Entities\ProjectEntity $projectEntity)
	{
		$this->query
			->where("e.project_id = {$projectEntity->getId()}");

		return $this;
	}

	/**
	 * Vybrat pouze viditelné nemovitosti pro daného uživatele
	 * @param Nette\Security\User $user
	 * @return this
	 */

	public function whereUserVisible($user)
	{
		$logged = $user->isInRole("broker") || $user->isInRole("admin") || $user->isInRole("callcentrum") || $user->isInRole("superAdmin");
		
		$this->query
			->where("IF(e.advert_status = 1 OR e.advert_status = 2, 1, " . ($logged ? "'logged'" : "0") . ")");

		return $this;
	}

	/**
	 * Vybrat nejdříve nemovitosti, které se mají zobrazit na úvodní straně
	 * @return this
	 */
	
	public function onHomepageFirst()
	{
		$this->query->orderBy("advert_onhomepage", "DESC");

		return $this;
	}

	/**
	 * Řadit podle data přidání sestupně
	 * @return this
	 */

	public function newestFirst()
	{
		$this->query->orderBy("created", "DESC");

		return $this;
	}

	/**
	 * Archivace všech entit v uložišti
	 * Pokud v koleci nejsou žádné entity, vrátíme TRUE
	 * @return bool výsledek
	 */

	public function archive()
	{
		if ($this->count()) {
			$result = new ArrayResult;

			foreach ($this->getAll() AS $entity) {
				$result->is($entity->archive());
			}

			return $result->result();
		} else {
			return true;
		}
	}
}