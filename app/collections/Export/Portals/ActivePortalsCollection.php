<?php

namespace App\Collections\Export\Portals;

/**
 * @Repository App\Repositories\Export\PortalsRepository;
 * @Entity App\Entities\Export\PortalEntity
 * @Service portals
 */

class ActivePortalsCollection extends \App\Collections\Export\PortalsCollection
{
	public function buildQuery()
	{
		parent::buildQuery();

		$this->query
			->where("p.portal_active = 1");
	}
}