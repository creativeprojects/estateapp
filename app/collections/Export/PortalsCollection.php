<?php

namespace App\Collections\Export;

/**
 * @Repository App\Repositories\Export\PortalsRepository;
 * @Entity App\Entities\Export\PortalEntity
 * @Service exports
 */

class PortalsCollection extends \CP\Collections\QueryCollection
{
	public function buildQuery()
	{
		$this->query
			->field("p.*")
			->from("{$this->service->repository->portals} AS p")
			->where("removed IS NULL");
	}

	public function save()
	{
		$savedIds = array();

		foreach ($this->getAll() AS $key => & $portal) {
			$portal["portal_id"] = $key;
			$portal["estate_id"] = $estate["estate_id"];
			$portal["export_timestamp"] = NULL;
			$portal = $this->repository->exportPortals->save($portal);

			$savedIds[] = $portal["portal_id"];
		}

		$removeBy = array(
			"estate_id" => $estate["estate_id"],
			"portal_id NOT" => $savedIds
		);

		$this->repository->exportPortals->removeBy($removeBy);
	}
}