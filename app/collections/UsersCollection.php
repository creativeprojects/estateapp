<?php

namespace App\Collections;

/**
 * @Repository App\Repositories\UsersRepository
 * @Entity App\Entities\UserEntity
 * @Service usersService
 */

class UsersCollection extends \CP\Collections\UsersCollection
{
	/**
	 * Najít uživatele pro daný developerský projekt
	 * @param App\Entities\ProjectEntity
	 * @reutrn this
	 */

	public function whereProject(\App\Entities\ProjectEntity $projectEntity)
	{
		$userIds = [];

		foreach ($projectEntity->usersCollection->getAll() AS $userEntity) {
			$userIds[] = $userEntity->userId;
		}

		$this->query->where((count($userIds) ? "u.id IN (" . implode(",", $userIds) . ")" : "0 = 1"));

		return $this;
	}

	/**
	 * Najít pouze makléře
	 * @return this
	 */
	
	public function whereBroker()
	{
		$this->query->where("role = 'broker'");

		return $this;
	}
}