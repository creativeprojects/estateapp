<?php

namespace App\Collections\Advices;

/**
 * @Repository App\Repositories\Advices\QuestionsRepository
 * @Entity App\Entities\Advices\QuestionEntity
 * @Service advicesQuestions
 */

class QuestionsCollection extends \CP\Collections\QueryCollection
{
	/**
	 * Společné vlastnosti pro všechny kolekce
	 * @return this
	 */

	public function buildQuery()
	{
		$this->query
			->field("*")
			->from($this->service->repository->advicesQuestions->getName())
			->whereDomain()
			->nonRemoved();

		return $this;
	}

	/**
	 * Řadit podle data přidání sestupně
	 * @return this
	 */

	public function newestFirst()
	{
		$this->query->orderBy("created", "DESC");

		return $this;
	}
}