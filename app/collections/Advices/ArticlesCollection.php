<?php

namespace App\Collections\Advices;

/**
 * @Repository App\Repositories\Advices\ArticlesRepository
 * @Entity App\Entities\Advices\ArticleEntity
 * @Service advicesArticles
 */

class ArticlesCollection extends \CP\Collections\ArticlesCollection
{
	/**
	 * Společné vlastnosti pro všechny kolekce
	 * TODO - až dokončíme Repository tak můžeme dát pryč
	 * @return this
	 */

	public function buildQuery()
	{
		$this->setRepository($this->service->repository->advicesArticles);

		parent::buildQuery();

		//$this->query->cache("blog/articles/lists");

		return $this;
	}
}