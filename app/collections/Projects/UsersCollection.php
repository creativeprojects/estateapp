<?php

namespace App\Collections\Projects;

/**
 * @Repository App\Repositories\Projects\UsersRepository
 * @Entity App\Entities\Projects\UserEntity
 * @Service projectsUsers
 */

class UsersCollection extends \CP\Collections\BaseCollection
{
	public function findByOwner()
	{
		$users = $this->service->repository->projectsUsers->getAllBy([
			"where" => [
				"projectId" => $this->parent->getId()
			],
			"order_by" => "rank ASC"
		]);

		$this->create($users);

		return $this;
	}

	public function get($key = null)
	{
		return $this;
	}

	public function set($values = null)
	{
		if (isset($values["userId"])) {
			unset($values["userId"]);
		}

		return parent::set($values);
	}

	public function save()
	{
		$savedIds = [];

		foreach ($this->getAll() AS $userEntity) {
			$userEntity->projectId = $this->parent->getId();

			if ($userEntity->save()) {
				$savedIds[] = $userEntity->getId();
			} else {
				return false;
			}
		}

		return $this->remove($savedIds);
	}

	public function remove($excludeIds = [])
	{
		$by = [
			"projectId" => $this->parent->getId()
		];

		if (!empty($excludeIds)) {
			$by["id NOT"] = $excludeIds;
		}

		return $this->service->repository->projectsUsers->removeBy($by);
	}
}