<?php

namespace App\Collections\Projects;

/**
 * @Repository App\Repositories\Projects\ArticlesRepository
 * @Entity App\Entities\Projects\ArticleEntity
 * @Service projectsArticles
 */

class ArticlesCollection extends \CP\Collections\ArticlesCollection
{
	/**
	 * Společné vlastnosti pro všechny kolekce
	 * TODO - až dokončíme Repository tak můžeme dát pryč
	 * @return this
	 */

	public function buildQuery()
	{
		$this->setRepository($this->service->repository->projectsArticles);

		parent::buildQuery();

		//$this->query->cache("projects/articles/lists");

		return $this;
	}
}