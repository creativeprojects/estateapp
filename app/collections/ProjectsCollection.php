<?php

namespace App\Collections;

/**
 * @Repository App\Repositories\ProjectsRepository
 * @Entity App\Entities\ProjectEntity
 * @Service projects
 */

class ProjectsCollection extends \CP\Collections\QueryCollection
{
	/**
	 * Společné vlastnosti pro všechny kolekce
	 * @return this
	 */

	public function buildQuery()
	{
		$this->query
			->field("p.*")
			->field("l.title")
			->from("{$this->service->repository->projects} AS p")
			->leftJoin("{$this->service->repository->projects->getNameLocale()} AS l ON l.id = p.id")
			->whereNonRemoved("p")
			->whereDomain("p")
			->orderBy("p.rank", "ASC")
			->cache($this->service->repository->projects);

		return $this;
	}

	/**
	 * Pouze aktivní projekty
	 * @return this
	 */

	public function whereActive()
	{
		$this->query->where("p.status IN ('prepare','progress','sale')");

		return $this;
	}

	/**
	 * Pouze dokončené projekty
	 * @return this
	 */

	public function whereFinished()
	{
		$this->query->where("p.status IN ('finished')");

		return $this;
	}

	/**
	 * Získat možnosti projektů pro použití v selecboxu
	 * @return array
	 */
	
	public function getOptions()
	{
		$options = [];

		foreach ($this->getAll() AS $projectEntity) {
			$options[$projectEntity->getId()] = $projectEntity->getTitle();
		}

		return $options;
	}
}