<?php

namespace App\Collections;

/**
 * @Repository App\Repositories\DemandsRepository
 * @Entity App\Entities\DemandEntity
 * @Service demands
 */

class DemandsCollection extends \CP\Collections\QueryCollection
{
	public function buildQuery()
	{
		$this->query
			->field("d.*")
			->from("{$this->service->repository->demands->getName()} AS d")
			->whereDomain("d")
			->whereNonRemoved("d");
	}
}