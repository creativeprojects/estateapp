<?php

namespace App\Router;

use Nette\Application\Routers\Route;

class RouterFactory extends \CP\Router\RouterFactory
{
	//TODO - routy jednotlivých komponent registrovat přímo v komponentách
	protected function adminModuleRouteList()
	{
		$this->adminRoute[] = new Route('admin/<presenter estates>/<action=default>[/<estate_id>]');
		$this->adminRoute[] = new Route('admin/<presenter demands>/<action=default>[/<demand_id>]');
		$this->adminRoute[] = new Route('admin/<presenter projects>/<action=default>[/<id>]');
		$this->adminRoute[] = new Route('admin/<presenter reviews>/<action=default>[/<review_id>]');
		$this->adminRoute[] = new Route('admin/<presenter contacts>/<action=default>[/<contacts_id>]');

		parent::adminModuleRouteList();
	}		
}