<?php

namespace App\Forms;

use CP\Forms\Form;

class ExportsForm extends \CP\Forms\Base
{

	public function create()
	{
		$this->form->addHidden("domainId");

		foreach ($this->presenter->portals as $portal) {
			$this->form->addCheckBox($portal["portal_key"], "Aktivován");
			foreach ($portal["portal_settings"] as $field) {
				$this->form->addText($field, $field);
			}
		}
		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	public function save($button)
	{
		$export = $button->parent->getValues();

		$this->presenter->domainsService->saveExportSettings($export);



		$this->presenter->flashMessage("Nastavení bylo uloženo", "alert-success");
		$this->presenter->redirect("Domains:default");
	}
}