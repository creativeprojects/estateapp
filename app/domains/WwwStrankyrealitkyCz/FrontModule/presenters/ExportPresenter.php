<?php

namespace WwwStrankyrealitkyCz\FrontModule\Presenters;

class ExportPresenter extends BasePresenter
{
	/** @var \App\Services\ExportService @inject */
	public $exportService;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/**
	 * $id = číslo portálu
	 * @param int $id
	 */
	public function actionEstates($id)
	{
		$this->exportService->export($id);
		die;
	}

	public function actionBrokers()
	{
		$this->exportService->exportOneBroker();
		die;
	}

	public function actionBrokerSreality()
	{
		$this->srealityExport->exportBroker('dfs');
	}
}