<?php

namespace WwwMistolerCz\FrontModule\Presenters;

use CP\Forms\ReviewForm;

class HomepagePresenter extends \App\FrontModule\Presenters\HomepagePresenter
{
	/** @var \CP\Services\ReviewsService @inject */
	public $reviewsService;

	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/** @var \CP\Collections\ReviewsCollection */
	public $reviewsCollection;

	/** @var \CP\Entities\ReviewEntity */
	public $reviewEntity;

	/**
	 * Úvodní stránka
	 * @return void
	 */

	public function actionDefault()
	{
		/** Nemovitosti */
		$this->estatesCollection = $this->estatesService->estatesCollection()->whereDomain()->whereActive();
		$this->estatesCollection->attach($this);

		//Omezit počet a seřadit náhodně
		$this->estatesCollection->query
			->limit(4)
			->orderBy("RAND()");

		//Reference
		$this->reviewsCollection = $this->reviewsService->getCollection();
	}

	/**
	 * Vykreslit úvodní stránku
	 * @return void
	 */

	public function renderDefault()
	{
		$this->template->estatesCollection = $this->estatesCollection->find();

		$this->template->reviewsCollection = $this->reviewsCollection->find();
	}

	/**
	 * Získat komponentu formuláře reference
	 * @param string $name jméno formuláře v šabloně
	 * @return CP\Forms\Form
	 */
	
	protected function createComponentReviewForm($name)
	{
		$form = new ReviewForm($this, $name);

		$this->reviewEntity = $this->reviewsService->getNewEntity();

		$form->setEntity($this->reviewEntity);

		$form->redirect = "Homepage:default";

		return $form->create();
	}
}