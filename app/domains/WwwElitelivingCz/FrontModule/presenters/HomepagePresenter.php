<?php

namespace WwwElitelivingCz\FrontModule\Presenters;

use App\FrontModule\Forms\SearchForm;

class HomepagePresenter extends \App\FrontModule\Presenters\BasePresenter
{
	/** @var \App\Services\HomepageService @inject */
	public $homepageService;

	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/**
	 * Úvodní stránka
	 * @return void
	 */

	public function actionDefault()
	{
		$this->estatesCollection = $this->estatesService->estatesCollection()->whereDomain()->whereActive();
		$this->estatesCollection->attach($this);

		//Omezit počet a seřadit pro homepage
		$this->estatesCollection->query
			->limit(4)
			->orderBy("advert_onhomepage", "DESC");
	}

	/**
	 * Vykreslit úvodní stránku
	 * @return void
	 */
	
	public function renderDefault()
	{
		$this->template->estatesCollection = $this->estatesCollection->find();
	}
}