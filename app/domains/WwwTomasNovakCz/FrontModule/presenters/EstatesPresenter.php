<?php

namespace WwwTomasNovakCz\FrontModule\Presenters;

class EstatesPresenter extends \App\FrontModule\Presenters\EstatesPresenter
{
	/**
	 * Vykreslení přehledu nemovitostí
	 * @param integer|null $advert_type nepovinný kód typu nemovitostí (byty, domy, ...)
	 * @param integer|null $advert_subtype nepovinný kód upřesnění typu nemovitostí (1+kk, 1+1, rodinný, ...)
	 * @param integer|null $advert_function nepovinný kód typu obchodu (prodej, pronájem, ...)
	 * @return void
	 */

	public function renderDefault($advert_type = null, $advert_subtype = null, $advert_function = null)
	{
		//Stránkování
		$this->estatesCollection->pagination->setItemsPerPage(10);

		//Řazení
		$this->estatesCollection->sorting
			->add("estate_title", "Názvu")
			->add("advert_price", "Ceny")
			->add("created", "Datumu přidání");

		$this->template->estatesCollection = $this->estatesCollection->find();
	}
}