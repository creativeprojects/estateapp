<?php

namespace WwwTomasNovakCz\FrontModule\Presenters;

use CP\Forms\ReviewForm;

class HomepagePresenter extends \App\FrontModule\Presenters\HomepagePresenter
{
	/** @var \CP\Services\ReviewsService @inject */
	public $reviewsService;

	/** @var \CP\Services\Blog\ArticlesService @inject */
	public $articlesService;

	/** Realitní poradna */
	use \App\Presenters\AdvicesPresenterTrait;

	/**
	 * Úvodní stránka
	 * @return void
	 */
	
	public function actionDefault()
	{
		$this->advicesQuestionEntity = $this->advicesQuestionsService->questionEntity()->blank();
	}

	/**
	 * Vykreslit úvodní stránku
	 * @return void
	 */

	public function renderDefault()
	{
		/**
		 * Reference
		 */
		
		$reviewsCollection = $this->reviewsService->reviewsCollection()
			->whereDomain()
			->limit(3);
		
		$this->template->reviewsCollection = $reviewsCollection->find();

		/**
		 * Nemovitosti
		 */
		
		$estatesCollection = $this->estatesService->estatesCollection()
			->whereDomain()
			->whereActive()
			->onHomepageFirst()
			->limit(4);

		$this->template->estatesCollection = $estatesCollection->find();

		/**
		 * Prodané nemovitosti
		 */
		
		$soldCollection = $this->estatesService->estatesCollection()
			->whereDomain()
			->whereSold()
			->onHomepageFirst()
			->newestFirst()
			->limit(3);

		$this->template->soldCollection = $soldCollection->find();

		/**
		 * Články blogu
		 */
		
		$articlesCollection = $this->articlesService->articlesCollection()
			->newestFirst()
			->limit(3);

		$this->template->articlesCollection = $articlesCollection->find();

		/**
		 * Realitní poradna
		 */
		
		$advicesCollection = $this->advicesArticlesService->articlesCollection()
			->newestFirst()
			->limit(3);

		$this->template->advicesCollection = $advicesCollection->find();
	}
}