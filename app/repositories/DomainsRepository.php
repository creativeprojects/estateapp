<?php

namespace Repository;

class domains extends \Repository\BaseDomainsRepository
{
	public $name = "domains";

	protected $cacheEnabled = true;

	public $fields = [
		"id" => [
			"type" => "primary",
			"deprecated" => "client_id"
		],
		"title" => [
			"type" => "varchar",
			"length" => 50,
			"deprecated" => "client_title"
		],
		"email" => [
			"type" => "email",
			"deprecated" => "client_email"
		],
		"backend" => "json",
		"components" => "json",
		"subdomain" => [
			"type" => "varchar",
			"length" => 30,
			"deprecated" => "client_subdomain"
		],
		"domain" => [
			"type" => "varchar",
			"length" => 30,
			"deprecated" => "client_domain"
		],
		"localdomain" => [
			"type" => "varchar",
			"length" => 30
		],
		"selectCountry" => [
			"type" => "bool",
			"default" => 0
		],
		"defaultCountry" => "country",
		"exportSettings" => "json",
		"importSettings" => "json"
	];

	protected $validateTable = null;

}