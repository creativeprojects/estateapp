<?php

namespace Repository;

class homepages extends \Repository\BaseRepository
{
	public $name = "homepages";

	public $validateTable = true;

	public $fields = [
		"id" => "primary",
	];
}