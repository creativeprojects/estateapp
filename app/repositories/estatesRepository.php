<?php

namespace Repository;

class estates extends \Repository\BaseRepository
{
	public $name = "estates";

	public $validateTable = true;

	public $fields = [
		"estate_id" => "primary",
		"userId" => [
			"type" => "int",
			"deprecated" => "user_id"
		],
		"external_id" => [
			"type" => "varchar",
			"length" => 30
		],
		"external_modified" => "timestamp",
		"advert_function" => "tinyint",
		"advert_lifetime" => "tinyint",
		"advert_price" => "double",
		"advert_price_original" => "double",
		"advert_price_currency" => "tinyint",
		"advert_price_unit" => "tinyint",
		"advert_type" => "tinyint",
		"advert_id" => "int",
		"advert_rkid" => [
			"type" => "varchar",
			"length" => 30
		],
		"advert_room_count" => "tinyint",
		"advert_subtype" => "tinyint",
		"balcony" => "tinyint",
		"basin" => "tinyint",
		"building_condition" => "tinyint",
		"building_type" => "tinyint",
		"cellar" => "tinyint",
		"estate_area" => "int",
		"floor_number" => "tinyint",
		"garage" => "tinyint",
		"loggia" => "tinyint",
		"object_type" => "tinyint",
		"ownership" => "tinyint",
		"parking_lots" => "tinyint",
		"project_id" => "int",
		"project_rkid" => [
			"type" => "varchar",
			"length" => 30
		],
		"seller_id" => "int",
		"seller_rkid" => [
			"type" => "varchar",
			"length" => 30
		],
		"terrace" => "tinyint",
		"usable_area" => "int",
		"acceptance_year" => "int",
		"advert_code" => [
			"type" => "varchar",
			"length" => 30
		],
		"advert_low_energy" => "tinyint",
		"advert_price_charge" => "tinyint",
		"advert_price_commission" => "tinyint",
		"advert_price_legal_services" => "tinyint",
		"advert_price_negotiation" => "tinyint",
		"advert_price_vat" => "tinyint",
		"annuity" => "int",
		"auction_date" => "datetime",
		"auction_date_tour" => "datetime",
		"auction_date_tour2" => "datetime",
		"auction_kind" => "tinyint",
		"auction_place" => [
			"type" => "varchar",
			"legnth" => 120
		],
		"auction_place_latitude" => "latitude",
		"auction_place_longitude" => "longitude",
		"balcony_area" => "int",
		"basin_area" => "int",
		"beginning_date" => "date",
		"building_area" => "int",
		"ceiling_height" => "double",
		"cellar_area" => "int",
		"cost_of_living" => "varchar",
		"easy_access" => "tinyint",
		"electricity" => "lightjson",
		"elevator" => "tinyint",
		"energy_efficiency_rating" => "tinyint",
		"energy_performance_certificate" => "tinyint",
		"energy_performance_summary" => "double",
		"extra_info" => "tinyint",
		"finish_date" => "date",
		"first_tour_date" => "datetime",
		"first_tour_date_to" => "datetime",
		"flat_class" => "tinyint",
		"floor_area" => "int",
		"floors" => "int",
		"furnished" => "tinyint",
		"garage_count" => "int",
		"garden_area" => "int",
		"garret" => "tinyint",
		"gas" => "lightjson",
		"gully" => "lightjson",
		"heating" => "lightjson",
		"loggia_area" => "int",
		"mortgage" => "tinyint",
		"mortgage_percent" => "double",
		"nolive_total_area" => "int",
		"object_age" => "int",
		"object_kind" => "tinyint",
		"object_location" => "tinyint",
		"offices_area" => "int",
		"parking" => "int",
		"personal" => "tinyint",
		"price_auction_principal" => "double",
		"price_expert_report" => "double",
		"price_minimum_bid" => "double",
		"production_area" => "int",
		"protection" => "tinyint",
		"ready_date" => "date",
		"reconstruction_year" => "int",
		"road_type" => "lightjson",
		"sale_date" => "date",
		"shop_area" => "int",
		"spor_percent" => "double",
		"steps" => "varchar",
		"store_area" => "int",
		"surroundings_type" => "tinyint",
		"telecommunication" => "lightjson",
		"terrace_area" => "int",
		"transport" => "lightjson",
		"underground_floors" => "int",
		"usable_area_ground" => "int",
		"user_status" => "tinyint",
		"water" => "lightjson",
		"workshop_area" => "int",
		"youtube_video" => "youtube",
		"advert_status" => [
			"type" => "tinyint",
			"notNull" => true,
			"default" => 0
		],
		"estate_title" => [
			"type" => "varchar",
			"locale" => true
		],
		"estate_description" => [
			"type" => "text",
			"locale" => true
		],
		"advert_price_text_note" => [
			"type" => "varchar",
			"locale" => true
		],
		"advert_price_hidden" => "bool",
		
		"advert_onhomepage" => "bool",
		"project_id" => "int",
		"contract_type" => [
			"type" => "tinyint",
			"notNull" => true,
			"default" => 0
		],
		"contract_sign_date" => "datetime",
		"contract_valid_date" => "datetime",
		"contract_exkl_valid_date" => "datetime",
		"internal_description" => "text",

		/**
		 * Lokalita
		 */
		
		"locality_country" => [
			"type" => "varchar",
			"length" => 50
		],
		"locality_country_code" => [
			"type" => "varchar",
			"length" => 2
		],
		"locality_state" => [
			"type" => "varchar",
			"length" => 40
		],
		"locality_state_uir" => [
			"type" => "int",
			"deprecated" => "id_kraj"
		],
		"locality_county" => [
			"type" => "varchar",
			"length" => 40
		],
		"locality_county_uir" => [
			"type" => "int",
			"deprecated" => "id_okres"
		],
		"locality_city" => [
			"type" => "varchar",
			"length" => 30
		],
		"locality_city_uir" => [
			"type" => "int",
			"deprecated" => "id_obec"
		],
		"locality_district" => [
			"type" => "varchar",
			"length" => 30
		],
		"locality_district_uir" => [
			"type" => "varchar",
			"length" => 10
		],
		"locality_street" => [
			"type" => "varchar",
			"length" => 40
		],
		"locality_street_uir" => [
			"type" => "varchar",
			"length" => 10
		],
		"locality" => "address",
		"locality_latitude" => "latitude",
		"locality_longitude" => "longitude",
		"locality_zip" => [
			"type" => "varchar",
			"length" => 10
		],
		"locality_inaccuracy_level" => [
			"type" => "tinyint",
			"length" => 1
		],
		"locality_ruian" => "int",
		"locality_ruian_level" => "int",
		"locality_uir" => "int",
		"locality_uir_level" => "int",
		"locality_citypart" => [
			"type" => "varchar",
			"length" => 40
		],
		"locality_co" => [
			"type" => "varchar",
			"length" => 10
		],
		"locality_cp" => [
			"type" => "varchar",
			"length" => 10
		],
		"locality_pc" => [
			"type" => "varchar",
			"length" => 15
		],
		"locality_ce" => "int",
		"locality_cj" => "int",
		"visible" => "bool"
	];

	public $indexes = [
		"broker" => [
			"columns" => ["userId"]
		],
		"type" => [
			"columns" => ["advert_type"]
		],
		"subtype" => [
			"columns" => ["advert_subtype"]
		],
		"function" => [
			"columns" => ["advert_function"]
		],
		"status" => [
			"columns" => ["advert_status"]
		],
		"project" => [
			"columns" => ["project_id"]
		],
		"onHomepage" => [
			"columns" => ["advert_onhomepage"]
		]
	];
}