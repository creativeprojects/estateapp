<?php

namespace Repository;

class syncUrbiumTasks extends \Repository\BaseRepository
{
	public $name = "sync_urbium_tasks";

	public $validateTable = true;

	public $fields = array(
		"id" => "primary",
		"external_id" => "int",
		"group" => [
			"type" => "varchar",
			"length" => 10
		],
		"operation" => [
			"type" => "varchar",
			"length" => 10
		],
		"parameters" => "json",
	);
}