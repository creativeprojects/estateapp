<?php

namespace Repository;

class demands extends \Repository\BaseRepository
{
	public $name = "demands";

	protected $validateTable = null;

	public $fields = array(
		"demand_id" => "primary",
		"userId" => [
			"type" => "int",
			"deprecated" => "user_id"
		],
		"demand_client_title" => "varchar",
		"demand_client_phone" => "varchar",
		"demand_client_email" => "varchar",
		"advert_type" => "tinyint",
		"advert_function" => "tinyint",
		"advert_subtype" => "json",
		"demand_locality" => "varchar",
		"demand_locality_radius" => "int",
		"demand_locality_latitude" => "double",
		"demand_locality_longitude" => "double",
		"demand_area_from" => "int",
		"demand_area_to" => "int",
		"demand_price_from" => "int",
		"demand_price_to" => "int",
		"demand_description" => "html",
		"demand_status" => array(
			"type" => "tinyint",
			"notNull" => true
		)
	);
}