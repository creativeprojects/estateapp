<?php

namespace Repository;

class estatesExports extends \Repository\BaseRepository
{
	public $name = "estates_exports";

	public $fields = array(
		"export_id" => "primary",
		"estate_id" => "int",
		"portal_id" => "int",
		"export_timestamp" => "timestamp",
		"last_export_timestamp" => "timestamp",
		"message" => "text",
		"url" => "varchar",
		"export" => "bool"
	);

	protected $validateTable = null;

}