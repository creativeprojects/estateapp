<?php

namespace Repository;

class advicesQuestions extends \Repository\BaseRepository
{
	public $name = "advices_questions";

	public $fields = [
		"id" => "primary",
		"name" => [
			"type" => "varchar",
			"length" => 100
		],
		"email" => "email",
		"question" => "lighthtml",
		"read" => "bool"
	];

	public $validateTable = true;
}