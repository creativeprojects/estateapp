<?php

namespace Repository;

class usersBroker extends \Repository\BaseRepository
{
	public $name = "users_broker";

	public $fields = [
		"id" => [
			"type" => "primary",
			"autoIncrement" => false,
			"deprecated" => "user_id"
		],
		"phone" => [
			"type" => "phone",
			"deprecated" => "broker_phone"
		]
	];

	public $validateTable = true;
}