<?php

namespace Repository;

class projectsUsers extends \Repository\BaseRepository
{
	public $name = "projects_brokers";

	public $fields = [
		"id" => "primary",
		"projectId" => [
			"type" => "int",
			"deprecated" => "project_id"
		],
		"userId" => [
			"type" => "int",
			"deprecated" => "user_id"
		],
		"rank" => [
			"type" => "rank",
			"deprecated" => "user_rank"
		]
	];

	public $validateTable = null;
}