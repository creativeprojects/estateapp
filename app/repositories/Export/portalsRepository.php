<?php

namespace Repository;

class portals extends \Repository\BaseRepository
{
	public $name = "portals";

	public $fields = array(
		"portal_id" => "primary",
		"portal_key" => "varchar",
		"portal_title" => "varchar",
		"portal_classname" => "varchar",
		"portal_url" => "varchar",
		"portal_active" => "bool",
		"export_broker" => "tinyint",
		"portal_settings" => "json"
	);

	protected $validateTable = null;

}