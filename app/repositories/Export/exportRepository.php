<?php

namespace Repository;

class export extends \Repository\BaseRepository
{
	public $name = "export";

	public $fields = array(
		"estate_id" => "primary",
		"images" => "timestamp",
		"images_error" => "varchar",
		"sreality" => "timestamp",
		"sreality_error" => "varchar",
		"idnes" => "timestamp",
		"idnes_error" => "text"
	);

	protected $validateTable = true;

}