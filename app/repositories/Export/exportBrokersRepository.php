<?php

namespace Repository;

class exportBrokers extends \Repository\BaseRepository
{
	public $name = "export_brokers";

	public $fields = array(
		"export_id" => "primary",
		"broker_id" => "int",
		"portal_id" => "int",
		"export_timestamp" => "timestamp",
		"last_export_timestamp" => "timestamp",
		"message" => "text",
		"url" => "varchar",
		"export" => "bool"
	);

	protected $validateTable = true;

}