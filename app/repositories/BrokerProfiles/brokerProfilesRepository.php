<?php

namespace Repository;

class brokerProfiles extends \Repository\BaseRepository
{
	public $name = "brokerprofiles";

	public $fields = [
		"id" => [
			"type" => "primary",
			"autoIncrement" => false,
			"deprecated" => "user_id"
		],
		"profile_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"profile_position" => array(
			"type" => "varchar",
			"locale" => true
		),
		"profile_description" => array(
			"type" => "html",
			"locale" => true
		),
		"profile_rank" => "tinyint"
	];

	public $validateTable = false;
}