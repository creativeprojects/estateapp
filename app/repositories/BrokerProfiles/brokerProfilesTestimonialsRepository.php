<?php

namespace Repository;

class brokerProfilesTestimonials extends \Repository\BaseRepository
{
	public $name = "brokerprofiles_testimonials";

	public $fields = array(
		"testimonial_id" => "primary",
		"userId" => [
			"type" => "int",
			"deprecated" => "user_id"
		],
		"testimonial_text" => array(
			"type" => "lighthtml",
			"locale" => true
		),
		"testimonial_author" => "varchar",
		"testimonial_rank" => "tinyint"
	);

	public $validateTable = null;
}