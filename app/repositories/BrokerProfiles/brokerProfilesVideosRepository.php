<?php

namespace Repository;

class brokerProfilesVideos extends \Repository\BaseRepository
{
	public $name = "brokerprofiles_videos";

	public $fields = array(
		"video_id" => "primary",
		"userId" => [
			"type" => "int",
			"deprecated" => "user_id"
		],
		"video_title" => array(
			"type" => "varchar",
			"locale" => true
		),
		"video_address" => "varchar",
		"video_description" => array(
			"type" => "lighthtml",
			"locale" => true
		),
		"video_code" => "youtube",
		"video_rank" => "tinyint"
	);

	public $validateTable = null;
}