<?php

namespace Repository;

class usersAdmin extends \Repository\BaseRepository
{
	public $name = "users_admin";

	public $fields = [
		"id" => [
			"type" => "primary",
			"autoIncrement" => false,
			"deprecated" => "user_id"
		]
	];

	public $validateTable = true;
}