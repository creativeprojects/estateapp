<?php

namespace Repository;

class projects extends \Repository\BaseRepository
{
	public $name = "projects";

	public $fields = [
		"id" => [
			"type" => "primary",
			"deprecated" => "project_id"
		],
		"title" => [
			"type" => "varchar",
			"locale" => true,
			"deprecated" => "project_title"
		],
		"description" => [
			"type" => "html",
			"locale" => true,
			"deprecated" => "project_description"
		],
		"address" => [
			"type" => "varchar",
			"deprecated" => "project_address"
		],
		"address_latitude" => [
			"type" => "double",
			"deprecated" => "project_address_latitude"
		],
		"address_longitude" => [
			"type" => "double",
			"deprecated" => "project_address_longitude"
		],
		"established" => [
			"type" => "date",
			"deprecated" => "project_established"
		],
		"finished" => [
			"type" => "date",
			"deprecated" => "project_finished"
		],
		"status" => [
			"type" => "varchar",
			"notNull" => true,
			"default" => "new",
			"deprecated" => "project_status"
		],
		"rank" => [
			"type" => "rank",
			"deprecated" => "project_rank"
		],
		"youtubeCode" => [
			"type" => "youtube",
			"deprecated" => "project_youtube_code"
		]
	];

	public $validateTable = null;
}