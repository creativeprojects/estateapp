<?php

define("APP_DIR", __DIR__);
define("WWW_DIR", realpath(__DIR__ . "/../www"));
define("VENDOR_DIR", realpath(APP_DIR . '/../vendor'));
define("TEMP_DIR", realpath(APP_DIR . '/../temp'));
define("CACHE_DIR", TEMP_DIR . '/cache');
define("LOG_DIR", realpath(APP_DIR . '/../log'));
define("CONFIG_DIR", APP_DIR . "/config");

define("SUBDOMAINS_CONFIG_PATH", CONFIG_DIR . "/parameters/parameters.subdomains.neon");
define("LOCALHOST_CONFIG_PATH", CONFIG_DIR . "/parameters/parameters.localhost.neon");
define("DEFAULT_CONFIG_PATH", CONFIG_DIR . "/config.neon");

if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	define("CP_DIR", realpath(APP_DIR . "/../../cp"));
} else {
	define("CP_DIR", VENDOR_DIR . "/creativeprojects/cp");
}

require VENDOR_DIR . '/autoload.php';

$configurator = new Nette\Configurator;

require CP_DIR . '/bootstrap.php';

return $container;
