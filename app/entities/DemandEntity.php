<?php

namespace App\Entities;

use CP\Utils\ArrayResult;

/**
 * @Repository App\Repositories\DemandsRepository
 * @Service demands
 */

class DemandEntity extends \CP\Entities\BaseEntity
{
	/**
	 * ID poptávky
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 */

	protected $demand_id;

	/**
	 * ID uživatele, kterému poptávka patří
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $userId;

	/**
	 * Jméno a příjmení poptívajícího
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $demand_client_title;

	/**
	 * Telefon poptávajícího
	 * @Property
	 * @Column (type = "varchar", length = 20)
	 */

	protected $demand_client_phone;

	/**
	 * Email poptávajícího
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $demand_client_email;

	/**
	 * Druh nemovitosti
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_type;

	/**
	 * Druh obchodu (prodej / pronájem)
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_function;

	/**
	 * Upřesňující druh nemovitosti
	 * @Property
	 * @Column (type = "json")
	 */

	protected $advert_subtype;

	/**
	 * Adresa, kde by poptávající chtěl bydlet
	 * @Property \CP\Entities\Properties\AddressProperty
	 * @Column (type = "address")
	 * @Radius
	 */

	protected $demand_locality;
	
	/**
	 * Plocha od
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $demand_area_from;

	/**
	 * Plocha do
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $demand_area_to;

	/**
	 * Cena od
	 * @Property
	 * @Column (type = "price")
	 */

	protected $demand_price_from;

	/**
	 * Cena do
	 * @Property
	 * @Column (type = "price")
	 */

	protected $demand_price_to;

	/**
	 * Doplňující informace k poptávce
	 * @Property \CP\Entities\Properties\HtmlProperty
	 * @Column (type = "lighthtml")
	 */

	protected $demand_description;

	/**
	 * Stav poptávky
	 * @Property
	 * @Column (type = "tinyint", notNull = true)
	 */

	protected $demand_status;

	/**
	 * @Property App\Entities\UserEntity
	 */

	protected $userEntity;

	/**
	 * Naplnit hodnotami
	 * @param mixed $values
	 * @return this
	 */

	public function create($values = null)
	{
		parent::create($values);

		$this->getUserEntity()->find($this->getUserId());

		return $this;
	}

	/**
	 * Získání poptávky podle ID
	 * @param $demand_id ID poptávky, kterou chci získat
	 * @return $this|bool
     */

	public function find($demand_id)
	{
		if(is_numeric($demand_id)) {
			$values = $this->service->repository->demands->getOne($demand_id);

			if ($values) {
				$this->create($values);

				return $this;
			}
		}

		return false;
	}

	/**
	 * Uložit poptávku
	 * @param array|Traversable|null $values
	 * @return bool
     */

	public function save($values = null)
	{
		if (!empty($values)) {
			$this->set($values);
		}

		//Pokud je poptávka přiřazena makléři, potom může být rovnou aktivní, pokud ne, musí ji admin přiřadit
		if ($this->getUserId()) {
			$this->demand_status->set(1);
		} else {
			$this->demand_status->set(0);

			$this->addAssignDemandNotification();
		}	

		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->demands->save($values)) {
			$this->set($values);

			return true;
		}

		return false;
	}

	/**
	 * Soft-delete poptávky
	 * @return bool
	 * @throws \Exception
     */

	public function remove()
	{
		if($this->getId()){
			return $this->service->repository->demands->removeOne($this->getId());
		} else {
			throw new \Exception("Can not remove DemandEntity without demand_id!");
		}
	}

	/**
	 * @param tinyint $demand_status
	 * @return mixed
	 */

	public function saveStatus($demand_status)
	{
		return $this->service->repository->demands->saveField($this->getId(), "demand_status", $demand_status);
	}

	/**
	 * @param \Nette\Security\User $user
	 * @return bool
     */

	public function isEditAllowed(\Nette\Security\User $user)
	{
		$isAllowed = new ArrayResult;

		$isAllowed->is($user->isAllowed('Admin:Demands', 'edit'));

		$isAllowed->is($this->isMy($user));

		return $isAllowed->result();
	}

	/**
	 * @param \Nette\Security\User $user
	 * @return bool
     */

	public function isRemoveAllowed(\Nette\Security\User $user)
	{
		$isAllowed = new ArrayResult;

		$isAllowed->is($user->isAllowed('Admin:Demands', 'remove'));

		$isAllowed->is($this->isMy($user));

		return $isAllowed->result();
	}

	/**
	 * @param \Nette\Security\User $user
	 * @return bool
     */

	public function isMy(\Nette\Security\User $user)
	{
		return $this->getCreated()->getUserId() == $user->getId() || $user->getAuthorizator()->roleInheritsFrom($user->getIdentity()->role, $this->getUserEntity()->getRole());
	}

	protected function addAssignDemandNotification()
	{
		//TODO - toto vytvoříme až to bude potřeba
	}
}