<?php

namespace App\Entities;

use CP\Utils\ArrayResult;
use CP\Utils\Cache;

/**
 * @Repository App\Repositories\EstatesRepository
 * @Service estates
 */

class EstateEntity extends \CP\Entities\BaseEntity
{
	/** Vlastnosti a a metody pro nastavení viditelnosti */
	use \CP\Entities\BaseEntity\VisibilityTrait;

	/** Základní vlastnosti */
	use EstateEntity\CommonTrait;

	/** PDF karta nemovitosti */
	use EstateEntity\PdfTrait;

	/** Plochy */
	use EstateEntity\AreaTrait;

	/** Dražba */
	use EstateEntity\AuctionTrait;

	/** Smlouva */
	use EstateEntity\ContractTrait;

	/** Počty */
	use EstateEntity\CountTrait;

	/** Datumy */
	use EstateEntity\DatesTrait;

	/** Energetický štítek */
	use EstateEntity\EnergyTrait;

	/** Lokalita */
	use EstateEntity\LocalityTrait;

	/** Budova */
	use EstateEntity\ObjectTrait;

	/** Cena */
	use EstateEntity\PriceTrait;

	/** Vlastnosti */
	use EstateEntity\PropertiesTrait;

	/** Export na Sreality */
	use EstateEntity\SrealityTrait;

	/** Titulek nemovitosti */
	use EstateEntity\TitleTrait;

	/** Obrázky */
	use EstateEntity\ImagesTrait;

	/** Makléř */
	use EstateEntity\UserTrait;

	/** Případ */
	use EstateEntity\CaseTrait;

	/** Exporty */
	use EstateEntity\ExportsTrait;

	/** Přílohy */
	use EstateEntity\AttachmentsTrait;

	/**
	 * Zakázka - ID
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 * @SlugParameter
	 */

	protected $estate_id;

	/** ??
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $user_status;

	/**
	 * Externí ID - pro synchronizaci při importu
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $external_id;

	/**
	 * Časová značna poslední modifikace v externím zdroji (Urbium apod.)
	 * @Property
	 * @Column (type = "timestamp")
	 */

	protected $external_modified;

	/**
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $extra_info;

	/** Převod do osobního vlastnictví? - nevím
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $personal;
	
	/** Spoření - ??
	 * @Property
	 * @Column (type = "double")
	 */

	protected $spor_percent;
	
	/** ??
	 * @Property
	 * @Column (type = "varchar")
	 */

	protected $steps;

	/** Youtube Video Code
	 * @Property \CP\Entities\Properties\YouTubeCodeProperty
	 * @Column (type = "varchar", length = 50)
	 */

	protected $youtube_video;

	/** Developerský projekt - ID
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $project_id;

	/** Interní - popis
	 * @Property CP\Entities\Properties\HtmlProperty
	 * @Column (type = "html")
	 */

	protected $internal_description;

	/**
	 * @Property App\Entities\ProjectEntity
	 */

	protected $estate_project;

	/**
	 * @Property CP\Entities\SlugEntity
	 */

	protected $estate_slug;

	/**
	 * Vytvořit novou nemovitost
	 * @return this
	 */

	public function blank()
	{
		$this->create($this->service->settings["defaults"]);

		return $this;
	}

	/**
	 * Naplnit hodnotami z repozitáře
	 * @param mixed $values
	 * @return this
	 */

	public function create($values = null)
	{
		parent::create($values);
		
		if ($this->project_id->get()) {
				$this->estate_project->find($this->project_id->get());
			}

		
		$this->estate_pdf->findByOwner();

		$this->estate_user->find($this->getUserId());

		$this->estate_images->findByOwner();

		$this->estate_slug->findByOwner();

		if (!$this->isInCollection()) {
			$this->findExports();

			$this->findAttachments();
		}

		return $this;
	}

	/**
	 * Najít nemovitosti podle ID
	 * @param integer $estate_id
	 * @param bool $includeRemoved
	 * @return this|null
	 */
	
	public function find($estate_id, $includeRemoved = false)
	{
		if (is_numeric($estate_id)) {
			$by = [
				"includeRemoved" => $includeRemoved,
				"where" => [
					"estate_id" => $estate_id
				]
			];

			return $this->findBy($by);
		}

		return null;
	}

	/**
	 * Získat nemovitosti podle externího ID - např. Urbium, apod.
	 * TODO - jak řešit smazané nemovitosti? obnovu?
	 * @param string $external_id
	 * @return this|null
	 */

	public function findByExternalId($external_id)
	{
		if (!empty($external_id)) {
			return $this->findBy(["external_id" => $external_id]);
		}

		return null;
	}

	/**
	 * Najít nemovitost podle zadaných kritérií
	 * @param array $by
	 * @return this|false
	 */

	protected function findBy($by)
	{
		$values = $this->service->repository->estates->getOneBy($by);

		if ($values) {
			return $this->create($values);
		}

		return null;
	}

	/**
	 * Uložit nemovitost
	 * @param array|Traversable|null $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);

		//Pokud je nová cena stejná nebo vyšší než původní, původní cenu, která se jinak uvádí jako sleva na stránkách, vynulovat
		if ($this->getId() && $this->advert_price->get() >= $this->advert_price_original->get()) {
			$this->advert_price_original->set(null);
		}

		$this->validateLocality();

		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->estates->save($values)) {
			$this->set($values);

			$result = new ArrayResult;

			$result->is($this->saveAttachments());

			$result->is($this->saveExports());

			$result->is($this->estate_images->save());

			$result->is($this->estate_slug->save());

			Cache::remove("estates");

			return $result->result();
		}

		return false;
	}

	/**
	 * Uložit zobrazení na úvodní stránce
	 * @param bool $advert_onhomepage
	 * @return bool
	 */

	public function saveOnHomepage($advert_onhomepage)
	{
		$advert_onhomepage = $advert_onhomepage ? true : false;

		$this->setAdvert_onhomepage($advert_onhomepage);

		return $this->save();
	}

	/**
	 * Soft-delete nemovitosti
	 * @return bool
	 * @throws Exception
	 */

	public function remove()
	{
		if ($this->getId()) {
			if ($this->service->repository->estates->removeOne($this->getId())) {
				$this->removeAttachments();

				$this->estate_images->remove();

				$this->removeExports();

				$this->removeAttachments();

				$this->estate_slug->remove();

				Cache::remove("estates");

				return true;
			}

			return false;
		} else {
			throw new \Exception("Can not remove EstateEntity without estate_id!");
		}
	}

	/**
	 * Nastavit nemovitost jako prodanou/pronajmutou a uložit
	 * Používáme při importu
	 * Nejsme schopni zjistit co přesně se s nemovitostí stalo, bereme že nemovitost byla automaticky prodána / pronajmuta, to bude asi nejčastější případ
	 * Pokud nebyl obchod získán, tak se musí ručně stav upravit u zakázky
	 * @return bool
	 */

	public function archive()
	{
		//$this->setAdvert_status(4); //Nastavit stav jako prodáno/pronajmuto podle nastavení v parameters.estatesParameters.neon

		//return $this->save();

		//TODO - zatím řešíme takto protože v kolekci nemáme načtené obrázky a když zavoláme ::save() tak to všechny smaže
		return $this->service->repository->estates->saveField($this->getId(), "advert_status", 4);
	}

	/**
	 * Je nemovitosti viditelná pro veřejnost?
	 * @param Nette\Secutiry\User $user
	 * @return bool
	 */

	public function isVisible($user, $visibleTypes = [1,2])
	{
		$logged = $user->isInRole("broker") || $user->isInRole("admin") || $user->isInRole("callcentrum") || $user->isInRole("superAdmin");
		
		$published = in_array($this->advert_status->get(), $visibleTypes) && $this->getVisible();

		if ($published) {
			return true;
		} elseif($logged) {
			return "logged";
		} else {
			return false;
		}
	}

	/**
	 * Filtrování zobrazovaných detailních informací nemovitosti podle obsahu a typu nemovitosti
	 * @return array
	 */

	public function filterVisibleInformation()
	{
		$group = [];

		//Projít všechny možnosti informací k nemovitosti a vyfiltrovat ty, které bychom měli zobrazit
		foreach ($this->service->estatesParameters AS $key => $field) {
			if ($this->isInformationVisible($key, $field)) {
				$group[$field["group"]][$key] = $field;
			}
		}

		//Nejedná se o byt a nemá výtah, nemá význam tuto informaci zobrazit
		if ($this->advert_type->get() != 1 && $this->elevator->get() == 2){
			unset($group["properties"]["elevator"]);
		}

		//Nejedná se o byt, nemusíme zobrazovat informaci o vlastnictví a převodu do OV
		if ($this->advert_function->get() != 1) {
			unset($group["common"]["ownership"]);
			unset($group["common"]["personal"]);
		}

		return $group;
	}

	/**
	 * Má být tato konkrétní informace viditelná?
	 * @param string $key klíč pole v estatesParameters
	 * @param array $field vlastnosti pole v estatesParameters
	 * @return bool
	 */

	protected function isInformationVisible($key, $field)
	{
		if ($this->isProperty($key) && !empty($this->$key->get())) { //Vlastnost EstateEntity existuje a není prázdná
			
			$isVisible = new ArrayResult;

			//Informace má v estatesParameters nastavenu skupinu, ve které se má zobrazit
			$isVisible->is(@$field["group"]);

			//Je tato informace viditelná pro daný typ nemovitosti
			$isVisible->is(in_array($key, $this->service->estatesParameters["advert_type"]["toggle"][$this->advert_type->get()]));

			//Jedná se o soubor, zkontrolovat jetli existuje
			if ($this->$key->get() instanceof \CP\Entities\Files\FileEntity) {
				$isVisible->is($this->estateEntity->$key->exists());
			}

			//Jedná se o kolekci či pole, spočítat položky
			if ($this->$key->get() instanceof \CP\Collections\BaseCollection || $this->$key->get() instanceof \Nette\Utils\ArrayHash) {
				$isVisible->is($this->$key->get()->count());
			}

			if ($key == "personal") { //Převod do OV zobrazit pouze pokud se již nejedná o osobní vlastnictví
				$isVisible->is($this->getOwnership() != 1);
			}

			return $isVisible->result();
		} else {
			return false;
		}
	}
}