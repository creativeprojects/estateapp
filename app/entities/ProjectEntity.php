<?php

namespace App\Entities;

use CP\Utils\ArrayResult;
use CP\Utils\Cache;

/**
 * @Repository App\Repositories\ProjectsRepository
 * @Service projects
 */

class ProjectEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", "key" => "PRI", autoIncrement = true)
	 * @SlugParameter
	 */

	protected $id;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 * @Localized
	 */

	protected $title;

	/**
	 * @Property CP\Entities\Properties\LocalizedHtmlProperty
	 * @Column (type = "html")
	 * @Localized
	 */

	protected $description;

	/**
	 * @Property CP\Entities\Properties\AddressProperty
	 */

	protected $address;
	
	/**
	 * @Property
	 * @Column (type = "date")
	 */

	protected $established;

	/**
	 * @Property
	 * @Column (type = "date")
	 */

	protected $finished;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 10)
	 */

	protected $status;

	/**
	 * @Property
	 * @Column (type = "rank")
	 */

	protected $rank;

	/**
	 * @Property CP\Entities\Properties\BooleanProperty
	 * @Column (type = "bool")
	 */

	protected $published;
	
	/**
	 * @Property CP\Entities\SlugEntity
	 */

	protected $slugEntity;

	/**
	 * @Property CP\Collections\Files\ImagesCollection
	 */

	protected $imagesCollection;

	/**
	 * @Property
	 * @Column (type = "youtube")
	 */

	protected $youtubeCode;

	/**
	 * @Property App\Collections\Projects\UsersCollection
	 */

	protected $usersCollection;

	/**
	 * Najít projekt podle ID
	 * @param integer $id
	 * @return this|null
	 * @throws Exception
	 */

	public function find($id)
	{
		if (is_numeric($id)) {
			$values = $this->service->repository->projects->getOne($id);

			if ($values) {
				$this->create($values);

				$this->getSlugEntity()->findByOwner();

				$this->getImagesCollection()->findByOwner();

				$this->getUsersCollection()->findByOwner();

				return $this;
			}

			return null;
		} else {
			throw new \Exception("Invalid ID: " . var_export($id, true) . " in '{$this->getReflection()->name}'");
		}
	}

	/**
	 * Uložit projekt
	 * @param mixed @values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);
		
		$this->setRank();

		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->projects->save($values)) {
			$this->set($values);

			Cache::remove("projects");

			$result = new ArrayResult;

			$result->is($this->getImagesCollection()->save());

			$result->is($this->getUsersCollection()->save());

			$result->is($this->getSlugEntity()->save());

			return $result->result();
		}

		return false;
	}

	/**
	 * Odebrat projekt
	 * @return bool
	 */

	public function remove()
	{
		if ($this->getId()) {
			Cache::remove("projects");
			
			if ($this->service->repository->projects->removeOne($this->getId())) {
				$result = new ArrayResult;

				$result->is($this->getImagesCollection()->remove());

				$result->is($this->getUsersCollection()->remove());

				$result->is($this->getSlugEntity()>remove());

				return $result->result();
			}

			return false;
		} else {
			throw new \Exception("Can not remove ProjectEntity without id!");
		}
	}

	/**
	 * Uložit publikování projektu
	 * @param bool $published
	 * @return bool
	 */

	public function savePublished($published)
	{
		$this->setPublished($published);

		return $this->save();
	}

	/**
	 * Posunout nahoru
	 * @return bool
	 */

	public function moveUp()
	{
		Cache::remove("projects");

		return $this->move(-1, [], $this->service->repository->projects, "id", "rank");
	}

	/**
	 * Posunout dozadu
	 * @return bool
	 */

	public function moveDown()
	{
		Cache::remove("projects");
		
		return $this->move(1, [], $this->service->repository->projects, "id", "rank");
	}

	/**
	 * Nastavit pořadí projektu
	 * @return this
	 */

	protected function setRank()
	{
		if (!$this->getRank()) {
			$this->rank->set($this->getMaxRank() + 1);
		}

		return $this;
	}

	/**
	 * Nejvyšší pořadí projektu
	 * @return integer
	 */

	protected function getMaxRank()
	{
		return $this->service->database->table($this->service->repository->projects->getName())
			->max("rank");
	}

	/**
	 * Je povolena editace?
	 * @param Nette\Security\User $user
	 * @return bool
	 */

	public function isEditAllowed($user)
	{
		$result = new ArrayResult;

		$result->is($user->isAllowed("Admin:Projects", "edit"));

		$result->is($this->isMy($user));

		return $result;
	}

	/**
	 * Je povoleno odebrání?
	 * @param Nette\Security\User $user
	 * @return bool
	 */
	
	public function isRemoveAllowed($user)
	{
		$result = new ArrayResult;

		$result->is($user->isAllowed("Admin:Projects", "remove"));

		$result->is($this->isMy($user));

		return $result;
	}

	/**
	 * Je projekt můj, nebo na něj mám oprávnění?
	 * @param Nette\Security\User $user
	 * @return bool
	 */
	
	
	public function isMy($user)
	{
		//TODO - hlídat klienta
		//TODO - IsMySubordinate podle userId místo role
		return empty($this->getCreated()->getUserId()) || $user->getId() == $this->getCreated()->getUserId();// || $user->getAuthorizator->isMySubordinate();
	}

	/**
	 * Získat čitelný stav projektu
	 * @return string
	 */
	
	public function getReadableStatus()
	{
		return $this->service->settings["statusOptions"][$this->getStatus()];
	}

	/**
	 * Získat možnosti stavu projektu
	 * @return array
	 */

	public function getStatusOptions()
	{
		return $this->service->settings["statusOptions"];
	}
}