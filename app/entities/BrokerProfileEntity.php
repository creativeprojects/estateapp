<?php

namespace App\Entities;

use CP\Utils\ArrayResult;

/**
 * @Repository App\Repositories\BrokerProfilesRepository
 * @Service brokerProfiles
 */

class BrokerProfileEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = false)
	 * @SlugParameter
	 * @Id
	 */

	protected $id;

	/**
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "varchar", length = 100)
	 * @SlugLabel
	 * @Localized
	 */
		
	protected $profile_title;

	/**
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "varchar", length = 100)
	 * @Localized
	 */
		
	protected $profile_position;
	
	/**
	 * @Property \CP\Entities\Properties\LocalizedHtmlProperty
	 * @Column (type = "html")
	 * @Localized
	 */
		
	protected $profile_description;

	/**
	 * @Property
	 * @Column (type = "tinyint")
	 */
		
	protected $profile_rank;

	/**
	 * @Property \CP\Collections\Files\ImagesCollection
	 */

	protected $profile_broker_images;

	/**
	 * @Property \CP\Collections\Files\ImagesCollection
	 */

	protected $profile_estates_images;

	/** 
	 * @Property \App\Collections\BrokerProfiles\TestimonialsCollection 
	 */

	protected $profile_testimonials;

	/** 
	 * @Property \App\Collections\BrokerProfiles\VideosCollection 
	 */

	protected $profile_videos;

	/** 
	 * @Property \CP\Entities\SlugEntity 
	 * @SlugAction "BrokerProfiles:detail"
	 */

	protected $profile_slug;
	
	/** Pseudoproperties */

	/** @Property \CP\Entities\Files\ImageEntity */

	protected $profile_mainimage;

	/** @Property \App\Entities\UserEntity */

	protected $profile_user;

	/** @Property */

	protected $activeRegions;

	/**
	 * Vytvořit novou profilovou stránku makléře
	 * @return this
	 */

	public function blank()
	{
		$this->create($this->settings["defaults"]);

		return $this;
	}

	/**
	 * Najít profilovou stránku
	 * @param integer $id
	 * @return this|null
	 * @throws Exception
	 */

	public function find($id)
	{
		if (!is_numeric($id)) {
			throw new \Exception("Invalid ID: " . print_r($id, true));
		}

		$query = [
			"where" => [
				"id" => $id
			],
			"additionalFields" => [
				"(SELECT CONCAT('[\"', GROUP_CONCAT(locality_county SEPARATOR '\",\"'), '\"]') FROM {$this->service->repository->estates} AS e WHERE e.userId = {$id}) AS activeRegions"
			]
		];

		$values = $this->service->repository->brokerProfiles->getOneBy($query);

		if ($values) {
			//TODO - toto pak řešit asi jako JSON property
			$values["activeRegions"] = json_decode($values["activeRegions"], true);

			$this->create($values);

			$this->profile_broker_images->findByOwner();

			$this->profile_estates_images->findByOwner();

			$this->profile_videos->findByParent();

			$this->profile_testimonials->findByParent();

			$this->profile_slug->findByOwner();

			$this->profile_user->find($this->getId());

			return $this;
		}
		
		return null;
	}

	public function save($values = null)
	{
		if(!empty($values)){
			$this->set($values);
		}

		$this->setRank();

		$values = $this->getRepositoryValues();

		if($values = $this->service->repository->brokerProfiles->save($values)){
			$this->set($values);
			
			$result = new ArrayResult;

			$result->is($this->profile_broker_images->save());

			$result->is($this->profile_estates_images->save());

			$result->is($this->profile_videos->save());

			$result->is($this->profile_testimonials->save());

			$result->is($this->profile_slug->save());

			return $result->result();
		}

		return $profile;
	}

	/**
	 * Odebrat profilovou stránku makléře
	 * @return bool
	 * @throws Exception
	 */

	public function remove()
	{
		if ($this->getId()) {
			if ($this->service->repository->brokerProfiles->removeOne($this->getId())) {
				$result = new ArrayResult;

				$result->is($this->profile_videos->remove());

				$result->is($this->profile_testimonials->remove());

				$result->is($this->profile_broker_images->remove());

				$result->is($this->profile_estates_images->remove());

				return $result->result();
			}

			return false;
		} else {
			throw new \Exception("Can not remove BrokerProfileEntity without id!");
		}
	}

	public function moveUp()
	{
		return $this->move(-1, [], $this->service->repository->brokerProfiles, "id", "profile_rank");
	}

	public function moveDown()
	{
		return $this->move(1, [], $this->service->repository->brokerProfiles, "id", "profile_rank");
	}

	protected function setRank()
	{
		if(!$this->profile_rank->get()){
			$this->profile_rank->set($this->getMaxRank() + 1);
		}
	}

	protected function getMaxRank()
	{
		$maxRank = $this->service->repository->brokerProfiles->getMaxBy("profile_rank", ["domainId" => $this->getDomainId()]);

		return $maxRank;
	}

	public function isEditAllowed($user, $acl = null)
	{
		$result = new ArrayResult;

		$result->is($this->isMy() || $user->getAuthorizator()->isMySubordinate($user, $this->profile_user->get()->role));

		return $result->result();
	}

	public function isMoveAllowed($user, $acl = null)
	{
		$result = new ArrayResult;

		$result->is($this->isMy() || $user->getAuthorizator()->isMySubordinate($user, $this->profile_user->get()->role));

		return $result->result();
	}

	public function isRemoveAllowed($user, $acl = null)
	{
		$result = new ArrayResult;

		$result->is($this->isMy() || $user->getAuthorizator()->isMySubordinate($user, $this->profile_user->get()->role));

		return $result->result();
	}

	public function isMy()
	{
		return $this->getCreated()->getUserId() == $this->service->user->getId() || $this->getId() == $this->service->user->getId();
	}
}