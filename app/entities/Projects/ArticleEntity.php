<?php

namespace App\Entities\Projects;

/**
 * @Repository App\Repositories\Projects\ArticlesRepository
 * @Service projectsArticles
 */

class ArticleEntity extends \CP\Entities\ArticleEntity
{
	/**
	 * Vytvořit nový článek
	 * TODO - Až dokončíme Repository tak můžeme dát pryč
	 * @return this
	 */

	public function build()
	{
		$this->setRepository($this->service->repository->projectsArticles);

		return parent::build();
	}
}