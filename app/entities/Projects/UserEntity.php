<?php

namespace App\Entities\Projects;

/**
 * @Repository App\Repositories\Projects\UsersRepository
 */

class UserEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", "key" => "PRI", autoIncrement = true)
	 */

	protected $id;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $projectId;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $userId;

	/**
	 * @Property
	 * @Column (type = "rank")
	 */

	protected $rank;

	/**
	 * @Property App\Entities\UserEntity
	 */

	protected $userEntity;

	/**
	 * Naplnit entitu hodnotami
	 * @param mixed $values
	 * @return this
	 */

	public function create($values = null)
	{
		parent::create($values);

		$this->getUserEntity()->find($this->getUserId());

		return $this;
	}

	/**
	 * Uložit užavatele projektu
	 * @param mixed $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);
		
		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->projectsUsers->save($values)) {
			$this->set($values);

			return true;
		}

		return false;
	}

	/**
	 * Odebrat uživatele projektu
	 * @return bool
	 */

	public function remove()
	{
		if ($this->getId()) {
			if ($this->service->repository->projectsUsers->removeOne($this->getId())) {
				return true;
			}

			return false;
		} else {
			throw new \Exception("Can not remove UserEntity without id!");
		}
	}
}