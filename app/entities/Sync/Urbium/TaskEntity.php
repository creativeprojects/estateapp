<?php

namespace App\Entities\Sync\Urbium;

class TaskEntity extends \CP\Entities\BaseEntity
{
	/**
	 * ID úlohy
	 * @Property CP\Entities\Properties\IdProperty
	 * @Id
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 */

	protected $id;

	/**
	 * Externí ID pro úlohu
	 * @Property
	 * @Column (type = "varchar", length = 20)
	 */

	protected $external_id;

	/**
	 * Skupina [brokers,estates]
	 * @Property
	 * @Column (type = "varchar", length = 10)
	 */

	protected $group;

	/**
	 * Operace [add,update]
	 * TODO - možná by tu nemuselo být a prostě ukládat podle external_id
	 * @Property
	 * @Column (type = "varchar", length = 10)
	 */

	protected $operation;

	/**
	 * Parametry úlohy
	 * @Property CP\Entities\Properties\JsonProperty
	 * @Column (type = "json")
	 */

	protected $parameters;

	/**
	 * @param string $group
	 * @param integer $external_id
	 * @return TaskEntity|bool
	 */

	public function find($group, $external_id)
	{
		if(!empty($group) && !empty($external_id)){
			return $this->findBy([
				"where" => [
					"group" => $group,
					"external_id" => $external_id
				]
			]);
		}

		return false;
	}

	/**
	 * Získat jednu aktivní úlohu
	 * @return $this|bool
	 */

	public function findOne()
	{
		return $this->findBy([
			"where" => [
				"domainId" => $this->service->thisDomainEntity->getId()
			],
			"order" => "created ASC"
		]);
	}

	/**
	 * @param array $by
	 * @return $this
	 */

	protected function findBy($by)
	{
		$values = $this->service->repository->syncUrbiumTasks->getOneBy($by);

		if($values){
			$this->create($values);

			return $this;
		}

		return false;
	}

	/**
	 * Uložit úlohu
	 * @param array|traversable\null $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$values = $this->getRepositoryValues();

		if($values = $this->service->repository->syncUrbiumTasks->save($values)){
			$this->set($values);

			return true;
		}

		return false;
	}

	/**
	 * Odstranění úlohy z uložiště
	 * @return bool
	 * @throws \Exception
	 */

	public function delete()
	{
		if($this->getId()){
			return $this->service->repository->syncUrbiumTasks->deleteOne($this->getId());
		} else {
			throw new \Exception("Can not remove TaskEntity without id!");
		}
	}
}