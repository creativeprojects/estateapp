<?php

namespace App\Entities\BrokerProfiles;

/**
 * @Repository App\Repositories\BrokerProfiles\VideosRepository
 * @Service brokerProfiles
 */

class VideoEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 */

	protected $video_id;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */
		
	protected $userId;
	
	/**
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "varchar", length = 100)
	 * @Localized
	 */
		
	protected $video_title;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */
		
	protected $video_address;

	/**
	 * @Property \CP\Entities\Properties\LocalizedHtmlProperty
	 * @Column (type = "lighthtml")
	 * @Localized
	 */
		
	protected $video_description;

	/**
	 * @Property
	 * @Column (type = "youtube")
	 */
		
	protected $video_code;

	/**
	 * @Property
	 * @Column (type = "tinyint")
	 */
		
	protected $video_rank;

	/**
	 * Vytvořit prázdnou entitu videa
	 * @param mixed $values
	 * @return this
	 */

	public function blank($values = null)
	{
		$this->create($this->service->settings["defaults"]);

		return $this;
	}

	/**
	 * Uložit video
	 * @param mixed $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);
		
		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->brokerProfilesVideos->save($values)) {
			$this->set($values);

			return true;
		}

		return false;
	}
}