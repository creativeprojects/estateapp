<?php

namespace App\Entities\BrokerProfiles;

/**
 * @Repository App\Repositories\BrokerProfiles\TestimonialsRepository
 * @Service brokerProfiles
 */

class TestimonialEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 */

	protected $testimonial_id;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */
		
	protected $userId;

	/**
	 * @Property \CP\Entities\Properties\LocalizedHtmlProperty
	 * @Column (type = "lighthtml")
	 * @Localized
	 */
		
	protected $testimonial_text;

	/**
	 * @Property
	 * @Column (type = "varchar")
	 */
		
	protected $testimonial_author;

	/**
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $testimonial_rank;

	/**
	 * Uložit referenci
	 * @param mixed $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);
		
		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->brokerProfilesTestimonials->save($values)) {
			$this->set($values);

			return true;
		}

		return false;
	}
}