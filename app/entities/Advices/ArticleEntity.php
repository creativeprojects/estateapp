<?php

namespace App\Entities\Advices;

/**
 * @Repository App\Repositories\Advices\ArticlesRepository
 * @Service advicesArticles
 */

class ArticleEntity extends \CP\Entities\ArticleEntity
{
	/**
	 * Vytvořit nový článek
	 * TODO - Až dokončíme Repository tak můžeme dát pryč
	 * @return this
	 */

	public function build()
	{
		$this->setRepository($this->service->repository->advicesArticles);

		return parent::build();
	}
}