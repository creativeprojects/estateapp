<?php

namespace App\Entities\Advices;

/**
 * @Repository App\Repositories\Advices\QuestionsRepository
 * @Service advicesQuestions
 */

class QuestionEntity extends \CP\Entities\BaseEntity
{
	/**
	 * ID otázky
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 */

	protected $id;

	/**
	 * Jméno tazatele
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */
	
	protected $name;

	/**
	 * Email tazatele
	 * @Property \CP\Entities\Properties\EmailProperty
	 * @Column (type = "email")
	 */

	protected $email;

	/**
	 * Text dotazu
	 * @Property \CP\Entities\Properties\LightHtmlProperty
	 * @Column (type = "lighthtml")
	 */

	protected $question;

	/**
	 * Bylo již přečteno?
	 * @Property \CP\Entities\Properties\DatetimeProperty
	 * @Column (type = "datetime")
	 */

	protected $read;

	/**
	 * Vytvořit prázdnout otázku
	 * @return this
	 */

	public function blank()
	{
		$this->create($this->service->settings["defaults"]);

		return $this;
	}

	/**
	 * Najít článek podle ID
	 * @param integer $id
	 * @return this|null
	 * @throws Exception
	 */

	public function find($id)
	{
		if (is_numeric($id)) {
			return $this->findBy(["id" => $id]);
		} else {
			throw new \Exception("Invalid QuestionEntity ID: " . print_r($id));
		}
	}

	/**
	 * Najít otázku podle zadaných kritérií
	 * @param array $parameters
	 * @return this|null
	 */

	protected function findBy($parameters)
	{
		$values = $this->service->repository->advicesQuestions->getOneBy($parameters);

		if ($values) {
			$this->create($values);

			return $this;
		}

		return null;
	}

	/**
	 * Uložit otázku
	 * @param mixed $values
	 * @return bool
	 */

	public function save($values = null)
	{
		$this->set($values);

		$values = $this->getRepositoryValues();

		if ($values = $this->service->repository->advicesQuestions->save($values)) {
			$this->set($values);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Soft-delete otázky
	 * @return bool
	 * @throws Exception
	 */

	public function remove()
	{
		if (!$this->getId()) {
			throw new \Exception("Can not remove QuestionEntity without id!");
		}

		if ($this->repository->removeOne($this->getId())) {
			return true;
		} else {
			return false;
		}
	}
}