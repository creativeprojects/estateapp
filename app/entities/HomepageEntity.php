<?php

namespace App\Entities;

use CP\Utils\ArrayResult;

/**
 * @Repository App\Repositories\HomepagesRepository
 * @Service homepage
 */

class HomepageEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 */

	protected $id;

	/**
	 * @Property CP\Entities\SlugEntity
	 */

	protected $slug;

	public function find()
	{
		$values = $this->service->repository->homepages->getOneBy(["domainId" => $this->service->thisDomainEntity->getId()]);

		if($values){
			$this->create($values);

			$this->slug->findByOwner();

			return $this;
		}
		
		return false;
	}

	public function save($values = null)
	{
		$this->set($values);

		$values = $this->getRepositoryValues();

		if($values = $this->service->repository->homepages->save($values)){
			$this->set($values);

			$this->slug->save();

			return true;
		}

		return false;
	}

	public function isEditAllowed($user)
	{
		$isAllowed = new ArrayResult;

		$isAllowed->is($user->isAllowed('Admin:Homepage', 'edit'));

		$isAllowed->is($this->isMy($user));

		return $isAllowed;
	}

	public function isMy($user)
	{
		return $this->getCreated()->getUserId() == $user->getId();
	}
}