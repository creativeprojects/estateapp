<?php

namespace App\Entities;

/**
 * @Repository App\Repositories\DomainsRepository
 * @Service domains
 */

class DomainEntity extends \CP\Entities\DomainEntity
{
	/**
	 * Nastavení exportů
	 * @Property CP\Entities\Properties\JsonProperty
	 * @Column (type = "json")
	 */

	protected $exportSettings;

	/**
	 * Nastavení importů
	 * @Property CP\Entities\Properties\JsonProperty
	 * @Column (type = "json")
	 */

	protected $importSettings;
}