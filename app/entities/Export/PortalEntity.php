<?php

namespace App\Entities\Export;

/**
 * @Repository App\Repositories\Export\PortalsRepository
 * @Service export
 */

class PortalEntity extends \CP\Entities\BaseEntity
{
	/**
	 * @Property \CP\Entities\Properties\IdProperty
	 * @Column (type = "integer", key = "PRI", autoIncrement = true)
	 * @Id
	 */

	protected $portal_id;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 30)
	 */

	protected $portal_key;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $portal_title;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 255)
	 */

	protected $portal_classname;

	/**
	 * @Property
	 * @Column (type = "varchar", length = 255)
	 */

	protected $portal_url;

	/**
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $portal_active;
	
	/**
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $export_broker;


	/**
	 * @Property
	 * @Column (type = "json")
	 */

	protected $portal_settings;

}