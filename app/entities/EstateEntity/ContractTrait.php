<?php

namespace App\Entities\EstateEntity;

trait ContractTrait
{
	/** Zprostředkovatelská smlouva - Typ
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $contract_type;

	/** Zprostředkovatelská smlouva - datum podpisu 
	 * @Property
	 * @Column (type = "date")
	 */

	protected $contract_sign_date;

	/** Zprostředkovatelská smlouva - platnost
	 * @Property
	 * @Column (type = "date")
	 */

	protected $contract_valid_date;

	/** Zprostředkovatelská smlouva - platnost exklusivity
	 * @Property
	 * @Column (type = "date")
	 */

	protected $contract_exkl_valid_date;

	/** Zprostředkovatelská smlouva - přílohy
	 * @Property CP\Collections\Files\FilesCollection
	 */

	protected $contract_pdf;
}