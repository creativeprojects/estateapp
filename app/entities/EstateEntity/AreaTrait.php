<?php

namespace App\Entities\EstateEntity;

trait AreaTrait
{
	/** Plocha - užitná
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $usable_area;

	/** Plocha - celková?
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $estate_area;

	/** Plocha - kanceláří
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $offices_area;

	/** Plocha - výrobní
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $production_area;

	/** Plocha - obchodní
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $shop_area;

	/** Plocha - Balkónu
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $balcony_area;

	/** Plocha - bazénu
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $basin_area;

	/** Plocha - budovy
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $building_area;

	/** Plocha - Sklepní
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $cellar_area;

	/** Plocha - podlahová ??
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $floor_area;

	/** Plocha - zahrady
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $garden_area;

	/** Plocha - Lodžie
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $loggia_area;

	/** Plocha - Nebytových prostor
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $nolive_total_area;

	/** Plocha - skladovací
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $store_area;

	/** Plocha - terasy
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $terrace_area;

	/** Plocha - zastavěná ??
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $usable_area_ground;

	/** Plocha - dílen
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $workshop_area;
}