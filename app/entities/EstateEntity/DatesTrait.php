<?php

namespace App\Entities\EstateEntity;

trait DatesTrait
{
	/** Datum - kolaudace
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $acceptance_year;

	/** Datum - zahájení výstavby
	 * @Property
	 * @Column (type = "date")
	 */

	protected $beginning_date;

	/** Datum - dokončení výstavby
	 * @Property
	 * @Column (type = "date")
	 */

	protected $finish_date;
	
	/** Datum - začítek první prohlídky
	 * @Property
	 * @Column (type = "datetime")
	 */

	protected $first_tour_date;
	
	/** Datum - konec první prohlídky
	 * @Property
	 * @Column (type = "datetime")
	 */

	protected $first_tour_date_to;

	/** Datum - Připraveno k nastěhování
	 * @Property
	 * @Column (type = "date")
	 */

	protected $ready_date;

	/** Datum - rok rekonstrukce
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $reconstruction_year;

	/** Datum - zahájení prodeje
	 * @Property
	 * @Column (type = "date")
	 */

	protected $sale_date;
}