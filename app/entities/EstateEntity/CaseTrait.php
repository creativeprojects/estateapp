<?php

namespace App\Entities\EstateEntity;

trait CaseTrait
{
	/** Smlouva o budoucí kupní smlouvě
	 * @Property CP\Collections\Files\FilesCollection
	 */

	protected $contract_sobks_pdf;

	/** Kupní smlouva
	 * @Property CP\Collections\Files\FilesCollection
	 */

	protected $contract_ks_pdf;

	/** Rezervační smlouva
	 * @Property CP\Collections\Files\FilesCollection
	 */

	protected $reservation_pdf;
}