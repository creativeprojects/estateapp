<?php

namespace App\Entities\EstateEntity;

use CP\Helpers\PriceHelper AS Price;

trait PriceTrait
{
	/** Cena
	 * @Property
	 * @Column (type = "price")
	 */

	protected $advert_price;

	/** Cena - původní - před změnou pro hlídání slev
	 * @Property
	 * @Column (type = "price")
	 */

	protected $advert_price_original;

	/** Cena - Měna
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_price_currency;

	/** Cena - jenotka
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_price_unit;

	/** Cena - poplatky zahrnuty v ceně
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_charge;
	
	/** Cena - provize zahrnuta v ceně
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_commission;

	/** Cena - právní servis zahrnut v ceně
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_legal_services;

	/** Cena - k jednání
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_negotiation;

	/** Cena - včetně DPH
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_vat;

	/** Cena - annuita
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $annuity;

	/** Hypotéka možná
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $mortgage;

	/** Hypotéka - PRSN?
	 * @Property
	 * @Column (type = "double")
	 */

	protected $mortgage_percent;

	/** Cena - poznámka
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "varchar")
	 * @Localized
	 */

	protected $advert_price_text_note;

	/** Cena - skrýt
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_price_hidden;

	public function getPrice()
	{
		if ($this->advert_price_currency->get()) {
			$currency = $this->service->estatesParameters["advert_price_currency"]["options"][$this->advert_price_currency->get()];
		} else {
			$currency = null;
		}

		$price = Price::format($this->advert_price->get(), $currency);

		return $price;
	}

	/**
	 * Získat formátované vlastnosti ceny
	 * @return string
	 */
	
	public function getReadablePriceProperties()
	{
		$priceProperties = @[
			//Jednotka ceny
			$this->service->estatesParameters["advert_price_unit"]["options"][$this->advert_price_unit->get()],
			//Provize
			$this->service->estatesParameters["advert_price_commission"]["options"][$this->advert_price_commission->get()],
			//Poplatky
			$this->service->estatesParameters["advert_price_charge"]["options"][$this->advert_price_charge->get()],
			//DPH - pokud není zvoleno 3: Neuvádět
			($this->advert_price_vat->get() != 3 ? $this->service->estatesParameters["advert_price_vat"]["options"][$this->advert_price_vat->get()] : null),
			//Právní služby
			$this->service->estatesParameters["advert_price_legal_services"]["options"][$this->advert_price_legal_services->get()]
		];

		$string = implode(", ", array_filter($priceProperties));

		return $string;
	}
}