<?php

namespace App\Entities\EstateEntity;

trait AuctionTrait
{
	/** Aukce - ??
	 * @Property
	 * @Column (type = "double")
	 */

	protected $price_auction_principal;
	
	/**
	 * Cena - posudek znalce
	 * @Property
	 * @Column (type = "double")
	 */

	protected $price_expert_report;

	/**
	 * Aukce - minimální příhoz
	 * @Property
	 * @Column (type = "double")
	 */

	protected $price_minimum_bid;

	/**
	 * Aukce - dokument oznámení
	 * @Property \CP\Collections\Files\FilesCollection
	 */

	protected $auction_advertisement_pdf;
	
	/**
	 * Aukce - datum a čas konání
	 * @Property
	 * @Column (type = "datetime")
	 */

	protected $auction_date;
	
	/**
	 * Aukce - datum a čas prohlídky
	 * @Property
	 * @Column (type = "datetime")
	 */

	protected $auction_date_tour;

	/**
	 * Aukce - datum a čas druhé prohlídky
	 * @Property
	 * @Column (type = "datetime")
	 */

	protected $auction_date_tour2;

	/**
	 * Aukce - typ
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $auction_kind;

	/**
	 * Aukce - místo konání
	 * @Property \CP\Entities\Properties\AddressProperty
	 * @Column (type = "address")
	 */

	protected $auction_place;

	/**
	 * Aukce - PDF výsledku aukce
	 * @Property \CP\Collections\Files\FilesCollection
	 */

	protected $auction_review_pdf;
}