<?php

namespace App\Entities\EstateEntity;

trait CommonTrait
{
	/** Zakázka - typ obchodu
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_function;

	/** Zakázka - platnost inzerátu - pro Sreality
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_lifetime;

	/** Zakázka - typ nemovitosti
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_type;

	/** Zakázka - upřesnění typu nemovitosti
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_subtype;
	
	/** Zakázka - Evidenční číslo
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $advert_code;

	/** Zakázka - stav
	 * @Property
	 * @Column (type = "tinyint", notNull = true)
	 */

	protected $advert_status;

	/** Zakázka - zobrazit na úvodní stránce
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_onhomepage;

	public function getReadableFunction()
	{
		return @$this->service->estatesParameters["advert_function"]["options"][$this->advert_function->get()];
	}

	public function getReadableType()
	{
		return @$this->service->estatesParameters["advert_type"]["options"][$this->advert_type->get()];
	}

	public function getReadableSubType()
	{
		return @$this->service->estatesParameters["advert_subtype"]["options"][$this->advert_type->get()][$this->advert_subtype->get()];
	}

	public function getReadableStatus()
	{
		return @$this->service->estatesParameters["advert_status"]["options"][$this->advert_status->get()];
	}
}