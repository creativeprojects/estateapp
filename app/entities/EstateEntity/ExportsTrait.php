<?php

namespace App\Entities\EstateEntity;

trait ExportsTrait
{
	/** 
	 * @Property App\Collections\Estates\ExportsCollection
	 */

	protected $estate_exports;

	protected function findExports()
	{
		$this->estate_exports->find();
	}

	protected function saveExports()
	{
		return $this->estate_exports->save();
	}

	protected function removeExports()
	{
		return $this->estate_exports->remove();
	}
}