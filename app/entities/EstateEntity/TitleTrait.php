<?php

namespace App\Entities\EstateEntity;

use Nette\Utils\Html;

trait TitleTrait
{
	/** 
	 * Zakázka - titulek
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "varchar")
	 * @Localized
	 * TODO - přejmenovat na advert_title
	 */

	protected $estate_title;

	/** 
	 * Zakázka - popis
	 * @Property \CP\Entities\Properties\LocalizedProperty
	 * @Column (type = "text")
	 * @Localized
	 * TODO - přejmenovat na advert_description
	 */

	protected $estate_description;

	/**
	 * Získat titulek nemovitosti
	 * Pokud není ručně zadán, tak vygenerovat
	 * @param string|null $locale nepovinný jazyk, ve kterém chci titulek získat, pokud není zadán, vezme se jazyk prostředí
	 * @return string
	 */

	public function getTitle($locale = null)
	{
		if(!$locale){ //Jazy nezadán, záskat z prostředí
			$locale = $this->service->translator->getLocale();
		}

		if ($this->estate_title->get()) { //Titulek je v inzerátu ručně definován
			$title = $this->estate_title->get()->{$locale};
		} else { //Titulek není v inzerátu definován, vygenerujeme
			$title = [
				$this->getReadableFunction(), //Druh obchodu (prodej, pronájem)
				$this->getReadableType(), //Druh nemovitosti (dům, byt, ...)
				$this->getReadableSubType() //Upřesnění druhu nemovitosti (1+kk, rodinný, ...)
			];

			$title = implode(", ", array_filter($title));
		}

		//Naformátovat m2 a m3 v titulku
		$title = m2($title);

		return $title;
	}

	/**
	 * HTML naformátovat titulek nemovitosti
	 * TODO - uslušnit
	 * @param Nette\Security\User $user uživatel, který nemovitost prohlíží
	 * @return Nette\Utils\Html
	 */
	
	public function getFormattedTitle($user)
	{
		$words = explode(",", $this->getTitle());

		$lead = [];
		$leadLength = 35;
		$i = 0;

		$safety = 100;

		while(count($words) && $leadLength > 0 && $safety){
			$leadLength -= strlen($words[$i]);

			$lead[] = $words[$i];

			unset($words[$i]);

			$safety--;

			$i++;
		}

		$lead = implode(", ", $lead);
		
		$small = implode(", ", $words);

		$html = "<span>" . strtoupper(substr($lead, 0, 1)) . substr($lead, 1) . "</span>";

		if(strlen($small)){
			$html .= "<small>" . $small . "</small>";
		}

		if($this->isVisible($user) === "logged"){
			$html .= " <small>({$this->service->estatesParameters["advert_status"]["options"][$this->advert_status->get()]})</small>";
		}

		return Html::el()->setHtml($html);
	}
}