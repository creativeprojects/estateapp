<?php

namespace App\Entities\EstateEntity;

use CP\Utils\GeoCoder;

trait LocalityTrait
{
	/**
	 * Lokalita - název země
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $locality_country;

	/**
	 * Lokalita - kód země
	 * @Property
	 * @Column (type = "varchar", length = 2)
	 */

	protected $locality_country_code;

	/**
	 * Lokalita - název kraje
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $locality_state;

	/**
	 * Lokalita - UIR kraje
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_state_uir;

	/**
	 * Lokalita - název okresu
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $locality_county;

	/**
	 * Lokalita - UIR okresu
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_county_uir;

	/**
	 * Lokalita - Město
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $locality_city;

	/**
	 * Lokalita - Část obce
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $locality_district;

	/**
	 * Lokalita - Část obce - UIR kód
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $locality_district_uir;
	
	/**
	 * Lokalita - UIR obce
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_city_uir;

	/** Lokalita - Ulice
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $locality_street;

	/**
	 * Lokalita - UIR ulice
	 * @Property
	 * @Column (type = "varchar", length = 100)
	 */

	protected $locality_street_uir;

	/**
	 * Lokalita - podlaží
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $floor_number;
	
	/**
	 * Lokalita - Adresa
	 * @Property \CP\Entities\Properties\AddressProperty
	 * @Column (type = "address")
	 */

	protected $locality;
	
	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_ruian;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_ruian_level;

	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_uir;
	
	/**
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_uir_level;

	/**
	 * Lokalita - PSČ
	 * @Property
	 * @Column (type = "varchar", length = 10)
	 */

	protected $locality_zip;

	/**
	 * Lokalita - Městská část - asi pro Prahu - proč to není jednotné s locality_district
	 * @Property
	 * @Column (type = "varchar", length = 50)
	 */

	protected $locality_citypart;

	/**
	 * Lokalita - Číslo orientační
	 * @Property
	 * @Column (type = "varchar", length = 20)
	 */

	protected $locality_co;
	
	/**
	 * Lokalita - Číslo popisné
	 * @Property
	 * @Column (type = "varchar", length = 20)
	 */

	protected $locality_cp;

	/**
	 * Lokalita - Číslo evidenční - katastr
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_ce;

	/**
	 * Lokalita - číslo jednotky - katastr
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_cj;

	/**
	 * Lokalita - Parcelní číslo - katastr
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $locality_pc;

	/**
	 * Lokalita - úroveň znepřesnění adresy v kartě nemovitosti
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $locality_inaccuracy_level;

	/**
	 * Lokalita - ochranné pásmo
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $protection;

	/**
	 * Lokalita - okolní zástavba
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $surroundings_type;

	/**
	 * Získat čitelnou polohu
	 * @return string
	 */

	public function getReadableObjectLocation()
	{
		return @$this->service->estatesParameters["object_location"]["options"][$this->object_location->get()];
	}

	/**
	 * Získat čitelnou okolní zástavbu
	 * @return string
	 */

	public function getReadableSurroundingType()
	{
		return @$this->service->estatesParameters["surroundings_type"]["options"][$this->surroundings_type->get()];
	}

	/**
	 * Vytvořit veřejnou adresu nemovitosti podle nastavení znepřesnění adresy
	 * @return string
	 */

	public function getPublicAddress()
	{
		$public_address = [];

		if ($this->locality_inaccuracy_level->get() == 1) {
			$street = implode(" ", array_filter([$this->locality_street->get(), $this->locality_cp->get()]));

			if ($this->locality_co->get()){
				$street .= "/" . $this->locality_co->get();
			}

			$public_address[] = $street;
		} elseif ($this->locality_inaccuracy_level->get() == 2) {
			$street = $this->locality_street->get();

			$public_address[] = $street;
		}
		
		$public_address[] = implode(" ", array_filter([postcode_format($this->locality_zip->get()), $this->locality_city->get()]));

		return implode(", ", array_filter($public_address));
	}

	protected function validateLocality()
	{
		//Pokud by selhala client-side GeoLokace, získat server-side
		if(!$this->locality->get()->latitude || !$this->locality->get()->longitude) {
			$this->setLocality(GeoCoder::getLocation("{$this->getLocality_street()} {$this->getLocality_co()} {$this->getLocality_cp()}, {$this->getLocality_city()}"));
		}
	}
}