<?php

namespace App\Entities\EstateEntity;

trait PropertiesTrait
{
	/** Vlastnosti - balkón
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $balcony;

	/** Vlatnosti - bazén
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $basin;

	/** Vlastnosti - sklep
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $cellar;

	/** Vlastnosti - garáž
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $garage;

	/** Vlatnosti - Lodžie
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $loggia;

	/** Vlatnosti - vlastnictví
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $ownership;

	/** Vlastnosti - terasa
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $terrace;

	/** Vlastnosti - výška stropu
	 * @Property
	 * @Column (type = "double")
	 */

	protected $ceiling_height;

	/** Vlastnosti - náklady na bydlení
	 * @Property
	 * @Column (type = "varchar")
	 */

	protected $cost_of_living;

	/** Vlastnosti - bezbariérový přístup
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $easy_access;

	/** Vlastnosti - Elektřina
	 * @Property
	 * @Column (type = "json")
	 */

	protected $electricity;
	
	/** Vlastnosti - výtah
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $elevator;

	/** Vlastnosti - třída bytu??
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $flat_class;

	/** Vlastnosti - vybaveno
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $furnished;

	/** Vlastnosti - podkroví
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $garret;

	/** Vlastnosti - Plyn
	 * @Property 
	 * @Column (type = "json")
	 */

	protected $gas;

	/** Vlastnosti - Kanalizace
	 * @Property
	 * @Column (type = "json")
	 */

	protected $gully;

	/** Vlastnosti - Topení
	 * @Property
	 * @Column (type = "json")
	 */

	protected $heating;

	/** Vlastnosti - přístupové cesty
	 * @Property
	 * @Column (type = "json")
	 */

	protected $road_type;

	/** Vlastnosti - telekomunikace
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $telecommunication;

	/** Vlastnosti - možnosti dopravy
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $transport;

	/** Vlastnosti - vodovod
	 * @Property
	 * @Column (type = "json")
	 */

	protected $water;
}