<?php

namespace App\Entities\EstateEntity;

trait EnergyTrait
{
	/** Energie - nískoenergetický
	 * @Property
	 * @Column (type = "boolean")
	 */

	protected $advert_low_energy;

	/** Energie - ??
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $energy_efficiency_rating;

	/** Energie - ??
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $energy_performance_certificate;
	
	/** Energie - ??
	 * @Property
	 * @Column (type = "double")
	 */

	protected $energy_performance_summary;

	/** Energie - příloha energetického šťítku
	 * @Property CP\Collections\Files\FilesCollection
	 */

	protected $energy_performance_attachment;
}