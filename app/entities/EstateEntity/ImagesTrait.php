<?php

namespace App\Entities\EstateEntity;

trait ImagesTrait
{
	/**
	 * @Property CP\Collections\Files\ImagesCollection
	 */

	protected $estate_images;

	/**
	 * Alias, až přejmenujeme na self::$images tak můžeme odebrat
	 * @return CP\Collections\Files\ImagesCollection
	 */

	public function getImages()
	{
		return $this->estate_images;
	}
}