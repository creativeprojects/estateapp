<?php

namespace App\Entities\EstateEntity;

use CP\Utils\ArrayResult;

trait UserTrait
{
	/** Makléř - ID
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $userId;

	/** @Property App\Entities\UserEntity */

	protected $estate_user;

	/**
	 * Mám na nemovitost autorský nárok?
	 * @param Nette\Security\User
	 * @return bool
	 */

	public function isMy($user)
	{
		$isMy = new ArrayResult;
		
		//Jedná se o klienta na aktuální doméně
		$isMy->is($this->getDomainId() == $this->service->thisDomainEntity->getId());

		//Makléř buĎ není uveden, nebo je to současně přihlášený uživatel, nebo se jedná a podřízeného aktuálně řihlášeného uživatele
		$isMy->is(empty($this->estate_user->getId()) || $this->estate_user->getId() == $user->getId() || ($this->estate_user->role && $user->getAuthorizator()->roleInheritsFrom($user->getIdentity()->role, $this->estate_user->role)));

		return $isMy->result();
	}

	/**
	 * Je povolena editace této nemovitosti aktuálně přihlášeným uživatelem?
	 * @param Nette\Security\User $user
	 * @return bool
	 */

	public function isEditAllowed($user)
	{
		$isAllowed = new ArrayResult;

		//Je komponenta editace povolena pro tohoto klienta (doménu)
		$isAllowed->is($this->service->thisDomainEntity->isComponentAllowed('EstatesAdmin', $user));

		//Je povoledna editace pro tohoto uživatele
		$isAllowed->is($user->isAllowed('Admin:Estates','edit'));

		//Mám autorské právo k této nemovitosti
		$isAllowed->is($this->isMy($user));

		return $isAllowed->result();
	}

	/**
	 * Je povoleno odebrání této nemovitosti aktuálně přihlášeným uživatelem?
	 * @param Nette\Security\User $user
	 * @return bool
	 */
	
	public function isRemoveAllowed($user)
	{
		$isAllowed = new ArrayResult;

		//Je komponenta editace povolena pro tohoto klienta (doménu)
		$isAllowed->is($this->service->thisDomainEntity->isComponentAllowed('EstatesAdmin', $user));

		//Je povoledno odebírání pro tohoto uživatele
		$isAllowed->is($user->isAllowed('Admin:Estates','remove'));

		//Mám autorské právo k této nemovitosti
		$isAllowed->is($this->isMy($user));

		return $isAllowed->result();
	}
}