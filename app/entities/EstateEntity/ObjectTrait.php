<?php

namespace App\Entities\EstateEntity;

trait ObjectTrait
{
	/** Objekt - stav budovy
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $building_condition;

	/** Objekt - typ budovy
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $building_type;

	/** Objekt - typ
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $object_type;

	
	/** Objekt - stáří
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $object_age;

	/** Objekt - typ
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $object_kind;

	/** Objekt - poloha
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $object_location;
}