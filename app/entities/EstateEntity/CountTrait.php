<?php

namespace App\Entities\EstateEntity;

trait CountTrait
{
	/** Počet - pokojů
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $advert_room_count;

	/** Počet - parkovacích míst
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $parking_lots;

	/** Počet - podlaží
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $floors;

	/** Počet - garáží
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $garage_count;

	/** Počet - parkovacích míst
	 * @Property
	 * @Column (type = "integer")
	 */

	protected $parking;

	/** Počet - podzemních podlaží
	 * @Property
	 * @Column (type = "tinyint")
	 */

	protected $underground_floors;
}