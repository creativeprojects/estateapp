<?php

namespace App\Entities\EstateEntity;

use CP\Utils\ArrayResult;

trait AttachmentsTrait
{
	protected function findAttachments()
	{
		foreach ($this->service->estatesParameters AS $key => $parameter) {
			if ($parameter["type"] == "base64") {
				$this->{$key}->findByOwner();
			}
		}
	}

	protected function saveAttachments()
	{
		$result = new ArrayResult;

		foreach ($this->service->estatesParameters AS $key => $parameter) {
			if ($parameter["type"] == "base64") {
				$result->is($this->{$key}->save());
			}
		}

		return $result->result();
	}

	protected function removeAttachments()
	{
		$result = new ArrayResult;

		foreach ($this->service->estatesParameters AS $key => $parameter) {
			if ($parameter["type"] == "base64") {
				$result->is($this->{$key}->remove());
			}
		}

		return $result->result();
	}
}