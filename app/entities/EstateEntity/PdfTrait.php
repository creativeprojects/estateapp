<?php

namespace App\Entities\EstateEntity;

use Joseki\Application\Responses\PdfResponse;
use Nette\Utils\Strings;
use CP\Utils\Files;

trait PdfTrait
{
	/** Zakázka - PDF
	 * @Property \CP\Entities\Files\PdfEntity
	 */

	protected $estate_pdf;

	/**
	 * Vytvořit PDF
	 * TODO - toto by měla dělat sama PdfEntity, máme to udělané, jen to nalinkovat
	 * @return string
	 */

	protected function createPdf()
	{
		$html = $this->getHtml();

		$pdf = new PdfResponse($html);

		$pdf->setSaveMode(PdfResponse::INLINE);

		$pdfName = Strings::webalize($this->getTitle());

		$pdfPath = Files::validateDir($this->service->thisDomainEntity->getPath() . "/" . $this->service->settings["pdfDir"]);

		$pdf->save(WWW_DIR . "/{$pdfPath}/", $pdfName);

		$path = "{$pdfPath}/{$pdfName}.pdf";

		return $path;
	}

	/**
	 * Uložit PDF zakázky
	 * @return bool
	 */

	public function savePdf()
	{
		$this->estate_pdf->setSrc($this->createPdf());

		$this->estate_pdf->setRel("estate_pdf");

		foreach($this->service->translator->getAvailableLocales() AS $locale){
			$this->estate_pdf->getAlt()->{$locale} = $this->getTitle($locale);
		}

		return $this->estate_pdf->save();
	}

	/**
	 * Získat HTML PDF
	 * TODO - toto by měla dělat sama PdfEntity - máme to tam, jen to nalinkovat
	 * @return string
	 */

	public function getHtml()
	{
		$parameters = array(
			"estateEntity" => $this,
			"estatesParameters" => $this->service->estatesParameters
		);

		$html = $this->service->latte
			->blank()
			->setParameters($parameters)
			->setFile("AdminModule/templates/Estates/pdf/{$this->service->translator->getLocale()}/estateCard.latte");

		return $html->getTemplate();
	}
}