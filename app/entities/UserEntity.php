<?php

namespace App\Entities;

/**
 * @Repository App\Repositories\UsersRepository
 * @Service userService
 */

class UserEntity extends \CP\Entities\UserEntity
{
	/**
	 * Telefon makléře
	 * @Property
	 * @Repository App\Repositories\Users\BrokersRepository
	 * @Column (type = "varchar", length = 20)
	 */

	protected $phone;

	/**
	 * Existuje profilová stránka makléře?
	 * @Property
	 */

	protected $brokerProfileExists;

	/**
	 * Existuje profilová stránka makléře?
	 * @return bool
	 */
	
	public function brokerProfileExists()
	{
		return $this->getBrokerProfileExists();
	}

	/**
	 * Uložit informace o makléři
	 * TODO - dodělat export makléřů
	 * @return bool
	 */

	public function saveBroker()
	{
		$values = $this->getRepositoryValues("App\Repositories\Users\BrokersRepository");

		if ($values = $this->service->repository->usersBroker->save($values)) {
			$this->set($values);

			//TODO - export makléřů
			//$this->service->exportService->saveBroker($this->values);

			return true;
		}

		return false;
	}

	/**
	 * Najít informace o makléři
	 * @return void
	 */

	public function findBroker()
	{
		$values = $this->service->repository->usersBroker->getOne($this->getId());

		if ($values) {
			$this->create($values);
		}
	}

	/**
	 * Odebrat informace o makléři
	 * TODO - dořešit exporty
	 * @return bool
	 */

	public function removeBroker()
	{
		if ($this->service->repository->usersBroker->removeOne($this->getId())) {
			//$this->service->exportService->removeBroker();

			return true;
		}

		return false;
	}

	/**
	 * Je povoleno makléře odebrat?
	 * Makléře nejleze odebrat pokud má nějaké nemovitosti či poptávky
	 * @param \Nette\Application\UI\Presenter $presenter
	 * @return bool
	 */

	public function isRemoveAllowedBroker($presenter)
	{
		$isRemoveAllowed = true;

		//Makléř má nemovitosti
		if ($this->getEstatesCount() > 0) {
			$presenter->flashMessage("Makléře nelze odebrat, protože má přiřazeny nemovitosti. Odeberte je nebo přiřaďte nemovitosti jinému makléři.", "alert-warning");

			$isRemoveAllowed = false;
		}

		//Makléř má poptávky
		if ($this->getDemandsCount() > 0) {
			$presenter->flashMessage("Makléře nelze odebrat, protože má přiřazeny poptávky. Odeberte je nebo přiřaďte poptávky jinému makléři.", "alert-warning");

			$isRemoveAllowed = false;
		}

		if ($isRemoveAllowed) {
			return true;
		} else {
			$this->redirect("this");

			return false;
		}
	}

	/**
	 * Spočítat nemovitosti tohoto makléře
	 * @return integer
	 */
	
	public function getEstatesCount()
	{
		$by = [
			"domainId" => $this->service->thisDomainEntity->getId(),
			"userId" => $this->getId()
		];

		$count = $this->service->repository->estates->getCountBy($by);

		return $count;
	}

	/**
	 * Spočítat poptávky tohoto makléře
	 * @return integer
	 */

	public function getDemandsCount()
	{
		$by = [
			"domainId" => $this->service->thisDomainEntity->getId(),
			"userId" => $this->getId()
		];

		$count = $this->service->repository->demands->getCountBy($by);

		return $count;
	}
}