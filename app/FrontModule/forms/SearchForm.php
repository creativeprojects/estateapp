<?php

namespace App\FrontModule\Forms;

class SearchForm extends \CP\Forms\BaseForm
{
	public function create()
	{
		$this->form->method = "get";

		$this->form->action = $this->presenter->link("Estates:default");

		$this->form->addSelect("advert_type", null, $this->presenter->estatesParameters["advert_type"]["options"]);

		$this->form->addSelect("advert_subtype", null, $this->presenter->estatesService->getAdvertSubtypeOptions("Nerozhoduje"));

		$this->form->addText("locality");

		$this->form->addContainer("area")
			->addtext("from");

		$this->form->addContainer("price")
			->addtext("to");

		$this->form->addsubmit("search");

		return $this->form;
	}
}