<?php

namespace App\FrontModule\Forms;

class DemandForm extends \App\Forms\DemandForm
{
	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->addNewDemandNotification();

			$this->presenter->flashMessage("Vaše poptávka byla uložena. Děkujeme za Váš zájem o naše služby", "alert-success");

			$this->presenter->redirect($this->redirect);
		} else {
			$this->presenter->flashMessage("Při ukládání poptávky došlo k chybě! Zkuste akci opakovat", "alert-danger");
		}
	}

	/**
	 * Vytvořit notifikaci nové poptávky
	 * @return void
	 */

	protected function addNewDemandNotification()
	{
		$this->presenter->notificationsService->notificationEntity()
			->setKey('newDemand')
			->setOwner($this->entity->getReflection()->getAnnotation("Repository"), $this->entity->getId())
			->setTemplate("AdminModule/templates/Demands/notifications/newDemand.latte")
			->setParameters($this->entity->getValues(true))
			->setUrl($this->presenter->link(':Admin:Demands:edit', ['demand_id' => $this->entity->getId()]))
			->setRoles(['admin','superAdmin'])
			->setDeadline('+ 1day')
			->save();
	}

	/**
	 * Přidat skryté pole uživatele, kterému poptávka patří
	 * @return void
	 */
	
	protected function addUser()
	{
		$this->form->addHidden("userId")
			->setValue($this->presenter->user->getId());
	}
}