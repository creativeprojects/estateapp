<?php

namespace App\FrontModule\Forms\Advices;

use CP\Forms\Form;

class QuestionForm extends \App\Forms\Advices\QuestionForm
{
	/**
	 * Odeslat otásku
	 * @param SubmitButton $button
	 * @return void
	 */
	
	public function save($button)
	{
		$values = $button->parent->getValues();

		if ($this->entity->save($values)) {
			$this->presenter->flashMessage("Vaše otázka byla položena. Odpověď můžete očekávat v nejbližších dnech", "alert-success");

			$this->presenter->redirect("Advices:default");
		} else {
			$this->presenter->flashMessage("Vaší otázku se nepodařilo odeslat! Zkuste akci zopakovat", "alert-danger");
		}
	}
}