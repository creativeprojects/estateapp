<?php

namespace App\FrontModule\Presenters;

use App\FrontModule\Forms\DemandForm;

class DemandsPresenter extends BasePresenter
{
	/** @var \App\Services\DemandsService @inject */
	public $demandsService;

	/** @var \App\Entities\DemandEntity */
	public $demandEntity;

	public function actionDefault()
	{
		$this->demandEntity = $this->demandsService->getNewEntity();
	}

	protected function createComponentDemandForm($name)
	{
		$form = new DemandForm($this, $name);

		$form->setRedirect("this");

		$form->setEntity($this->demandEntity);
		
		return $form->create();
	}
}