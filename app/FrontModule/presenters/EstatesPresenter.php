<?php

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;
use CP\Forms\EmailForm;
use CP\Utils\ArrayResult;

class EstatesPresenter extends BasePresenter
{
	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/** @var \App\Entities\EstateEntity */
	public $estateEntity;

	/**
	 * Zobrazit přehled nemovitostí
	 * @param integer|null $advert_type nepovinný kód typu nemovitostí (byty, domy, ...)
	 * @param integer|null $advert_subtype nepovinný kód upřesnění typu nemovitostí (1+kk, 1+1, rodinný, ...)
	 * @param integer|null $advert_function nepovinný kód typu obchodu (prodej, pronájem, ...)
	 * @return void
	 */

	public function actionDefault($advert_type = null, $advert_subtype = null, $advert_function = null)
	{
		$this->estatesCollection = $this->estatesService->estatesCollection()
			->whereDomain()
			->whereActive()
			->whereVisible()
			->whereUserVisible($this->user);

		if ($advert_type || $advert_subtype) { //Pouze zadaný typ či subtyp nemovitosti
			$this->estatesCollection->whereType($advert_type, $advert_subtype);
		}

		if ($advert_function) { //Pouze zadaný typ obchodu (prodej / pronájem ...)
			$this->estatesCollection->whereFunction($advert_function);
		}

		$this->estatesCollection->attach($this);
	}

	/**
	 * Vykreslení přehledu nemovitostí
	 * @param integer|null $advert_type nepovinný kód typu nemovitostí (byty, domy, ...)
	 * @param integer|null $advert_subtype nepovinný kód upřesnění typu nemovitostí (1+kk, 1+1, rodinný, ...)
	 * @param integer|null $advert_function nepovinný kód typu obchodu (prodej, pronájem, ...)
	 * @return void
	 */

	public function renderDefault($advert_type = null, $advert_subtype = null, $advert_function = null)
	{
		//Stránkování
		$this->estatesCollection->pagination->setItemsPerPage(20);

		//Řazení
		$this->estatesCollection->sorting
			->add("estate_title", "Názvu")
			->add("advert_price", "Ceny")
			->add("created", "Datumu přidání");

		//Zadání výchozích hodnot filtru podle zadaných vlastností seznamu
		$this->estatesCollection->filter->setValues([
			"advert_type" => $advert_type, 
			"advert_subtype" => $advert_subtype, 
			"advert_function" => $advert_function
		]);

		$this->template->estatesCollection = $this->estatesCollection->find();
	}

	/**
	 * Detail nemovitosti podle ID
	 * @param integer $estate_id ID nemovitosti, kterou cheme zobrazit
	 * @return void
	 */

	public function actionDetail($estate_id)
	{
		if ($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)) {
			$this->template->estateEntity = $this->estateEntity;

			$this->emailEntity = $this->emailsService->emailEntity()->blank()
				->setFrom("", "")
				->setBody("")
				->addTo($this->estateEntity->estate_user->getEmail(), $this->estateEntity->estate_user->getName())
				//TODO - přepracovat na localeTemplate
				->setSubjectTemplate("templates/Emails/Messages/{$this->translator->getLocale()}/estateFormSubject.latte", $this->estateEntity->getValues());
			

		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vytvoření PDF karty nemovitosti a její zobrazení
	 * @param integer $estate_id
	 * @return void
	 */

	public function actionPdf($estate_id)
	{
		if($this->estateEntity = $this->estatesService->estateEntity()->find($estate_id)){
			//TODO - Nevytvářet ho pokaždé znovu, jen pokud není vytvořené
			if($this->estateEntity->savePDF()) {
				$this->redirectUrl($this->estateEntity->estate_pdf->getBasePath());
			} else {
				$this->flashMessage("PDF kartu nemovitosti se nepodařilo vytvořit! Zkuste akci opakovat", "alert-danger");

				$this->restoreRequest($this->backlink);
				$this->redirect("Estates:detail", $this->estateEntity->getId());
			}
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Získání komponenty formuláře pro dotaz k nemovitosti
	 * @param string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentEmailForm($name)
	{
		$form = new EmailForm($this, $name);

		$form->setEntity($this->emailEntity);

		$form
			->setEditable("fromName", true)
			->setEditable("fromEmail", true)
			->setEditable("body", true)
			->setLabel("fromName", "Vaše jméno")
			->setLabel("fromEmail", "Váš email")
			->setLabel("body", "Vaš dotaz k nemovitosti")	
			->setRequired("fromName", "Uveďte své jméno")
			->setRequired("fromEmail", "Zadejte svůj email")
			->setRequired("body", "Uveďte Vás dotaz k nemovitosti")
			//TODO - přepracovat na localeTemplate
			->setPostProcessMessageTemplate("templates/Emails/Messages/{$this->translator->getLocale()}/estateFormMessage.latte", $this->estateEntity->getValues())
			->setFlashMessage("Váš dotaz k nemovitosti ev. č. {$this->estateEntity->advert_code} byl odeslán makléři", "Při odesílání dotazu došlo k chybě. Zkuste dotaz odeslat znovu.");

		return $form->create();
	}
	
	/**
	 * Získání komponenty formuláře filtrování nemovitostí
	 * @param string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentFilterForm($name)
	{
		return $this->estatesCollection->filter->getControl($name);
	}

	/**
	 * Získání komponenty formuláře řazení nemovitostí
	 * @param string $name
	 * @return CP\Forms\Form
	 */

	protected function createComponentSortingForm($name)
	{
		return $this->estatesCollection->sorting->getControl($name);
	}

	/**
	 * Zobrazení prodaných nemovitostí
	 * @return void
	 */

	public function actionSold()
	{
		$this->estatesCollection = $this->estatesService->estatesCollection()
			->whereDomain()
			->whereSold()
			->newestFirst()
			->attach($this);
	}

	/**
	 * Vykreslení přehledu prodaných nemovitostí
	 * @return void
	 */
	
	public function renderSold()
	{
		$this->renderDefault();
	}
}