<?php

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;

class ContactsPresenter extends BasePresenter
{
	use \CP\FrontModule\Presenters\ContactsPresenterTrait;

	/** @var \App\Collections\UsersCollection */
	public $brokersCollection;

	/** @var \App\Services\UsersService @inject */
	public $usersService;

	public function renderDefault($contacts_id)
	{
		$this->template->brokersCollection = $this->usersService->getBrokersCollection()->find();
	}
}