<?php

namespace App\FrontModule\Presenters;

class SyncPresenter extends BasePresenter
{
    /** @var \App\Services\Sync\UrbiumService @inject */
    public $urbiumService;

    /**
     * Synchronizace makléřů a nemovitostí z Urbia
     * @param string|null $group
     * @param integer|null $external_id
     * @return void
     */

    public function renderUrbium($group = null, $external_id = null)
    {
        $this->template->log = $this->urbiumService->sync($group, $external_id);
    }
}