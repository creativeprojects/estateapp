<?php

namespace App\FrontModule\Presenters;

class BlogPresenter extends BasePresenter
{
	use \CP\FrontModule\Presenters\BlogPresenterTrait;
}