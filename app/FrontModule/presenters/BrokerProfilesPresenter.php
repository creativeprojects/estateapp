<?php

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;

class BrokerProfilesPresenter extends BasePresenter
{
	/** @var \App\Services\BrokerProfilesService @inject */
	public $brokerProfilesService;

	/** @var \App\Entities\BrokerProfileEntity */
	public $brokerProfileEntity;

	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/**
	 * Makléřský profil
	 * @param integer $id
	 * @return void
	 */

	public function actionDetail($id)
	{
		if ($this->brokerProfileEntity = $this->brokerProfilesService->getEntity($id)) {
			
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit makléřský profil
	 * @return void
	 */
	
	public function renderDetail()
	{
		$this->template->brokerProfileEntity = $this->brokerProfileEntity;

		$this->template->estatesCollection = $this->estatesService->estatesCollection()->whereActive()->whereUserId($this->brokerProfileEntity->getId());

		$this->template->estatesCollection->attach($this);

		$this->template->estatesCollection->pagination->setItemsPerPage(20);

		$this->template->estatesCollection->find();
	}
}