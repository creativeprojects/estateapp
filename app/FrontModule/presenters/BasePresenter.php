<?php

namespace App\FrontModule\Presenters;

use CP\Forms\EmailForm;
use App\FrontModule\Forms\SearchForm;

abstract class BasePresenter extends \CP\FrontModule\Presenters\BasePresenter
{
	/** Menu */
	use \CP\FrontModule\Presenters\MenusTrait;

	/** @var \CP\Entities\EmailEntity */
	public $questionEntity;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/**
	 * Před vykreslením
	 * @return void
	 */

	public function startup()
	{
		parent::startup();

		/**
		 * Vytvořit entitu emailu pro dotazový formulář
		 */

		$this->questionEntity = $this->emailsService->emailEntity()->blank()
			->setFrom("", "")
			->setBody("")
			->setSubject("Dotaz z webových stránek " . domain(false))
			->addTo($this->thisDomainEntity->getEmail(), $this->thisDomainEntity->getTitle());
	}

	/**
	 * Získat komponentu formuláře vyhledávání nemovitostí
	 * @param string $name jméno formuláře v šabloně
	 * @return CP\Forms\Form
	 */

	protected function createComponentSearchForm($name)
	{
		$form = new SearchForm($this, $name);

		return $form->create();
	}

	/**
	 * Získat kompoentntu dotazového formuláře
	 * @param string $name jméno formuláře v šabloně
	 * @return CP\Forms\Form
	 */
	
	protected function createComponentQuestionForm($name)
	{
		$form = new EmailForm($this, $name);

		$form->setEntity($this->questionEntity);

		$form
			->setEditable("fromName", true)
			->setEditable("fromEmail", true)
			->setEditable("body", true)
			
			->setLabel("fromName", "Vaše jméno")
			->setLabel("fromEmail", "Váš email")
			->setLabel("body", "Vaše zpráva")
			->setLabel("send", "Odeslat dotaz")
			
			->setRequired("fromName", "Uveďte své jméno")
			->setRequired("fromEmail", "Zadejte svůj email")
			->setRequired("body", "Uveďte Vás dotaz")
			//TODO - přepracovat na localeTemplate
			->setPostProcessMessageTemplate("templates/Emails/Messages/{$this->translator->getLocale()}/questionFormMessage.latte")
			
			->setFlashMessage("Váš dotaz byl odeslán", "Při odesílání dotazu došlo k chybě. Zkuste dotaz odeslat znovu.");

		return $form->create();
	}
}