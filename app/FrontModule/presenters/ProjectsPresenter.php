<?php

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;

class ProjectsPresenter extends BasePresenter
{
	/** Základní vlastnosti společnés pro všechny moduly */
	use \App\Presenters\ProjectsPresenterTrait;

	/**
	 * Přehled projektů
	 * @return void
	 */

	public function renderDefault()
	{
		$this->template->projectsCollection = $this->projectsService->projectsCollection()
			->attach($this)
			->itemsPerPage(16)
			->find();
	}
	
	/**
	 * Detail projektu
	 * @param integer $id ID projektu, pro který zobrazujeme detail
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionDetail($id)
	{
		if (is_numeric($id) && ($this->projectEntity = $this->projectsService->projectEntity()->find($id))) {
			
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit detail developerského projektu
	 * @return void
	 */

	public function renderDetail()
	{
		$this->template->projectEntity = $this->projectEntity;

		/** Makléři tohoto projektu */
		$this->template->usersCollection = $this->usersService->usersCollection()
			->whereBroker()
			->whereProject($this->projectEntity)
			->find();

		/** Články */
		$this->template->articlesCollection = $this->articlesService->articlesCollection()
			->whereOwner($this->projectEntity)
			//->attach($this)
			->itemsPerPage(5)
			->find();

		/** Nemovitosti zařazené do tohoto projektu */
		$this->template->estatesCollection = $this->estatesService->estatesCollection()
			->whereActive()
			->whereProject($this->projectEntity)
			->attach($this)
			->itemsPerPage(10)
			->find();
	}

	/**
	 * Článek tohoto projektu
	 * @param integer $id ID článku, který chceme zobrazit
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionArticle($id)
	{
		if (is_numeric($id) && ($this->articleEntity = $this->articlesService->articleEntity()->find($id)) && ($this->projectEntity = $this->projectsService->projectEntity()->find($this->articleEntity->ownerId))) {

		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit článek projektu
	 * @return void
	 */

	public function renderArticle()
	{
		$this->template->projectEntity = $this->projectEntity;

		$this->template->articleEntity = $this->articleEntity;
	}
}