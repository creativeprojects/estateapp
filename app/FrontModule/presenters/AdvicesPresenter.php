<?php

namespace App\FrontModule\Presenters;

use Nette\Application\BadRequestException;

class AdvicesPresenter extends \App\FrontModule\Presenters\BasePresenter
{
	/** Základní vlastnosti presenteru */
	use \App\Presenters\AdvicesPresenterTrait;

	/**
	 * Přehled článků
	 * @return void
	 */

	public function actionDefault()
	{
		$this->advicesQuestionEntity = $this->advicesQuestionsService->questionEntity()->blank();
	}

	/**
	 * Přehled článků
	 * @return void
	 */

	public function renderDefault()
	{
		$this->template->articlesCollection = $this->advicesArticlesService->articlesCollection()
			->newestFirst()
			->wherePublished()
			->attach($this)
			->itemsPerPage(20)
			->find();
	}

	/**
	 * Detail článku
	 * @param integer $id ID článku, který chceme zobrazit
	 * @return void
	 * @throws BadRequestException
	 */

	public function actionDetail($id)
	{
		if (is_numeric($id) && $this->advicesArticleEntity = $this->advicesArticlesService->articleEntity()->findPublished($id)) {
			
		} else {
			throw new BadRequestException;
		}
	}

	/**
	 * Vykreslit detail článku
	 * @return void
	 */

	public function renderDetail()
	{
		$this->template->articleEntity = $this->advicesArticleEntity;
	}
}