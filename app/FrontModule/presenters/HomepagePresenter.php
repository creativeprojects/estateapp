<?php

namespace App\FrontModule\Presenters;

class HomepagePresenter extends BasePresenter
{
	/** @var \App\Services\HomepageService @inject */
	public $homepageService;

	/** @var \App\Collections\EstatesCollection */
	public $estatesCollection;

	/**
	 * Úvodní stránka
	 * @return void
	 */

	public function actionDefault()
	{
		//Výběr nemovitostí
		$this->estatesCollection = $this->estatesService->estatesCollection()->whereDomain()->whereActive();
		$this->estatesCollection->attach($this);
		
		//4 nemovitosti, které mají advert_onhomepage = TRUE
		//Pokud nejsou 4 s advert_onhomepage, najít 4 první nemovitosti
		$this->estatesCollection->query
			->limit(4)
			->orderBy("advert_onhomepage", "DESC");
	}

	/**
	 * Vykreslení úvodní stránky
	 * @return void
	 */

	public function renderDefault()
	{
		//Seznamy typů nemovitostí (Byty, Domy, ...)
		$this->template->lists = $this->homepageService->getLists();

		//Aktivní okresy - okresy ve kterých má RK aktivní nemovitosti
		//TODO - neměli bychom sem zahrnout i prodané nemovitosti? Třeba jinou barvou
		$this->template->activeRegions = $this->homepageService->getActiveRegionsByDomain();

		//Vybrané nemovitosti
		$this->template->estatesCollection = $this->estatesCollection->find();
	}
}