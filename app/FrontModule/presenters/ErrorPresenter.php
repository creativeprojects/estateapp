<?php

namespace App\FrontModule\Presenters;

class ErrorPresenter extends BasePresenter
{
	use \CP\Presenter\ErrorPresenterTrait;
}