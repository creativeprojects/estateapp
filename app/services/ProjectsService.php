<?php

namespace App\Services;

use App\Collections\ProjectsCollection;
use App\Collections\UsersCollection;
use App\Entities\ProjectEntity;

class ProjectsService extends BaseService
{
	/** @var \App\Services\UserService @inject */
	public $userService;

	/**
	 * Získat novou instanci entity projektu
	 * @return App\Entities\ProjectEntity
	 */

	public function projectEntity()
	{
		return new ProjectEntity;
	}

	/**
	 * Získat nvou instanci kolekce projektů
	 * @return App\Collections\ProjectsCollection
	 */

	public function projectsCollection()
	{
		return new ProjectsCollection;
	}

	public function getUsersCollectionByEntity(ProjectEntity $projectEntity)
	{
		$userIds = [];

		foreach ($projectEntity->project_users AS $userEntity) {
			$userIds[] = $userEntity->getId();
		}

		return $this->getUsersCollectionByUserIds($userIds);
	}

	public function getUsersCollectionByUserIds($userIds = [])
	{
		$usersCollection = new UsersCollection;

		$usersCollection->query
			->where((count($userIds) ? "id IN (" . implode(",", $userIds) . ")" : "0 = 1"));

		return $usersCollection;
	}

	public function getUsersCollection($projectEntity)
	{
		$usersCollection = new UsersCollection;

		$usersCollection->query
			->leftJoin("{$this->repository->projectsUsers} AS pu ON pu.userId = u.id")
			->where("pu.project_id = {$projectentity->getId()}");

		return $usersCollection;
	}

	/** Options **/

	public function getOptionsByDomain()
	{
		$projectsCollection = $this->projectsCollection()->find();
		
		$options = [
			"" => "Není součástí developerského projektu"
		];

		foreach ($projectsCollection->getAll() AS $projectEntity) {
			$options[$projectEntity->getId()] = $projectEntity->getTitle() . " (" . $projectEntity->getReadableStatus() . ")";
		}

		return $options;
	}

	public function getUserOptionsByDomain($disabledUserId = [])
	{
		$options = [];

		//TODO - načíst z USersCollection
		$users = $this->repository->users->getAllBy(["domainId" => $this->thisDomainEntity->getId(), "role" => "broker"]);

		foreach ($users AS $user) {
			if (!in_array($user["id"], $disabledUserId))
				$options[$user["id"]] = "{$user["name"]}";
		}

		return $options;
	}

	/** Other */

	//TODO - na co se používá?
	public function getMenu()
	{
		dump("MUSIME PREPRACOVAT");
	}
}