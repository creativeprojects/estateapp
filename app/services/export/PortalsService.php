<?php

namespace App\Services\Export;

class PortalsService extends \App\Services\BaseService
{
	public function getActiveCollection()
	{
		return new \App\Collections\Export\Portals\ActivePortalsCollection($this);
	}
}