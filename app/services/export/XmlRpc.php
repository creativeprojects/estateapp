<?php

namespace App\Services\ExportService;

use Nette,
	Nette\Utils\DateTime,
	CP;

class XmlRpc extends \App\Services\ExportService
{
	public $parent;

	protected $clientId;

	protected $password;

	protected $key;

	public function __construct($parent)
	{
		$this->parent = $parent;

		$this->usersService = $parent->usersService;

		$this->clientService = $parent->clientService;
	}

	public function export($estate)
	{
		// TODO projekty
		unset($estate["project_id"]);
		unset($estate["energy_performance_attachment"]);

		$this->setClientInfo($estate["clientId"]);
		$this->login();
		if ($estate["export"] == 1 AND in_array($estate["advert_status"], array(1, 2)) AND $estate["removed"] == NULL) {
			$result = $this->_export($estate);
		} else {
			$result = $this->_delete($estate);
		}
		$this->logout();
		return $result;
	}

	private function _export($estate)
	{
		$estateId = $estate["estate_id"];

		$images = $estate["images"];

		$estate = $this->changeDescription($estate);
		$estate = $this->addRequiredBool($estate);
		$estate = $this->setSeller($estate);
		if ($estate["advert_status"] == 2) {
			$estate["extra_info"] = 1;
		}

		$estate = $this->translate($estate);

		$msg = new \xmlrpcmsg("addAdvert");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$msg->addParam(php_xmlrpc_encode($estate));

		$response = $this->client->send($msg);

		$this->sessionId = $this->computeSessionId($this->sessionId);
		$msg = new \xmlrpcmsg("listAdvert");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$list = $this->client->send($msg);
		foreach (php_xmlrpc_decode($list->value())["output"] as $advert) {
			if ($advert["advert_rkid"] == $estateId) {
				$url = $advert["advert_url"];
				break;
			}
		}

		$this->sessionId = $this->computeSessionId($this->sessionId);
		$result = php_xmlrpc_decode($response->value());

		if (floor($result["status"] / 100) != 2) {
			$message = "{$result["status"]} - {$result["statusMessage"]}";
			return array(
				"error" => TRUE,
				"message" => $message,
				"url" => isset($url) ? $url : NULL
			);
		} else {
			$this->uploadImages($estateId, $images);
			$message = "{$result["status"]} - {$result["statusMessage"]}";
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => $url ? $url : NULL
			);
		}
	}

	private function uploadImages($estateId, $images)
	{
		$msg = new \xmlrpcmsg("listPhoto");
		$msg->addParam(php_xmlrpc_encode($this->sessionId), "string");
		$msg->addParam(new \xmlrpcval("", "int"));
		$msg->addParam(new \xmlrpcval($estateId, "string"));
		$list = $this->client->send($msg);

		$this->sessionId = $this->computeSessionId($this->sessionId);

		foreach (php_xmlrpc_decode($list->value())["output"] as $img) {
			$imagesExported[$img["photo_rkid"]] = $img;
		}

//		dump($imagesExported);
//		die;
		foreach ($images as $key => $image) {
			if (!isset($imagesExported[$key]) OR $image->rank != @$imagesExported[$key]["order"]) {
				$filePath = WWW_DIR . "/" . $image->src;
				$pathInfo = pathinfo($filePath);
				$fileName = $pathInfo["dirname"] . "/../export/" . $pathInfo["basename"];
				$openFile = fopen($fileName, "r");
				if ($openFile) {
					$contents = fread($openFile, filesize($fileName));
					fclose($openFile);
				}
				$img = array(
					"data" => new \xmlrpcval($contents, "base64"),
					"main" => new \xmlrpcval($image->main, "boolean"),
					"order" => new \xmlrpcval($image->rank, "int"),
					"photo_rkid" => new \xmlrpcval($image->id, "string")
				);

				$msg = new \xmlrpcmsg("addPhoto");
				$msg->addParam(php_xmlrpc_encode($this->sessionId));
				$msg->addParam(php_xmlrpc_encode(0));
				$msg->addParam(php_xmlrpc_encode((string)$estateId));
				$msg->addParam(php_xmlrpc_encode($img));

				$response = $this->client->send($msg);
				$this->sessionId = $this->computeSessionId($this->sessionId);
				$imgResult = php_xmlrpc_decode($response->value());
				if (floor($imgResult["status"] / 100) != 2) {
					$message = "{$imgResult["status"]} - {$imgResult["statusMessage"]}";
					return array(
						"error" => TRUE,
						"message" => $message,
						"url" => isset($url) ? $url : NULL
					);
				}
			}
			if (isset($imagesExported[$key])) {
				unset($imagesExported[$key]);
			}
		}

		foreach ($imagesExported as $delImg) {
			$msg = new \xmlrpcmsg("delPhoto");
			$msg->addParam(php_xmlrpc_encode($this->sessionId), "string");
			$msg->addParam(new \xmlrpcval("", "int"));
			$msg->addParam(new \xmlrpcval($delImg["photo_rkid"], "string"));
			$list = $this->client->send($msg);

			$this->sessionId = $this->computeSessionId($this->sessionId);
		}
	}

	private function _delete($estate)
	{
		$exportedEstates = $this->getExportedEstates();
		if (array_key_exists($estate['estate_id'], $exportedEstates)) {
			// delete estate
			$msg = new \xmlrpcmsg("delAdvert");
			$msg->addParam(php_xmlrpc_encode($this->sessionId));
			$msg->addParam(php_xmlrpc_encode(0));
			$msg->addParam(php_xmlrpc_encode((string)$estate["estate_id"]));
			$response = $this->client->send($msg);
			$this->sessionId = $this->computeSessionId($this->sessionId);
			$result = php_xmlrpc_decode($response->value());
			$message = $result["status"] . " - " . $result["statusMessage"];
			if (floor($result["status"] / 100) != 2) {
				return array(
					"error" => TRUE,
					"message" => $message,
					"url" => NULL
				);
			}
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => NULL
			);
		}
		return NULL;
	}

	private function getExportedEstates()
	{
		$msg = new \xmlrpcmsg("listAdvert");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$estates = php_xmlrpc_decode($response->value())["output"];

		foreach ($estates as $key => $estate) {
			$estates[$estate["advert_rkid"]] = $estate;
			unset($estates[$key]);
		}
		return $estates;
	}

//////////////////////////////////// EXPORT BROKER //////////////////////////////////

	public function exportBroker($broker)
	{
		$this->setClientInfo($broker["clientId"]);
		$this->login();

		if ($broker["export"] == 1 && $broker["removed"] == NULL) {
			return $this->_exportBroker($broker);
		} else {
			return $this->_deleteBroker($broker);
		}
	}

	private function _deleteBroker($broker)
	{
		$exportedBrokers = $this->getExportedBrokers();
		if (array_key_exists($broker['id'], $exportedBrokers)) {
			// delete estate
			$msg = new \xmlrpcmsg("delSeller");
			$msg->addParam(php_xmlrpc_encode($this->sessionId));
			$msg->addParam(php_xmlrpc_encode(0));
			$msg->addParam(php_xmlrpc_encode((string)$broker["id"]));
			$response = $this->client->send($msg);
			$this->sessionId = $this->computeSessionId($this->sessionId);
			$result = php_xmlrpc_decode($response->value());
			$message = $result["status"] . " - " . $result["statusMessage"];
			if (floor($result["status"] / 100) != 2) {
				return array(
					"error" => TRUE,
					"message" => $message,
					"url" => NULL
				);
			}
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => NULL
			);
		}
		return NULL;
	}

	private function _exportBroker($broker)
	{
		$this->setClientInfo($broker["clientId"]);
		$this->login();

		$seller = array(
			"client_login" => $broker["email"],
			"client_name" => $broker["name"],
			"contact_gsm" => $broker["phone"]
		);

		$msg = new \xmlrpcmsg("addSeller");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$msg->addParam(php_xmlrpc_encode(0));
		$msg->addParam(php_xmlrpc_encode((string)$broker["id"]));
		$msg->addParam(php_xmlrpc_encode($seller));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$result = php_xmlrpc_decode($response->value());
		$this->logout($this->sessionId);
		if (floor($result["status"] / 100) != 2) {
			if ($result["status"] == 463) {
				$message = "Makléřův email není registrován na Sreality.cz";
			}
			$message = "{$result["status"]} - {$result["statusMessage"]}";

			return array(
				"error" => TRUE,
				"message" => $message,
				"url" => NULL
			);
		} else {
			return array(
				"error" => FALSE,
				"message" => $result["status"] . " - " . $result["statusMessage"],
				"url" => NULL
			);
		}
	}

	private function getExportedBrokers()
	{
		$msg = new \xmlrpcmsg("listSeller");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$brokers = php_xmlrpc_decode($response->value())["output"];

		foreach ($brokers as $key => $broker) {
			$brokers[$broker["seller_rkid"]] = $broker;
			unset($broker[$key]);
		}
		return $brokers;
	}

	public function login()
	{
		$hash = $this->getHash();
		$this->sessionId = $this->computeSessionId($hash);

		$params = array(new \xmlrpcval($this->sessionId));
		$msg = new \xmlrpcmsg("login", $params);

		$response = $this->client->send($msg);

		$login = php_xmlrpc_decode($response->value());
		if ($login["status"] != 200) {
			die("Chyba pri prihlaseni [{$login["status"]}]: {$login["statusMessage"]}");
		}
		$GLOBALS['xmlrpc_internalencoding'] = 'UTF-8';
		$this->sessionId = $this->computeSessionId($this->sessionId);
	}

	public function logout()
	{
		$params = array(new \xmlrpcval($this->sessionId));
		$msg = new \xmlrpcmsg("logout", $params);
		$response = $this->client->send($msg);

		$result = php_xmlrpc_decode($response->value());
		if ($result["status"] != 200) {
			die("Chyba pri odhlaseni [{$result["status"]}]: {$result["statusMessage"]}");
		}
	}

	public function getHash()
	{
		$msg = new \xmlrpcmsg("getHash");
		$params = new \xmlrpcval($this->clientId, "int");
		$msg->addParam(php_xmlrpc_encode($params));

		$response = $this->client->send($msg);

		if (!$response->val) {
			die("Chyba při připojení: {$response->errno} - {$response->errstr}");
		}
		$getHash = php_xmlrpc_decode($response->value());

		if ($getHash["status"] != 200) {
			die("Chyba pri volani getHash [{$getHash["status"]}]: {$getHash["statusMessage"]}");
		}
		return $this->hash($getHash);
	}

	public function hash($getHash)
	{
		return $getHash["output"][0]["sessionId"];
	}

	public function computeSessionId($hash)
	{
		$newVarPart = md5($hash . $this->password . $this->key);
		return substr($hash, 0, 48) . $newVarPart;
	}
}