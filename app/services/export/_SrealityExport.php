<?php

namespace App\Services\ExportService;

use Nette,
	Nette\Utils\DateTime,
	CP;

// Dokumentace: https://admin.sreality.cz/doc/import.pdf

class SrealityExport_ extends \App\Services\ExportService
{
	public $client;

	private $clientId;

	private $password;

	private $key;

	private $sessionId;

	public function __construct(
	Nette\Database\Context $database, Nette\Security\IUserStorage $user, Nette\Localization\ITranslator $translator, CP\Emails\Service $emailsService, CP\Notifications\Service $notificationsService, CP\Files\Service $filesService, CP\Services\Slugs $slugsService, CP\Regions\Service $regionsService
	)
	{
		parent::__construct($database, $user, $translator, $emailsService, $notificationsService, $filesService, $slugsService, $regionsService);

		// !!!!
		$this->clientId = $clientsService->getById(1)["sreality_client_id"];

		$this->client = new \xmlrpc_client("/RPC2", "import.sreality.cz", 80);
	}

	public function exportEstate($estate)
	{

		$this->setClientInfo($estate["clientId"]);
		$this->login();

		$msg = new \xmlrpcmsg("listAdvert");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$result = php_xmlrpc_decode($response->value());

		$exists = FALSE;
		foreach ($result["output"] as $srealityEstate) {
			if ($srealityEstate["advert_rkid"] == $estate["estate_id"]) {
				$exists = TRUE;
				break;
			}
		}
		$estateId = $estate["estate_id"];
		if (in_array($estate["advert_status"], array(1, 2))) {

			$images = $estate["images"];

			$estate = $this->changeDescription($estate);
			$estate = $this->addRequiredBool($estate);
			$estate = $this->setSeller($estate);
			if ($estate["advert_status"] == 2) {
				$estate["extra_info"] = 1;
			}

			$estate = $this->translate($estate);

			$msg = new \xmlrpcmsg("addAdvert");
			$msg->addParam(php_xmlrpc_encode($this->sessionId));
			$msg->addParam(php_xmlrpc_encode($estate));

			$response = $this->client->send($msg);
			$this->sessionId = $this->computeSessionId($this->sessionId);
			$result = php_xmlrpc_decode($response->value());


			if (floor($result["status"] / 100) != 2) {
				return "Chyba pri exportu inzeratu na SReality: {$result["status"]} - {$result["statusMessage"]}";
			} else {
				foreach ($images as $image) {
					$filePath = WWW_DIR . "/" . $image->src;
					$pathInfo = pathinfo($filePath);
					$fileName = $pathInfo["dirname"] . "/../export/" . $pathInfo["basename"];
					$openFile = fopen($fileName, "r");
					if ($openFile) {
						$contents = fread($openFile, filesize($fileName));
						fclose($openFile);
					}
					$img = array(
						"data" => new \xmlrpcval($contents, "base64"),
						"main" => new \xmlrpcval($image->main, "boolean"),
						"order" => new \xmlrpcval($image->rank, "int"),
						"photo_rkid" => new \xmlrpcval($image->id, "string")
					);

					$msg = new \xmlrpcmsg("addPhoto");
					$msg->addParam(php_xmlrpc_encode($this->sessionId));
					$msg->addParam(php_xmlrpc_encode(0));
					$msg->addParam(php_xmlrpc_encode((string)$estateId));
					$msg->addParam(php_xmlrpc_encode($img));

					$response = $this->client->send($msg);
					$this->sessionId = $this->computeSessionId($this->sessionId);
					$imgResult = php_xmlrpc_decode($response->value());
					if (floor($imgResult["status"] / 100) != 2) {
						return "Chyba pri exportu inzeratu na SReality: {$result["status"]} - {$result["statusMessage"]}";
					}
				}
			}
		} elseif ($exists && !in_array($estate["advert_status"], array(1, 2))) {
			// delete estate
			$msg = new \xmlrpcmsg("delAdvert");
			$msg->addParam(php_xmlrpc_encode($this->sessionId));
			$msg->addParam(php_xmlrpc_encode(0));
			$msg->addParam(php_xmlrpc_encode((string)$estateId));
			$response = $this->client->send($msg);
			$this->sessionId = $this->computeSessionId($this->sessionId);
			$result = php_xmlrpc_decode($response->value());
		}

		$this->logout($this->sessionId);
		return TRUE;
	}

	public function exportBroker($broker)
	{
		$this->setClientInfo($this->clientService->getClient()["id"]);
		$this->login();

		$seller = array(
			"client_login" => $broker["email"],
			"client_name" => $broker["name"],
			"contact_gsm" => $broker["phone"]
		);

		$msg = new \xmlrpcmsg("addSeller");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$msg->addParam(php_xmlrpc_encode(0));
		$msg->addParam(php_xmlrpc_encode((string)$broker["id"]));
		$msg->addParam(php_xmlrpc_encode($seller));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$result = php_xmlrpc_decode($response->value());

		if (floor($result["status"] / 100) != 2) {
			if ($result["status"] == 463) {
				return "Chyba pri exportu inzeratu na SReality: Makléřův email není registrován na Sreality.cz";
			}
			return "Chyba pri exportu inzeratu na SReality: {$result["status"]} - {$result["statusMessage"]}";
		}

		$this->logout($this->sessionId);
	}

	public function delBroker($brokerId, $clientId)
	{
		$this->setClientInfo($clientId);
		$this->login();

		$msg = new \xmlrpcmsg("delSeller");
		$msg->addParam(php_xmlrpc_encode($this->sessionId));
		$msg->addParam(php_xmlrpc_encode(0));
		$msg->addParam(php_xmlrpc_encode((string)$brokerId));
		$response = $this->client->send($msg);
		$this->sessionId = $this->computeSessionId($this->sessionId);
		$result = php_xmlrpc_decode($response->value());

		$this->logout($this->sessionId);
		return $response;
	}

//	public function getHash()
//	{
//		$msg = new \xmlrpcmsg("getHash");
//		$params = new \xmlrpcval($this->clientId, "int");
//		$msg->addParam(php_xmlrpc_encode($params));
//
//		$response = $this->client->send($msg);
//		$getHash = php_xmlrpc_decode($response->value());
//
//		if ($getHash["status"] != 200) {
//			die("Chyba pri volani getHash [{$getHash["status"]}]: {$getHash["statusMessage"]}");
//		}
//
//		return $getHash["output"][0]["sessionId"];
//	}
//
//	public function login()
//	{
//		$hash = $this->getHash();
//		$this->sessionId = $this->computeSessionId($hash);
//
//		$params = array(new \xmlrpcval($this->sessionId));
//		$msg = new \xmlrpcmsg("login", $params);
//		$response = $this->client->send($msg);
//
//		$login = php_xmlrpc_decode($response->value());
//		if ($login["status"] != 200) {
//			die("Chyba pri prihlaseni [{$login["status"]}]: {$login["statusMessage"]}");
//		}
//		$GLOBALS['xmlrpc_internalencoding'] = 'UTF-8';
//		$this->sessionId = $this->computeSessionId($this->sessionId);
//	}
//
//	public function computeSessionId($hash)
//	{
//		$newVarPart = md5($hash . $this->password . $this->key);
//		return substr($hash, 0, 48) . $newVarPart;
//	}
//
//	public function logout($sessionId)
//	{
//		$params = array(new \xmlrpcval($this->sessionId));
//		$msg = new \xmlrpcmsg("logout", $params);
//		$response = $this->client->send($msg);
//
//		$result = php_xmlrpc_decode($response->value());
//		if ($result["status"] != 200) {
//			die("Chyba pri odhlaseni [{$result["status"]}]: {$result["statusMessage"]}");
//		}
//	}

	public function changeDescription($estate)
	{
		$estate["description"] = $estate["estate_description"]["cs"];
		$estate["advert_price_text_note"] = $estate["advert_price_text_note"]["cs"];
		$estate["advert_rkid"] = (string)$estate["estate_id"];
		$estate["garage_count"] = (int)$estate["garage_count"];
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		$estate["workshop_area"] = (int)$estate["workshop_area"];
		$estate["production_area"] = (int)$estate["production_area"];
		$estate["acceptance_year"] = (int)$estate["acceptance_year"];
		$estate["parking"] = (int)$estate["parking"];
		$estate["basin_area"] = (int)$estate["basin_area"];
		$estate["nolive_total_area"] = (int)$estate["nolive_total_area"];
		$estate["energy_performance_summary"] = (int)$estate["energy_performance_summary"];
		$estate["object_age"] = (int)$estate["object_age"];
		$estate["store_area"] = (int)$estate["store_area"];
		$estate["offices_area"] = (int)$estate["offices_area"];
		$estate["floors"] = (int)$estate["floors"];
		$estate["terrace_area"] = (int)$estate["terrace_area"];
		$estate["reconstruction_year"] = (int)$estate["reconstruction_year"];
		$estate["balcony_area"] = (int)$estate["balcony_area"];
		$estate["underground_floors"] = (int)$estate["underground_floors"];
		$estate["building_area"] = (int)$estate["building_area"];
		$estate["shop_area"] = (int)$estate["shop_area"];
		$estate["cellar_area"] = (int)$estate["cellar_area"];
		$estate["usable_area_ground"] = (int)$estate["usable_area_ground"];
		$estate["garden_area"] = (int)$estate["garden_area"];
		$estate["floor_area"] = (int)$estate["floor_area"];
		$estate["ceiling_height"] = (int)$estate["ceiling_height"];
		$estate["mortgage_percent"] = (int)$estate["mortgage_percent"];



//		$estate["sale_date"] = date(DATE_ISO8601, strtotime('2010-12-30 23:21:46'));
//		$estate["sale_date"] = new DateTime(time());
//		$estate["sale_date"]->format(DateTime::ISO8601);
//		$estate["sale_date"] = "20060107T01:53:00";
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		return $estate;
	}

	public function setSeller($estate)
	{
		$estate["seller_rkid"] = (string)$estate["userId"];
		return $estate;
	}

	public function addRequiredBool($estate)
	{
		$required = array(
			"balcony",
			"loggia",
			"terrace",
			"cellar",
			"parking_lots",
			"garage",
			"basin"
		);
		foreach ($required as $value) {
			if ($estate[$value] === NULL) {
				$estate[$value] = 0;
			}
		}
		return $estate;
	}
	/*	 * *****************************************************************
	 * Překlady
	 * **************************************************************** */
	public $translateTable = array(
		"description" => NULL,
		"advert_function" => NULL,
		"advert_lifetime" => NULL,
		"advert_price" => NULL,
		"advert_price_original" => NULL,
		"advert_price_currency" => NULL,
		"advert_price_unit" => NULL,
		"advert_type" => NULL,
		"locality_city" => NULL,
		"locality_zip" => NULL,
		"locality_inaccuracy_level" => NULL,
		"advert_id" => NULL,
		"advert_rkid" => NULL,
		"advert_room_count" => NULL,
		"advert_subtype" => NULL,
		"balcony" => NULL,
		"basin" => NULL,
		"building_condition" => NULL,
		"building_type" => NULL,
		"cellar" => NULL,
		"estate_area" => NULL,
		"floor_number" => NULL,
		"garage" => NULL,
		"locality_latitude" => NULL,
		"locality_longitude" => NULL,
		"locality_ruian" => NULL,
		"locality_ruian_level" => NULL,
		"locality_uir" => NULL,
		"locality_uir_level" => NULL,
		"loggia" => NULL,
		"object_type" => NULL,
		"ownership" => NULL,
		"parking_lots" => NULL,
		"project_id" => NULL,
		"project_rkid" => NULL,
		"terrace" => NULL,
		"usable_area" => NULL,
		"acceptance_year" => NULL,
		"advert_code" => NULL,
		"advert_low_energy" => NULL,
		"advert_price_charge" => NULL,
		"advert_price_commission" => NULL,
		"advert_price_legal_services" => NULL,
		"advert_price_negotiation" => NULL,
		"advert_price_vat" => NULL,
		"annuity" => NULL,
		"auction_advertisement_pdf" => NULL,
//		"auction_date" => NULL,
//		"auction_date_tour" => NULL,
//		"auction_date_tour2" => NULL,
		"auction_kind" => NULL,
		"auction_place" => NULL,
		"auction_review_pdf" => NULL,
		"balcony_area" => NULL,
		"basin_area" => NULL,
//		"beginning_date" => NULL,
		"building_area" => NULL,
		"ceiling_height" => NULL,
		"cellar_area" => NULL,
		"cost_of_living" => NULL,
		"easy_access" => NULL,
		"electricity" => NULL,
		"elevator" => NULL,
		"energy_efficiency_rating" => NULL,
		"energy_performance_attachment" => NULL,
		"energy_performance_certificate" => NULL,
		"energy_performance_summary" => NULL,
		"extra_info" => NULL,
//		"finish_date" => NULL,
//		"first_tour_date" => NULL,
//		"first_tour_date_to" => NULL,
		"flat_class" => NULL,
		"floor_area" => NULL,
		"floors" => NULL,
		"furnished" => NULL,
		"garage_count" => NULL,
		"garden_area" => NULL,
		"garret" => NULL,
		"gas" => NULL,
		"gully" => NULL,
		"heating" => NULL,
		"locality_citypart" => NULL,
		"locality_co" => NULL,
		"locality_cp" => NULL,
		"locality_street" => NULL,
		"loggia_area" => NULL,
		"mortgage" => NULL,
		"mortgage_percent" => NULL,
		"nolive_total_area" => NULL,
		"object_age" => NULL,
		"object_kind" => NULL,
		"object_location" => NULL,
		"offices_area" => NULL,
		"parking" => NULL,
		"personal" => NULL,
		"price_auction_principal" => NULL,
		"price_expert_report" => NULL,
		"price_minimum_bid" => NULL,
		"production_area" => NULL,
		"protection" => NULL,
//		"ready_date" => NULL,
		"reconstruction_year" => NULL,
		"road_type" => NULL,
//		"sale_date" => NULL,
		"shop_area" => NULL,
		"spor_percent" => NULL,
		"steps" => NULL,
		"store_area" => NULL,
		"surroundings_type" => NULL,
		"telecommunication" => NULL,
		"terrace_area" => NULL,
		"transport" => NULL,
		"underground_floors" => NULL,
		"usable_area_ground" => NULL,
		"user_status" => NULL, // ??
		"water" => NULL,
		"workshop_area" => NULL,
		"youtube_video" => NULL, // fORMAT??
		"advert_price_hidden" => NULL,
		"advert_price_text_note" => NULL,
		"locality_citypart" => NULL,
		"locality_co" => NULL,
		"locality_cp" => NULL,
		"locality_street" => NULL,
		"seller_id" => NULL,
		"seller_rkid" => NULL,
		"extra_info" => NULL
	);

}