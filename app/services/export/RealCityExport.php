<?php

namespace App\Services;

use Nette,
	Nette\Utils\DateTime,
	CP;

// Dokumentace: http://realitymix.centrum.cz/import/documentation/xml-rpc/

class RealCityExport extends XmlRpc
{

	public function setClientInfo($clientId)
	{
		$client = $this->clientService->getById($clientId);

		$this->clientId = (string)$client["export_settings"]["realcity_client_id"];
		$this->password = $client["export_settings"]["realcity_password"];
		$this->key = $client["export_settings"]["realcity_key"];

		$this->client = new \xmlrpc_client("/xmlrpc/server.php", "import.realcity.cz", 80);
	}

	public function hash($getHash)
	{
		return $getHash["output"][0]["session_id"];
	}

	public function computeSessionId($hash)
	{
		$newVarPart = md5($hash . md5($this->password) . $this->key);
		return substr($hash, 0, 48) . $newVarPart;
	}

	public function changeDescription($estate)
	{
		$estate["description"] = $estate["estate_description"]["cs"];
		$estate["advert_price_text_note"] = $estate["advert_price_text_note"]["cs"];
		$estate["advert_rkid"] = (string)$estate["estate_id"];
		$estate["garage_count"] = (int)$estate["garage_count"];
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		$estate["workshop_area"] = (int)$estate["workshop_area"];
		$estate["production_area"] = (int)$estate["production_area"];
		$estate["acceptance_year"] = (int)$estate["acceptance_year"];
		$estate["parking"] = (int)$estate["parking"];
		$estate["basin_area"] = (int)$estate["basin_area"];
		$estate["nolive_total_area"] = (int)$estate["nolive_total_area"];
		$estate["energy_performance_summary"] = (int)$estate["energy_performance_summary"];
		$estate["object_age"] = (int)$estate["object_age"];
		$estate["store_area"] = (int)$estate["store_area"];
		$estate["offices_area"] = (int)$estate["offices_area"];
		$estate["floors"] = (int)$estate["floors"];
		$estate["terrace_area"] = (int)$estate["terrace_area"];
		$estate["reconstruction_year"] = (int)$estate["reconstruction_year"];
		$estate["balcony_area"] = (int)$estate["balcony_area"];
		$estate["underground_floors"] = (int)$estate["underground_floors"];
		$estate["building_area"] = (int)$estate["building_area"];
		$estate["shop_area"] = (int)$estate["shop_area"];
		$estate["cellar_area"] = (int)$estate["cellar_area"];
		$estate["usable_area_ground"] = (int)$estate["usable_area_ground"];
		$estate["garden_area"] = (int)$estate["garden_area"];
		$estate["floor_area"] = (int)$estate["floor_area"];
		$estate["ceiling_height"] = (int)$estate["ceiling_height"];
		$estate["mortgage_percent"] = (int)$estate["mortgage_percent"];



//		$estate["sale_date"] = date(DATE_ISO8601, strtotime('2010-12-30 23:21:46'));
//		$estate["sale_date"] = new DateTime(time());
//		$estate["sale_date"]->format(DateTime::ISO8601);
//		$estate["sale_date"] = "20060107T01:53:00";
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		return $estate;
	}

	public function setSeller($estate)
	{
		$estate["seller_rkid"] = (string)$estate["userId"];
		return $estate;
	}

	public function addRequiredBool($estate)
	{
		$required = array(
			"balcony",
			"loggia",
			"terrace",
			"cellar",
			"parking_lots",
			"garage",
			"basin"
		);
		foreach ($required as $value) {
			if ($estate[$value] === NULL) {
				$estate[$value] = 0;
			}
		}
		return $estate;
	}
	/*	 * *****************************************************************
	 * Překlady
	 * **************************************************************** */
	public $translateTable = array(
		"description" => NULL,
		"advert_function" => NULL,
		"advert_lifetime" => NULL,
		"advert_price" => NULL,
		"advert_price_original" => NULL,
		"advert_price_currency" => NULL,
		"advert_price_unit" => NULL,
		"advert_type" => NULL,
		"locality_city" => NULL,
		"locality_zip" => NULL,
		"locality_inaccuracy_level" => NULL,
		"advert_id" => NULL,
		"advert_rkid" => NULL,
		"advert_room_count" => NULL,
		"advert_subtype" => NULL,
		"balcony" => NULL,
		"basin" => NULL,
		"building_condition" => NULL,
		"building_type" => NULL,
		"cellar" => NULL,
		"estate_area" => NULL,
		"floor_number" => NULL,
		"garage" => NULL,
		"locality_latitude" => NULL,
		"locality_longitude" => NULL,
		"locality_ruian" => NULL,
		"locality_ruian_level" => NULL,
		"locality_uir" => NULL,
		"locality_uir_level" => NULL,
		"loggia" => NULL,
		"object_type" => NULL,
		"ownership" => NULL,
		"parking_lots" => NULL,
		"project_id" => NULL,
		"project_rkid" => NULL,
		"terrace" => NULL,
		"usable_area" => NULL,
		"acceptance_year" => NULL,
		"advert_code" => NULL,
		"advert_low_energy" => NULL,
		"advert_price_charge" => NULL,
		"advert_price_commission" => NULL,
		"advert_price_legal_services" => NULL,
		"advert_price_negotiation" => NULL,
		"advert_price_vat" => NULL,
		"annuity" => NULL,
		"auction_advertisement_pdf" => NULL,
//		"auction_date" => NULL,
//		"auction_date_tour" => NULL,
//		"auction_date_tour2" => NULL,
		"auction_kind" => NULL,
		"auction_place" => NULL,
		"auction_review_pdf" => NULL,
		"balcony_area" => NULL,
		"basin_area" => NULL,
//		"beginning_date" => NULL,
		"building_area" => NULL,
		"ceiling_height" => NULL,
		"cellar_area" => NULL,
		"cost_of_living" => NULL,
		"easy_access" => NULL,
		"electricity" => NULL,
		"elevator" => NULL,
		"energy_efficiency_rating" => NULL,
		"energy_performance_attachment" => NULL,
		"energy_performance_certificate" => NULL,
		"energy_performance_summary" => NULL,
		"extra_info" => NULL,
//		"finish_date" => NULL,
//		"first_tour_date" => NULL,
//		"first_tour_date_to" => NULL,
		"flat_class" => NULL,
		"floor_area" => NULL,
		"floors" => NULL,
		"furnished" => NULL,
		"garage_count" => NULL,
		"garden_area" => NULL,
		"garret" => NULL,
		"gas" => NULL,
		"gully" => NULL,
		"heating" => NULL,
		"locality_citypart" => NULL,
		"locality_co" => NULL,
		"locality_cp" => NULL,
		"locality_street" => NULL,
		"loggia_area" => NULL,
		"mortgage" => NULL,
		"mortgage_percent" => NULL,
		"nolive_total_area" => NULL,
		"object_age" => NULL,
		"object_kind" => NULL,
		"object_location" => NULL,
		"offices_area" => NULL,
		"parking" => NULL,
		"personal" => NULL,
		"price_auction_principal" => NULL,
		"price_expert_report" => NULL,
		"price_minimum_bid" => NULL,
		"production_area" => NULL,
		"protection" => NULL,
//		"ready_date" => NULL,
		"reconstruction_year" => NULL,
		"road_type" => NULL,
//		"sale_date" => NULL,
		"shop_area" => NULL,
		"spor_percent" => NULL,
		"steps" => NULL,
		"store_area" => NULL,
		"surroundings_type" => NULL,
		"telecommunication" => NULL,
		"terrace_area" => NULL,
		"transport" => NULL,
		"underground_floors" => NULL,
		"usable_area_ground" => NULL,
		"user_status" => NULL, // ??
		"water" => NULL,
		"workshop_area" => NULL,
		"youtube_video" => NULL, // fORMAT??
		"advert_price_hidden" => NULL,
		"advert_price_text_note" => NULL,
		"locality_citypart" => NULL,
		"locality_co" => NULL,
		"locality_cp" => NULL,
		"locality_street" => NULL,
		"seller_id" => NULL,
		"seller_rkid" => NULL,
		"extra_info" => NULL
	);

}