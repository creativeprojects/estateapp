<?php

namespace App\Services;

use Nette,
	Nette\Utils\DateTime,
	CP;

// Dokumentace: http://realitymix.centrum.cz/import/documentation/xml-rpc/

class RealityMixExport extends XmlRpc
{
	public $hash;

	public function setClientInfo($clientId)
	{
		$client = $this->clientsService->getById($clientId);

		$this->clientId = (string)$client["export_settings"]["realitymix_client_id"];
		$this->password = $client["export_settings"]["realitymix_password"];
		$this->key = $client["export_settings"]["realitymix_key"];

		$this->client = new \xmlrpc_client("/import/rpc/", "realitymix.centrum.cz", 80);
	}

	public function export($estate)
	{
		$counter = $this->parent->repository->clients->getOne($estate["clientId"])["realitymix_export_counter"];
//		dump($counter);
//		if ($counter <= 10) {
//			dump($counter + 1);
//		}
//		die;
		if ($counter <= 10) {
			unset($estate["project_id"]);
			unset($estate["energy_performance_attachment"]);

			$this->setClientInfo($estate["clientId"]);
			$this->login();
			if ($estate["export"] == 1 AND in_array($estate["advert_status"], array(1, 2)) AND $estate["removed"] == NULL) {
				$estate["user_status"] = 1;
				$result = $this->_export($estate);
			} else {
				$result = $this->_delete($estate);
			}
			$this->logout();
			$this->parent->repository->clients->save(array(
				"id" => $estate["clientId"],
				"realitymix_export_counter" => $counter + 1
			));
			return $result;
		}

		return NULL;
	}

	private function _export($estate)
	{
		$estate["rkid"] = $estate["estate_id"];
		$estate["total_area"] = "1";
		$estate["commercial_kind"] = "1";
		$estateId = $estate["estate_id"];

		$images = $estate["images"];

		$estate = $this->changeDescription($estate);
		$estate = $this->addAreaUnit($estate);
		$estate = $this->changeAdvertSubtype($estate);
		$estate = $this->addUir($estate);
		$estate = $this->addRequiredBool($estate);
		$estate = $this->setSeller($estate);
		if ($estate["advert_status"] == 2) {
			$estate ["extra_info"] = 1;
		}

		$estate = $this->translate($estate);

		$params = array(
			php_xmlrpc_encode($this->hash[0]),
			php_xmlrpc_encode($estate)
		);
		$msg = new \xmlrpcmsg("addAdvert", $params);
		$response = $this->client->send($msg);
		$msg = new \xmlrpcmsg("listAdvert", array(php_xmlrpc_encode($this->hash[0])));
		$list = $this->client->send($msg);
// TODO url??
		foreach (php_xmlrpc_decode($list->value())["output"] as $advert) {
			if ($advert["rkid"] == $estateId) {
				$url = "nourl";
				break;
			}
		}
		$result = php_xmlrpc_decode($response->value());

		if (floor($result["status"] / 100) != 2) {
			$message = "{$result["status"]} - {$result["statusMessage"]}";
			return array(
				"error" => TRUE,
				"message" => $message,
				"url" => isset($url) ? $url : NULL
			);
		} else {
			$this->uploadImages($estateId, $images);

			$message = "{$result["status"]} - {$result["statusMessage"]}";
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => isset($url) ? $url : NULL
			);
		}
	}

	private function uploadImages($estateId, $images)
	{
		$msg = new \xmlrpcmsg("listPhoto");
		$msg->addParam(php_xmlrpc_encode($this->hash[0]), "string");
		$msg->addParam(new \xmlrpcval("", "int"));
		$msg->addParam(new \xmlrpcval($estateId, "string"));
		$list = $this->client->send($msg);

		$imagesExported = array();
		foreach (php_xmlrpc_decode($list->value())["output"] as $img) {
			$imagesExported[$img["photo_rkid"]] = $img;
		}

		foreach ($images as $key => $image) {
			if (!isset($imagesExported[$key]) OR $image->rank != @$imagesExported[$key]["order"]) {
				$filePath = WWW_DIR . "/" . $image->src;
				$pathInfo = pathinfo($filePath);
				$fileName = $pathInfo["dirname"] . "/../export/" . $pathInfo["basename"];
				$openFile = fopen($fileName, "r");
				if ($openFile) {
					$contents = fread($openFile, filesize($fileName));
					fclose($openFile);
				}
				$img = array(
					new \xmlrpcval($this->hash[0], "string"), // session_id
					new \xmlrpcval(""), // advert_id
					new \xmlrpcval($estateId, "int"), // rkid
					new \xmlrpcval($contents, "base64"), // data
					new \xmlrpcval($image->main, "boolean"), // main
				);

				$msg = new \xmlrpcmsg("addPhoto", $img);

				$response = $this->client->send($msg);
				$imgResult = php_xmlrpc_decode($response->value());
				if (floor($imgResult["status"] / 100) != 2) {
					$message = "{$imgResult["status"]} - {$imgResult["statusMessage"]}";
					return array(
						"error" => TRUE,
						"message" => $message,
						"url" => isset($url) ? $url : NULL
					);
				}
			}
			if (isset($imagesExported[$key])) {
				unset($imagesExported[$key]);
			}
		}

		foreach ($imagesExported as $delImg) {
			$msg = new \xmlrpcmsg("delPhoto");
			$msg->addParam(php_xmlrpc_encode($this->hash[0]), "string");
			$msg->addParam(new \xmlrpcval($delImg["photo_id"], "int"));
			$msg->addParam(new \xmlrpcval("", "string"));
			$list = $this->client->send($msg);
		}
	}

	private function _delete($estate)
	{
		$exportedEstates = $this->getExportedEstates();
		if (array_key_exists($estate['estate_id'], $exportedEstates)) {
			// delete estate
			$params = array(
				php_xmlrpc_encode($this->hash[0]),
				php_xmlrpc_encode(""),
				php_xmlrpc_encode($estate["estate_id"])
			);
			$msg = new \xmlrpcmsg("delAdvert", $params);
			$response = $this->client->send($msg);
			$result = php_xmlrpc_decode($response->value());
			$message = $result["status"] . " - " . $result["statusMessage"];
			if (floor($result["status"] / 100) != 2) {
				return array(
					"error" => TRUE,
					"message" => $message,
					"url" => NULL
				);
			}
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => NULL
			);
		}
		return NULL;
	}

	private function getExportedEstates()
	{
		$params = array(php_xmlrpc_encode($this->hash[0]));
		$msg = new \xmlrpcmsg("listAdvert", $params);
		$response = $this->client->send($msg);
		$estates = php_xmlrpc_decode($response->value())["output"];

		foreach ($estates as $key => $estate) {
			$estates[$estate["rkid"]] = $estate;
			unset($estates[$key]);
		}
		return $estates;
	}

	public function hash($getHash)
	{
		$this->hash = $getHash["output"];
		return $this->hash;
	}

	public function login()
	{
		$hash = $this->getHash();

		$password = md5(md5($this->password) . $hash[1]);

		$params = array(new \xmlrpcval($hash[0]), new \xmlrpcval($password), new \xmlrpcval($this->key));
		$msg = new \xmlrpcmsg("login", $params); $response = $this->client->send($msg);

		$login = php_xmlrpc_decode($response->value());

		if ($login["status"] != 200) {
			die("Chyba pri prihlaseni [ {$login["status"]}]: {$login["statusMessage"]}");
		}
		$GLOBALS['xmlrpc_internalencoding'] = 'UTF-8';
	}

	public function logout()
	{
		$params = array(new \xmlrpcval($this->hash[0]));
		$msg = new \xmlrpcmsg("logout", $params); $response = $this->client->send($msg);

		$result = php_xmlrpc_decode($response->value());
		if ($result["status"] != 210) {
			die("Chyba pri odhlaseni [ {$result["status"]}]: {$result["statusMessage"]}");
		}
	}

	public function exportBroker($broker)
	{
		$this->setClientInfo($broker["clientId"]);
		$this->login();

		if ($broker["export"] == 1 && $broker["removed"] == NULL) {
			return $this->_exportBroker($broker);
		} else {
			return $this->_deleteBroker($broker);
		}
	}

	private function _deleteBroker($broker)
	{
		$exportedBrokers = $this->getExportedBrokers();
		if (array_key_exists($broker['id'], $exportedBrokers)) {
			// delete estate
			$msg = new \xmlrpcmsg("delSeller");
			$msg->addParam(php_xmlrpc_encode(0));
			$msg->addParam(php_xmlrpc_encode((string)$broker["id"]));
			$response = $this->client->send($msg);
			$result = php_xmlrpc_decode($response->value());
			$message = $result["status"] . " - " . $result["statusMessage"];
			if (floor($result["status"] / 100) != 2) {
				return array(
					"error" => TRUE,
					"message" => $message,
					"url" => NULL
				);
			}
			return array(
				"error" => FALSE,
				"message" => $message,
				"url" => NULL
			);
		}
		return NULL;
	}

	private function _exportBroker($broker)
	{
		$this->setClientInfo($broker["clientId"]);
		$this->login();

		$seller = array(
			"client_login" => $broker["email"],
			"client_name" => $broker["name"],
			"contact_gsm" => $broker["phone"]
		);

		$params = array(
			php_xmlrpc_encode($this->hash[0]), // session_id
			php_xmlrpc_encode($broker["name"]), // client_name
			php_xmlrpc_encode($broker["phone"]), // contact_gsm
			php_xmlrpc_encode($broker["email"]), // contact_email
			php_xmlrpc_encode($broker["id"]), // seller_rkid
			php_xmlrpc_encode(""), // contact_phone
			php_xmlrpc_encode(""), // contact_icq
			php_xmlrpc_encode(""), // makler_note
			php_xmlrpc_encode("")// photo
		);
		$msg = new \xmlrpcmsg("addSeller", $params);
		$response = $this->client->send($msg);
		$result = php_xmlrpc_decode($response->value());
		$this->logout();
		if (floor($result["status"] / 100) != 2) {
			if ($result["status"] == 463) {
				$message = "Makléřův email není registrován na Sreality.cz";
			}
			$message = "{$result["status"]} - {$result["statusMessage"]}";

			return array(
				"error" => TRUE,
				"message" => $message,
				"url" => NULL
			);
		} else {
			return array(
				"error" => FALSE,
				"message" => $result["status"] . " - " . $result["statusMessage"],
				"url" => NULL
			);
		}
	}

////////////////////////////////////////////

	public function changeDescription($estate)
	{
		$estate["description"] = $estate["estate_description"]["cs"];
		$estate["advert_price_text_note"] = $estate["advert_price_text_note"]["cs"];
		$estate["rkid"] = (string)$estate["estate_id"];
		$estate["garage_count"] = (int)$estate["garage_count"];
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		$estate["workshop_area"] = (int)$estate["workshop_area"];
		$estate["production_area"] = (int)$estate["production_area"];
		$estate["acceptance_year"] = (int)$estate["acceptance_year"];
		$estate["parking"] = (int)$estate["parking"];
		$estate["basin_area"] = (int)$estate["basin_area"];
		$estate["nolive_total_area"] = (int)$estate["nolive_total_area"];
		$estate["energy_performance_summary"] = (int)$estate["energy_performance_summary"];
		$estate["object_age"] = (int)$estate["object_age"];
		$estate["store_area"] = (int)$estate["store_area"];
		$estate["offices_area"] = (int)$estate["offices_area"];
		$estate["floors"] = (int)$estate["floors"];
		$estate["terrace_area"] = (int)$estate["terrace_area"];
		$estate["reconstruction_year"] = (int)$estate["reconstruction_year"];
		$estate["balcony_area"] = (int)$estate["balcony_area"];
		$estate["underground_floors"] = (int)$estate["underground_floors"];
		$estate["building_area"] = (int)$estate["building_area"];
		$estate["shop_area"] = (int)$estate["shop_area"];
		$estate["cellar_area"] = (int)$estate["cellar_area"];
		$estate["usable_area_ground"] = (int)$estate["usable_area_ground"];
		$estate["garden_area"] = (int)$estate["garden_area"];
		$estate["floor_area"] = (int)$estate["floor_area"];
		$estate["ceiling_height"] = (int)$estate["ceiling_height"];
		$estate["mortgage_percent"] = (int)$estate["mortgage_percent"];



//		$estate["sale_date"] = date(DATE_ISO8601, strtotime('2010-12-30 23:21:46'));
//		$estate["sale_date"] = new DateTime(time());
//		$estate["sale_date"]->format(DateTime::ISO8601);
//		$estate["sale_date"] = "20060107T01:53:00";
		$estate["loggia_area"] = (int)$estate["loggia_area"];
		return $estate;
	}

	public function setSeller($estate)
	{
		$estate["seller_rkid"] = (string)$estate["userId"];
		return $estate;
	}

	public function addRequiredBool($estate)
	{
		$required = array(
			"balcony",
			"loggia",
			"terrace",
			"cellar",
			"parking_lots",
			"garage",
			"basin"
		);
		foreach ($required as $value) {
			if ($estate[$value] === NULL) {
				$estate[$value] = 0;
			}
		}
		return $estate;
	}

	public function changeAdvertType($estate)
	{
		if ($estate["adverty_type"] == 1) { //byty
			$estate["advert_type"] = 4; //		4 Byty
		} elseif ($estate["advert_type"] == 2) { // domy
			switch ($estate["advert_subtype"]) {
				case 33:
				case 43:
					$estate["advert_type"] = 10; //		10 Chaty a rekreační objekty
					break;
				case 35:
					$estate["advert_type"] = 5; //		5 Historické objekty
					break;
				case 44:
					$estate["advert_type"] = 1; //		1 Zemědělské objekty
					break;
				default:
					$estate["advert_type"] = 6; //		6 Domy a vily
					break;
			}
		} elseif ($estate["advert_type"] == 4) { // komerční
			$estate["advert_type"] = 2; //		2 Komerční objekty
		} elseif ($estate["advert_type"] == 5) { // ostatní
			$estate["advert_type"] = 11; //		11 Malé objekty a garáže
		}
		return $estate;
	}

	public function changeAdvertSubtype($estate)
	{
		$change = array(
			18 => 1, // 18: Komerční => 1 pro komerční výstavbu
			19 => 2, //	19: Bydlení => 2 pro bydlení
			23 => 6, // 23: Zahrady => 6 zahrada
			21 => 4, // 21: Lesy => 4 les
			22 => 5, // 22: Louky => 5 trvalý travní porost
			20 => 3, // 20: Pole => 3 zemědělská
			24 => 7, // 24: Ostatní => 7 ostatní
			46 => 7, // 46: Rybníky => 7 ostatní
			48 => 7  // 48: Sady/vinice => 7 ostatní
		);
		if ($estate["advert_type"] == 3) {
			$estate["advert_subtype"] = $change[$estate["advert_subtype"]];
		}
		return $estate;
	}

	public function addAreaUnit($estate)
	{
		if ($estate["advert_type"] == 3) {
			$estate["area_unit"] = 2;
		}
		return $estate;
	}

	public function addUir($estate)
	{
		$estate["uir"] = $estate["locality_city_uir"];
		$estate["uir_level"] = 3;
		return $estate;
	}
	/*	 * *****************************************************************
	 * Překlady
	 * **************************************************************** */
	public $translateTable = array(
		"description" => NULL,
		"advert_function" => NULL,
		"advert_lifetime" => NULL,
		"advert_price" => NULL,
		"advert_price_original" => NULL,
		"advert_price_currency" => NULL,
		"advert_price_unit" => NULL,
		"advert_type" => NULL,
		"locality_city" => "advert_city",
		"locality_zip" => "advert_zip",
		"locality_inaccuracy_level" => NULL,
		"advert_id" => NULL,
		"rkid" => NULL,
		"advert_room_count" => NULL,
		"advert_subtype" => "estate_kind",
		"balcony" => NULL,
		"basin" => NULL,
		"building_condition" => NULL,
		"building_type" => NULL,
		"cellar" => NULL,
		"estate_area" => NULL,
		"floor_number" => NULL,
		"garage" => NULL,
		"locality_latitude" => "gps_latitude",
		"locality_longitude" => "gps_longitude",
		"locality_ruian" => NULL,
		"locality_ruian_level" => NULL,
		"locality_uir" => NULL,
		"locality_uir_level" => NULL,
		"loggia" => NULL,
		"object_type" => NULL,
		"ownership" => NULL,
		"parking_lots" => NULL,
		"project_id" => NULL,
		"project_rkid" => NULL,
		"terrace" => NULL,
		"usable_area" => NULL,
		"acceptance_year" => NULL,
		"advert_code" => NULL,
		"advert_low_energy" => NULL,
		"advert_price_charge" => NULL,
		"advert_price_commission" => NULL,
		"advert_price_legal_services" => NULL,
		"advert_price_negotiation" => NULL,
		"advert_price_vat" => NULL,
		"annuity" => NULL,
		"auction_advertisement_pdf" => NULL,
//		"auction_date" => NULL,
//		"auction_date_tour" => NULL,
//		"auction_date_tour2" => NULL,
		"auction_kind" => NULL,
		"auction_place" => NULL,
		"auction_review_pdf" => NULL,
		"balcony_area" => NULL,
		"basin_area" => NULL,
//		"beginning_date" => NULL,
		"building_area" => NULL,
		"ceiling_height" => NULL,
		"cellar_area" => NULL,
		"cost_of_living" => NULL,
		"easy_access" => NULL,
		"electricity" => NULL,
		"elevator" => NULL,
		"energy_efficiency_rating" => NULL,
		"energy_performance_attachment" => NULL,
		"energy_performance_certificate" => NULL,
		"energy_performance_summary" => NULL,
		"extra_info" => NULL,
//		"finish_date" => NULL,
//		"first_tour_date" => NULL,
//		"first_tour_date_to" => NULL,
		"flat_class" => NULL,
		"floor_area" => NULL,
		"floors" => NULL,
		"furnished" => NULL,
		"garage_count" => NULL,
		"garden_area" => NULL,
		"garret" => NULL,
		"gas" => NULL,
		"gully" => NULL,
		"heating" => NULL,
		"locality_citypart" => NULL,
		"locality_co" => NULL,
		"locality_cp" => NULL,
		"locality_street" => NULL,
		"loggia_area" => NULL,
		"mortgage" => NULL,
		"mortgage_percent" => NULL,
		"nolive_total_area" => NULL,
		"object_age" => NULL,
		"object_kind" => NULL,
		"object_location" => NULL,
		"offices_area" => NULL,
		"parking" => NULL,
		"personal" => NULL,
		"price_auction_principal" => NULL,
		"price_expert_report" => NULL,
		"price_minimum_bid" => NULL,
		"production_area" => NULL,
		"protection" => NULL,
//		"ready_date" => NULL,
		"reconstruction_year" => NULL,
		"road_type" => NULL,
//		"sale_date" => NULL,
		"shop_area" => NULL,
		"spor_percent" => NULL,
		"steps" => NULL,
		"store_area" => NULL,
		"surroundings_type" => NULL,
		"telecommunication" => NULL,
		"terrace_area" => NULL,
		"transport" => NULL,
		"underground_floors" => NULL,
		"usable_area_ground" => NULL,
		"user_status" => NULL, // ??
		"water" => NULL,
		"workshop_area" => NULL,
		"youtube_video" => NULL, // fORMAT??
		"advert_price_hidden" => NULL,
		"advert_price_text_note" => NULL,
		"locality_citypart" => "advert_citypart",
		"locality_co" => "advert_co",
		"locality_cp" => "advert_cp",
		"locality_street" => "advert_street",
		"seller_id" => NULL,
		"seller_rkid" => NULL,
		"extra_info" => NULL,
		"total_area" => NULL, // ????? není v dokumentaci!!!!
		"commercial_kind" => NULL,
		"area_unit" => NULL,
		"uir" => NULL,
		"uir_level" => NULL
	);

}