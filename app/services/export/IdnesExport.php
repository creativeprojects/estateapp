<?php

namespace App\Services\ExportService;

use Nette,
	Nette\Utils\Image,
	CP;

// Dokumentace: http://reality.idnes.cz/doc/reality-specifikace.zip

class IdnesExport extends \App\Services\ExportService
{
	public $ftp_server;

	public $ftp_user;

	public $ftp_password;

	public $import_login;

	public $import_password;

	public $importUrl;

	public $exportClient;

	public $parent;

	public $usersService;

	public $clientService;

	public function __construct($parent)
	{
		$this->parent = $parent;

		$this->usersService = $parent->usersService;

		$this->clientService = $parent->clientService;

		$this->ftp_server = "ftp.reality.idnes.cz";
		$this->importUrl = "http://rk.mobil.cz/import/";
	}

	public function setClientInfo($clientId)
	{
		$this->exportClient = $this->parent->clientsService->getById($clientId);

		$this->ftp_user = $this->exportClient["export_settings"]["idnes_ftp_user"];
		$this->ftp_password = $this->exportClient["export_settings"]["idnes_ftp_password"];
		$this->import_login = $this->exportClient["export_settings"]["idnes_import_login"];
		$this->import_password = $this->exportClient["export_settings"]["idnes_import_password"];
	}

	public function export($estate)
	{
		$this->setClientInfo($estate["clientId"]);

		if ($estate["export"] == 1 AND in_array($estate["advert_status"], array(1, 2))) {
			return $this->_export($estate);
		} else {
			return $this->_delete($estate);
		}
	}

	private function _delete($estate)
	{
		$exportedEstates = $this->getExportedEstates();

		if (in_array($estate['estate_id'], $exportedEstates)) {
			$delete = file_get_contents($this->importUrl . "nemo_remove.php?utf8=1&login={$this->import_login}&pwd={$this->import_password}&id_exported={$estate['estate_id']}");
			if (substr($delete, 0, 2) != "OK") {
				return array(
					"error" => TRUE,
					"message" => $delete,
					"url" => $this->getExportUrl($estate["estate_id"])
				);
			}
			return array(
				"error" => FALSE,
				"message" => $delete,
				"url" => $this->getExportUrl($estate["estate_id"])
			);
		}
		return NULL;
	}

	/**
	 * Vrací pole estate_id exportovaných nemovitostí
	 * @return array
	 */
	private function getExportedEstates()
	{
		$exportedEstates = file_get_contents($this->importUrl . "get_list.php?utf8=1&login={$this->import_login}&pwd={$this->import_password}");

		$exportedEstates = explode("\r\n", $exportedEstates);

		unset($exportedEstates[0]);
		unset($exportedEstates[count($exportedEstates)]);

		return array_combine($exportedEstates, $exportedEstates);
	}

	private function _export($estate)
	{
		$this->setClientInfo($estate["clientId"]);
		$photos = $estate["images"];
		$estateId = $estate["estate_id"];

		$estate = $this->changeAdvertType($estate);
		$estate = $this->changeAdvertSubtype($estate);
		$estate = $this->changeAdvertStatus($estate);
		$estate = $this->changeCurrency($estate);
		$estate = $this->changeAdvertPriceUnit($estate);
		$estate = $this->changeBuildingType($estate);
		$estate = $this->changeBuildingCondition($estate);
		$estate = $this->changeFurnished($estate);
		$estate = $this->changeRoadType($estate);
		$estate = $this->changeLocality($estate);
		$estate = $this->changeGas($estate);
		$estate = $this->changeWater($estate);
		$estate = $this->changeGully($estate);
		$estate = $this->addBalcony($estate);
		$estate = $this->changeEnergyEfficiency($estate);
		$estate = $this->isExclusive($estate);
		$estate = $this->isPriceWithCharge($estate);
		$estate = $this->isPriceWithCommission($estate);
		$estate = $this->getLocalityNumber($estate);
		$estate = $this->getParkingSum($estate);
		$estate = $this->changeSelectToBool($estate);
		$estate = $this->communication($estate);

		switch ($estate["advert_status"]) {
			case 1:
				$estate["f_stav_zakazky"] = 1; // aktivní zakázka
				break;
			case 2:
				$estate["f_stav_zakazky"] = 4; // rezervováno
				break; // 2 - prodáno, 3 - pronajato
		}

		$estate = $this->addUser($estate);
		$estate = $this->translate($estate);

		$xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><nabidka></nabidka>");
		$xml->addChild("akce", "update");
		foreach ($estate as $key => $estateValue) {
			if (!is_array($estateValue)) {
				$est = $xml->addChild($key, $estateValue);
			} else {
				foreach ($estateValue as $value) {
					$est = $xml->addChild($key, htmlspecialchars($value));
				}
			}
		}
		foreach ($photos as $photo) {
			$rank = $photo->rank;
			$filePath = WWW_DIR . "/" . $photo->src;
			$pathInfo = pathinfo($filePath);
			$fileName = $pathInfo["dirname"] . "/../export/" . $pathInfo["basename"];
			$photo = $xml->addChild("foto");
			$photo->addChild('ord', $rank);
			$hash = md5_file($fileName);
			$photo->addChild('hash', $hash);
			$photo->addChild('filename', $this->import_login . '_' . $hash . '.' . pathinfo($fileName, PATHINFO_EXTENSION));

			$upload = $this->uploadFile($fileName, $hash);
			if ($upload !== TRUE) {
				return $upload;
			}
		}

		$fileName = WWW_DIR . "/" . $this->exportClient ["client_dir"] . "/files/export/" . $estateId . ".xml";
		$file = $xml->saveXML($fileName);
		$upload = $this->uploadFile($fileName, $estateId);

		if ($upload !== TRUE) {
			return $upload;
		}
		$login = file_get_contents($this->importUrl . "check_login.php?utf8=1&login={$this->import_login}&pwd={$this->import_password}");
		$result = file_get_contents($this->importUrl . "import.php?utf8=1&login={$this->import_login}&pwd={$this->import_password}&filename={$this->import_login}_{$estateId}.xml");

		if (substr($login, 0, 2) != "OK") {
			return array(
				"error" => TRUE,
				"message" => $result,
				"url" => $this->getExportUrl($estateId)
			);
		}
		return array(
			"error" => FALSE,
			"message" => $result,
			"url" => $this->getExportUrl($estateId)
		);
	}

	public function getExportUrl($estateId)
	{

		$response = file_get_contents($this->importUrl . "nemo_detail.php?utf8=1&login={$this->import_login}&pwd={$this->import_password}&id_exported={$estateId}");
		if (substr($response, 0, 2) == "OK") {
			$url = substr($response, 4);
			return $url;
		}
		return NULL;
	}

	public function changeAdvertFunction($estate)
	{
		$change = array(
			1 => 1,
			2 => 2,
			3 => 4
		);
		$estate["advert_function"] = $change[
			$estate["advert_function"]];
		return $estate;
	}

	public function changeAdvertPriceUnit($estate)
	{
		if ($estate["advert_function"] == 1 OR $estate ["advert_function"] == 3) {
			$change = array(
				1 => 1,
				3 => 2
			);
			$estate["advert_price_unit_sell"] = $change[$estate["advert_price_unit"]];
		} elseif ($estate["advert_function"] == 2) {
			$change = array(
				1 => 1,
				2 => 1,
				3 => 2,
				4 => 2,
				5 => 3,
				6 => 4,
				7 => 7,
				8 => 6,
				9 => 6,
				10 => 6
			);
			$estate["advert_price_unit_buy"] = $change[
				$estate["advert_price_unit"]];
		}
		return $estate;
	}

	public function changeAdvertStatus($estate)
	{
		$change = array(
			1 => 1,
			2 => 1,
			3 => 4
		);
		switch ($estate['advert_status']) {
			case 1:
			case 2:
			case 3:
				$estate["advert_status"] = $change[$estate["advert_status"]];
				break;
			case 4:
				if ($estate["advert_function"] == 1 OR $estate ["advert_function"] == 3)
					$estate["advert_status"] = 2;

				elseif ($estate["advert_function"] == 2)
					$estate[
						"advert_status"] = 3;
				break;
		}
		return $estate;
	}

	public function changeCurrency($estate)
	{
		$change = array(
			1 => 0,
			2 => 2,
			3 => 1
		);
		$estate["advert_price_currency"] = $change[
			$estate["advert_price_currency"]];
		return $estate;
	}

	public function changeAdvertType($estate)
	{

		if ($estate['advert_type'] == 5) {
			$estate['advert_type'] = 6;
		} elseif ($estate['advert_type'] == 2) {
			switch ($estate['advert_subtype']) {
				case 33:
				case 43:
				case 44:
					$estate['advert_type'] = 5;
					break;
				default:
					$estate['advert_type'] = 2;
					break;
			}
		} elseif ($estate['advert_type'] == 3) {


			$estate['advert_type'] = 4;
		}
		return $estate;
	}

	public function changeAdvertSubtype($estate)
	{
		$change = array(
			2 => 1,
			3 => 2,
			4 => 3,
			5 => 4,
			6 => 5,
			7 => 6,
			8 => 7,
			9 => 8,
			10 => 9,
			11 => 9,
			12 => 9,
			16 => 11,
			47 => 10,
			18 => 2,
			19 => 1,
			20 => 4,
			21 => 5,
			22 => 8,
			23 => 6,
			24 => 7,
			46 => 9,
			48 => 7,
			25 => 1,
			26 => 5,
			27 => 6,
			28 => 3,
			29 => 9,
			30 => 10,
			31 => 12,
			32 => 13,
			38 => 11,
			49 => 13,
		);
		if ($estate ["advert_type"] == 1 OR $estate ["advert_type"] == 3 OR $estate ["advert_type"] == 4) {
			$estate["advert_subtype"] = $change[$estate["advert_subtype"]];
		} elseif ($estate["advert_type"] == 2) {
			$estate["advert_subtype"] = $estate["object_kind"];
		} else {
			unset(
				$estate["advert_subtype"]);
		}
		return $estate;
	}

	public function changeBuildingType($estate)
	{
		$change = array(
			1 => 3,
			2 => 1,
			3 => 4,
			4 => 7,
			5 => 2,
			6 => 6,
			7 => 5
		);
		$estate["building_type"] = $change[$estate["building_type"]];
		return $estate;
	}

	public function changeBuildingCondition($estate)
	{
		if ($estate["advert_type"] == 1) {
			$change = array(
				1 => 3,
				2 => 3,
				3 => 6,
				4 => 9,
				5 => 8,
				6 => 1,
				7 => 6,
				8 => 5,
				9 => 2
			);
			$estate["building_condition_apartment"] = isset($change[$estate["building_condition"]]) ? $change[$estate["building_condition"]] : NULL;
		} else {
			$change = array(
				1 => 3,
				2 => 3,
				3 => 7,
				4 => 9,
				5 => 8,
				6 => 1,
				7 => 6,
				8 => 5,
				9 => 2
			);

			$estate["building_condition_building"] = isset($change[$estate["building_condition"]]) ? $change[$estate["building_condition"]] :
				NULL;
		}

		return $estate;
	}

	public function changeFurnished($estate)
	{
		$change = array(
			1 => 1,
			2 => 3,
			3 => 2,
		);
		$estate["furnished"] = $change[$estate["furnished"]];
		return $estate;
	}

	public function changeRoadType($estate)
	{
		$change = array(
			1 => 1,
			2 => 3,
			3 => 2,
			4 => 4
		);
		if ($estate["road_type"]) {
			$estate["road_type"] = $change[$estate["road_type"][0]];
		}
		return $estate;
	}

	public function changeLocality($estate)
	{
		$change = array(
			1 => 1,
			2 => 3,
			3 => 2,
			4 => 4,
			5 => 3,
			6 => 6,
			7 => 5,
		);
		$estate["object_location"] = $change[
			$estate["object_location"]];
		return $estate;
	}

	public function changeGas($estate)
	{
		$change = array(
			1 => 1,
			2 => 2,
		);
		if ($estate["gas"]) {
			$estate["gas"] = $change[$estate["gas"][0]];
		}
		return $estate;
	}

	public function changeWater($estate)
	{
		$change = array(
			1 => 2,
			2 => 1,
		);

		if ($estate["water"]) {
			foreach ($estate["water"] as $key => $value) {
				$estate["water"][$key] = $change[
					$estate["water"][$key]];
			};
		}
		return $estate;
	}

	public function changeGully($estate)
	{
		$change = array(
			1 => 2,
			2 => 4,
			3 => 1,
			4 => 1,
		);
		if ($estate["gully"]) {
			foreach ($estate["gully"] as $key => $value) {
				$estate["gully"][$key] = $change[
					$estate["gully"][$key]];
			};
		}
		return $estate;
	}

	public function addBalcony($estate)
	{
		if ($estate["balcony"])
			$estate[
				"balcony_loggia_terrace"][] = 1;
		if ($estate["loggia"])
			$estate[
				"balcony_loggia_terrace"][] = 2;
		if ($estate["terrace"])
			$estate[
				"balcony_loggia_terrace"][] = 3;
		return $estate;
	}

	public function changeEnergyEfficiency($estate)
	{
		$change = array(
			0 => NULL,
			1 => 1,
			2 => 2,
			3 => 3,
			4 => 4,
			5 => 5,
			6 => 6,
			7 => 7
		);
		$estate["energy_efficiency_rating"] = isset($change[$estate["energy_efficiency_rating"]]) ? $change[$estate["energy_efficiency_rating"]] : NULL;
		return $estate;
	}

	public function isExclusive($estate)
	{
		if (isset($estate ["contract_type"]) && $estate["contract_type"] == 1) {


			$estate["exclusive"] = 1;
		}
		return $estate;
	}

	public function isPriceWithCharge($estate)
	{
		$estate["advert_price_charge"] = $estate[
			"advert_price_charge"] - 1;
		return $estate;
	}

	public function isPriceWithCommission($estate)
	{
		$estate["advert_price_commission"] = $estate[
			"advert_price_commission"] - 1;
		return $estate;
	}

	public function getLocalityNumber($estate)
	{
		$estate ["locality_number"] = $estate["locality_cp"] .
			"/" . $estate["locality_co"];
		return $estate;
	}

	public function getParkingSum($estate)
	{
		$estate["parking_sum"] = $estate[
			"garage_count"] + $estate["parking"];
		return $estate;
	}

	public function changeSelectToBool($estate)
	{
		$change = array('elevator',
			'easy_access',
		);
		foreach ($change as $param) {
			$estate[$param] = $estate[$param] - 1;
		}
		return $estate;
	}

	public function communication($estate)
	{
		if ($estate["telecommunication"]) {
			foreach ($estate["telecommunication"] as $value) {
				switch ($value) {
					case 1:
						$estate["b_telefon"] = 1;
						break;
					case 2:
						$estate["b_internet"] = 1;
						break;
					case 3:
					case 4:
						$estate["b_kabletv"] = 1;

						break;
				}
			}
		}
		return $estate;
	}

	public function uploadFile($file, $id_exported)
	{
		$ftp_server = $this->ftp_server;
		$ftp_user = $this->ftp_user;
		$ftp_password = $this->ftp_password;
		$import_login = $this->import_login;

		$remote_file = $import_login . "_" . $id_exported . "." . pathinfo($file, PATHINFO_EXTENSION);

		$conn_id = ftp_connect($ftp_server);

		if (is_bool($conn_id)) {
			return "There was a problem while uploading {$file}. No connection. ";
		}
		$login_result = ftp_login($conn_id, $ftp_user, $ftp_password);
		ftp_pasv($conn_id, true);

		if (ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
			return TRUE;
		} else {
			return "There was a problem while uploading

	{$file}";
		}
		ftp_close($conn_id);
	}

	public function addUser($estate)
	{
		$user = $this->usersService->getById($estate["userId"]);
		$estate["makler_jmeno"] = $user["name"];
		$estate["makler_email"] = $user["email"];
		$estate["makler_id"] = $user["id"];
		$estate["makler_mobil"] = $user["phone"];

		$photo = array_shift($user["portrait"]);

		if ($photo) {
			$filePath = WWW_DIR . "/" . $photo->src;
			$pathInfo = pathinfo($filePath);
			if (!file_exists($pathInfo["dirname"] . "/export/" . $pathInfo["basename"])) {
				$image = Image::fromFile($filePath);
				$image->resize(100, 100);
			}

			$estate["makler_foto"] = base64_encode($image);
		}
		return $estate;
	}

	public function delBroker()
	{

	}
	/*	 * *****************************************************************
	 * Překlady
	 * **************************************************************** */
	public $translateTable = array(
		"estate_id" => "id_exported",
		"advert_code" => "id_nemo_ext",
		"advert_type" => "isa",
		"advert_subtype" => "isa_sub",
		"advert_function" => "f_typ_nabidky",
		"ownership" => "f_vlastnictvi",
		"advert_status" => "f_stav_zakazky",
		"advert_price_currency" => "f_mena",
		"advert_price_unit_sell" => "f_cena_za_prodej",
		"advert_price_unit_buy" => "f_cena_za_pronajem",
		"building_type" => "f_konstrukce",
		"building_condition_building" => "f_stav_budovy",
		"building_condition_apartment" => "f_stav_bytu",
		"furnished" => "f_vybaveni",
		"road_type" => "f_komunikace",
		"object_location" => "f_lokalita",
		"gas" => "f_plyn",
		"water" => "mul_voda",
		"balcony_loggia_terrace" => "mul_balkon",
		"heating" => "mul_topeni",
		"energy_efficiency_rating" => "f_epc_class",
		"energy_performance_certificate" => "f_epc_regulation",
		"exclusive" => "b_exclusive",
		"advert_price" => "cena",
		"advert_price_hidden" => "b_cena_hide",
		"advert_price_commsion" => "b_vcetne_provize",
		"advert_price_charge" => "b_vcetne_poplatku",
		"locality_longitude" => "gps_lng",
		"locality_latitude" => "gps_lat",
		"locality_zip" => "zip",
		"locality_number" => "ulice_nr",
		"locality_street" => "ulice",
		"estate_title" => "titulek",
		"estate_description" => "popis",
		"annuity" => "cena_anuita",
		"floors" => "podlazi_nad",
		"floor_number" => "f_podlazi_nr",
		"floors" => "podlazi_pod",
		"building_area" => "pl_zastavena",
		"estate_area" => "pl_parcela",
		"usable_area" => "pl_uzitna",
		"garden_area" => "pl_zahrada",
		"balcony_area" => "pl_balkon",
		"terrace_area" => "pl_terasa",
		"production_area" => "pl_vyrobni",
		"shop_area" => "pl_obchodni",
		"store_area" => "pl_skladovaci",
		"object_age" => "rok_vystavby",
		"acceptance_year" => "rok_kolaudace",
		"reconstruction_year" => "rok_rekonstrukce",
		"ready_date" => "dt_nastehovani",
		"parking_sum" => "sum_parking",
		"advert_room_count" => "sum_mistnosti",
		"elevator" => "b_vytah",
		"easy_access" => "b_bezbarier",
		"garret" => "b_puda",
		"b_telefon" => NULL,
		"b_internet" => NULL,
		"b_kabel_tv" => NULL,
		"cellar" => "b_sklep",
		"basin" => "bazén",
		"locality_state_uir" => NULL,
		"locality_county_uir" => NULL,
		"locality_city_uir" => NULL,
		"makler_jmeno" => NULL,
		"makler_email" => NULL,
		"makler_id" => NULL,
		"makler_mobil" => NULL,
		"makler_foto" => NULL,
		"f_stav_zakazky" => NULL
	);
}