<?php

namespace App\Services\ExportService;

use Nette,
	Nette\Utils\Image,
	CP;

class ImagesExport extends \App\Services\ExportService
{

	public function exportEstate($estate)
	{

		foreach ($estate["images"] as $file) {

			$filePath = WWW_DIR . "/" . $file->src;
			$pathInfo = pathinfo($filePath);
			if (!file_exists($pathInfo["dirname"] . "/export/" . $pathInfo["basename"])) {
				$image = Image::fromFile($filePath);
				$image->resize(800, 800);
				$image->save($pathInfo["dirname"] . "/../export/" . $pathInfo["basename"], 70, Image::JPEG);
			}
		}
		return TRUE;
	}
}