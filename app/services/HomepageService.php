<?php

namespace App\Services;

use Nette\Utils\Strings;
use App\Entities\HomepageEntity;

class HomepageService extends BaseService
{
	public function getEntity()
	{
		$demandEntity = new HomepageEntity;

		return $demandEntity->find();
	}

	public function getActiveRegionsByDomain()
	{
		$regions = $this->database->query("SELECT DISTINCT(locality_county) AS locality_county FROM {$this->repository->estates} WHERE domainId = {$this->thisDomainEntity->getId()} AND removed IS NULL AND advert_status IN (1,2)")->fetchAll();

		foreach($regions AS & $region){
			$region = (array) $region;

			$region = Strings::webalize(strtolower($region["locality_county"]));
		}

		return $regions;
	}

	public function getLists()
	{
		$forced = & $this->settings["forcedLists"];
		$estatesParameters = & $this->estatesParameters;

		$lists = array();

		$advert_typeCounts = $this->database->query("SELECT advert_type, COUNT(*) AS advert_count FROM {$this->repository->estates} WHERE domainId = {$this->thisDomainEntity->getId()} AND advert_status IN (1,2) GROUP BY advert_type")->fetchPairs("advert_type", "advert_count");

		$advert_subtypeCounts = $this->database->query("SELECT advert_subtype, COUNT(*) AS advert_count FROM {$this->repository->estates} WHERE domainId = {$this->thisDomainEntity->getId()} AND advert_status IN (1,2) GROUP BY advert_subtype")->fetchPairs("advert_subtype", "advert_count");

		foreach ($estatesParameters["advert_type"]["options"] AS $advert_type => $advert_typeTitle) {
			$advert_functionCounts = $this->database->query("SELECT advert_function, COUNT(*) AS advert_count FROM {$this->repository->estates} WHERE domainId = {$this->thisDomainEntity->getId()} AND advert_status IN (1,2) AND advert_type = {$advert_type} GROUP BY advert_function")->fetchPairs("advert_function", "advert_count");

			if ($forced || @$advert_typeCounts[$advert_type]) {
				$lists[$advert_type] = array(
					"count" => @$advert_typeCounts[$advert_type],
					"title" => $advert_typeTitle,
					"url" => "",
					"lists" => array(
						"advert_function" => array(),
						"advert_subtype" => array()
					)
				);

				$this->createSlug($advert_typeTitle, array("advert_type" => $advert_type));

				foreach ($estatesParameters["advert_function"]["options"] AS $advert_function => $advert_functionTitle) {
					if ($forced || @$advert_functionCounts[$advert_function]) {
						$lists[$advert_type]["lists"]["advert_function"][$advert_function] = array(
							"count" => @$advert_functionCounts[$advert_function],
							"title" => $advert_functionTitle,
							"url" => ""
						);

						$this->createSlug(array($advert_typeTitle, $advert_functionTitle), array("advert_type" => $advert_type, "advert_function" => $advert_function));
					}
				}

				foreach ($estatesParameters["advert_subtype"]["options"][$advert_type] AS $advert_subtype => $advert_subtypeTitle) {
					if ($forced || @$advert_subtypeCounts[$advert_subtype]) {
						$lists[$advert_type]["lists"]["advert_subtype"][$advert_subtype] = array(
							"count" => @$advert_subtypeCounts[$advert_subtype],
							"title" => $advert_subtypeTitle,
							"url" => ""
						);

						$this->createSlug(array($advert_typeTitle, $advert_subtypeTitle), array("advert_type" => $advert_type, "advert_subtype" => $advert_subtype));
					}
				}
			}
		}

		return array('forced' => $forced, 'lists' => $lists);
	}

	private function createSlug($title, $parameters, $forced = false){
		
	}
}