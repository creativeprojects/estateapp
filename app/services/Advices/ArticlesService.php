<?php

namespace App\Services\Advices;

use App\Entities\Advices\ArticleEntity;
use App\Collections\Advices\ArticlesCollection;

class ArticlesService extends \CP\Services\BaseService
{
	/**
	 * Získat novou instanci entity článku blogu
	 * @return App\Entities\Advices\ArticleEntity
	 */

	public function articleEntity()
	{
		return new ArticleEntity;
	}

	/**
	 * Získat novou instanci kolekce článků
	 * @return App\Collections\Advices\ArticlesCollection
	 */

	public function articlesCollection()
	{
		return new ArticlesCollection;
	}
}