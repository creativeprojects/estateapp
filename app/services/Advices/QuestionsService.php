<?php

namespace App\Services\Advices;

use App\Entities\Advices\QuestionEntity;
use App\Collections\Advices\QuestionsCollection;

class QuestionsService extends \CP\Services\BaseService
{
	/**
	 * Získat novou instanci entity otázky
	 * @return App\Entities\Advices\QuestionEntity
	 */

	public function questionEntity()
	{
		return new QuestionEntity;
	}

	/**
	 * Získat novou instanci kolekce otázek
	 * @return App\Collections\Advices\QuestionsCollection
	 */

	public function questionsCollection()
	{
		return new QuestionsCollection;
	}
}