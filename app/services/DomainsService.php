<?php

namespace App\Services;

use App\Entities\DomainEntity;
use App\Collections\DomainsCollection;

/**
 * Rozšíření služby domén o další informace o doméně
 */

class DomainsService extends \CP\Services\DomainsService
{
	/**
	 * Získat novou instanci entity domény
	 * @return App\Entities\DomainEntity
	 */

	public function domainEntity()
	{
		return new DomainEntity(null, null, $this);
	}

	/**
	 * Získat novou instanci kolekce domén
	 * @return App\Collection\DomainsCollection
	 */

	public function domainsCollection()
	{
		return new DomainsCollection;
	}
}