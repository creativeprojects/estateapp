<?php

namespace App\Services;

use Nette\Utils\Strings;
use App\Entities\BrokerProfileEntity;

class BrokerProfilesService extends BaseService
{
	/** @var \App\Services\UsersService @inject */
	public $usersService;

	public $settings = [
		"defaults" => []
	];

	/**
	 * Získat novou instanci entity profilové stránky makléře
	 * @return App\Entities\BrokerProfileEntity
	 */
	
	public function brokerProfileEntity()
	{
		return new BrokerProfileEntity;
	}
}