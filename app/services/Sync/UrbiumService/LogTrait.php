<?php

namespace App\Services\Sync\UrbiumService;

trait LogTrait
{
	/**
	 * Uložit log so souboru
	 * @return bool
	 */

	public function saveLog()
	{
		$logFilePath = LOG_DIR . "/sync-urbium-{$this->thisDomainEntity->getId()}.txt";

		$content = @file_get_contents($logFilePath);

		$newContent = implode("\n", $this->log) . "\n----------------------------------------------";

		return file_put_contents($logFilePath, $newContent . "\n" . $content);
	}

	/**
	 * Logovat začátek operace
	 * @return void
	 */

	protected function logStart()
	{
		$this->log("Započato v " . date("H:i:s, j.n.Y"));
	}

	/**
	 * Logovat ukončení operace
	 * @return void
	 */

	protected function logEnd()
	{
		$this->log("Ukončeno v " . date("H:i:s, j.n.Y"));
	}
}