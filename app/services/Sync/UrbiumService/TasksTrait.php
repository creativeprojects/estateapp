<?php

namespace App\Services\Sync\UrbiumService;

use CP\Utils\ArrayResult;
use App\Entities\Sync\Urbium\TaskEntity;

trait TasksTrait
{
	/**
	 * Získat jednu úlohu ke zpracování
	 * Pokud nezašleme požadavek na konkrétní úlohu, najdeme nejstarší aktuální
	 * @param string|null $group
	 * @param integer|null $external_id
	 * @return TaskEntity|false
	 */

	protected function getTask($group = null, $external_id = null)
	{
		$taskEntity = new TaskEntity($this);

		if(!empty($group) && !empty($external_id)) { //Zadán požadavek na konkrétní úlohu - používá se pouze při testování
			return $taskEntity->find($group, $external_id);
		} else { //Najít nejstarší aktuální úlohu
			return $taskEntity->findOne();
		}
	}

	/**
	 * Zkontrolovat makléře a nemovitosti jestli nenajdeme nějaký úkol pro změnu
	 * @return bool
	 */

	protected function checkTasks()
	{
		$result = new ArrayResult;

		if($this->settings["importBrokers"]){ //Zkontrolovat změny v makléřích
			$this->log("Kontrola úloh - makléři");

			$result->is($this->checkBrokerTasks());
		}

		if($this->settings["importEstates"] && $this->settings["importBrokers"]){ //Maximální frekvence pořadavků do Urbia je 1x za 3s, pro jistotu jsme dali 4
			sleep(4);
		}

		if($this->settings["importEstates"]){ //Zkontrolovat změny v nemovitostech
			$this->log("Kontrola úloh - nemovitosti");

			$result->is($this->checkEstateTasks());
		}

		return $result->result();
	}

	/**
	 * Vykonat úlohu
	 * @param App\Entities\Sync\Urbium\TaskEntity
	 * @return bool
	 */

	protected function processTask($taskEntity)
	{
		switch($taskEntity->group){
			case "estates": //Vykonat úlohu zakázky
				$result = $this->processEstateTask($taskEntity);

				break;
			case "brokers": //Vykonat úlohu makléře
				$result = $this->processBrokerTask($taskEntity);

				break;
			default:
				$result = true;
		}

		//Pokud byl úloha vykonána a nejsme v režimu testování, úlohu odstranit
		if($result && !$this->debug){
			$taskEntity->delete();
		}

		return $result;
	}

	/**
	 * Uložit novou úlohu
	 * @param string $group
	 * @param string $external_id
	 * @param App\Collections\EstatesCollection|App\Collections\UsersCollection $collection
	 * @return bool
	 */

	protected function saveTask($group, $external_id, $collection)
	{
		$taskEntity = new TaskEntity($this);

		$taskEntity->group = $group;

		$taskEntity->external_id = $external_id;

		//TODO - jak řešit obnovu úlohy (zakázka / makléř byl/a pak se odstranila, ale pak se zase objeví v XML feedu) - může k tomu vůbec dojít?
		$taskEntity->operation = $collection->exists($external_id) ? "update" : "add";

		if ($taskEntity->save()) {
			$this->log("Nová úloha č. <b>{$taskEntity->id}</b> skupiny <b>{$taskEntity->group}</b> pro vykonání operace <b>{$taskEntity->operation}</b> na XML záznamu č. <b>{$taskEntity->external_id}</b> byla uložena");

			return true;
		} else {
			$this->log("Novou úlohu " . print_r($taskEntity->getValues(true), true) . " se nepodařilo uložit!");

			return false;
		}
	}

}
