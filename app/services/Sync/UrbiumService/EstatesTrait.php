<?php

namespace App\Services\Sync\UrbiumService;

use CP\Utils\ArrayResult;

trait EstatesTrait
{
	/** Překladové metody Urbium -> StránkyRealitky.Cz */
	use EstatesTrait\TranslationTrait;

	/**
	 * Zkontrolovat změny v zakázkách a případně uložit úkoly
	 * @return bool
	 */

	protected function checkEstateTasks()
	{
		$xmlEstates = $this->getXmlEstates();

		if ($xmlEstates) {
			$estatesCollection = $this->getEstatesCollection();

			$this->log("V databázi nalezeno " . $estatesCollection->count() . " zakázek");

			foreach ($xmlEstates AS $key => $estate) {
				if ($this->isEstateForSync($estate)) { //Pokud je zakázka určena k synchronizaci
					
					//Pokud byla zakázka upravena, nebo zatím neexistuje, uložit novou úlohu do databáze
					if ($this->wasEstateModified($estate, $estatesCollection->getOne($estate["id_zakazky"]))) {
						$result = $this->saveTask("estates", $estate["id_zakazky"], $estatesCollection);
					} else { //Zakázka je aktuální
						$this->log("Zakázka č. <b>{$estate["id_zakazky"]}</b> je aktuální");

						$result = true;
					}

					if ($result) { //Úloha byla uložena, tuto zakázku odebereme ze seznamu zakázek v databázi
						if ($estatesCollection->exists($estate["id_zakazky"])) {
							$estatesCollection->offsetUnset($estate["id_zakazky"]);
						}
					} else { //Nepodařilo se úlohu uložit, ukončit proces
						return false;
					}
				}
			}

			//Zakázky, které zůstali v seznamu, nejsou tedy v XML souboru, se musí odebrat archivovat protože již nejsou pravděpodobně aktuální
			return $this->archiveNonfoundEstates($estatesCollection);
		}

		return false;
	}

	/**
	 * Získat seznam hlaviček zakázek a datumů poslední úpravy z Urbia
	 * @return array|false
	 */

	public function getXmlEstates()
	{
		$url = "http://www.eurobydleni.cz/download/zakazka_list.php";

		if ($xml = $this->getXml($url)) {
			if (isset($xml["data"]["row"]) && count($xml["data"]["row"])) {
				
				//Oprava formátu asociativního pole z XML pokud existuje pouze 1 záznam
				$estates = $this->correctXmlArrayFormat($xml["data"]["row"]);

				$this->log("Ve zdroji nalezeno " . count($estates) . " zakázek");

				if (is_numeric($this->settings["importEstates"])) {
					$this->log("Budeme importovat pouze zakázky pro makléře s ID {$this->settings["importEstates"]}, kterých je ve zdroji " . $this->getEstatesCountByBrokerId($estates, $this->settings["importEstates"]));
				}

				return $estates;
			}
		}

		$this->log(print_r($xml, true));

		return false;
	}

	/**
	 * Spočítat zakázky makléře podle zadaného id_maklere
	 * @param array $estates
	 * @param string $id_maklere
	 * @return int
	 */

	protected function getEstatesCountByBrokerId($estates, $id_maklere)
	{
		$count = 0;

		foreach($estates AS $estate) {
			if ($estate["id_maklere"] == $id_maklere) {
				$count++;
			}
		}

		return $count;
	}

	/**
	 * Získat kolekci všech nemovitostí s klíčem externího ID
	 * @return App\Collections\EstatesCollection
	 */

	protected function getEstatesCollection()
	{
		$estatesCollection = $this->estatesService->estatesCollection();

		$estatesCollection->query
			->field("external_id")
			->field("external_modified")
			->whereDomain()
			->whereNonRemoved()
			->where("external_id IS NOT NULL")
			//->where("advert_status IN (1,2,3)")
			->key("external_id");

		return $estatesCollection->find();
	}

	/**
	 * Má se zakázka synchronizovat?
	 * @param array $estate
	 * @return bool
	 */

	protected function isEstateForSync($estate)
	{
		$forSync = new ArrayResult;

		//Importujeme všechny nemovitosti RK nebo jen konkrétního makléře, proto filtrovat, jestli je tato nemovitost zadaného makléře
		$forSync->is($this->settings["importEstates"] === true || $this->settings["importEstates"] == $estate["id_maklere"]);

		return $forSync->result();
	}

	/**
	 * Byla zakázka upravena, nebo neexistuje u nás v databázi
	 * @param array $estate
	 * @param App\Entities\Entity $estateEntity
	 * @return bool
	 */

	protected function wasEstateModified($estate, $estateEntity)
	{
		$wasModified = new ArrayResult;

		//Nemovitost v databázi neexistuje
		$wasModified->isNot($estateEntity instanceof \App\Entities\EstateEntity);

		//Nemovitost byla upravena
		$wasModified->is(strtotime($estate["datum_modifikace"]) > strtotime(@$estateEntity->external_modified));

		return $wasModified->atLeastOne();
	}

	/**
	 * Odebrat zakázky, které nebyly v Urbiu nalezeny
	 * @param App\Collections\EstatesCollection $estatesCollection
	 * @return bool
	 */

	protected function archiveNonfoundEstates($estatesCollection)
	{
		if ($estatesCollection->count()) {
			$result = $estatesCollection->archive();

			$this->log("Archivujeme <b>" . $estatesCollection->count() . "</b> nemovitostí, které nejsou v XML feedu ... " . ($result ? "OK" : "Fail"));

			return $result;
		} else {
			$this->log("Nenalezeny žádné neaktuální zakázky k odebrání z databáze");

			return true;
		}
	}

	/**
	 * Získání dat pro zakázku
	 * @param string $id_zakazky
	 * @return array|null|false pokud je zakázka validní vracíme array, pokud se jedná o již nevalidní zakázku, vracíme null, pokud došlo k chybě, vracíme false
	 */

	public function getXmlEstate($id_zakazky)
	{
		$url = "http://www.eurobydleni.cz/download/zakazka_detail.php?id_zakazky={$id_zakazky}";

		if ($xml = $this->getXml($url)) {
			if ($estate = $this->validateXmlEstate($xml)) {
				return $estate;
			} else {
				return null;
			}
		} elseif ($xml["message"] == "požadované ID zakázky již není k dispozici"){
			return null;
		} else {
			//$this->log("Chyba při zpracování dotazu na: {$url} návratová hodnota: " . print_r($xml, true));

			return false;
		}
	}

	/**
	 * Zvalidovat příchozí XML zakázky
	 * @param array $xml
	 * @return array|false
	 */

	protected function validateXmlEstate($xml)
	{
		$estate = $xml["data"];

		if ($this->isXmlEstateValid($estate)) {
			$estate["row"] = $this->correctEmptyValues($estate["row"]);
			
			$estate["images"] = $this->correctXmlArrayFormat($estate["images"]["image"]);

			return $estate;
		}

		return false;
	}

	/**
	 * Kontrola jestli je získaná nabídka validní a bude se importovat
	 * @param array $estate
	 * @return bool
	 */

	protected function isXmlEstateValid($estate)
	{
		$result = new ArrayResult;

		$result->is(@strlen(trim($estate["row"]["id"])));

		$result->is(@strlen(trim($estate["row"]["popisz"])));

		return $result->result();
	}

	/**
	 * Vykonat úlohu
	 * Nějak nám zlobí párování externích ID, tak ignorujeme "add"/"update" stribut úlohy a zjištujeme existenci nemovitosti ještě jednou zde
	 * @param App\Entities\Sync\Urbium\TaskEntity $taskEntity
	 * @return bool
	 */

	protected function processEstateTask($taskEntity)
	{
		$estate = $this->getXmlEstate($taskEntity->external_id);

		if (is_array($estate)) { //Zakázka je validní, provést aktulizaci či přidání
			//Zkusit najít namovitost v databázi podle externího ID
			$estateEntity = $this->estatesService->estateEntity()->findByExternalId($taskEntity->external_id);

			if (!$estateEntity) { //Nemovitost v databázi neexistuje, vytvořit novou nemovitost
				$estateEntity = $this->estatesService->estateEntity()->blank();
			}

			//Uložit nemovitost
			return $this->saveEstate($estateEntity, $estate);

		} elseif ($estate === null) { //Nabídka byla načtena, ale není validní, ignorovat
			$this->log("Nabídka č. {$taskEntity->external_id} není validní! Ignoruji");

			return true;
		} else { //Informace o nabídce se vůbec nepodařilo načíst
			$this->log("Informace o nabídce č. {$taskEntity->external_id} se nepodařilo načíst! Ignoruji");

			return true;
		}
	}

	/**
	 * Uložit nemovitost
	 * @param App\Entities\EstateEntity $estateEntity
	 * @param array $estate
	 * @return bool
	 */

	protected function saveEstate($estateEntity, $estate)
	{
		//Jedná se o přidání nové nemovitosti nebo a aktualizace, používáme pouze pro log
		$operation = $estateEntity->getId() ? "update" : "add";

		//Přeložit hodnoty z Urbia do našich hodnot pro EstateEntity
		$this->translateEstateValues($estateEntity, $estate["row"]);

		//Přeložit obrázky z Urbia do našich hodnot pro EstateEntity
		$this->translateEstateImages($estateEntity, $estate["images"]);

		//Uložit nemovitost
		$result = $estateEntity->save();

		switch ($operation) {
			case "update":
				$this->log("Aktualizuji nemovitost <b>" . substr($estateEntity->estate_title, 0, 100) . "</b> ... " . ($result ? "OK" : "Fail"));
				break;
			case "add":
				$this->log("Přidávám novou nemovitost <b>" . substr($estateEntity->estate_title, 0, 100) . "</b> ... " . ($result ? "OK" : "Fail"));
				break;
		}

		return $result;
	}
}