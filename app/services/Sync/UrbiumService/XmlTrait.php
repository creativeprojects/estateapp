<?php

namespace App\Services\Sync\UrbiumService;

use CP\Utils\ArrayResult;

trait XmlTrait
{
	/**
	 * Získat XML ze zadané URL
	 * Pokud se získání nezdaří, uskutečnit zadaný počet pokusů
	 * @param string $url
	 * @return array|false
	 */
	protected function getXml($url)
	{
		for($a = 1;$a < self::XML_ATTEMPTS_COUNT; $a++){
			$authUrl = $url . (strpos($url, "?") === false ? "?" : "&") . $this->getAuthString();

			$this->log("Pokus o získání informací z '{$url}' č. {$a}");

			if($xml = simplexml_load_file($authUrl, 'SimpleXMLElement', LIBXML_NOCDATA)){
				$xml = json_decode(json_encode($xml), true);
			}
			
			if (isset($xml["status"]) && $xml["status"] == "ERROR") {
				$this->log(@"ERROR - {$xml["message"]}");

				return false;

				break;
			} elseif ($this->validateXml($xml)) {
				$this->log("Načteno ...");
				
				break;
			} else {
				$sleep = rand(1,10);

				sleep($sleep);
			}
		}

		return $xml;
	}

	/**
	 * Oprava formátu pole XML, pokud je totiž nalezen pouze jeden záznam, tak to mění strukturu pole
	 * @param array $array
	 * @return array
	 */

	protected function correctXmlArrayFormat($array)
	{
		if(!is_numeric(key($array))){
			$array = array($array);
		}

		return $array;
	}

	/**
	 * Oprava prázdných polí
	 * @param array $values
	 * @return array
	 */

	protected function correctEmptyValues($values)
	{
		foreach($values AS & $value){
			if($value == []){
				$value = null;
			}
		}

		return $values;
	}

	/**
	 * Kontrola jestli jsem opravdu dostal data, nikoliv nějaký error
	 * @param mixed $xml
	 * @return bool
	 */

	protected function validateXml($xml)
	{
		$result = new ArrayResult;

		$result->is(@is_array($xml));
		
		$result->is(@array_key_exists("data", $xml));
		
		$result->is(@is_array($xml["data"]));
		
		$result->is(@array_key_exists("row", $xml["data"]));

		return $result->result();
	}
}