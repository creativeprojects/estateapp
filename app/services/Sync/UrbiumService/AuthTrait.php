<?php

namespace App\Services\Sync\UrbiumService;

use CP\Utils\ArrayResult;

trait AuthTrait
{
	protected $auth;

	/**
	 * Ověření zda-li jsou zadány přihlašovací údaje do Urbia
	 * @return bool
	 * @throws \Exception
	 */

	protected function loadAuth()
	{
		$result = new ArrayResult;

		$result->is(@$this->settings["auth"]["username"]);

		$result->is(@$this->settings["auth"]["password"]);

		if($result->result()){
			$this->setAuth();

			return true;
		} else {
			$this->log("Neplatné přihlašovací údaje do Urbia!" . print_r($this->settings["auth"], true));

			return false;
		}
	}

	/**
	 * Nastavit autorizaci
	 */

	protected function setAuth()
	{
		$this->auth = (object) $this->settings["auth"];
	}

	/**
	 * Získat GET query pro ověření identity při požadavku
	 * @return string
	 */

	protected function getAuthString()
	{
		return "username={$this->auth->username}&password={$this->auth->password}";
	}
}