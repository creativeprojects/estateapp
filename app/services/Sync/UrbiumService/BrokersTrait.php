<?php

namespace App\Services\Sync\UrbiumService;

trait BrokersTrait
{
	/**
	 * Zjistit jestli nejsou v Urbiu nový či upravení makléři
	 * @return bool
	 */

	protected function checkBrokerTasks()
	{
		$brokers = $this->getXmlBrokers();

		if ($brokers){
			$brokersCollection = $this->getBrokersCollection();

			$this->log("V databázi nalezeno " . $brokersCollection->count() . " makléřů");

			foreach ($brokers AS $key => $broker) {
				if($this->isBrokerForSync($broker, $brokersCollection->getOne($broker["id_maklere"]))){ //Pokud byl makléř upraven, nebo zatím neexistuje, uložit úlohu
					$result = $this->saveTask("brokers", $broker["id_maklere"]);

					if($result){ //Úloha byla uložena, odebrat ze seznamu nalezených maklřů
						unset($brokersCollection[$broker["id_maklere"]]);
					} else { //Došlo k chybě. nepokračovat v operaci
						return false;
					}
				}
			}

			//Odebrat makléře, kteří jsou v databázi, ale nebyly nalezeni v Urbiu
			return $this->removeNonfoundBrokers($brokersCollection);
		}

		return false;
	}

	/**
	 * Načíst hlavičky makléřů z Urbia
	 * @return array|false
	 */

	protected function getXmlBrokers()
	{
		$url = "http://www.eurobydleni.cz/download/makler_list.php";

		if ($xml = $this->getXml($url)) {
			if(isset($xml["data"]["row"]) && count($xml["data"]["row"])){
				
				//Oprava formátu asociativního pole z XML pokud existuje pouze 1 záznam
				$brokers = $this->correctXmlArrayFormat($xml["data"]["row"]);

				$this->log("Ve zdroji nalezeno " . count($brokers) . " makléřů");
				
				return $brokers;
			}
		}

		$this->log(print_r($xml, true));

		return false;
	}

	/**
	 * Získat kolekci současných makléřů s klíčem external_id
	 * @return App\Collections\UsersCollection
	 */

	protected function getBrokersCollection()
	{
		$brokersCollection = $this->usersService->usersCollection();

		$brokersCollection->query
			->key("external_id");

		return $brokersCollection;
	}

	/**
	 * Odebrat makléře, kteří nebyly nalezeni v Urbiua, ale jsou v naší databázi
	 * @param App\Collections\UsersCollection $brokersCollection
	 * @return bool
	 */

	protected function removeNonfoundBrokers($brokersCollection)
	{
		return $brokersCollection->remove();
	}

	/**
	 * Získat z urbia informace o makléři
	 * @param string $idmaklere
	 * @return array|false
	 */

	public function getXmlBroker($id_maklere)
	{
		$url = "http://www.eurobydleni.cz/download/makler_detail.php?id_maklere={$id_maklere}";

		if($xml = $this->getXml($url)){
			if($this->isXmlBrokerValid($xml)){
			 	$broker = $xml["data"]["row"];

			 	if($this->isXmlBrokerValid($broker)){
					$broker = $this->correctEmptyValues($broker);

					return $broker;
				} else {
					return null;
				}
			}
		}

		$this->log("Chyba při zpracování dotazu na: {$url} návratová hodnota: " . print_r($xml, true));

		return false;
	}

	/**
	 * Vykonání úlohy
	 * @param App\Entities\Sync\Urbium\TaskEntity $taskEntity
	 * @return bool
	 */

	protected function processBrokerTask($taskEntity)
	{
		$broker = $this->getXmlBroker($taskEntity->external_id);

		if(is_array($broker)){ //Validní hodnoty pro makléře
			$broker = $this->translateBrokerValues($broker);

			switch($task->operation){
				case "add": //Makléř zatím neexistuje u nás v databázi, přidat
					$userEntity = $this->userService->userEntity()->blank();
					
					break;
				case "update": //Makléř již existuje v databázi, aktualizovat informace
					$userEntity = $this->userService->userEntity()->findByExternalId($taskEntity->external_id);

					break;
			}

			//Uložíme uživatele
			return $userEntity->save($broker);

		} elseif($broker === null) { //Informace o makléři nejsou validní, ignorovat
			$this->log("Informace o makléři č. {$taskEntity->external_id} nejsou validní, ignoruji");

			return true;
		} else { //Došlo k chybě
			$this->log("Informace o makléři č. {$taskEntity->external_id} se nepodařilo načíst!");

			return false;
		}
	}

	/**
	 * TODO - Případná kontrola, zda-li jsou informace o makléři validní a bude se importovat
	 * @param array $xml
	 * @return bool
	 */

	protected function isXmlBrokerValid($xml)
	{
		return true;		
	}

	/**
	 * Makléř neexistuje či byl změněn
	 * @param array $broker
	 * @param App\Entities\UserEntity $userEntity
	 * @return bool
	 */

	protected function isBrokerForSync($broker, $userEntity)
	{
		$result = new ArrayResult;

		//Makléř u nás neexistuje
		$result->isNot($userEntity instanceof \App\Entities\UserEntity);

		//Makléř byl upraven
		$result->is(strtotime($broker["datum_modifikace"]) > strtotime(@$userEntity->last_modified));
		
		return $result->atLeastOne();
	}

	/**
	 * Přeložit hodnoty z Urbia na náš formát
	 * @param array $broker
	 * @return array
	 */

	protected function translateBrokerValues($broker)
	{
		$values = array_extend($currentBroker, $broker);

		return $values;
	}

	protected function createBrokerName($broker)
	{
		$brokerName = implode(" ", array(trim($broker["jmeno"]), trim($broker["prijmeni"])));

		return $brokerName;
	}
}