<?php

namespace App\Services\Sync\UrbiumService\EstatesTrait\TranslationTrait;

trait TranslationMethodsTrait
{
	/**
	 * Získat z typ_nemovitosti náš kód advert_type
	 * @param integer $typ_nemovitosti
	 * @param integer $typ_nemovitosti_u
	 * @return integer
	 */
	
	protected function getAdvertType($typ_nemovitosti, $typ_nemovitosti_u)
	{
		//Urbium má některé typ_nemovitosti_u zařazené v jiných typ_nemovitosti, než my, proto musíme nejdříve přeložit tyto vyjímky
		$advert_subtype = [
			415 => 11, //Činžovní dům - Urbium ho má mezi domy, my mezi komerčními nemovitostmi
			27 => 8, //Chalupa - Urbium ji má mezi rekreačními objekty, my mezi domy
			26 => 8, //Chata - Urbium ji má mezi rekreačnimi objekty, my mezi domy
			28 => 8, //Zemědělská usedlost - Urbium ji má mezi rekreačními objekty, my mezi domy
			48 => 8, //Zámek, hrad, památka - Urbium je má mezi ostatními, my mezi domy
		];

		if(array_key_exists($typ_nemovitosti_u, $advert_subtype)){
			$typ_nemovitosti = $advert_subtype[$typ_nemovitosti_u];
		}

		$advert_type = [
			7 => 1, //Byty
			8 => 2, //Domy
			11 => 4, //Komerční nemovitosti
			13 => 5, //Ostatní
			12 => 3, //Pozemky
			9 => 5, //Rekreační objekty - my tuto kategorii nemáme, zařadíme mezi ostatní
		];

		return @$advert_type[$typ_nemovitosti];
	}

	/**
	 * Získat z typ_nemovitosti_u náš kód advert_subtype
	 * @param integer $typ_nemovitosti_u
	 * @return integer
	 */

	protected function getAdvertSubType($typ_nemovitosti_u)
	{
		$advert_subtype = [
			15 => 3, //1+1
			16 => 2, //1+kk
			17 => 5, //2+1
			18 => 4, //2+kk
			19 => 7, //3+1
			20 => 6, //3+kk
			21 => 9, //4+1
			229 => 9, //4+2 - tento typ my nemáme, tak z něj uděláme 4+1
			22 => 8, //4+kk
			202 => 11, //5+1
			230 => 11, //5+2 - tento typ nemáme, uděláme z něj 5+1
			256 => 10, //5+kk
			203 => 12, //6+1 - my už máme pouze 6 a více
			231 => 12, //6+2 - my už máme pouze 6 a více
			257 => 12, //6+kk - my už máme pouze 6 a více
			227 => 12, //7+1 - my už máme pouze 6 a více
			232 => 12, //7+2 - my už máme pouze 6 a více
			228 => 12, //8+1 - my už máme pouze 6 a více
			233 => 12, //8+2 - my už máme pouze 6 a více
			249 => 16, //atypický/jiný
			14 => 2, //Garsoniera - nemáme tento typ, uděláme z něj 1+kk
			615 => 47, //Pokoj
			250 => 16, //Půdní byt - tento typ nemáme, uděláme z něj atypický byt
			204 => 12, //více než 6+1
			415 => 38, //Činžovní dům - tento typ_nemovitosti_u přesouváme z domů do komerčních nemovitostí 
			24 => 37, //Rodinný dům
			25 => 39, //Vila
			27 => 43, //Chalupa
			26 => 33, //Chata
			28 => 44, //Zemědělská usedlost
			32 => 25, //Administrativní budova - my tento typ nemáe, uděláme z ní Kanceláře
			36 => 29, //Hotel - my tento typ nemáme, uděláme z něj Ubytování
			30 => 25, //Kancelář
			31 => 28, //Obchodní prostor
			39 => 32, //Ostatní komerční objekt
			37 => 29, //Pension - my tento typ nemáme, uděláme z něj Ubytování
			35 => 25, //Provozní areál - my tento typ nemáme, uděláme z něj Kancelář
			38 => 30, //Restaurace, bar
			33 => 26, //Skladovací prostory
			34 => 27, //Výrobní prostory
			459 => 31, //Zemědělský objekt
			45 => 21, //Les
			44 => 22, //Louka
			40 => 19, //Pozemek, bydlení
			41 => 18, //Pozemek, komerční
			672 => 48, //Sady, vinice
			46 => 46, //Vodní plocha
			42 => 23, //Zahrada
			43 => 20, //Zemědělská půda
			47 => 34, //Garáž
			612 => 52, //Garážové stání
			614 => 53, //Mobilheimy
			49 => 36, //Nezařazené
			613 => 36, //Parkovací stání - my tento typ nemáme, zařadíme do Ostatní / ostatní
			661 => 51, //Půdní prostor
			48 => 35, //Zámek, hrad, památka - typ přeřadíme mezi domy, urbium to má mezi ostatními
		];

		return @$advert_subtype[$typ_nemovitosti_u];
	}

	/**
	 * Získat z typ_prodeje náš kód advert_function
	 * @param integer $typ_nemovitosti_u
	 * @return integer
	 */

	protected function getAdvertFunction($typ_prodeje)
	{
		$advert_function = [
			5 => 3, //dražba
			2 => 1, //prodej
			3 => 2, //pronájem
		];

		return @$advert_function[$typ_prodeje];
	}

	/**
	 * Získat z "stav" náš kód "advert_status"
	 * @param integer $typ_nemovitosti_u
	 * @return integer
	 */

	protected function getAdvertStatus($stav)
	{
		$advert_status = [
			51 => 1, //Aktivní
			251 => 1, //Aktivní, jen vlastní WEB - my toto řešíme zvlášť, necháme jako aktivní
			349 => 5, //Archivováno - my tento stav nemáme, nastavíme Nezískáno
			348 => 5, //Konec smlouvy - my tento stav nemáme, nastavíme Nezískáno
			55 => 0, //Nezobrazovat, neinzerovat - my máme stav V příprav, ale je to asi to samé
			243 => 0, //Příprava zakázky
			53 => 4, //Prodáno/pronajmuto
			52 => 3, //Rezervováno - u nás nebude dále inzerováno
			548 => 2, //Rezervováno, jen vlastní web - u nás bude dále inzerováno
			56 => 0, //Smazat z databáze - my tento stav nemáme, ale myslím, že k nám stejně tento stav nepřijde, kdyby přišel tak bude nastaveno jako v přípravě aby to svítilo a mohl to uživatel smazat
		];

		return @$advert_status[$stav];
	}

	/**
	 * Získat z "velikost" náš kód "advert_room_count"
	 * @param integer $velikost
	 * @return integer
	 */

	protected function getAdvertRoomCount($velikost)
	{
		$advert_room_count = [
			15 => 1, //1+1
			16 => 1, //1+kk
			17 => 2, //2+1
			18 => 2, //2+kk
			19 => 3, //3+1
			20 => 3, //3+kk
			21 => 4, //4+1
			229 => 4, //4+2
			22 => 4, //4+kk
			202 => 5, //5+1
			230 => 5, //5+2
			256 => 5, //5+kk
			203 => 5, //6+1
			231 => 5, //6+2
			257 => 5, //6+kk
			227 => 5, //7+1
			232 => 5, //7+2
			228 => 5, //8+1
			233 => 5, //8+2
			249 => 6, //atypický/jiný
			14 => 1, //Garsoniera
			615 => 1, //Pokoj
			250 => 6, //Půdní byt
			204 => 5, //více než 6+1
		];

		return @$advert_room_count[$velikost];
	}

	/**
	 * Získat z "vlastnictvi" náš kód "ownership"
	 * @param integer $vlastnictvi
	 * @return integer
	 */

	protected function getOwnership($vlastnictvi)
	{
		$owenrship = [
			122 => 2, //Družstevní
			125 => 4, //Jiné
			123 => 3, //Obecní
			121 => 1, //Osobní
			124 => 4, //U majitele
		];

		return @$ownership[$vlastnictvi];
	}

	/**
	 * Získat z "podlazi" náše číslo "floor_number"
	 * @param integer $podlazi
	 * @return integer
	 */

	protected function getFloorNumber($podlazi)
	{
		$floor_number = [
			131 => 1, //1. patro
			185 => 10, //10. patro
			184 => 11, // 11. patro
			463 => 12, //12. patro
			462 => 13, //13. patro
			464 => 14, //14. patro
			465 => 15, //15. patro
			466 => 16, //16. patro
			467 => 17, //17. patro
			468 => 18, //18. patro
			469 => 19, //19. patro
			132 => 2, //2. patro
			133 => 3, //3. patro
			134 => 4, //4. patro
			135 => 5, //5. patro
			136 => 6, //6. patro
			181 => 7, //7. patro
			182 => 8, //8. patro
			183 => 9, //9. patro
			130 => 0, //Přízemí
			128 => 0, //Snížené přízemí
			127 => -1, //Suterén
			129 => 1, //Zvýšené přízemí
		];

		return @$floor_number[$podlazi];
	}

	/**
	 * Získat z "topeni" náš kód "heating"
	 * @param integer $topeni
	 * @return integer
	 */

	protected function getHeating($topeni)
	{
		$heating = [
			252 => 0, //Bez topení
			95 => 3, //Lokální elektrické
			96 => 2, //Lokální palivové
			94 => 1, //Lokální plynové
			97 => 9, //Lokální tepelné čerpadlo
			93 => 7, //ÚT dálkové
			91 => 6, //ÚT elektrické
			90 => 4, //ÚT plynové
			92 => 5, //ÚT tuhá paliva
		];

		$heating = @$heating[$topeni];

		//Upravíme do array formátu, protože máme toto pole nastaveno jako multiselect, do budoucna možná zrušíme a budeme mít jen jednu možnost topení
		if ($heating) {
			$heating = [$heating];
		} else {
			$heating = [];
		}

		return $heating;
	}

	/**
	 * Získat z "konstrukce_budovy" náš kód "building_type"
	 * @param integer $topeni
	 * @return integer
	 */

	protected function getBuildingType($konstrukce_budovy)
	{
		$building_type = [
			114 => 2, //Cihlová
			117 => 1, //Dřevostavba
			118 => 3, //Kamenná
			119 => 4, //Montovaná
			115 => 5, //Panelová
			116 => 6, //Skeletová
			195 => 7, //Smíšená
		];

		return @$building_type[$konstrukce_budovy];
	}

	/**
	 * Vytvořit seznam elektrických sítí ze "site"
	 * @param string $site
	 * @return array
	 */

	protected function getElectricity($site)
	{
		$site = $this->explodeValues($site);

		$electricityOptions = [
			553 => 1, //120V
			82 => 2, //220V - my tento typ nemáme, 220V už se ani nedělá, nastavíme 230V
			404 => 2, //230V
			83 => 3, //380V
			405 => 4, //400V
		];

		$electricity = [];

		foreach($site AS $sit){
			if(array_key_exists($sit, $electricityOptions)){
				$electricity[] = $electricityOptions[$sit];
			}
		}

		return $electricity;
	}

	/**
	 * Vytvořit seznam telekomunikací ze "site"
	 * @param string $site
	 * @return array
	 */

	protected function getTelecommunication($site)
	{
		$site = $this->explodeValues($site);

		$telecommunicationOptions = [
			86 => 2, //Internet
			85 => 1, //Telefon
			87 => 4, //TV kabel
			88 => 3, //TV satelit
		];

		//TODO - ještě nemáme 5 - kabelové rozvody, 6 - ostatní

		$telecommunication = [];

		foreach($site AS $sit){
			if(array_key_exists($sit, $telecommunicationOptions)){
				$telecommunication[] = $telecommunicationOptions[$sit];
			}
		}

		return $telecommunication;
	}

	/**
	 * Z vlastností "vybaveni" získat náš kód "furnished"
	 * @param string $vybaveni
	 * @return integer
	 */

	protected function getFurnished($vybaveni)
	{
		if ($this->hasProperty(99, $vybaveni)) { //Zařízeno nábytkem
			return 1;
		} elseif ($this->hasProperty(544, $vybaveni)) { //Nezařízeno nábytkem
			return 2;
		} else { //Zařízeno částečně nábytkem
			return 3;
		}
	}

	/**
	 * Získat z "stav_objektu" náš kód "building_condition"
	 * @param integer $stav_objektu
	 * @return integer
	 */

	protected function getBuildingCondition($stav_objektu)
	{
		$building_condition = [
			111 => 2, //Dobrý
			565 => 7, //K demolici
			423 => 6, //Novostavba
			361 => 9, //Po rekonstrukci
			360 => 8, //Před rekonstrukcí
			564 => 5, //Projekt
			112 => 3, //Špatný
			563 => 4, //Ve výstavbě
			110 => 1, //Velmi dobrý
		];

		return @$building_condition[$stav_objektu];
	}

	/**
	 * Získat z "typ_budovy" náš kód "object_kind"
	 * @param integer $stav_objektu
	 * @return integer
	 */

	protected function getObjectKind($typ_budovy)
	{
		$object_kind = [
			149 => 5, //Atriový
			148 => 8, //Dvojdům
			358 => 7, //Dvougenerační - my tento typ nemáme, nastavíme "Vícegenrační"
			410 => 4, //Nízkoenergetický - my logicky tento typ nemáme, nastavíme "Samostatný"
			146 => 1, //Řadový
			357 => 2, //Rohový
			147 => 4, //Samostatně stojící
			150 => 6, //Terasový
			451 => 3, //V bloku
			359 => 7, //Vícegenerační
		];

		return @$object_kind[$typ_budovy];
	}

	/**
	 * Získat z "umisteni" náš kód "object_location"
	 * @param integer $umisteni
	 * @return integer
	 */

	protected function getObjectLocation($umisteni)
	{
		$object_location = [
			138 => 1, //Centrum obce
			140 => 2, //Klidná část obce
			141 => 4, //Okraj obce
			143 => 6, //Polosamota
			139 => 3, //Rušná část obce
			144 => 7, //Samota
			142 => 5, //Sídliště
		];

		return @$object_location[$umisteni];
	}
	
	/**
	 * Vytvořit seznam možností dopravy z "doprava"
	 * @param string $doprava
	 * @return array
	 */
	
	protected function getTransport($doprava)
	{
		$transportOptions = [
			172 => 5, //Autobus
			549 => 2, //Dálnice
			168 => 7, //Metro
			550 => 4, //MHD
			551 => 6, //Přístav
			171 => 3, //Silnice
			169 => 9, //Tramvaj
			170 => 8, //Trolejbus
			167 => 1, //Vlak
		];

		$doprava = $this->explodeValues($doprava);

		$transport = [];

		foreach($doprava AS $_doprava){
			if(array_key_exists($_doprava, $transportOptions)){
				$transport[] = $transportOptions[$_doprava];
			}
		}

		return $transport;
	}

	/**
	 * Získat z "cena_mena" náš kód "advert_price_currency"
	 * @param integer $cena_mena
	 * @return integer
	 */

	protected function getAdvertPriceCurrency($cena_mena)
	{
		$advert_price_currency = [
			61 => 3, //Euro
			62 => 3, //Euro/m2
			59 => 1, //Kč
			60 => 1, //Kč/m2
			63 => 2, //USD
			64 => 2, //USD/m2
			568 => 3, //Euro/hodina
			236 => 3, //Euro/měs
			238 => 3, //Euro/měs/m2
			310 => 3, //Euro/noc
			235 => 3, //Euro/rok
			237 => 3, //Euro/rok/m2
			567 => 1, //Kč/hodina
			66 => 1, //Kč/měs
			68 => 1, //Kč/měs/m2
			309 => 1, //Kč/noc
			65 => 1, //Kč/rok
			67 => 1, //Kč/rok/m2
			574 => 2, //USD/den
			569 => 2, //USD/hodina
			571 => 2, //USD/m2/měsíc
			572 => 2, //USD/m2/rok
			570 => 2, //USD/měsíc
			573 => 2, //USD/rok
		];

		return @$advert_price_currency[$cena_mena];
	}

	/**
	 * Získat z "cena_mena" náš kód "advert_price_unit"
	 * @param integer $cena_mena
	 * @return integer
	 */

	protected function getAdvertPriceUnit($cena_mena)
	{
		$advert_price_unit = [
			61 => 1, //Euro
			62 => 3, //Euro/m2
			59 => 1, //Kč
			60 => 3, //Kč/m2
			63 => 1, //USD
			64 => 3, //USD/m2
			568 => 8, //Euro/hodina
			236 => 2, //Euro/měs
			238 => 4, //Euro/měs/m2
			310 => 7, //Euro/noc
			235 => 6, //Euro/rok
			237 => 5, //Euro/rok/m2
			567 => 8, //Kč/hodina
			66 => 2, //Kč/měs
			68 => 4, //Kč/měs/m2
			309 => 7, //Kč/noc
			65 => 6, //Kč/rok
			67 => 5, //Kč/rok/m2
			574 => 7, //USD/den
			569 => 8, //USD/hodina
			571 => 4, //USD/m2/měsíc
			572 => 5, //USD/m2/rok
			570 => 2, //USD/měsíc
			573 => 6, //USD/rok
		];

		return @$advert_price_unit[$cena_mena];
	}

	/**
	 * Získat z "es_trida_narocnosti" náš kód "enery_efficiency_rating"
	 * @param integer $es_trida_narocnosti
	 * @return integer
	 */

	protected function getEnergyEfficiencyRating($es_trida_narocnosti)
	{
		$energy_efficiency_rating = [
			534 => 1, //A - mimořádně úsporná
			535 => 2, //B - velmi úsporná
			536 => 3, //C - úsporná
			537 => 4, //D - méně úsporná
			538 => 5, //E - Nehospodárná 
			539 => 6, //F - velmi nehospodárná
			540 => 7, //G - mimořádně nehospodárná
		];

		return @$energy_efficiency_rating[$es_trida_narocnosti];
	}

	/**
	 * Získat z "es_vyhlaska" náš kód "energy_performance_certificate"
	 * @param integer $es_vyhlaska
	 * @return integer
	 */

	protected function getEnergyPerformanceCertificate($es_vyhlaska)
	{
		$energy_performance_certificate = [
			542 => 2, //vyhláška 78/2013 Sb.
			636 => 1, //vyhláška č. 148/2007 Sb.
		];

		return @$energy_performance_certificate[$es_vyhlaska];
	}

	/**
	 * Získat z "stat" náš kód "locality_country_code"
	 * @param integer $stat
	 * @return string
	 */

	protected function getLocalityCountryCode($stat)
	{
		$locality_country_code = [
			263 => "CZ", //ČR
			264 => "SK", //SK
		];

		return @$locality_country_code[$stat];
	}

	/**
	 * Získat z "stat" název země "locality_country"
	 * @param integer $stat
	 * @return string
	 */

	protected function getLocalityCountry($stat)
	{
		$locality_country = [
			263 => "Česká Republika", //ČR
			264 => "Slovensko", //SK
		];

		return @$locality_country[$stat];
	}

	/**
	 * Oprava popisu nemovitosti - Urbium nakonec přidává odkaz na svůj portál, ten odebereme
	 * @param string $popis
	 * @return string $popis
	 */
	
	protected function getEstateDescription($popis)
	{
		//Pokud je popis řetezec, oteřtujeme to kvůli nevalidnímu anglickému popisu
		//DEMAND - až bude potřeba, tak doplníme řešení pro anglický popis, který nyní ignotrujeme protože přicházi jako array
		if(is_string($popis)){
			$suffix = strpos($popis, "<a href");

			if($suffix !== false){
				$popis = substr($popis, 0, $suffix);
			}

			return $popis;
		} else {
			return null;
		}
	}

	/**
	 * Existuje hledaná vlastnost v seznamu vlastností?
	 * @param integer $value
	 * @param array $values
	 * @param array $return - pokud potřebujeme navrátit něco jiného než 0|1
	 * @return bool
	 */	

	protected function hasProperty($value, $values, $return = array(0,1))
	{
		$values = $this->explodeValues($values);

		$result = in_array($value, $values) ? 0 : 1;

		return $return[$result];
	}

	/**
	 * Vytvořit array z vlastností oddělených čárkou
	 * @param string $values
	 * @return array
	 */

	protected function explodeValues($values)
	{
		return explode(",", $values);
	}

	//TODO - vybavenost - my ji můbec nemáme, ale možná bychom měli mít
	/*179 Kompletní síť obchodů a služeb [ 173 ]
	177 Pošta [ 173 ]
	178 Supermarket [ 173 ]
	174 Škola [ 173 ]
	175 Školka [ 173 ]
	180 Úřady [ 173 ]
	176 Zdravotnická zařízení [ 173 ]*/

	//TODO - site, ktere my nemáme
	/*157 Voda, pitná [ 80 ]
	158 Voda, užitková [ 80 ]
	84 Kanalizace [ 80 ]
	159 Klimatizace [ 80 ]
	81 Plyn [ 80 ]
	461 Studna [ 80 ]*/
}