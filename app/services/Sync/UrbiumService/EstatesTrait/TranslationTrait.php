<?php

namespace App\Services\Sync\UrbiumService\EstatesTrait;

use CP\Utils\GeoCoder;

trait TranslationTrait
{
	/** Překladové metody */
	use TranslationTrait\TranslationMethodsTrait;

	/**
	 * Přeložit hodnoty získané z Urbia do našeho formátu
	 * @param App\Entities\EstateEntity $estateEntity
	 * @param array $values
	 * @return void
	 */

	protected function translateEstateValues($estateEntity, $values)
	{
		/**
		 * Interní označení
		 */

		//ID nemovitosti v Urbiu
		$estateEntity->external_id = $values["id"];

		//Datum vytvoření zakázky
		$estateEntity->created = $values["datum"];

		//Datum a čas poslední modifikace zakázky v Urbiu
		$estateEntity->external_modified = $values["datum_modifikace"];

		//ID uživatele - nejdeme uživatele podle external_id
		$estateEntity->setUserId($this->userService->userEntity()->findByExternalId($values["idu"])->getId());

		/**
		 * Základní zařazení
		 */

		//Druh obchodu
		$estateEntity->advert_function = $this->getAdvertFunction($values["typ_prodeje"]);

		//Typ nemovitosti
		$estateEntity->advert_type = $this->getAdvertType($values["typ_nemovitosti"], $values["typ_nemovitosti_u"]);

		//Upřesnění typu nemovitosti
		$estateEntity->advert_subtype = $this->getAdvertSubType($values["typ_nemovitosti_u"]);

		//Stav zakázky
		$estateEntity->advert_status = $this->getAdvertStatus($values["stav"]);

		//Vlastnictví
		$estateEntity->ownership = $this->getOwnership($values["vlastnictvi"]);
		
		/**
		 * Specifikace inzerátu
		 */

		//Evidenční číslo zakázky
		$estateEntity->advert_code = $values["ec"];

		//Tituek inzerátu
		$estateEntity->estate_title = [
			"cs" => $values["popisz"],
			"en" => $values["popisz_en"]
		];

		//Textový popis nemovitosti
		$estateEntity->estate_description = [
			"cs" => $this->getEstateDescription($values["popis"]),
			"en" => $this->getEstateDescription($values["popis_en"])
		];

		/**
		 * Počty
		 */

		//Počet pokojů
		$estateEntity->advert_room_count = $this->getAdvertRoomCount($values["velikost"]);

		//Počet parkovacích míst
		$estateEntity->parking = $values["parkovani_pocet_mist"];

		//Počet podlaží
		$estateEntity->floors = $values["pocet_np"];

		/**
		 * Vlastnosti
		 */

		//Topení
		$estateEntity->heating = $this->getHeating($values["topeni"]);

		//Stavba
		$estateEntity->building_type = $this->getBuildingType($values["konstrukce_budovy"]);

		//Elektřina
		$estateEntity->electricity = $this->getElectricity($values["site"]);

		//Telekomunikace
		$estateEntity->telecommunication = $this->getTelecommunication($values["site"]);

		//Balkón
		$estateEntity->balcony = $this->hasProperty(101, $values["vybaveni"]);

		//Bazén
		$estateEntity->basin = $this->hasProperty(552, $values["vybaveni"]);

		//Bezbariérový přístup
		$estateEntity->easy_access = $this->hasProperty(597, $values["vybaveni"], [1,2]);

		//Garáž
		$estateEntity->garage = $this->hasProperty(106, $values["vybaveni"]);

		//Lodžie
		$estateEntity->loggia = $this->hasProperty(406, $values["vybaveni"]);

		//Parkování
		$estateEntity->parking_lots = $this->hasProperty(105, $values["vybaveni"]);

		//Sklep
		$estateEntity->cellar = $this->hasProperty(107, $values["vybaveni"]);

		//Terasa
		$estateEntity->terrace = $this->hasProperty(102, $values["vybaveni"]);

		//Výtah
		$estateEntity->elevator = $this->hasProperty(108, $values["vybaveni"], [1,2]);

		//Vybaveno nábytkem
		$estateEntity->furnished = $this->getFurnished($values["vybaveni"]);

		//Stav objektu
		$estateEntity->building_condition = $this->getBuildingCondition($values["stav_objektu"]);

		/**
		 * Plochy
		 */

		//TODO - musíme otestovat jak to správně nastavit
		//Plocha - nevím jestli je to správně, my u estate_area máme napsáno plocha pozemku
		$estateEntity->estate_area = $values["plocha"] ? $values["plocha"] : $values["plocha_pozemek"];

		//Obytná plocha - nevím jestli je to správně zařazeno
		$estateEntity->usable_area = $values["plocha_obytna"];

		//Zastavná plocha
		$estateEntity->building_area = $values["plocha_zastavena"];

		//Plocha zahrady
		$estateEntity->garden_area = $values["plocha_zahrada"];
		
		//Plocha nebytových prostor
		$estateEntity->nolive_total_area = $values["plocha_nebyt"];

		//Obchodní plocha
		$estateEntity->shop_area = $values["plocha_obchodni"];

		//Plocha kanceláří
		$estateEntity->offices_area = $values["plocha_kancelar"];

		/**
		 * Lokalita
		 */

		//Poloha objektu
		$estateEntity->object_location = $this->getObjectLocation($values["umisteni"]);

		//Podlazí ve které se byt nachází
		$estateEntity->floor_number = $this->getFloorNumber($values["podlazi"]);

		//Poloha domu
		$estateEntity->object_kind = $this->getObjectKind($values["typ_budovy"]);

		//Země
		$estateEntity->locality_country = $this->getLocalityCountry($values["stat"]);
		$estateEntity->locality_country_code = $this->getLocalityCountryCode($values["stat"]);

		//Kraj
		$estateEntity->locality_state_uir = $values["kraj_kod"];
		$estateEntity->locality_state = $values["kraj_nazev"];

		//Okres
		$estateEntity->locality_county_uir = $values["okres_kod"];
		$estateEntity->locality_county = $values["okres_nazev"];

		//Obec
		$estateEntity->locality_city_uir = $values["obec_kod"];
		$estateEntity->locality_city = $values["obec_nazev"];

		//Část obce
		$estateEntity->locality_district_uir = $values["cobce_kod"];
		$estateEntity->locality_district = $values["cobce_nazev"];

		//Městská část
		//praha_kod => "0"
		$estateEntity->locality_citypart = $values["praha_nazev"];

		//Ulice
		$estateEntity->locality_street_uir = $values["ulice_kod"];
		$estateEntity->locality_street = $values["ulice_nazev"];

		//PSČ
		$estateEntity->locality_zip = $values["psc"];

		//Latitude
		$estateEntity->locality->latitude = GeoCoder::dmsToDec($values["gps_x"]);

		//Longitude
		$estateEntity->locality->longitude = GeoCoder::dmsToDec($values["gps_y"]);

		//Číslo bytu
		$estateEntity->locality_cj = $values["cislo_bytu"];

		//Parcelní číslo
		$estateEntity->locality_pc = $values["parcelni_cislo"];

		//Doprava
		$estateEntity->transport = $this->getTransport($values["doprava"]);

		/**
		 * Cena
		 */

		//Cena 
		$estateEntity->advert_price = $values["cena"];

		//Cena před slevou
		$estateEntity->advert_price_original = $values["sleva"];

		//Měna ceny
		$estateEntity->advert_price_currency = $this->getAdvertPriceCurrency($values["cena_mena"]);

		//Jendotka ceny
		$estateEntity->advert_price_unit = $this->getAdvertPriceUnit($values["cena_mena"]);

		//Poplatky
		$estateEntity->advert_price_charge = $this->hasProperty(396, $values["cena_konstrukce"], [1,2]);

		//Provize RK
		$estateEntity->advert_price_commission = $this->hasProperty(398, $values["cena_konstrukce"], [1,2]);

		//DPH
		$estateEntity->advert_price_vat = $this->hasProperty(399, $values["cena_konstrukce"], [1,2]);

		//Právní servis
		$estateEntity->advert_price_legal_services = $this->hasProperty(400, $values["cena_konstrukce"], [1,2]);

		//Cena k jednání
		$estateEntity->advert_price_negotiation = $this->hasProperty(403, $values["cena_konstrukce"]);

		//Skrýt cenu
		$estateEntity->advert_price_hidden = $values["cena_neuvadet"];

		//Poznámka k ceně, připojíme i poznámky k poplatkům
		$estateEntity->advert_price_text_note = implode(", ", array_filter([$values["cena_poznamka"], $values["poplatky"]]));

		/**
		 * Energetický štítek
		 */

		//Nízkoenergetický
		$estateEntity->advert_low_energy = $values["typ_budovy"] == 410 ? 0 : 1;

		//Energetická náročnost budovy
		$estateEntity->energy_efficiency_rating = $this->getEnergyEfficiencyRating($values["es_trida_narocnosti"]);

		//PENB dle vyhlášky
		$estateEntity->energy_performance_certificate = $this->getEnergyPerformanceCertificate($values["es_vyhlaska"]);

		//Ukazatel energetické náročnosti budovy
		$estateEntity->energy_performance_summary = $values["es_ukazatel_narocnosti"];

		//ID společnosti v Urbium - zatím nepoužíváme, do budoucna možná bude potřeba
		//ids => "2308" (4)

		//My výměnu vůbec nemáme, měli bysme mít?
		//vymena => "0"

		//exklusivita => "0" - až bude potřeba, zpracujeme do našeho contract_type

		//My využití vůbec nemáme, měli bysme mít?
		//vyuziti => "0"

		//My toto nemáme, je to jen duplikát jiných vlastností
		//charakteristika => NULL

		//popis_prohlidky => NULL

		//Poznámka k sociálnímu zařízení - my nepoužíváme
		//soc_zarizeni => NULL

		//My nemáme a hlavně proč uvádět počet PP, vždy je přeci jen jedno?
		//pocet_pp => "0"

		//My nepoužíváme tyto poznámky
		//site_poznamka => NULL
		//vybaveni_poznamka => NULL
		//nebyt_prostory => NULL
		//plocha_poznamka => NULL
		//ucel_nemovitosti => "0"
		//poz_dostupnost => NULL
		//poz_teren => NULL
		//poz_stavby => NULL
		//poz_porosty => NULL
		//druh_zarizeni => NULL
		//is_poznamka => "0"

		//Nepoužívámí, ale možná bysme měli přidat
		//pocet_bytu => NULL
		//pocet_kancelari => NULL

		//Vybavenost - my nemáme, ale asi bysme mít měli
		//vybavenost => NULL

		//Nevím jestli máme
		//kod_ku => "0"
	}

	/**
	 * Přeložit informace o obrázcích do našich hodnot a zkopírovat nové obrázky k nám na server
	 * @param App\Entities\EstateEntity $estateEntity
	 * @param array $values
	 */

	protected function translateEstateImages($estateEntity, $values)
	{
		$images = [];

		$this->log("V Urbiu nalezeno <b>" . count($values) . " obrázků</b>, u nás nalezeno <b>" . $estateEntity->estate_images->count() . " obrázků</b>");

		foreach ($values AS $image) {
			if($src = $this->copyImage($image["foto_url"], $this->thisDomainEntity->getPath() . "/files/estates/images")){ //Pokud se podařilo zkopírovat obrázek ze serveru Urbia k nám
				
				//Popisek obrázku je nekdy načten jako array, nevíme proč musíme vynutit string
				$image_alt = implode((array) $image["foto_popis"]);

				$images[$image["foto_id"]] = [
					"src" => $src,
					"alt" => [
						"cs" => $image_alt
					],
					"rank" => $image["foto_poradi"],
					"externalId" => $image["foto_id"]
				];
			}
		}

		//Odebereme ze současných obrázků ty, které nejsou v XML
		//Odebíráme pouze ty, které mají definováno "external_id" abychom nemazali ručně přidané obrázky v naší administraci
		//odebreme ze seznamu nových ty, které už máme u nás v databázi
		//Zatím neřešíme editaci obrázku, pokud obrázek u nás existuje, tak ho ignorujeme, neaktulizujeme
		foreach ($estateEntity->estate_images->getAll() AS $key => $imageEntity) {
			if (isset($images[$imageEntity->externalId])) {
				$this->log("Obrázek č. <b>{$imageEntity->getExternalId()}</b>, <b>{$imageEntity->getSrc()}</b> u nás již existuje, ignoruji");

				unset($images[$imageEntity->externalId]);
			} else {
			//} elseif(!empty($imageEntity->external_id)) { //Odebrat pouze obrázky s definovaným "external_id" abychom nesmazali uživatelsky přidané obrázky v naší administraci
				$this->log("Odebírání obrázku č. <b>{$imageEntity->getExternalId()}</b>, <b>{$imageEntity->getSrc()}</b>");

				unset($estateEntity->estate_images[$key]);
			}
		}

		//Přidat obrázky, které u nás ještě nemáme, ale json v XML
		foreach($images AS $image){
			$this->log("Přidávání obrázku č. <b>{$image["externalId"]}</b>, <b>{$image["src"]}</b>");
			
			$estateEntity->estate_images->addValues($image);
		}
	}
}