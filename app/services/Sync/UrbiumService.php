<?php

namespace App\Services\Sync;

use CP\Utils\Files;

/**
 * Dokumentace API na http://www.eurobydleni.cz/download/
 */

class UrbiumService extends \App\Services\BaseService
{
	const XML_ATTEMPTS_COUNT = 3;

	/** Vše co souvisí s úlohami */
	use UrbiumService\TasksTrait;

	/** Vše co souvisí s XML */
	use UrbiumService\XmlTrait;

	/** Vše co souvisí s logování importu */
	use UrbiumService\LogTrait;

	/** Vše co souvisí s autorizací */
	use UrbiumService\AuthTrait;

	/** Vše co souvisí se zakázkami */
	use UrbiumService\EstatesTrait;

	/** Vše co souvisí s makléři */
	use UrbiumService\BrokersTrait;

	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	/** @var \App\Services\UserService @inject */
	public $userService;

	/** @var bool */
	public $debug;

	/** @var array výchozí nastavení služby */
	public $settings = [
		"importEstates" => false,
		"importBrokers" => false,
		"auth" => [
			"username" => null,
			"password" => null
		]
	];

	/**
	 * Synchronizace dat z Urbia
	 * @param string|null $group
	 * @param integer|null $external_id
	 * @return array
	 */

	public function sync($group = null, $external_id = null)
	{
		//Uložit začátek operace do logu
		$this->logStart();

		if($this->loadAuth()) {
			$taskEntity = $this->getTask($group, $external_id);

			if ($taskEntity) { //Úloha ke zpracování nalezena, zpracovat
				$this->processTask($taskEntity);
			} else { //Žádná úloha nenalezena, zkontrolovat nové úlohy
				$this->checkTasks();
			}
		}

		//Uložit ukončení operace do logu
		$this->logEnd();

		//Uložit log so souboru
		$this->saveLog();

		//Vracíme array logu
		return $this->log;
	}

	/**
	 * Zkopírování obrázku ze serveru Urbia k nám
	 * @param string $remote URL souboru na serveru Urbia
	 * @param string $path adresář, kam budeme obrázek kopírovat
	 * @return string|false
	 */

	protected function copyImage($remote, $path)
	{
		//Vytvořit cestu k obrázku u nás
		$local = Files::getValidPath($path . "/" . pathinfo($remote, PATHINFO_BASENAME));

		//Zkopírovat obrázek z Urbia k nám
		if (!is_file(WWW_DIR . "/" . $local)) {
			$result = copy($remote, WWW_DIR . "/" . $local);

			$this->log("Kopírování obrázku z Urbia <b>$remote</b> k nám <b>" . $local . "</b> ... " . ($result ? "OK" : "Fail"));
		} else {
			$result = true;

			$this->log("Obrázek <b>{$remote}</b> u nás už existuje, nemusíme kopírovat");
		}

		//Pokud kopírování proběhlo v pořádku, vrátit cestu obrázku u nás, jinak vrátit false
		if ($result) {
			return $local;
		} else {
			return false;
		}
	}
}