<?php

namespace App\Services\BrokerProfiles;

use App\Entities\BrokerProfiles\TestimonialEntity;
use App\Collections\BrokerProfiles\TestimonialsCollection;

class TestimonialsService extends \App\Services\BaseService
{
	public $settings = [
		"defaults" => []
	];

	/**
	 * Získat novou instanci etity reference
	 * @return App\Entities\BrokerProfiles\TestimonialEntity
	 */

	public function testimonialEntity()
	{
		return new TestimonialEntity;
	}

	/**
	 * Získat novou instanci kolekce referencí
	 * @return App\Collections\BrokerProfiles\TestimonialsCollection
	 */

	public function testimonialsCollection()
	{
		return new TestimonialsCollection;
	}
}