<?php

namespace App\Services\BrokerProfiles;

use App\Entities\BrokerProfiles\VideoEntity;
use App\Collections\BrokerProfiles\VideosCollection;

class VideosService extends \App\Services\BaseService
{
	public $settings = [
		"defaults" => []
	];

	/**
	 * Získat novou instanci entity videa
	 * @return App\Entities\BrokerProfiles\VideoEntity
	 */

	public function videoEntity()
	{
		return new VideoEntity;
	}

	/**
	 * Získat novou instanci kolekce videí
	 * @return App\Collections\BrokerProfiles\VideosCollection
	 */

	public function videosCollection()
	{
		return new VideosCollection;
	}
}