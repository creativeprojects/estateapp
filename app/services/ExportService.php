<?php

namespace App\Services;

class ExportService extends BaseService
{
	/** @var \App\Services\EstatesService @inject */
	public $estatesService;

	public $usersService;

	public function export($portal_key)
	{
		$portal = $this->getPortalByKey($portal_key);

		$export = $this->getOneToExport($portal["portal_id"]);

		if ($export) {
			$service = "\App\Services\\ExportService\\" . $portal["portal_classname"];
			$server = new $service($this);

			$done = $server->export($export);
			return $this->saveExportedInfo($export, $done["error"], $done["message"], $done["url"]);
		}
		return FALSE;
	}

	public function saveExportedInfo($export, $error, $message, $url)
	{
		$update = array(
			"export_id" => $export["export_id"],
			"message" => preg_replace('/[\s]+/mu', ' ', (preg_replace("/\r|\n/", " ", $message))),
			"url" => preg_replace("/\r|\n/", " ", $url)
		);

		if ($error == FALSE) {
			$update["export_timestamp"] = date('Y-m-d G:i:s');
			$update["last_export_timestamp"] = date('Y-m-d G:i:s');
		}
		$this->repository->exportPortals->save($update);
	}

	public function saveExportedBrokerInfo($export, $error, $message, $url)
	{
		$update = array(
			"export_id" => $export["export_id"],
			"message" => preg_replace('/[\s]+/mu', ' ', (preg_replace("/\r|\n/", " ", $message))),
			"url" => preg_replace("/\r|\n/", " ", $url)
		);

		if ($error == FALSE) {
			$update["export_timestamp"] = date('Y-m-d G:i:s');
			$update["last_export_timestamp"] = date('Y-m-d G:i:s');
		}
		$this->repository->exportBrokers->save($update);
	}

	public function getOneToExport($portal_id)
	{
		$export = $this->database
				->query("SELECT ex.estate_id, ex.export, ex.export_id "
					. "FROM {$this->repository->exportPortals->getName()} as ex "
					. "LEFT JOIN {$this->repository->estates->getName()} AS es ON ex.estate_id = es.estate_id "
					. "WHERE (ex.export_timestamp IS NULL AND ex.portal_id = {$portal_id} AND ex.export = 1 AND es.advert_status IN (1, 2)) OR "
					. "(ex.portal_id = {$portal_id} AND ex.export_timestamp IS NULL AND ex.last_export_timestamp IS NOT NULL AND (ex.export = 0 OR es.advert_status NOT IN (1,2))) "
					. "LIMIT 1")->fetch();
		if ($export) {
			$estate = $this->estatesService->getById($export["estate_id"], true);
			if (!$estate) {
				$estate["estate_id"] = $export["estate_id"];
			}
			$estate["export"] = $export["export"];
			$estate["export_id"] = $export["export_id"];
			return $estate;
		}
		return $export;
	}

	public function getOneBrokerToExport()
	{
		$export = $this->database
				->query("SELECT e.broker_id, e.export, e.export_id, e.portal_id "
					. "FROM {$this->repository->exportBrokers->getName()} as e "
					. "LEFT JOIN {$this->repository->users->getName()} AS u ON e.broker_id = u.id "
					. "WHERE (e.export_timestamp IS NULL AND e.export = 1)"
//					. "OR (e.export_timestamp IS NULL AND e.last_export_timestamp IS NOT NULL AND (e.export = 0 OR u.advert_status NOT IN (1,2))) "
					. "LIMIT 1")->fetch();
		if ($export) {

			$by = array(
				"includeRemoved" => TRUE,
				"where" => array(
					"id" => $export["broker_id"]
				)
			);
			$broker = $this->usersService->getOneBy($by);
			$broker["export"] = $export["export"];
			$broker["export_id"] = $export["export_id"];
			$broker["portal_id"] = $export["portal_id"];
			return $broker;
		}
		return $export;
	}

	public function exportOneBroker()
	{
		$export = $this->getOneBrokerToExport();

		if ($export) {
			$portal = $this->getPortalById($export["portal_id"]);

			$service = "\App\Services\\" . $portal["portal_classname"];
			$server = new $service($this);
			$done = $server->exportBroker($export);

			return $this->saveExportedBrokerInfo($export, $done["error"], $done["message"], $done["url"]);
		}
		return FALSE;
	}

	/**
	 * Ukládá exporty uživatů
	 * @param array $broker
	 */
	public function saveBroker($broker)
	{
		$portals = $this->repository->portals->getAllBy(array("export_broker" => 1));

		$exports = $this->repository->exportBrokers->getAllBy(array("broker_id" => $broker["id"]));

		foreach ($exports as $key => $value) {
			$exports[$value["portal_id"]] = $value;
			unset($exports[$key]);
		}

		foreach ($portals AS $values) {
			$portal["export_id"] = isset($exports[$values["portal_id"]]["export_id"]) ? $exports[$values["portal_id"]]["export_id"] : NULL;
			$portal["portal_id"] = $values["portal_id"];
			$portal["broker_id"] = $broker["id"];
			$portal["export"] = 1;
			$portal["export_timestamp"] = NULL;
			$portal = $this->repository->exportBrokers->save($portal);
		}
	}

	public function removeFromExportServers()
	{
		foreach ($this->getContext()->parameters["export"]["sources"] as $key => $source) {
			//TODO - hotfix property_exists - vyřešit líp jak kontrolovat jestli zdroj existuje
			if (property_exists($this, $source) && $source != "imagesExport") {
				$done = $this->$source->delBroker($userId, $this->client->id);
			}
		}
	}

	/**
	 * Vrátí informace o realitním portálu pro export
	 * @param int $portal_id
	 * @return array
	 */
	public function getPortalById($portal_id)
	{
		return $this->repository->portals->getOne($portal_id);
	}

	public function getPortalByKey($portal_key)
	{
		return $this->repository->portals->getOneBy(array("portal_key" => $portal_key));
	}

	public function translate($estate)
	{
		foreach ($estate as $key => $value) {
			if (array_key_exists($key, $this->translateTable) && $this->translateTable[$key] != NULL && $value !== NULL) {
				$estate[$this->translateTable [$key]] = $value;
				unset($estate[$key]);
			} elseif (!array_key_exists($key, $this->translateTable) OR $value === NULL) {
				unset($estate[$key]);
			}
		}
		return $estate;
	}

	public function getPortalsSettings()
	{
		$portals = $this->repository->portals->getAllBy(array("portal_active" => 1));
		return $portals;
	}
}