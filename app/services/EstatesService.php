<?php

namespace App\Services;

use App\Collections\EstatesCollection;
use App\Entities\EstateEntity;

class EstatesService extends BaseService
{
	/** @var \App\Services\ProjectsService @inject */
	public $projectsService;

	/** @var \App\Services\UserService @inject */
	public $userService;

	/**
	 * Získat novou instanci entity nemovitosti
	 * @return App\Entities\EstateEntity
	 */

	public function estateEntity()
	{
		return new EstateEntity;
	}

	/**
	 * Získat novou instanci kolekce nemovitostí
	 * @return App\Collections\EstatesCollection
	 */

	public function estatesCollection()
	{
		return new EstatesCollection;
	}

	/**
	 * Získat možnosti subtypu nemovitosti pro select
	 * Získámě všechny a následně se vyfiltrují JavaScriptem jen adekvátní pro zvolený typ nemovitosti
	 * @param string $allLabel popisek pro možnost "nerozhoduje"
	 * @param string $allKey hodnota optionu pro možnost "nerozhoduje"
	 * @return array
	 */
	
	public function getAdvertSubtypeOptions($allLabel = null, $allKey = "_all")
	{
		$options = [];

		if ($allLabel) {
			$options[$allKey] = $allLabel;
		}

		foreach ($this->estatesParameters["advert_subtype"]["options"] AS $advert_type) {
			foreach ($advert_type AS $key => $label)
				$options[$key] = $label;
		}

		return $options;
	}

	/**
	 * Získat data pro seznamy typů nemovitostí
	 * @return array
	 */

	public function getAdvertTypeList()
	{
		$lists = [];

		$advert_typeCounts = $this->database->query("SELECT advert_type, COUNT(*) AS advert_count FROM {$this->repository->estates} WHERE domainId = {$this->thisDomainEntity->getId()} AND advert_status IN (1,2) GROUP BY advert_type")->fetchPairs("advert_type", "advert_count");

		foreach ($this->estatesParameters["advert_type"]["options"] AS $advert_type => $advert_typeTitle) {
			if (@$advert_typeCounts[$advert_type]) {
				$lists[$advert_type] = array(
					"count" => @$advert_typeCounts[$advert_type],
					"title" => $advert_typeTitle,
				);
			}
		}

		return $lists;
	}

	/**
	 * Získat statistiky nemovotostí
	 * @return array
	 */

	public function getStats()
	{
		$default = [
			0 => 0,
			1 => 0,
			2 => 0,
			3 => 0,
			4 => 0,
			5 => 0
		];

		switch ($this->user->role) {
			case 'superAdmin':
			case 'admin':
				$estates = $this->database->query("SELECT advert_status, count(*) AS estatesCount FROM {$this->repository->estates->getName()} WHERE domainId = {$this->thisDomainEntity->getId()} AND removed IS NULL GROUP BY advert_status")->fetchPairs('advert_status', 'estatesCount');

				break;
			case 'broker':
				$estates = $this->database->query("SELECT advert_status, count(*) AS estatesCount FROM {$this->repository->estates->getName()} WHERE userId = {$this->user->getId()} AND removed IS NULL GROUP BY advert_status")->fetchPairs('advert_status', 'estatesCount');

				break;
		}

		$result = array_extend($default, $estates);

		$result['sum'] = array_sum($estates);

		return $result;
	}

	/**
	 * Získat kolekci URL adres pro seznamy typů nemovitostí
	 * @return CP\Collections\SlugsCollection
	 */
	
	public function getSlugsCollection()
	{
		$slugsCollection = $this->slugsService->slugsCollection();

		$slugsCollection->query
			->where("action = 'Estates:default'")
			->where("domainId = {$this->thisDomainEntity->getId()}");

		return $slugsCollection;
	}
}