<?php

namespace App\Services\Projects;

use App\Entities\Projects\ArticleEntity;
use App\Collections\Projects\ArticlesCollection;

class ArticlesService extends \App\Services\BaseService
{
	/**
	 * Získat novou instanci entity článku projektu
	 * @return App\Entities\Projects\ArticleEntity
	 */

	public function articleEntity()
	{
		return new ArticleEntity;
	}

	/**
	 * Získat novou instanci kolekce článků
	 * @return App\Collections\Projects\ArticlesCollection
	 */

	public function articlesCollection()
	{
		return new ArticlesCollection;
	}
}