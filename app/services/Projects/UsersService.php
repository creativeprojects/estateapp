<?php

namespace App\Services\Projects;

use App\Entities\Projects\UserEntity;
use App\Collections\Projects\UsersCollection;

class UsersService extends \App\Services\BaseService
{
	/**
	 * Získat novou instanci entity makléře projektu
	 * @return App\Entities\Projects\UserEntity
	 */

	public function userEntity()
	{
		return new UserEntity;
	}

	/**
	 * Získat novou instanci makléřů projektu
	 * @return App\Collections\Projects\UsersCollection
	 */

	public function usersCollection()
	{
		return new UsersCollection;
	}
}