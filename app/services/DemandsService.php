<?php

namespace App\Services;

use App\Collections\DemandsCollection;
use App\Entities\DemandEntity;

class DemandsService extends BaseService
{
	/** @var \App\Services\UserService @inject */
	public $userService;

	public $settings = [
		"defaults" => []
	];

	public function getNewEntity()
	{
		$demandEntity = new DemandEntity;

		$demandEntity->create($this->settings["defaults"]);

		return $demandEntity;
	}

	public function getEntity($demand_id)
	{
		$demandEntity = new DemandEntity;

		return $demandEntity->find($demand_id);
	}

	public function getCollection()
	{
		$demandsCollection = new DemandsCollection;

		$demandsCollection->query
			->where("d.demand_status IN (0,1)");

		return $demandsCollection;
	}

	public function getArchiveCollection()
	{
		$demandsCollection = new DemandsCollection;

		$demandsCollection->query
			->where("d.demand_status = 2");

		return $demandsCollection;
	}

	public function getCollectionByLoggedUser()
	{
		$demandsCollection = $this->getCollection();

		$demandsCollection->query
			->where("d.userId = " . $this->user->getId());

		return $demandsCollection;
	}

	public function getArchiveCollectionByLoggedUser()
	{
		$demandsCollection = $this->getArchiveCollection();

		$demandsCollection->query
			->where("d.userId = " . $this->user->getId());

		return $demandsCollection;
	}

	public function getStats()
	{
		$default = array(
			0 => 0,
			1 => 0,
			2 => 0
		);

		$demands = $this->database->table($this->repository->demands->getName())->where(array(
				'domainId' => $this->thisDomainEntity->getId(),
				'removed' => NULL
			))->fetchPairs('demand_id', 'demand_status');


		$result = array_replace($default, array_count_values($demands));

		$result['sum'] = count($demands);

		return $result;
	}

	public function getNewCountByDomain()
	{
		$by = array(
			"domainId" => $this->thisDomainEntity->getId(),
			"demand_status" => 0
		);

		$count = $this->repository->demands->getCountBy($by);

		return $count;
	}
}