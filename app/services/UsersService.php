<?php

namespace App\Services;

class UsersService extends \CP\Services\UsersService
{
	public function usersCollection()
	{
		$usersCollection = parent::usersCollection();

		$usersCollection->query
			->field("b.phone")
			->from("LEFT OUTER JOIN {$this->repository->usersBroker} AS b ON b.id = u.id")
			->field($this->getSqlProfileExists("u.id"));

		return $usersCollection;
	}

	public function getBrokersCollection()
	{
		$usersCollection = $this->usersCollection();

		$usersCollection->query
			->where("u.role = 'broker'");

		return $usersCollection;
	}

	public function getBrokerOptions()
	{
		$usersCollection = $this->getBrokersCollection()->find();

		$options = [];

		foreach ($usersCollection->getAll() AS $userEntity) {
			$options[$userEntity->getId()] = $userEntity->getName();
		}

		return $options;
	}

	public function getSqlProfileExists($id)
	{
		$sql = "(SELECT COUNT(*) FROM {$this->repository->brokerProfiles} AS b WHERE b.id = {$id}) AS broker_profile_exists";

		return $sql;
	}
}