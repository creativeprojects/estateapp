<?php

namespace App\Services;

class UserService extends BaseUserService
{
	public function countBrokers()
	{
		$by = array(
			'domainId' => $this->thisDomainEntity->getId(),
			'role' => 'broker',
		);

		$count = $this->repository->users->getCountBy($by);

		return $count;
	}

	public function getStats()
	{
		$most_adverts = $this->database
				->query("
					SELECT name as most_adverts FROM users,
					(SELECT userId, MAX(counted) as counts FROM(SELECT userId, COUNT(*) AS counted FROM {$this->repository->estates->getName()} 
					WHERE removed IS NULL AND domainId = {$this->thisDomainEntity->getId()} GROUP BY userId) AS counts) AS maxcount WHERE users.id = maxcount.userId")->fetch('name');

		if ($most_adverts)
			$most_adverts = $most_adverts->most_adverts;

		$most_sales = $this->database
				->query("SELECT name as most_sales FROM users,(SELECT userId, MAX(counted) as counts FROM(SELECT userId, COUNT(*) AS counted FROM {$this->repository->estates->getName()} WHERE removed IS NULL AND advert_status = 4 AND domainId = {$this->thisDomainEntity->getId()} GROUP BY userId) AS counts) AS maxcount WHERE users.id = maxcount.userId")->fetch('name');

		if ($most_sales)
			$most_sales = $most_sales->most_sales;

		$brokerStats = array(
			"most_adverts" => $most_adverts ? $most_adverts : null,
			"most_sales" => $most_sales ? $most_sales : null
		);

		return $brokerStats;
	}
}