<?php

namespace App\Forms\Advices;

use CP\Forms\Form;

abstract class QuestionForm extends \CP\Forms\BaseForm
{
	/**
	 * Vytvořit formulář
	 * @return CP\Forms\Form
	 */

	public function create()
	{
		$this->form->addText("name")
			->setRequired("Uvěďtě své jméno");

		$this->form->addText("email")
			->setRequired("Uvěďtě svůj email")
			->addRule(Form::EMAIL, 'Zadejte platnou emailovou adresu');

		$this->form->addTextArea("question")
			->setRequired("Uveďte svou otázku");

		$this->form->addSubmit("save")
			->onClick[] = [$this, "save"];

		$this->setDefaults();

		return $this->form;
	}
}