<?php

namespace App\Forms\User;

use CP\Forms\Form;

class BrokerForm extends \CP\Forms\User\UserForm
{
	public function create()
	{
		parent::create();

		$this->form->addText("phone")
			->addCondition(Form::FILLED)
				->addRule(Form::PATTERN, 'Telefonní číslo může obsahovat pouze číslice a mezery', '.*[0-9].*');

		$this->setDefaults();

		return $this->form;
	}
}