<?php

namespace App\Forms\Users;

class BrokerForm extends \App\Forms\User\BrokerForm
{
	public function save($button)
	{
		$values = $button->parent->getValues();

		$this->entity->set($values);

		$signUp = $this->entity->getId() ? false : true;

		//Ukládá se prostřednictvím služby kvůli callbackům různých rolí
		//if ($this->presenter->userService->saveEntity($this->entity)) {
		if($this->entity->save()){
			if ($signUp) {
				$this->flashMessage("Makléřský účet byl vytvořen. Přihlašovací údaje byly odeslány na email makléře", 'alert-success');
			} else {
				$this->flashMessage("Změny v makléřském účtu byly uloženy", 'alert-success');

				//TODO - odhlásit trvale přihlášeného uživatele kterého jsem upravil
				//TODO - úprava by asi měla být zablokována pro současné přihlášené uživatele
			}

			$this->redirect("Users:default");
		} else {
			if ($signUp) {
				$this->flashMessage("Při vytváření makléřského účtu došlo k chybě!", 'alert-danger');
			} else {
				$this->flashMessage("Při ukládání změn makléřského účtu došlo k chybě!", 'alert-danger');
			}
		}
	}
}