<?php

namespace App\Forms;

use CP\Forms\Form;

abstract class DemandForm extends \CP\Forms\BaseForm
{
	private $redirect;

	public function setRedirect($redirect)
	{
		$this->redirect = $redirect;

		return $this;
	}

	public function create()
	{
		$this->form->addHidden("demand_id");

		$this->addUser();

		$this->form->addText("demand_client_title")
			->setRequired("Vyplňte jméno klienta");

		$this->form->addText("demand_client_phone")
			->setRequired("Vyplňte telefoní číslo klienta");

		$this->form->addText("demand_client_email")
			->addCondition(Form::FILLED)
				->addRule(Form::EMAIL, "Vyplňte platnou emailovou adresu");

		$this->form->addSelect("advert_type", null, $this->presenter->estatesParameters["advert_type"]["options"]);

		$this->form->addSelect("advert_function", null, $this->presenter->estatesParameters["advert_function"]["options"]);

		$this->form->addCheckboxList("advert_subtype", null, $this->presenter->estatesService->getAdvertSubtypeOptions());

		$this->form->addAddress("demand_locality", null, true)
			->setRequired("Vyplňte požadovanou lokalitu");

		$this->form->addHTML("demand_description");

		$this->form->addText("demand_area_from");

		$this->form->addText("demand_area_to");

		$this->form->addText("demand_price_from");

		$this->form->addText("demand_price_to");

		$this->form->addSubmit("save")
			->onClick[] = array($this, "save");

		$this->setDefaults();

		return $this->form;
	}

	abstract protected function addUser();

	abstract public function save($button);
}